﻿using RestSharp;
using System;
using System.Collections.Generic;

namespace TTL_API.Services.Extensions
{
    public class SMSHelper
    {
        public string PhoneNo { get; set; }
        public string Content { get; set; }
        public string Send()
        {
            try
            {
                // var apiKey = "34D616780B2EC90484F7DB8FEF89C7";
                // var secretKey = "81350B8E88AFF4D222C2BD903638FF";
                // var brandName = "TAM-TRI-LUC";
                // var smsParam = new SMSParam()
                // {
                //     username = "tamtriluc_api",
                //     password = "P@ss885!@#098",
                //     phonenumber = PhoneNo,
                //     message = Content,
                //     brandname = "TAM TRI LUC",
                //     type = "0"
                // };
                // var client = new RestClient("http://221.132.39.104:8088/api/sendsms");
                // client.Timeout = -1;
                // var request = new RestRequest(Method.POST);
                // request.AddHeader("Content-Type", "application/json");
                //request.AddParameter("application/json", new JavaScriptSerializer().Serialize(smsParam), ParameterType.RequestBody);
                // IRestResponse response = client.Execute(request);
                // var smsResult = JsonConvert.DeserializeObject<SMSResult>(response.Content);
                // return "";
                return SendMobiphone();
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }
        public bool IsMobiphone()
        {
            try
            {
                var listPhone = new List<string>();
                listPhone.Add("090");
                listPhone.Add("093");
                listPhone.Add("089");
                listPhone.Add("070");
                listPhone.Add("079");
                listPhone.Add("078");
                listPhone.Add("077");
                listPhone.Add("076");
                var firstNumber = PhoneNo.Substring(0, 3);
                if (listPhone.Contains(firstNumber))
                    return true;
                return false;
            }
            catch { return false; }
        }
        public string SendMobiphone()
        {
            try
            {
                var client = new RestClient(string.Format("http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?Phone={1}&ApiKey=34D616780B2EC90484F7DB8FEF89C7&SecretKey=81350B8E88AFF4D222C2BD903638FF&Brandname=TAM-TRI-LUC&SmsType=2&Content={0}", Content, PhoneNo));
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                request.AddHeader("Cookie", "ASP.NET_SessionId=0s2dblo5xpstfvfjf1j1mdr0");
                IRestResponse response = client.Execute(request);
                return response.Content;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
    public class SMSParam
    {
        public string username { get; set; }
        public string password { get; set; }
        public string phonenumber { get; set; }
        public string message { get; set; }
        public string brandname { get; set; }
        public string type { get; set; }
    }
    public class SMSResult
    {
        public string id { get; set; }
        public string description { get; set; }
        public string messageid { get; set; }
    }

}
