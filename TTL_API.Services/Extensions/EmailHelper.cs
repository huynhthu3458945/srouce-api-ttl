﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;

namespace TTL_API.Services.Extensions
{
    public class EmailHelper
    {
        public string EMailFrom { get; set; }
        public string DisplayName { get; set; }
        public List<string> EmailTos { get; set; }
        public string SMTP_USERNAME { get; set; }
        public string SMTP_PASSWORD { get; set; }
        public string HOST { get; set; }
        public int PORT { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public string MailReply { get; set; }
        public Stream fileSteam { get; set; }
        public string FileAttach { get; set; }
        public EmailHelper()
        {
            EMailFrom = "ad.quantri.tamtriluc@gmail.com";
            DisplayName = "Siêu Trí Nhớ Học Đường";
            SMTP_USERNAME = "AKIATRULCFHW5IPSZMXN";
            SMTP_PASSWORD = "BFT0jUdoFKFDqEAO9C95a2ScTF5cDzWrdyWYO9hjYUqm";
            PORT = 587;
            HOST = "email-smtp.ap-southeast-1.amazonaws.com";
        }
        public string DoSendMail()
        {
            try
            {
                MailMessage message = new MailMessage();
                message.IsBodyHtml = true;
                message.From = new MailAddress(EMailFrom, DisplayName);
                foreach (var item in EmailTos)
                {
                    message.To.Add(new MailAddress(item));
                }
                //message.To.Add(new MailAddress(EmailTos));
                message.ReplyTo = new MailAddress(string.IsNullOrEmpty(this.MailReply) ? this.EMailFrom : this.MailReply, this.DisplayName);
                if (!string.IsNullOrEmpty(FileAttach))
                    message.Attachments.Add(new Attachment(fileSteam, FileAttach));
                message.Subject = Title;
                message.Body = Body;
                using (var client = new System.Net.Mail.SmtpClient(HOST, PORT))
                {
                    client.Credentials = new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                    client.EnableSsl = true;
                    try
                    {
                        client.Send(message);
                    }
                    catch (Exception ex)
                    {
                        return ex.ToString();
                    }
                }
                return null;
            }
            catch (Exception ex) { return ex.ToString(); }
        }
        public string SendEmailsThread()
        {
            string result = "";
            Thread thread = new Thread(
                delegate ()
                {
                    result = DoSendMail();
                }
            );
            thread.Start();
            return result;
        }
    }
}
