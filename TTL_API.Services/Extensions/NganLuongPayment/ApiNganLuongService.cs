﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.Utils;
using static TTL_API.DbContext.Models.Utils.NganLuongModels;

namespace TTL_API.Services.Extensions.NganLuongPayment
{
    public class ApiNganLuongService
    {
        //private IConfiguration _configuration;
        #region Normal Post
        public WithdrawMoneyResponse SetCashoutRequest(WithdrawMoneyRequest requestContent, string payment_method = "NL")
        {
            String requestinfo = GetParamWithdrawMoneyPost(requestContent);
            String result = HttpPost(requestinfo);
            result = result.Replace("&", "&amp;");

            var json = JsonConvert.DeserializeObject<WithdrawMoneyResponse>(result);
            return json;
        }
        
        //public ResponseCheckOrder GetTransactionDetail(RequestCheckOrder info)
        //{
        //    String request = "";
        //    request += "function=" + info.Funtion;
        //    request += "&version=" + info.Version;
        //    request += "&merchant_id=" + info.Merchant_id;
        //    request += "&merchant_password=" + CreateMD5Hash(info.Merchant_password);
        //    request += "&token=" + info.Token;
        //    String result = HttpPost(request);
        //    result = result.Replace("&", "&amp;");
        //    XmlDocument dom = new XmlDocument();
        //    dom.LoadXml(result);
        //    XmlNodeList root = dom.DocumentElement.ChildNodes;

        //    ResponseCheckOrder objResult = new ResponseCheckOrder();


        //    objResult.errorCode = root.Item(0).InnerText;
        //    objResult.token = root.Item(1).InnerText;
        //    objResult.description = root.Item(2).InnerText;
        //    objResult.transactionStatus = root.Item(3).InnerText;
        //    //objResult.receiver_email = root.Item(4).InnerText; //receiver_email
        //    objResult.order_code = root.Item(5).InnerText;
        //    objResult.paymentAmount = root.Item(6).InnerText; //total_amount
        //    //objResult.payment_method = root.Item(7).InnerText; //payment_method
        //    //objResult.bank_code = root.Item(8).InnerText; //bank_code
        //    //objResult.payment_type = root.Item(9).InnerText; //payment_type
        //    //objResult.description = root.Item(10).InnerText; //order_description
        //    //objResult.tax_amount = root.Item(11).InnerText; //tax_amount
        //    //objResult.discount_amount = root.Item(12).InnerText; //discount_amount
        //    //objResult.fee_shipping = root.Item(13).InnerText; //fee_shipping
        //    //objResult.return_url = root.Item(14).InnerText; //return_url
        //    // objResult.cancel_url = root.Item(15).InnerText; //cancel_url
        //    objResult.payerName = root.Item(16).InnerText; //buyer_fullname
        //    objResult.payerEmail = root.Item(17).InnerText; //buyer_email
        //    objResult.payerMobile = root.Item(18).InnerText; //buyer_mobile
        //    //objResult.buyer_address = root.Item(19).InnerText; //buyer_address
        //    //objResult.affiliate_code = root.Item(20).InnerText; //affiliate_code
        //    objResult.transactionId = root.Item(21).InnerText;


        //    objResult.errorCode = root.Item(0).InnerText;
        //    objResult.token = root.Item(1).InnerText;
        //    objResult.description = root.Item(2).InnerText;
        //    objResult.transactionStatus = root.Item(3).InnerText;
        //    objResult.order_code = root.Item(5).InnerText;
        //    objResult.paymentAmount = root.Item(6).InnerText;
        //    objResult.transactionId = root.Item(21).InnerText;

        //    /*
        //    String error_code =root.Item(0).InnerText;
        //    String token =root.Item(1).InnerText;
        //    String description =root.Item(2).InnerText;
        //    String transaction_status =root.Item(3).InnerText;
        //    String receiver_email =root.Item(4).InnerText;
        //    String order_code =root.Item(5).InnerText;
        //    String total_amount =root.Item(6).InnerText;
        //    String payment_method =root.Item(7).InnerText;
        //    String bank_code =root.Item(8).InnerText;
        //    String payment_type =root.Item(9).InnerText;
        //    String order_description =root.Item(10).InnerText;
        //    String tax_amount =root.Item(11).InnerText;
        //    String discount_amount =root.Item(12).InnerText;
        //    String fee_shipping =root.Item(13).InnerText;
        //    String return_url =root.Item(14).InnerText;
        //    String cancel_url =root.Item(15).InnerText;
        //    String buyer_fullname =root.Item(16).InnerText;            
        //    String buyer_email =root.Item(17).InnerText;
        //    String buyer_mobile =root.Item(18).InnerText;
        //    String buyer_address =root.Item(19).InnerText;
        //    String affiliate_code =root.Item(20).InnerText;
        //    String transaction_id =root.Item(21).InnerText;
        // */
        //    return objResult;
        //}
        private static String GetParamWithdrawMoneyPost(WithdrawMoneyRequest info)
        {

            String request = "";

            request += "func=" + info.Funtion;
            request += "&merchant_id=" + info.Merchant_id;
            request += "&merchant_password=" + CreateMD5Hash(info.Merchant_password);
            request += "&receiver_email=" + info.Receiver_email;
            request += "&ref_code=" + info.RefCode;
            request += "&total_amount=" + info.Total_amount;
            request += "&account_type=" + info.AccountType;
            request += "&bank_code=" + info.bank_code;
            request += "&card_fullname=" + info.Card_fullname;
            request += "&card_number=" + info.CardNumber;
            request += "&card_month=" + info.CardMonth;
            request += "&card_year=" + info.CardYear;
            request += "&branch_name=" + info.BranchName;
            request += "&zone_id=" + info.ZoneId;
            request += "&reason=" + info.Reason;

            return request;
        }
        //Dùng để kiểm tra trạng thái giao dịch rút tiền(hàm cũ)
        public CheckWithdrawalStatusResponse CheckCashout(CheckWithdrawalStatusRequestOld requestContent, string payment_method = "NL")
        {

            //requestContent.Payment_method = payment_method;
            String requestinfo = GetParamCheckWithdrawalStatus(requestContent);
            String result = HttpPost(requestinfo);
            result = result.Replace("&", "&amp;");
            var json = JsonConvert.DeserializeObject<CheckWithdrawalStatusResponse>(result);

            return json;
        }
        private static String GetParamCheckWithdrawalStatus(CheckWithdrawalStatusRequestOld info)
        {

            String request = "";

            request += "func=" + info.Funtion;
            request += "&merchant_id=" + info.Merchant_id;
            request += "&merchant_password=" + CreateMD5Hash(info.Merchant_password);
            request += "&receiver_email=" + info.Receiver_email;
            request += "&transaction_id=" + info.TransactionId;
            request += "&ref_code=" + info.RefCode;

            return request;
        }
        //Dùng để kiểm tra trạng thái giao dịch rút tiền(hàm mới)
        public IList<CheckWithdrawalStatusResponse> CheckCashoutList(CheckWithdrawalStatusRequestNew requestContent, string payment_method = "NL")
        {
            //requestContent.Payment_method = payment_method;
            String requestinfo = GetParamCheckWithdrawalStatusNew(requestContent);
            var result1 = HttpPost(requestinfo);
            //result = result.Replace("&", "&amp;");
            var json = JsonConvert.DeserializeObject<IList<CheckWithdrawalStatusResponse>>(result1);

            return json;
        }
        private static String GetParamCheckWithdrawalStatusNew(CheckWithdrawalStatusRequestNew info)
        {
            String request = "";

            request += "func=" + info.Funtion;
            request += "&merchant_id=" + info.Merchant_id;
            request += "&merchant_password=" + CreateMD5Hash(info.Merchant_password);
            request += "&receiver_email=" + info.Receiver_email;
            request += "&transaction_id=" + info.TransactionId;
            request += "&ref_code=" + info.RefCode;

            return request;
        }
        private static String HttpPost(string postData)
        {

            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] data = encoding.GetBytes(postData);

            // Prepare web request...
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("https://sandbox.nganluong.vn/nl35/withdraw.api.post.php");
            myRequest.Method = "POST";
            myRequest.ContentType = "application/x-www-form-urlencoded";
            myRequest.ContentLength = data.Length;
            Stream newStream = myRequest.GetRequestStream();
            // Send the data.

            newStream.Write(data, 0, data.Length);
            newStream.Close();

            HttpWebResponse response = (HttpWebResponse)myRequest.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string output = reader.ReadToEnd();
            response.Close();
            return output;
        }
        private static String CreateMD5Hash(string input)
        {
            // Use input string to calculate MD5 hash
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("x2"));
            }
            return sb.ToString();
        }
        public string GetErrorMessage(string _ErrorCode, string culture)
        {
            string _Message = "";
            switch (_ErrorCode)
            {
                case "00":
                    _Message = culture == "en-US" ? "Successful" : "Thành công";
                    break;
                case "01":
                    _Message = culture == "en-US" ? "Merchant is not allowed to use this method" : "Merchant không được phép sử dụng phương thức này";
                    break;
                case "02":
                    _Message = culture == "en-US" ? "Incorrect card information" : "Thông tin thẻ sai định dạng";
                    break;
                case "03":
                    _Message = culture == "en-US" ? "Merchant information is incorrect" : "Thông tin merchant không chính xác";
                    break;
                case "04":
                    _Message = culture == "en-US" ? "There was an error during the connection" : "Có lỗi trong quá trình kết nối";
                    break;
                case "05":
                    _Message = culture == "en-US" ? "Invalid amount" : "Số tiền không hợp lệ";
                    break;
                case "06":
                    _Message = culture == "en-US" ? "Invalid cardholder name" : "Tên chủ thẻ không hợp lệ";
                    break;
                case "07":
                    _Message = culture == "en-US" ? "Invalid account number" : "Số tài khoản không hợp lệ";
                    break;
                case "08":
                    _Message = culture == "en-US" ? "Error connecting to the bank. The error occurs when the bank is maintaining and upgrading without coming from the merchant" : "Lỗi kết nối tới ngân hàng. Lỗi xảy ra khi ngân hàng đang bảo trì, nâng cấp mà không xuất phát từ merchant";
                    break;
                case "09":
                    _Message = culture == "en-US" ? "Bank_code is not valid" : "Bank_code không hợp lệ";
                    break;
                case "10":
                    _Message = culture == "en-US" ? "Account balance is not enough to make a transaction" : "Số dư tài khoản không đủ để thực hiện giao dịch";
                    break;
                case "11":
                    _Message = culture == "en-US" ? "Invalid reference code ( ref_code )" : "Mã tham chiếu ( ref_code ) không hợp lệ";
                    break;
                case "12":
                    _Message = culture == "en-US" ? "The reference code ( ref_code ) already exists" : "Mã tham chiếu ( ref_code ) đã tồn tại";
                    break;
                case "14":
                    _Message = culture == "en-US" ? "Function is not correct" : "Function không đúng";
                    break;
                case "16":
                    _Message = culture == "en-US" ? "Receiver_email is locked or blocked and cannot be transacted" : "Receiver_email đang bị khóa hoặc phong tỏa không thể giao dịch";
                    break;
                case "17":
                    _Message = culture == "en-US" ? "Invalid account_type" : "Account_type không hợp lệ";
                    break;
                case "18":
                    _Message = culture == "en-US" ? "The bank is under maintenance" : "Ngân hàng đang bảo trì";
                    break;
                case "99":
                    _Message = culture == "en-US" ? "An unknown error" : "Lỗi không xác định";
                    break;
            }
            return _Message;
        }
        #endregion

        #region RestSharp
        public async Task<WithdrawMoneyResponse> SetCashoutRequestWithRestSharp(WithdrawMoneyRequest requestContent,string url, string payment_method = "NL")
        {
            //var url = "https://sandbox.nganluong.vn/nl35/withdraw.api.post.php";
            var client = new RestClient(url);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            String requestinfo = GetParamWithdrawMoneyPost(requestContent);
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] data = encoding.GetBytes(requestinfo);
            request.AddHeader("ContentType", "multipart/form-data");
            request.AddParameter("func", requestContent.Funtion);
            request.AddParameter("merchant_id", requestContent.Merchant_id);
            request.AddParameter("merchant_password", CreateMD5Hash(requestContent.Merchant_password));
            request.AddParameter("receiver_email", requestContent.Receiver_email);
            request.AddParameter("ref_code", requestContent.RefCode);
            request.AddParameter("total_amount", requestContent.Total_amount);
            request.AddParameter("account_type", requestContent.AccountType);
            request.AddParameter("bank_code", requestContent.bank_code);
            request.AddParameter("card_fullname", requestContent.Card_fullname);
            request.AddParameter("card_number", requestContent.CardNumber);
            //request.AddParameter("card_month", requestContent.CardMonth);
            //request.AddParameter("card_year", requestContent.CardYear);
            //request.AddParameter("branch_name", requestContent.BranchName);
            //request.AddParameter("zone_id", requestContent.ZoneId);
            //request.AddParameter("reason", requestContent.Reason);
            var response = await client.ExecuteAsync(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var resContent = response.Content;
                var dataSuccess = JsonConvert.DeserializeObject<WithdrawMoneyResponse>(resContent);
                return dataSuccess;
            }
            else
            {
                var resErrContent = response.Content;
                var errData = JsonConvert.DeserializeObject<WithdrawMoneyResponse>(resErrContent);
                return errData;
            }
        }
        public async Task<CheckWithdrawalStatusResponse> CheckCashoutListWithRestSharp(CheckWithdrawalStatusRequestNew requestContent,string url, string payment_method = "NL")
        {
            //var url = "https://sandbox.nganluong.vn/nl35/withdraw.api.post.php";
            var client = new RestClient(url);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("ContentType", "multipart/form-data");
            request.AddParameter("func", requestContent.Funtion);
            request.AddParameter("merchant_id", requestContent.Merchant_id);
            request.AddParameter("merchant_password", CreateMD5Hash(requestContent.Merchant_password));
            request.AddParameter("receiver_email", requestContent.Receiver_email);
            //request.AddParameter("transaction_id", requestContent.TransactionId);
            request.AddParameter("ref_code", requestContent.RefCode);
            var response = await client.ExecuteAsync(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var resContent = response.Content;
                var dataSuccess = JsonConvert.DeserializeObject<CheckWithdrawalStatusResponse>(resContent);
                return dataSuccess;
            }
            else
            {
                var resErrContent = response.Content;
                var errData = JsonConvert.DeserializeObject<CheckWithdrawalStatusResponse>(resErrContent);
                return errData;
            }
        }
        public async Task<CheckWithdrawalStatusResponse> CheckCashoutListV2WithRestSharp(string orderCode, string sercurePass,string merchantId, string url)
        {
            //var checkWith = new CheckWithdrawalStatusResponse();
            
            var testdata = $"{orderCode}|{sercurePass}";
            var checkSum = CreateMD5Hash(testdata);
            var client = new RestClient(url);
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("ContentType", "multipart/form-data");
            request.AddParameter("merchant_id", merchantId);
            request.AddParameter("checksum", checkSum);
            request.AddParameter("order_code", orderCode);
            
            var response = await client.ExecuteAsync(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var resContent = response.Content;
                var dataSuccess = JsonConvert.DeserializeObject<CheckWithdrawalStatusResponse>(resContent);
                return dataSuccess;
            }
            else
            {
                var resErrContent = response.Content;
                var errData = JsonConvert.DeserializeObject<CheckWithdrawalStatusResponse>(resErrContent);
                return errData;
            }
        }
        public async Task<GetBalanceResponse> CheckBalanceRestSharp(GetBalanceRequest request, string url)
        {
            //var url = "https://sandbox.nganluong.vn/nl35/service/merchant/getBalance";
            var client = new RestClient(url);
            client.Timeout = -1;
            var requestGetBalance = new RestRequest(Method.POST);
            requestGetBalance.AddParameter("merchant_id", request.merchant_id);
            requestGetBalance.AddParameter("merchant_password", request.merchant_password);
            requestGetBalance.AddParameter("merchant_email", request.merchant_email);
            var response = await client.ExecuteAsync(requestGetBalance);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var resContent = response.Content;
                var dataSuccess = JsonConvert.DeserializeObject<GetBalanceResponse>(resContent);
                return dataSuccess;
            }
            else
            {
                var resErrContent = response.Content;
                var errData = JsonConvert.DeserializeObject<GetBalanceResponse>(resErrContent);
                return errData;
            }
            //return null;
        }
        
        #endregion
    }
    public class GetBalanceResponse
    {
        public string error_code { get; set; }
        public string error_message { get; set; }
        public List<BalanceNganLuong> data { get; set; }
    }
    public class GetBalanceRequest
    {
        public string merchant_id { get; set; }
        public string merchant_password { get; set; }
        public string merchant_email { get; set; }
    }
    public class BalanceNganLuong
    {
        public long balance { get; set; }
    }
}
