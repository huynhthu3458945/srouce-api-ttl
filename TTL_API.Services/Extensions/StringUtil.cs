﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace TTL_API.Services.Extensions
{
    public static class StringUtil
    {
        public static string CheckLetter(string preString, string maxValue, int length)
        {
            string yearCurrent = DateTime.Now.Year.ToString().Substring(2, 2);
            string monthCurrent = DateTime.Now.Month.ToString(); // "4"
            //khi thang hien tai nho hon 9 thi cong them "0" vao
            if (Convert.ToInt32(monthCurrent) <= 9)
            {
                monthCurrent = "0" + monthCurrent;
            }
            //Khi tham so select o database la null khoi tao so dau tien
            if (String.IsNullOrEmpty(maxValue))
            {
                string ret = "1";
                while (ret.Length < length)
                {
                    ret = "0" + ret;
                }
                return preString + yearCurrent + monthCurrent + "-" + ret;
            }
            else
            {
                string preStringMax = maxValue.Substring(0, maxValue.IndexOf("-") - 4);
                string maxNumber = maxValue.Substring(maxValue.IndexOf("-") + 1);
                string monthYear = maxValue.Substring(maxValue.IndexOf("-") - 4, 4);
                string monthDb = monthYear.Substring(2, 2); //as "04"

                string stringTemp = maxNumber;
                //Khi thang trong gia tri max bang voi thang create thi cong len cho 1
                if (monthDb == monthCurrent)
                {
                    int strToInt = Convert.ToInt32(maxNumber);
                    maxNumber = Convert.ToString(strToInt + 1);
                    while (maxNumber.Length < stringTemp.Length)
                        maxNumber = "0" + maxNumber;
                }
                else //reset
                {
                    maxNumber = "1";
                    while (maxNumber.Length < stringTemp.Length)
                    {
                        maxNumber = "0" + maxNumber;
                    }
                }

                return preStringMax + yearCurrent + monthCurrent + "-" + maxNumber;
            }
        }

        public static string GetProductID(string preString, string maxValue, int length)
        {
            //Khi tham so select o database la null khoi tao so dau tien
            if (String.IsNullOrEmpty(maxValue))
            {
                string ret = "1";
                while (ret.Length < length)
                {
                    ret = "0" + ret;
                }
                return preString + ret;
            }
            else
            {
                string maxNumber = Regex.Match(maxValue, @"\d+").Value;
                //Khi thang trong gia tri max bang voi thang create thi cong len cho 1
                int strToInt = Convert.ToInt32(maxNumber);
                maxNumber = Convert.ToString(strToInt + 1);
                while (maxNumber.Length < length)
                    maxNumber = "0" + maxNumber;

                return preString + maxNumber;
            }
        }

        public static string GetDescriptionEnum(System.Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }
        public static string ConvertToUnsign(string str)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = str.Normalize(NormalizationForm.FormD);
            var temp1 = regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D').Replace(' ', '-');
            return Regex.Replace(temp1, "[^0-9a-zA-Z]+", "-").ToLower();
        }
        public static string[] getFirstNameLastNameArr(String fullName)
        {
            var listNames = new string[2];
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = fullName.Trim().Normalize(NormalizationForm.FormD);
            var _fullName = regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');

            List<string> names = _fullName.Split(' ').ToList();
            string firstName = names.First();
            listNames[0] = firstName ?? " ";
            names.RemoveAt(0);
            var lastName = String.Join(" ", names.ToArray());
            listNames[1] = lastName ?? " ";
            return listNames;
        }

        public static string getLastNameCommaFirstName(String fullName)
        {
            List<string> names = fullName.Split(' ').ToList();
            string firstName = names.First();
            names.RemoveAt(0);

            return String.Join(" ", names.ToArray()) + ", " + firstName;
        }

        public static string ConvertStringToDate(string date)
        {
            char[] splitter = { '/' };
            if (String.IsNullOrEmpty(date) || date == "") return null;
            string[] splitDate = date.Split(splitter);
            if (splitDate.Length != 3) return null;
            return splitDate[1].PadLeft(2, '0') + "/" + splitDate[0].PadLeft(2, '0') + "/" + splitDate[2];
        }

        public static string UppercaseWords(string value)
        {
            char[] array = value.ToLower().ToCharArray();
            // Handle the first letter in the string.
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            // Scan through the letters, checking for spaces.
            // ... Uppercase the lowercase letters following spaces.
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }
        public static string RebuildPhoneNo(string phoneNo)
        {
            string newNumber = string.Empty;
            string[] numbers = Regex.Split(phoneNo, @"\D+");
            foreach (string value in numbers)
            {
                if (!string.IsNullOrEmpty(value))
                {
                    newNumber += value;
                }
            }
            return newNumber;
        }
        public static bool Validate(string emailAddress)
        {
            if (string.IsNullOrEmpty(emailAddress))
                return false;
            var regex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            bool isValid = Regex.IsMatch(emailAddress, regex, RegexOptions.IgnoreCase);
            return isValid;
        }
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
        public static string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }
        public static string ToShortGuid(this Guid newGuid)
        {
            string modifiedBase64 = Convert.ToBase64String(newGuid.ToByteArray())
                .Replace('+', '-').Replace('/', '_') // avoid invalid URL characters
                .Substring(0, 22);
            return modifiedBase64;
        }

        public static Guid ParseShortGuid(string shortGuid)
        {
            string base64 = shortGuid.Replace('-', '+').Replace('_', '/') + "==";
            Byte[] bytes = Convert.FromBase64String(base64);
            return new Guid(bytes);
        }

    }
}
