﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_API.Services.Extensions
{
    public class NotificationPush
    {
        public static async Task< string> PushNotifications(string url, MstNotificationsReq reqData)
        {
            try
            {
                if (string.IsNullOrEmpty(reqData.Image))
                    reqData.Image = "https://5phutthuocbai.com/img/LogoSTNHD.png";
                var client = new RestClient(url);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("application/json", JsonConvert.SerializeObject(reqData, Formatting.Indented), ParameterType.RequestBody);
                IRestResponse response =await client.ExecuteAsync(request);
                var resContent = response.Content;
                JObject jmessage = JObject.Parse(resContent);
                if (jmessage.GetValue("data") != null)
                {
                    var data = jmessage.GetValue("data").ToString();
                    return data;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
    public class MstNotificationsReq
    {
        
        public List<int> ListUserId { get; set; }
        public DateTime? Date { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }      
        public string Name { get; set; }      
        public string Summary { get; set; }    
        public string Title { get; set; }
        public string Type { get; set; }
        //public int IdPost { get; set; }
    }
}
