﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Dapper;

namespace TTL_API.Services.Implements
{
    public class Function_OrganizationService : IFunction_OrganizationService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public Function_OrganizationService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IConfiguration configuration)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            Configuration = configuration;
        }
        public IEnumerable<Function_Organization> GetById(int organizationId, int applicationId)
        {
            var query = _unitOfWork.Repository<Function_Organization>().Where(z => !z.IsDelete && z.Status 
            && z.OrganizationId == organizationId
            && (applicationId == 0 || (applicationId > 0 && applicationId == z.ApplicationId))
            ).Join(
            _unitOfWork.Repository<FunctionSystem>().Where(z=>!z.IsDelete && z.Status
            && (applicationId == 0 || (applicationId > 0 && applicationId == z.ApplicationId))
            ),
            p => p.FunctionSystemId,
            c => c.Id,
            (p, c) => p);
            //var query = _unitOfWork.Repository<Function_Organization>().Where(z=>!z.IsDelete && z.Status && z.OrganizationId == organizationId).ToList();
            return query;
        }
        
        public async Task<bool> AddAsync(IEnumerable<Function_Organization> function_OrganizationList)
        {
            try
            {
                var resInfo = JsonConvert.SerializeObject(function_OrganizationList, Formatting.Indented);
                _logger.WriteInfoLog($"AddFunction_Organization --> resInfo: {resInfo}");
                await _unitOfWork.Repository<Function_Organization>().AddRangeAsync(function_OrganizationList);
                await _unitOfWork.SaveChangesAsync();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> UpdateAsync(IEnumerable<Function_Organization> function_OrganizationList)
        {
            try
            {
                bool result = false;
                var res = await DeleteAsync(function_OrganizationList.FirstOrDefault().OrganizationId);
                if (res)
                {
                    result = await AddAsync(function_OrganizationList);
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int organizationId)
        {
            try
            {
                bool result = false;
                var tmp = _unitOfWork.Repository<Function_Organization>().Where(x => x.OrganizationId == organizationId).AsNoTracking().ToList();
                if (tmp != null)
                {
                    tmp.ForEach(z => z.IsDelete = true);
                     var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteFunction_Organization --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Function_Organization>().UpdateRange(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public async Task<bool> UpdateIsDeleteAsync(List<Function_Organization> listNotcheck, bool isDelete)
        {
            try
            {
                bool result = false;
                if (listNotcheck != null)
                {
                    listNotcheck.ForEach(z => z.IsDelete = isDelete);
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteFunction_Organization --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Function_Organization>().UpdateRange(listNotcheck);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
