﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Dapper;
using AutoMapper;

namespace TTL_API.Services.Implements
{
    public class ProductMapsService : IProductMapsService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IMapper _mapper;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public ProductMapsService(ILoggerService logger
            , IGenericDbContext<TTL_APIContext> unitOfWork
            , IConfiguration configuration
            , IRoleService roleService
            , IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            Configuration = configuration;
        }
        public async Task<ResponseList> GetAllAsync(ProductMapsFitter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<ProductMaps>().AsNoTracking();
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            var res = _mapper.Map<IEnumerable<ProductMapsModel>>(query.Where(z => !z.IsDelete 
             && (fitter.ProductId == 0 || (fitter.ProductId > 0 && z.ProductId == fitter.ProductId))
             && (fitter.KeyProductId == 0 || (fitter.KeyProductId > 0 && z.KeyProductId == fitter.KeyProductId))
             && (fitter.OrganizationId == 0 || (fitter.OrganizationId > 0 && z.OrganizationId == fitter.OrganizationId))
            ).OrderBy(x => x.Id).Skip(start).Take(limit).ToList());

            res.ToList().ForEach(z =>
            {
                z.ProductName = _unitOfWork.Repository<Product>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.ProductId).FirstOrDefault()?.Title;
                z.KeyProductName = _unitOfWork.Repository<ProductNumber>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.KeyProductId).FirstOrDefault()?.Title;
            });
            result.ListData = res;
            return result;
        }
        public async Task<ProductMaps> GetByIdAsync(int id)
        {
            ProductMaps client = null;
            client = _unitOfWork.Repository<ProductMaps>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return client;
        }    
        public async Task<ProductMaps> GetByProductMapsbyPorductId(int productId)
        {
            ProductMaps client = null;
            client = _unitOfWork.Repository<ProductMaps>()
            .Where(x => x.ProductId == productId).AsNoTracking().FirstOrDefault();
            return client;
        }
        public async Task<ProductMaps> GetProductMapsAsync(ProductMaps ProductMaps)
        {
            var targetUser = ProductMapsAsync(ProductMaps);
            return targetUser;
        }
       
        private ProductMaps ProductMapsAsync(ProductMaps ProductMaps)
        {
            ProductMaps user = null;
            user = _unitOfWork.Repository<ProductMaps>()
            .Where(x => !x.IsDelete && x.Id == ProductMaps.Id).AsNoTracking().FirstOrDefault();
            return user;
        }
        public async Task<ProductMaps> AddAsync(ProductMaps ProductMaps)
        {
            //    var userMax = _unitOfWork.Repository<ProductMaps>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            //    ProductMaps.Id = userMax == null ? 1 : userMax.Id + 1;
            var resInfo = JsonConvert.SerializeObject(ProductMaps, Formatting.Indented);
            _logger.WriteInfoLog($"AddProductMaps --> resInfo: {resInfo}");
            await _unitOfWork.Repository<ProductMaps>().AddAsync(ProductMaps);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<ProductMaps>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            ProductMaps.Id = userMax == null ? 1 : userMax.Id;

            return ProductMaps;
        }

        public async Task<bool> UpdateAsync(ProductMaps ProductMaps)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<ProductMaps>().Where(x => x.Id == ProductMaps.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.KeyProductId = ProductMaps.KeyProductId; 
                    tmp.ModifiedBy = ProductMaps.ModifiedBy;
                    tmp.ModifiedOn = ProductMaps.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateProductMaps --> resInfo: {resInfo}");
                    _unitOfWork.Repository<ProductMaps>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<ProductMaps>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteProductMaps --> resInfo: {resInfo}");
                    _unitOfWork.Repository<ProductMaps>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
