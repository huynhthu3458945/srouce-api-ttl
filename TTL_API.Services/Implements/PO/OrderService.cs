﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;
using TTL_API.Services.Extensions;
using System.ComponentModel;
using System.Net.Http;
using System.Net.Http.Headers;
using TTL_API.DbContext.Models.Utils;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using Dapper;
using TTL_API.Services.Interfaces;

namespace TTL_API.Services.Implements
{
    public class OrderService : IOrderService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IBillService _billService;
        private readonly ICustomerService _customerService;
        private readonly INganLuongService _nganLuongService;
        private HttpClientHandler clientHandler = new HttpClientHandler();
        public OrderService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper,
            IBillService billService, IConfiguration configuration, ICustomerService customerService, INganLuongService nganLuongService)
        {
            // clientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
            _logger = logger;
            _mapper = mapper;
            _billService = billService;
            _unitOfWork = unitOfWork;
            Configuration = configuration;
            _customerService = customerService;
            _nganLuongService = nganLuongService;
        }
        public async Task<ResponseList> GetListOrderByPhoneParent(OrderFitter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Order>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == fitter.OrganizationId
                && (z.CustomerPhoneParent == fitter.CustomerPhoneParent || z.SalePhone == fitter.CustomerPhoneParent || z.AcceptedPhone == fitter.CustomerPhoneParent)
                && (fitter.OrderStatusId == 0 || (fitter.OrderStatusId > 0 && fitter.OrderStatusId == z.OrderStatusId))
                && (string.IsNullOrEmpty(fitter.CustomerCode) || (!string.IsNullOrEmpty(fitter.CustomerCode) && z.CustomerCode.ToLower().Contains(fitter.CustomerCode.ToLower())))
                && (string.IsNullOrEmpty(fitter.Phone) || (!string.IsNullOrEmpty(fitter.Phone) && z.Phone.ToLower().Contains(fitter.Phone.ToLower())))
                && (!fitter.FromDate.HasValue && !fitter.ToDate.HasValue || (fitter.FromDate.HasValue && z.CreateOn.Value.Date >= fitter.FromDate.Value && z.CreateOn.Value.Date <= fitter.ToDate.Value))
             );
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            var res = _mapper.Map<IEnumerable<OrderModel>>(query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList());
            res.ToList().ForEach(z =>
            {
                //z.CustomerName = _unitOfWork.Repository<Customer>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.CustomerId).FirstOrDefault()?.FullName;
                z.CustomerName = z.FullName;
                z.OrderStatusCode = _unitOfWork.Repository<OrderStatus>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.OrderStatusId).FirstOrDefault()?.Code;
                z.OrderStatusName = _unitOfWork.Repository<OrderStatus>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.OrderStatusId).FirstOrDefault()?.Title;
            });

            result.ListData = res;
            return result;
        }
        public async Task<ResponseList> GetAllAsync(OrderFitter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Order>().AsNoTracking().Where(z => !z.IsDelete 
                && z.OrganizationId == fitter.OrganizationId
                && (z.CreateBy == fitter.CreateBy || fitter.CreateBy == 81)
                //&& (fitter.CustomerId == 0 || (fitter.CustomerId > 0 && fitter.CustomerId == z.CustomerId))
                && (fitter.OrderStatusId == 0 || (fitter.OrderStatusId > 0 && fitter.OrderStatusId == z.OrderStatusId))
                && (string.IsNullOrEmpty(fitter.CustomerCode) || (!string.IsNullOrEmpty(fitter.CustomerCode) && z.CustomerCode.ToLower().Contains(fitter.CustomerCode.ToLower())))
                && (string.IsNullOrEmpty(fitter.Phone) || (!string.IsNullOrEmpty(fitter.Phone) && z.Phone.ToLower().Contains(fitter.Phone.ToLower())))
                && (!fitter.FromDate.HasValue && !fitter.ToDate.HasValue || (fitter.FromDate.HasValue && z.CreateOn.Value.Date >= fitter.FromDate.Value && z.CreateOn.Value.Date <= fitter.ToDate.Value))
             );
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            var res = _mapper.Map<IEnumerable<OrderModel>>(query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList());
            res.ToList().ForEach(z =>
            {
                //z.CustomerName = _unitOfWork.Repository<Customer>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.CustomerId).FirstOrDefault()?.FullName;
                z.CustomerName = z.FullName;
                z.OrderStatusCode = _unitOfWork.Repository<OrderStatus>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.OrderStatusId).FirstOrDefault()?.Code;
                z.OrderStatusName = _unitOfWork.Repository<OrderStatus>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.OrderStatusId).FirstOrDefault()?.Title;
            });

            result.ListData = res;
            return result;
        }
        public async Task<OrderModel> GetByCodeAsync(string code)
        {
            var res = _mapper.Map<OrderModel>(_unitOfWork.Repository<Order>().Where(x => x.Code == code).AsNoTracking().FirstOrDefault());
            res.OrderStatusCode = _unitOfWork.Repository<OrderStatus>().Where(x => x.Id == res.OrderStatusId).AsNoTracking().FirstOrDefault().Code;
            return res;
        }
        public async Task<OrderModel> GetByIdAsync(int id)
        {
            var res = _mapper.Map<OrderModel>(_unitOfWork.Repository<Order>().Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
            res.OrderDetail = _unitOfWork.Repository<OrderDetail>().Where(x => x.OrderId == id).AsNoTracking().ToList();
            return res;
        }
        public async Task<ResponseList> GetViewByIdAsync(int id)
        {
            var result = new ResponseList();
            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GET.ORDER.BY.ID");
                paramaters.Add("@OrderId", id);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<OrderDetailView>("SP_COMMON", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
                var retCode = paramaters.Get<Int32>("@RetCode");
                var retText = paramaters.Get<string>("@RetText");

                result.ListData = res.OrderByDescending(x => x.Id).ToList();
            }
            return result;
        }
        public async Task<string> GenerateCode(string type)
        {
            var codeMax = _unitOfWork.Repository<Order>().AsNoTracking().Where(z => z.Code.ToLower().Contains(type.ToLower())).OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault()?.Code;
            return StringUtil.CheckLetter(type, codeMax, 6);
        }
        public async Task<OrderModel> AddAsync(OrderModel Order)
        {
            if (Order.PayTypeId == 1) // Chuyển khoản
            {
                Order.OrderStatusId = _unitOfWork.Repository<OrderStatus>().AsNoTracking().FirstOrDefault(x => x.Code == "600").Id;
            }
            else if (Order.ParentId == 2) // Thanh toán online
            {
                Order.OrderStatusId = _unitOfWork.Repository<OrderStatus>().AsNoTracking().FirstOrDefault(x => x.Code == "100").Id;
            }
            // Get Kênh Bán hàng
            var channel = _unitOfWork.Repository<Channel>().AsNoTracking().FirstOrDefault(x => x.AccountId == Order.CreateBy);
            if(channel != null  )
            {
                Order.Ref_Id = channel.Id;
                Order.Ref_Code = channel.Code;
            }

            var resInfo = JsonConvert.SerializeObject(Order, Formatting.Indented);
            _logger.WriteInfoLog($"AddOrder --> resInfo: {resInfo}");
            //Order.CreateOn = DateTime.Now;
            await _unitOfWork.Repository<Order>().AddAsync(Order);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Order>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Order.Id = userMax == null ? 1 : userMax.Id;
            if (Order.OrderDetail != null && Order.OrderDetail.Count() > 0)
            {
                Order.OrderDetail.ToList().ForEach(z =>
                {
                    z.OrderId = Order.Id;
                    z.CreateBy = Order.CreateBy;
                    z.CreateOn = Order.CreateOn;
                });
                await _unitOfWork.Repository<OrderDetail>().AddRangeAsync(Order.OrderDetail);
                await _unitOfWork.SaveChangesAsync();
            }
            if (Order.PayTypeId == 1) // Chuyển khoản
            {
                var resCallApiAddOrderSS = await CallApiAddOrderSS(Order, Order.OrderDetail.ToList(), true);
                Order.OrderCodeSS = resCallApiAddOrderSS.orderCode;
                // Cập nhật OrderCodeSS
                resInfo = JsonConvert.SerializeObject(Order, Formatting.Indented);
                _logger.WriteInfoLog($"UpdateOrder --> resInfo: {resInfo}");
                _unitOfWork.Repository<Order>().Update(Order);
                await _unitOfWork.SaveChangesAsync();
            }
            else if (Order.PayTypeId == 2) // Thanh toán online
            {
                // Trả link thanh toán
                var domainAppTTL = Configuration["ExtensionUrls:APPTTL_URL"];
                Order.LinkPayment = $"{domainAppTTL}/PO/Payment?id={Order.Id}";
                //var functionPaymant = new FunctionsPayment(_logger, Configuration);
                //Order.URLRequestOnePay = "URLRequestOnePay";
                //Order = await functionPaymant.InvoiceByOnePay(Order);
                // Cập nhật link thanh toán và req Onpay
                resInfo = JsonConvert.SerializeObject(Order, Formatting.Indented);
                _logger.WriteInfoLog($"UpdateOrder --> resInfo: {resInfo}");
                _unitOfWork.Repository<Order>().Update(Order);
                await _unitOfWork.SaveChangesAsync();
            }
            return Order;
        }
        public async Task<OrderModel> AddByShareAsync(OrderReq Order, int createBy)
        {
            try
            {
                var model = _mapper.Map<OrderModel>(Order);
                model.Code = await GenerateCode("DH");
                model.CreateBy = createBy;
                // Get Kênh Bán hàng
                var channel = _unitOfWork.Repository<Channel>().AsNoTracking().FirstOrDefault(x => x.AccountId == createBy);
                if (channel != null)
                {
                    model.Ref_Id = channel.Id;
                    model.Ref_Code = channel.Code;
                }
                model.CreateBy = createBy;
                model.CreateOn = DateTime.Now;

                if (Order.payTypeCode == "100") // Chuyển khoản
                {
                    model.OrderStatusId = _unitOfWork.Repository<OrderStatus>().AsNoTracking().FirstOrDefault(x => x.Code == "600").Id;
                }
                else if (Order.payTypeCode == "200") // Thanh toán online
                {
                    model.OrderStatusId = _unitOfWork.Repository<OrderStatus>().AsNoTracking().FirstOrDefault(x => x.Code == "100").Id;
                }
                model.PayTypeId = _unitOfWork.Repository<PayType>().AsNoTracking().FirstOrDefault(x => x.Code == Order.payTypeCode).Id;
                var viewCustomer = await _customerService.GetAllByPhoneStoreAsync(68, Order.phone, string.Empty);
                var resViewList = ((IEnumerable<object>)viewCustomer.ListData).Cast<ViewCustomer>().OrderByDescending(z => z.Ref_Source).FirstOrDefault();
                if (resViewList != null)
                {
                    model.CustomerPhoneParent = resViewList.Ref_Phone;
                    model.CustomerCodeParent = resViewList.Ref_Code_ERP;
                    model.CustomerCode = resViewList.Code;
                    model.FullName = resViewList.FullName;
                    model.Phone = resViewList.Phone;
                    model.Email = resViewList.Email;
                }
                else // add Customer
                {
                    CustomerModel customer = new CustomerModel();
                    customer.FullName = Order.fullName;
                    customer.Phone = Order.phone;
                    customer.Email = Order.email;
                    customer.Address = Order.address;
                    customer.GenderId = 3;
                    customer.CreateBy = 81;
                    customer.CustomerStatusId = 1;
                    customer.CustomerTypeId = 1;
                    customer.OrganizationId = 68;
                    var customerRes = await _customerService.AddAsync(customer);
                    model.CustomerCode = customerRes.Code;
                }
                model.OrganizationId = 68;
                model.TransportId = 1;
                model.OrderType = 1;

                model.OrderDetail = _mapper.Map<List<OrderDetail>>(Order.orderDetail);
                if (model.OrderDetail != null && model.OrderDetail.Count() > 0)
                {
                    model.OrderDetail.ToList().ForEach(z =>
                    {
                        z.CreateBy = model.CreateBy;
                        z.CreateOn = model.CreateOn;
                        z.Price = _unitOfWork.Repository<Product>().AsNoTracking().FirstOrDefault(x => x.Id == z.ProductId).Price;
                        z.OriginalPrice = _unitOfWork.Repository<Product>().AsNoTracking().FirstOrDefault(x => x.Id == z.ProductId).Price;
                        z.TotalPrice = z.Price * z.Quantity;
                        z.UnitId = _unitOfWork.Repository<Product>().AsNoTracking().FirstOrDefault(x => x.Id == z.ProductId).UnitId;
                    });
                }

                model.TotalAmount = model.OrderDetail.Sum(z => z.Price * z.Quantity);
                var resInfo = JsonConvert.SerializeObject(model, Formatting.Indented);
                _logger.WriteInfoLog($"AddByShare --> resInfo: {resInfo}");
                //Order.CreateOn = DateTime.Now;
                await _unitOfWork.Repository<Order>().AddAsync(model);
                await _unitOfWork.SaveChangesAsync();

                //model.OrderDetail = _mapper.Map<List<OrderDetail>>(Order.orderDetail);
                if (model.OrderDetail != null && model.OrderDetail.Count() > 0)
                {
                    model.OrderDetail.ToList().ForEach(z =>
                    {
                        z.OrderId = model.Id;
                    });
                    await _unitOfWork.Repository<OrderDetail>().AddRangeAsync(model.OrderDetail);
                    await _unitOfWork.SaveChangesAsync();
                }
                var resLink = string.Empty;
                if (Order.payTypeCode == "100") // Chuyển khoản
                {
                    var resCallApiAddOrderSS = await CallApiAddOrderSS(model, model.OrderDetail.ToList(), true);
                    model.OrderCodeSS = resCallApiAddOrderSS.orderCode;
                    // Cập nhật OrderCodeSS
                    resInfo = JsonConvert.SerializeObject(model, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateOrder --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Order>().Update(model);
                    await _unitOfWork.SaveChangesAsync();
                }
                else if (Order.payTypeCode == "200") // Thanh toán online
                {
                    // Trả link thanh toán
                    var domainAppTTL = Configuration["ExtensionUrls:APPTTL_URL"];
                    model.LinkPayment = $"{domainAppTTL}/PO/Payment?id={model.Id}";
                    //var functionPaymant = new FunctionsPayment(_logger, Configuration);
                    //Order.URLRequestOnePay = "URLRequestOnePay";
                    //model = await functionPaymant.InvoiceByOnePay(model);

                    // Cập nhật link thanh toán và req Onpay
                    resInfo = JsonConvert.SerializeObject(model, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateOrder --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Order>().Update(model);
                    await _unitOfWork.SaveChangesAsync();
                    // Trả link thanh toán
                    //resLink = model.LinkPayment;
                }
                return model;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return null;
            }
        }

        public async Task<bool> UpdateAsync(OrderModel Order)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Order>().Where(x => x.Id == Order.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.CustomerId = Order.CustomerId;
                    tmp.PriceId = Order.PriceId;
                    tmp.DiscountValue = Order.DiscountValue;
                    tmp.TotalDiscountAmount = Order.TotalDiscountAmount;
                    tmp.Note = Order.Note;
                    tmp.IsDelivery = Order.IsDelivery;
                    tmp.DeliveryFee = Order.DeliveryFee;
                    tmp.TotalAmount = Order.TotalAmount;
                    var orderStatus = await _unitOfWork.Repository<OrderStatus>().Where(x => x.Id == Order.OrderStatusId).AsNoTracking().FirstOrDefaultAsync();
                    if (orderStatus.Code != "600")
                        tmp.OrderStatusId = Order.OrderStatusId;
                    tmp.OrderStatusId = Order.OrderStatusId;
                    tmp.FullName = Order.FullName;
                    tmp.Email = Order.Email;
                    tmp.Phone = Order.Phone;
                    tmp.Phone2 = Order.Phone2;
                    tmp.Address = Order.Address;
                    tmp.PayTypeId = Order.PayTypeId;
                    tmp.NoteDeliveryFee = Order.NoteDeliveryFee;
                    tmp.IsDiscountPercent = Order.IsDiscountPercent;
                    tmp.NoteDiscount = Order.NoteDiscount;
                    tmp.IsVat = Order.IsVat;
                    tmp.InvoiceValue = Order.InvoiceValue;
                    tmp.InvoiceCode = Order.InvoiceCode;
                    tmp.InvoiceDay = Order.InvoiceDay;
                    tmp.CompanyCode = Order.CompanyCode;
                    tmp.CompanyName = Order.CompanyName;
                    tmp.CompanyAddress = Order.CompanyAddress;
                    tmp.TotalCustomerToPay = Order.TotalCustomerToPay;
                    tmp.ExtraMoney = Order.ExtraMoney;
                    tmp.Ref_Id = Order.Ref_Id;
                    tmp.Ref_Code = Order.Ref_Code;
                    tmp.MT_DiscountKindId = Order.MT_DiscountKindId;
                    if (Order.PayTypeId == 1) // Chuyển khoản
                    {
                        tmp.LinkPayment = string.Empty;
                        tmp.URLRequestOnePay = string.Empty;
                    }
                    else if (Order.ParentId == 2) // Thanh toán online
                    {
                        var domainAppTTL = Configuration["ExtensionUrls:APPTTL_URL"];
                        tmp.LinkPayment = $"{domainAppTTL}/PO/Payment?id={Order.Id}";
                        //var functionPaymant = new FunctionsPayment(_logger, Configuration);
                        //Order.URLRequestOnePay = "URLRequestOnePay";
                        //Order = await functionPaymant.InvoiceByOnePay(Order);
                        //tmp.URLRequestOnePay = Order.URLRequestOnePay;
                    }

                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateOrder --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Order>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();

                    if (Order.OrderDetail != null && Order.OrderDetail.Count() > 0)
                    {
                        var orderDetails = _unitOfWork.Repository<OrderDetail>().AsNoTracking().Where(z => z.OrderId == Order.Id);
                        _unitOfWork.Repository<OrderDetail>().RemoveRange(orderDetails);
                        await _unitOfWork.SaveChangesAsync();
                        Order.OrderDetail.ToList().ForEach(z =>
                        {
                            z.OrderId = Order.Id;
                            z.CreateBy = tmp.CreateBy; z.CreateOn = tmp.CreateOn;
                            z.ModifiedBy = Order.ModifiedBy; z.ModifiedOn = Order.ModifiedOn;
                        });
                        await _unitOfWork.Repository<OrderDetail>().AddRangeAsync(Order.OrderDetail);
                        await _unitOfWork.SaveChangesAsync();
                    }
                    //await UpdateStatus(Order.Id, Order.OrderStatusId);

                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public async Task<bool> UpdateStatusAsync(int id, string OrderStatusCode)
        {
            try
            {
                bool result = false;
                var Order = _mapper.Map<OrderModel>(_unitOfWork.Repository<Order>().Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
                Order.OrderDetail = _unitOfWork.Repository<OrderDetail>().Where(x => x.OrderId == id).AsNoTracking().ToList();
                var OrderStatus = await _unitOfWork.Repository<OrderStatus>().Where(x => x.Code == OrderStatusCode).AsNoTracking().FirstOrDefaultAsync();
                if (OrderStatus.Code == "600") // Đã Thanh Toán
                {
                    // Combo 
                    var combos = Order.OrderDetail.Where(z => z.ComboId > 0);
                    var productCombo = _unitOfWork.Repository<Combo_Product>().AsNoTracking().Where(x => combos.Select(z => z.ComboId).Contains(x.ComboId))
                        .Join(
                        _unitOfWork.Repository<Product>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == Order.OrganizationId
                         ),
                    p => p.ProductId,
                    c => c.Id,
                    (p, c) => new OrderDetail()
                    {
                        ProductId = c.Id,
                        //Price = c.CostPrice,
                        //PriceDiscount = (c.SaleDeadLine.HasValue && c.SaleDeadLine.Value >= DateTime.Now.Date) ? c.Price * c.Sale / 100 : 0,
                        UnitId = c.UnitId,
                        Quantity = p.Quantity,
                        //TotalPrice = c.Price - ((c.SaleDeadLine.HasValue && c.SaleDeadLine.Value >= DateTime.Now.Date) ? c.Price * c.Sale / 100 : 0),
                        TotalPrice = p.Quantity * c.CostPrice,
                    });
                    var orderDetails = Order.OrderDetail.Where(z => z.ProductId > 0).ToList();
                    orderDetails.AddRange(productCombo);
                    // lấy giá gốc
                    //orderDetails.ForEach(z =>
                    //{
                    //    z.Price = _unitOfWork.Repository<Product>().AsNoTracking().Where(x => x.Id == z.ProductId).FirstOrDefault().CostPrice;
                    //});

                    var groupByOrderDetails = orderDetails.GroupBy(z => new { z.ProductId, z.Price, z.UnitId, z.DT_DiscountKindId, z.PriceDiscount }).Select(
                        x => new OrderDetail
                        {
                            ProductId = x.Key.ProductId,
                            UnitId = x.Key.UnitId,
                            Price = x.Key.Price,
                            DT_DiscountKindId = x.Key.DT_DiscountKindId,
                            PriceDiscount = x.Key.PriceDiscount,
                            Quantity = x.Sum(z => z.Quantity),
                            TotalPrice = x.Sum(z => z.Quantity) * x.Key.Price
                        });

                    // check tồn kho sản phẩm số (MMTN)
                    var productType = await _unitOfWork.Repository<ProductType>().FirstOrDefaultAsync(z => z.Code == "SPS");
                    var productNumber = _unitOfWork.Repository<Product>().Where(z => z.ProductTypeId == productType.Id && z.IsInventory && z.OrganizationId == Order.OrganizationId).ToList();
                    // Lấy ra sản phẩm số có check tồn kho
                    var productAllNumberEndQuantity = groupByOrderDetails.Where(z => productNumber.Select(z => z.Id).Contains(z.ProductId)).ToList();

                    // Sản phẩm
                    var res = await GenerateExportProduct(id, Order, groupByOrderDetails);
                    var resCallApiAddOrderSS = await CallApiAddOrderSS(Order, groupByOrderDetails.ToList());
                    Order.OrderCodeSS = resCallApiAddOrderSS.orderCode;
                    if (!res)
                        return false;
                }

                //Luồng duyệt
                //Update Status
                if (Order != null)
                {
                    Order.OrderStatusId = OrderStatus.Id;
                    var resInfo = JsonConvert.SerializeObject(Order, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateOrder --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Order>().Update(Order);
                    await _unitOfWork.SaveChangesAsync();

                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public async Task<string> CheckEndQuantityProductNumber(int id, string OrderStatusCode)
        {
            try
            {
                string res = string.Empty;
                var Order = _mapper.Map<OrderModel>(_unitOfWork.Repository<Order>().Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
                Order.OrderDetail = _unitOfWork.Repository<OrderDetail>().Where(x => x.OrderId == id).AsNoTracking().ToList();
                var OrderStatus = await _unitOfWork.Repository<OrderStatus>().Where(x => x.Code == OrderStatusCode).AsNoTracking().FirstOrDefaultAsync();
                if (OrderStatus.Code == "600") // Đã Thanh Toán
                {
                    // Combo 
                    var combos = Order.OrderDetail.Where(z => z.ComboId > 0);
                    var productCombo = _unitOfWork.Repository<Combo_Product>().AsNoTracking().Where(x => combos.Select(z => z.ComboId).Contains(x.ComboId))
                        .Join(
                        _unitOfWork.Repository<Product>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == Order.OrganizationId
                         ),
                    p => p.ProductId,
                    c => c.Id,
                    (p, c) => new OrderDetail()
                    {
                        ProductId = c.Id,
                        Price = c.CostPrice,
                        //PriceDiscount = (c.SaleDeadLine.HasValue && c.SaleDeadLine.Value >= DateTime.Now.Date) ? c.Price * c.Sale / 100 : 0,
                        UnitId = c.UnitId,
                        Quantity = p.Quantity,
                        //TotalPrice = c.Price - ((c.SaleDeadLine.HasValue && c.SaleDeadLine.Value >= DateTime.Now.Date) ? c.Price * c.Sale / 100 : 0),
                        TotalPrice = p.Quantity * c.CostPrice,
                    });
                    var orderDetails = Order.OrderDetail.Where(z => z.ProductId > 0).ToList();
                    orderDetails.AddRange(productCombo);
                    // lấy giá gốc
                    //orderDetails.ForEach(z =>
                    //{
                    //    z.Price = _unitOfWork.Repository<Product>().AsNoTracking().Where(x => x.Id == z.ProductId).FirstOrDefault().CostPrice;
                    //});

                    var groupByOrderDetails = orderDetails.GroupBy(z => new { z.ProductId, z.Price, z.UnitId }).Select(
                        x => new OrderDetail
                        {
                            ProductId = x.Key.ProductId,
                            UnitId = x.Key.UnitId,
                            Price = x.Key.Price,
                            Quantity = x.Sum(z => z.Quantity),
                            TotalPrice = x.Sum(z => z.Quantity) * x.Key.Price
                        });

                    // check tồn kho sản phẩm số (MMTN)
                    var productType = await _unitOfWork.Repository<ProductType>().FirstOrDefaultAsync(z => z.Code == "SPS");
                    var productNumber = _unitOfWork.Repository<Product>().Where(z => z.ProductTypeId == productType.Id && z.IsInventory && z.OrganizationId == Order.OrganizationId).ToList();
                    // Lấy ra sản phẩm số có check tồn kho
                    var productAllNumberEndQuantity = groupByOrderDetails.Where(z => productNumber.Select(z => z.Id).Contains(z.ProductId)).ToList();

                    foreach (var item in productAllNumberEndQuantity)
                    {
                        var query = _unitOfWork.Repository<StoreDetail>().AsNoTracking().Where(z => !z.IsDelete && !z.Status && z.ProductId == item.ProductId);
                        var product = await _unitOfWork.Repository<Product>().AsNoTracking().FirstOrDefaultAsync(x => x.Id == item.ProductId);
                        if (query.Count() < item.Quantity)
                            res += $"Tồn kho - Sản phẩm - {product.Title} : {query.Count()} nhỏ hơn số lượng xuất: {item.Quantity}.\n";
                    }
                }
                return res;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return ex.ToString();
            }
        }
        //public async Task<string> CreatePayMent(int id, string cardList)
        //{
        //    try
        //    {
        //        string res = string.Empty;
        //        var Order = _mapper.Map<OrderModel>(_unitOfWork.Repository<Order>().Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
        //        var orderList =_unitOfWork.Repository<OrderDetail>().Where(x => x.OrderId == Order.Id).AsNoTracking().ToList();
        //        var functionPaymant = new FunctionsPayment(_logger, Configuration);
        //        switch (cardList)
        //        {
        //            case "OnPay":
        //                Order = await functionPaymant.InvoiceByOnePay(Order);
        //                break;
        //            case "NganLuong":
        //                string cityName = "Ho Chi Minh";
        //                Order = await functionPaymant.InvoiceByNganLuongPayAsync(_nganLuongService, Order, orderList.Count, cityName);
        //                break;
        //        }
        //        // Cập nhật link thanh toán và req Onpay
        //        _logger.WriteInfoLog($"Update--URLRequestOnePay -- {cardList} --> URLRequestOnePay: {Order.URLRequestOnePay}");
        //        _unitOfWork.Repository<Order>().Update(Order);
        //        await _unitOfWork.SaveChangesAsync();
        //        return Order.URLRequestOnePay;
        //    }
        //    catch (Exception ex)
        //    {
        //        await _logger.WriteErrorLogAsync(ex);
        //        return ex.ToString();
        //    }
        //}

        private async Task<bool> GenerateExportProduct(int id, Order order, IEnumerable<OrderDetail> orderDetails)
        {
            try
            {
                var listProductAll = new EcMstOrderResponeMKT();
                var listProductAllMMTN = new List<StoreDetail>();
                var listProductAllVMM = new List<RetailSalesOrderVMM>();
                var dateNow = DateTime.Now;
                var productType = await _unitOfWork.Repository<ProductType>().FirstOrDefaultAsync(z => z.Code == "SPS");
                var productNumberAll = _unitOfWork.Repository<Product>().Where(z => z.ProductTypeId == productType.Id && z.OrganizationId == order.OrganizationId).ToList();

                // Lấy ra sản phẩm số
                var productNumbers = orderDetails.Where(z => productNumberAll.Select(z => z.Id).Contains(z.ProductId)).ToList();

                productType = await _unitOfWork.Repository<ProductType>().FirstOrDefaultAsync(z => z.Code == "KHOAHOC-SUKIEN");
                var productNumberAllKHSK = _unitOfWork.Repository<Product>().Where(z => z.ProductTypeId == productType.Id && z.OrganizationId == order.OrganizationId).ToList();
                // Lấy ra sản phẩm khóa học và sự kiện
                var productKHSK = orderDetails.Where(z => productNumberAllKHSK.Select(z => z.Id).Contains(z.ProductId)).ToList();

                var bill = new BillModel();
                var billDetails = new List<BillDetail>();

                decimal totalAmount = 0;
                int discountValue = 0;
                decimal totalDiscountAmount = 0;

                // Sản phẩm vật lý
                var products = orderDetails.Where(z => !productNumbers.Select(z => z.ProductId).Contains(z.ProductId) && !productKHSK.Select(z => z.ProductId).Contains(z.ProductId));
                if (products.Count() > 0)
                {
                    // tạo phiếu xuất kho Vật lý
                    bill.BillCategoryId = _unitOfWork.Repository<BillCategory>().Where(z => z.Code == "XKTD").FirstOrDefault().Id;
                    bill.BillType = 2; // Xuất
                    bill.BillStatusId = _unitOfWork.Repository<BillStatus>().Where(z => z.Code == "100").FirstOrDefault().Id;
                    bill.Code = await _billService.GenerateCode("PX");
                    bill.OrderId = id;
                    bill.IsDelivery = order.IsDelivery;
                    bill.DeliveryFee = order.DeliveryFee;
                    bill.OrganizationId = order.OrganizationId;
                    bill.CreateBy = order.CreateBy;
                    bill.CreateOn = dateNow;

                    foreach (var item in products)
                    {
                        var billDetail = new BillDetail()
                        {
                            ProductId = item.ProductId,
                            UnitId = item.UnitId,
                            Quantity = item.Quantity,
                            QuantityReal = item.Quantity,
                            Price = item.Price,
                            PriceDiscount = item.PriceDiscount,
                            DT_DiscountKindId = item.DT_DiscountKindId,
                            TotalPrice = item.TotalPrice
                        };
                        billDetails.Add(billDetail);
                        totalAmount += item.TotalPrice;
                    }
                    bill.TotalAmount = totalAmount;
                    bill.DiscountValue = order.DiscountValue;
                    //bill.TotalDiscountAmount = (totalAmount * order.DiscountValue / 100) - order.DeliveryFee;
                    // lấy total từ master
                    bill.TotalDiscountAmount = order.TotalDiscountAmount;
                    bill.BillDetail = billDetails;
                    await _billService.AddAsync(bill);
                }

                // Sản phẩm số
                if (productNumbers.Count() > 0)
                {
                    var listProduct5PTB = new List<OrderDetail>();
                    var listProductVMM = new List<OrderDetail>();
                    //var listProductMMTN = new List<StoreDetail>();
                    foreach (var storeId in productNumberAll.GroupBy(z => z.StoreId).Select(z => z.Key)) // Kho sản phẩm số
                    {
                        // sản phẩm theo kho số
                        productNumbers = orderDetails.Where(z => productNumberAll.Where(z => z.StoreId == storeId).Select(z => z.Id).Contains(z.ProductId)).ToList();
                        if (productNumbers.Count > 0)
                        {
                            bill = new BillModel();
                            billDetails = new List<BillDetail>();
                            // tạo phiếu xuất kho sản phẩm số
                            bill.BillCategoryId = _unitOfWork.Repository<BillCategory>().Where(z => z.Code == "XKTD").FirstOrDefault().Id;
                            bill.BillType = 2; // Xuất
                            bill.BillStatusId = _unitOfWork.Repository<BillStatus>().Where(z => z.Code == "600").FirstOrDefault().Id; // Đã xuất kho
                            bill.Code = await _billService.GenerateCode("PX");
                            bill.OrderId = id;

                            //bill.IsDelivery = Order.IsDelivery;
                            //bill.DeliveryFee = Order.DeliveryFee;
                            bill.StoreId = storeId;
                            bill.OrganizationId = order.OrganizationId;
                            bill.CreateBy = order.CreateBy;
                            bill.CreateOn = dateNow;
                            totalAmount = 0;
                            discountValue = 0;
                            totalDiscountAmount = 0;
                            var store = _unitOfWork.Repository<Store>().AsNoTracking().FirstOrDefault(z => z.Id == storeId);
                            foreach (var item in orderDetails.Where(z => productNumbers.Select(z => z.ProductId).Contains(z.ProductId)))
                            {
                                if (store.Code == "KHO5PTB")
                                {
                                    listProduct5PTB.Add(item);
                                }
                                else if (store.Code == "KHOVMM")
                                {
                                    listProductVMM.Add(item);
                                }
                                else if (store.Code == "MMTN-LNLG")
                                {
                                    //listProductMMTN.Add(item);
                                    bill.StoreId = storeId;
                                    bill.ProductId = item.ProductId;
                                    bill.Quantity = item.Quantity;
                                    bill.TotalAmount += item.TotalPrice;
                                }
                                var billDetail = new BillDetail()
                                {
                                    ProductId = item.ProductId,
                                    UnitId = item.UnitId,
                                    Quantity = item.Quantity,
                                    QuantityReal = item.Quantity,
                                    Price = item.Price,
                                    PriceDiscount = item.PriceDiscount,
                                    DT_DiscountKindId = item.DT_DiscountKindId,
                                    TotalPrice = item.TotalPrice
                                };
                                billDetails.Add(billDetail);
                                totalAmount += item.TotalPrice;
                            }
                            bill.TotalAmount = totalAmount;
                            //bill.DiscountValue = discountValue;
                            //bill.TotalDiscountAmount = totalAmount * discountValue / 100;
                            bill.BillDetail = billDetails;
                            var resBill = await _billService.AddAsync(bill);

                            if (store.Code == "KHO5PTB")
                            {
                                listProductAll = await CallApi5PTB(order, listProduct5PTB);
                            }
                            else if (store.Code == "KHOVMM")
                            {
                                listProductAllVMM = await CallApiVMM(order, listProductVMM);
                            }
                            else if (store.Code == "MMTN-LNLG")
                            {
                                listProductAllMMTN = resBill.StoreDetail.ToList();
                            }
                        }
                    }
                }
                // Sản phẩm Khọc học và sự kiện
                if (productKHSK.Count() > 0)
                {
                    if (productKHSK.Count > 0)
                    {
                        bill = new BillModel();
                        billDetails = new List<BillDetail>();
                        // tạo phiếu xuất kho sản phẩm số
                        bill.BillCategoryId = _unitOfWork.Repository<BillCategory>().Where(z => z.Code == "XKTD").FirstOrDefault().Id;
                        bill.BillType = 2; // Xuất
                        bill.BillStatusId = _unitOfWork.Repository<BillStatus>().Where(z => z.Code == "600").FirstOrDefault().Id; // Đã xuất kho
                        bill.Code = await _billService.GenerateCode("PX");
                        bill.OrderId = id;

                        //bill.IsDelivery = Order.IsDelivery;
                        //bill.DeliveryFee = Order.DeliveryFee;
                        //bill.StoreExportId = storeId;
                        bill.OrganizationId = order.OrganizationId;
                        bill.CreateBy = order.CreateBy;
                        bill.CreateOn = dateNow;
                        totalAmount = 0;
                        discountValue = 0;
                        totalDiscountAmount = 0;
                        foreach (var item in orderDetails.Where(z => productKHSK.Select(z => z.ProductId).Contains(z.ProductId)))
                        {
                            var billDetail = new BillDetail()
                            {
                                ProductId = item.ProductId,
                                UnitId = item.UnitId,
                                Quantity = item.Quantity,
                                QuantityReal = item.Quantity,
                                Price = item.Price,
                                PriceDiscount = item.PriceDiscount,
                                DT_DiscountKindId = item.DT_DiscountKindId,
                                TotalPrice = item.TotalPrice
                            };
                            billDetails.Add(billDetail);
                            totalAmount += item.TotalPrice;
                        }
                        bill.TotalAmount = totalAmount;
                        //bill.DiscountValue = discountValue;
                        //bill.TotalDiscountAmount = totalAmount * discountValue / 100;
                        bill.BillDetail = billDetails;
                        await _billService.AddAsync(bill);
                    }
                }
                // Send Mail 
                // var emailInfo = Mapper.Map<MailSetup>(_mailSetupService.Get());
                var strTr = string.Empty;
                int index = 1;
                foreach (var item in orderDetails)
                {
                    var codeActive = string.Empty;
                    var product = await _unitOfWork.Repository<Product>().Where(z => z.Id == item.ProductId).FirstOrDefaultAsync();
                    var productMaps = _unitOfWork.Repository<ProductMaps>().Where(z => z.ProductId == item.ProductId).FirstOrDefault();
                    if (productMaps != null)
                    {
                        // 5PTB
                        var productNumber = _unitOfWork.Repository<ProductNumber>().Where(z => z.Id == productMaps.KeyProductId).FirstOrDefault();
                        if (!string.IsNullOrEmpty(productNumber.KeyProduct))
                        {
                            var product1 = listProductAll.ListProductActive.Where(z => z.ProductId == Guid.Parse(productNumber.KeyProduct));
                            codeActive = string.Join(", ", product1.Select(z => z.ActiveCode));
                        }
                        else // VMM
                        {
                            if (listProductAllVMM.Count > 0)
                            {
                                var product2 = listProductAllVMM.Where(z => z.PrductId == item.ProductId);
                                codeActive = string.Join(", ", product2.Select(z => z.ActionCode));
                            }
                        }


                    }
                    if (product.IsInventory)
                    {
                        var product1 = listProductAllMMTN.Where(z => z.ProductId == item.ProductId);
                        codeActive = string.Join(", ", product1.Select(z => z.CodeValue));
                    }


                    strTr += $"     <tr>"
                           + $"       <th scope=\"row\">{index}</th>"
                           + $"       <td>{product.Title}</td>"
                           + $"       <td style=\"text-align: center\">{string.Format("{0:#,##0}", item.Quantity)}</td>"
                           + $"       <td style=\"text-align: right\">{string.Format("{0:#,##0}", item.Price)} đ</td>"
                           + $"       <td style=\"text-align: right\">{string.Format("{0:#,##0}", item.TotalPrice)} đ</td>"
                           + $"       <td>{codeActive}</td>"
                           + $"     </tr>";
                    index++;
                }
                var mailContent =
                                  $"<!DOCTYPE html>                 "
                                  + $"<html>                          "
                                  + $"<head>                          "
                                  + $"<style>                         "
                                  + $"table, td, th {{                 "
                                  + $"  border: 1px solid;            "
                                  + $"}}                               "
                                  + $"                                "
                                  + $"table {{                         "
                                  + $"  width: 100%;                  "
                                  + $"  border-collapse: collapse;    "
                                  + $"}}                               "
                                  + $"</style>                        "
                                  + $"</head>                         "
                                  + $"<body>                          "

                                 + $"Mã đơn hàng: {order.Code}<br/>"
                                 + $"Tên Khách Hàng: <strong>{order.FullName}</strong><br/>"
                                 + $"Số điện thoại: {order.Phone}<br/>"
                                 + $"Email: {order.Email}<br/>"
                                 + $"Tạm tính: {string.Format("{0:#,##0}", order.TotalAmount + order.TotalDiscountAmount)} đ<br/>"
                                 + $"Giảm giá: {string.Format("{0:#,##0}", order.TotalDiscountAmount)} đ<br/>"
                                 + $"Tổng cộng: {string.Format("{0:#,##0}", order.TotalAmount)} đ<br/>"

                                 + $"<table class=\"table\">"
                                 + $"   <thead>"
                                 + $"     <tr>"
                                 + $"       <th scope=\"col\">#</th>"
                                 + $"       <th scope=\"col\">Tên sản phẩm</th>"
                                 + $"       <th scope=\"col\">Số lượng</th>"
                                 + $"       <th scope=\"col\">Giá</th>"
                                 + $"       <th scope=\"col\">Thành tiền</th>"
                                 + $"       <th scope=\"col\">Mã kích hoạt</th>"
                                 + $"     </tr>"
                                 + $"   </thead>"
                                 + $"   <tbody>"
                                 + $"{strTr}"
                                 + $"   </tbody>"
                                 + $" </table>"
                                 + $" </body>"
                                 + $" </html>";

                var emailTos = new List<string>();
                emailTos.Add(order.Email);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = "TÂM TRÍ LỰC",
                    Title = $"[Tâm Trí Lực] - ĐƠN HÀNG - {order.Code}",
                    Body = mailContent,
                    MailReply = "cauhinh@gmail.com"
                };
                _mailHelper.SendEmailsThread();
                return true;
            }
            catch (Exception ex)
            {
                _logger.WriteInfoLog($"CallApi5PTB");
                _logger.WriteErrorLog(ex);
                return false;
            }
        }

        private async Task<EcMstOrderResponeMKT> CallApi5PTB(Order order, List<OrderDetail> listProduct5PTB)
        {
            try
            {
                RequestGeneralERP requestGeneralERP = new RequestGeneralERP();
                requestGeneralERP.CustomerEmail = order.Email;
                requestGeneralERP.CustomerFullName = order.FullName;
                requestGeneralERP.CustomerPhone = order.Phone;
                requestGeneralERP.CustomerAddress = order.Address;
                requestGeneralERP.TotalAmount = listProduct5PTB.Sum(z => z.TotalPrice);
                //requestGeneralERP.DiscountValue = order.DiscountValue;
                requestGeneralERP.PriceDiscount = (double)listProduct5PTB.Sum(z => z.PriceDiscount);
                requestGeneralERP.ItemProducts = new List<ItemProduct>();
                var urlAPI = string.Empty;
                foreach (var item in listProduct5PTB)
                {
                    // Call Api 
                    var productMaps = _unitOfWork.Repository<ProductMaps>().Where(z => z.ProductId == item.ProductId).FirstOrDefault();
                    var productNumber = _unitOfWork.Repository<ProductNumber>().Where(z => z.Id == productMaps.KeyProductId).FirstOrDefault();
                    urlAPI = productNumber.UrlApiCreate;
                    requestGeneralERP.TonkenKey = productNumber.Token;
                    requestGeneralERP.ItemProducts.Add(new ItemProduct()
                    {
                        Quantity = item.Quantity,
                        ProductId = Guid.Parse(productNumber.KeyProduct)
                    });
                }
                // Call Api -- 5PTB
                using (HttpClient httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);

                    var resInfo = JsonConvert.SerializeObject(requestGeneralERP, Formatting.Indented);
                    _logger.WriteInfoLog($"CallApi5PTB --> resInfo: {resInfo}");

                    var response = await httpClient.PostAsJsonAsync($"{urlAPI}", requestGeneralERP);
                    var resResponse = (await response.Content.ReadAsAsync<GetResponse<EcMstOrderResponeMKT>>());
                    if (resResponse.RetCode == RetCodeEnum.Ok)
                        return resResponse.Data;
                }
                return null;
            }
            catch (Exception ex)
            {
                _logger.WriteInfoLog($"CallApi5PTB --> error");
                _logger.WriteErrorLog(ex);
                return null;
            }
        }

        private async Task<List<RetailSalesOrderVMM>> CallApiVMM(Order order, List<OrderDetail> listProductVMM)
        {
            try
            {
                var listAll = new List<RetailSalesOrderVMM>();
                InvoiceCreateRequestVMM requestVMM = new InvoiceCreateRequestVMM();
                requestVMM.CustomerEmail = order.Email;
                requestVMM.CustomerName = order.FullName;
                requestVMM.CustomerPhone = order.Phone;
                requestVMM.TotalAmount = (double)listProductVMM.Sum(z => z.TotalPrice);
                var urlAPI = string.Empty;
                var i = 0;
                foreach (var item in listProductVMM)
                {
                    int quantity = item.Quantity;
                    while (i < quantity)
                    {
                        var productMaps = _unitOfWork.Repository<ProductMaps>().Where(z => z.ProductId == item.ProductId).FirstOrDefault();
                        var productNumber = _unitOfWork.Repository<ProductNumber>().Where(z => z.Id == productMaps.KeyProductId).FirstOrDefault();
                        urlAPI = productNumber.UrlApiCreate;

                        // Call Api Create -- VMM
                        using (HttpClient httpClient = new HttpClient(clientHandler, false))
                        {
                            requestVMM.ApiKey = productNumber.Token;
                            requestVMM.Culture = "vi-VN";
                            requestVMM.LevelCode = "PAY";
                            requestVMM.ItemCode = productNumber.LevelCode;
                            requestVMM.ItemName = productNumber.LevelName;
                            requestVMM.DurationCode = productNumber.DurationCode;
                            requestVMM.DurationName = productNumber.DurationName;

                            httpClient.DefaultRequestHeaders.Accept.Clear();
                            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);
                            var resInfo = JsonConvert.SerializeObject(requestVMM, Formatting.Indented);
                            _logger.WriteInfoLog($"CallApiVMM -- Create --> resInfo: {resInfo}");

                            var response = await httpClient.PostAsJsonAsync($"{productNumber.UrlApiCreate}", requestVMM);
                            var resResponse = (await response.Content.ReadAsAsync<GetResponse<RetailSalesOrderVMM>>());
                            if (resResponse.RetCode == RetCodeEnum.Ok)
                            {
                                // Call Api Update -- VMM
                                using (HttpClient httpClientUpdate = new HttpClient(clientHandler, false))
                                {
                                    InvoiceUpdateRequestVMM requestUpdateVMM = new InvoiceUpdateRequestVMM();
                                    requestUpdateVMM.ApiKey = productNumber.Token;
                                    requestUpdateVMM.Code = resResponse.Data.Code;
                                    requestUpdateVMM.StatusCode = "SUCCESS";

                                    httpClientUpdate.DefaultRequestHeaders.Accept.Clear();
                                    httpClientUpdate.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);

                                    resInfo = JsonConvert.SerializeObject(requestUpdateVMM, Formatting.Indented);
                                    _logger.WriteInfoLog($"CallApiVMM -- Update --> resInfo: {resInfo}");

                                    var responseUpdate = await httpClientUpdate.PostAsJsonAsync($"{productNumber.UrlApiUpdate}", requestUpdateVMM);
                                    var resResponseUpdate = (await responseUpdate.Content.ReadAsAsync<GetResponse<RetailSalesOrderVMM>>());
                                    resResponseUpdate.Data.PrductId = item.ProductId;

                                    if (resResponseUpdate.RetCode == RetCodeEnum.Ok)
                                        listAll.Add(resResponseUpdate.Data);
                                }
                            }
                        }
                        i++;
                    }
                    i = 0;
                }
                return listAll;

            }
            catch (Exception ex)
            {
                _logger.WriteInfoLog($"CallApiVMM --> error");
                _logger.WriteErrorLog(ex);
                return null;
            }
        }

        private async Task<OrderReqSS> CallApiAddOrderSS(Order order, List<OrderDetail> listProduct, bool isAutoProcess = false)
        {
            try
            {
                // call API Sang SS
                var orderReqSS = new OrderReqSS();
                orderReqSS.tokenKey = Configuration["Tokens:Key"];
                orderReqSS.fullNameBuyer = order.FullName;
                orderReqSS.emailBuyer = order.Email;
                orderReqSS.phoneBuyer = order.Phone;
                orderReqSS.phoneSeller = order.SalePhone;
                orderReqSS.totalAmount = (int)order.TotalAmount;
                orderReqSS.autoProcess = isAutoProcess;
                orderReqSS.fromSource = "AdminTtl";
                orderReqSS.orderDetails = new List<OrderDetailSS>();
                if (listProduct != null && listProduct.Count > 0)
                {
                    foreach (var item in listProduct)
                    {
                        var orderDetailSS = new OrderDetailSS();
                        orderDetailSS.productCode = _unitOfWork.Repository<Product>().AsNoTracking().FirstOrDefault(x => x.Id == item.ProductId).Code;
                        orderDetailSS.quantity = item.Quantity;
                        orderDetailSS.totalAmount = (int)item.TotalPrice;
                        orderDetailSS.productPrice = (int)item.Price;
                        orderReqSS.orderDetails.Add(orderDetailSS);
                    }
                }

                // Call Api add Order SS 
                using (HttpClient httpClient = new HttpClient(clientHandler, false))
                {
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);

                    var resInfo = JsonConvert.SerializeObject(orderReqSS, Formatting.Indented);
                    _logger.WriteInfoLog($"CallApiAddOrderSS[{order.Code}] --> resInfo: {resInfo}");

                    var domain = Configuration["ExtensionUrls:APIV2_URL"];
                    var urlAPI = "/api/ViMstSalesOrderProductType/create-order-ttl";

                    var response = await httpClient.PostAsJsonAsync($"{domain}{urlAPI}", orderReqSS);
                    var resResponse = (await response.Content.ReadAsAsync<GetResponse<OrderReqSS>>());
                    if (resResponse.RetCode == RetCodeEnum.Ok)
                        return resResponse.Data;
                }
                return null;
            }
            catch (Exception ex)
            {
                _logger.WriteInfoLog($"CallApiAddOrderSS --> error");
                _logger.WriteErrorLog(ex);
                return null;
            }
        }


        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Order>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteOrder --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Order>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public class GetResponse<T>
        {
            public RetCodeEnum RetCode { get; set; }
            public string RetText { get; set; }
            public T Data { get; set; }
        }

        public enum RetCodeEnum
        {
            [Description("OK")]
            Ok = 0,
            [Description("Api Error")]
            ApiError = 1,
            [Description("Not Exists")]
            ResultNotExists = 2,
            [Description("Parammeters Invalid")]
            ParammetersInvalid = 3,
            [Description("Parammeters Not Found")]
            ParammetersNotFound = 4,
            [Description("Not delete")]
            ApiNoDelete = 5,
            [Description("Not Role")]
            ApiNotRole = 6
        }
    }
}
