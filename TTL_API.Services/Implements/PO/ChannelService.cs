﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;

namespace TTL_API.Services.Implements
{
    public class ChannelService : IChannelService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public ChannelService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Channel>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId).Join(
                _unitOfWork.Repository<Account>().AsNoTracking().Where(z => !z.IsDelete && z.Status),
                p => p.AccountId,
                c => c.Id,
                (p, c) => new ChannelModel()
                {
                    Id = p.Id,
                    Code = p.Code,
                    Title = p.Title,
                    AccountName = c.UserName,
                    Status = c.Status,
                    IsDelete = p.IsDelete,
                });
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllChannelCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Channel>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Code + " - " + z.Title }).ToList();
            return result;
        }
        public async Task<Channel> GetByCodeAsync(string code)
        {
            Channel Channel = null;
            Channel = _unitOfWork.Repository<Channel>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Channel;
        }
        public async Task<ChannelModel> GetByIdAsync(int id)
        {
            var res = _mapper.Map<ChannelModel>(_unitOfWork.Repository<Channel>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
            return res;
        }
        public async Task<ChannelModel> AddAsync(ChannelModel Channel)
        {
            var resInfo = JsonConvert.SerializeObject(Channel, Formatting.Indented);
            _logger.WriteInfoLog($"AddChannel --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Channel>().AddAsync(Channel);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Channel>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Channel.Id = userMax == null ? 1 : userMax.Id;
            return Channel;
        }

        public async Task<bool> UpdateAsync(ChannelModel Channel)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Channel>().Where(x => x.Id == Channel.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = Channel.Code;
                    tmp.Title = Channel.Title;
                    tmp.Status = Channel.Status;
                    tmp.AccountId = Channel.AccountId;
                    tmp.ModifiedBy = Channel.ModifiedBy;
                    tmp.ModifiedOn = Channel.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateChannel --> resInfo: {resInfo}");
                    _unitOfWork.Repository<DbContext.Models.Channel>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Channel>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteChannel --> resInfo: {resInfo}");
                    _unitOfWork.Repository<DbContext.Models.Channel>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
