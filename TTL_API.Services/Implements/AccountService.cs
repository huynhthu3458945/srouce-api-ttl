﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Dapper;
using AutoMapper;

namespace TTL_API.Services.Implements
{
    public class AccountService : IAccountService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IRoleService _roleService;
        private readonly IAccount_RoleService _account_RoleService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public AccountService(ILoggerService logger
            , IGenericDbContext<TTL_APIContext> unitOfWork
            , IConfiguration configuration
            , IAccount_RoleService account_RoleService
            , IRoleService roleService
            , IMapper mapper)
        {
            _logger = logger;
            _logger = logger;
            _unitOfWork = unitOfWork;
            _roleService = roleService;
            _account_RoleService = account_RoleService;
            Configuration = configuration;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Account>().AsNoTracking();
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            var accoutOrg = await _unitOfWork.Repository<Account_Organization>().AsNoTracking().Where(z => z.OrganizationId == organizationId).ToListAsync();
            result.ListData = query.Where(z => !z.IsDelete && accoutOrg.Select(z => z.AccountId).Contains(z.Id)).OrderBy(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllComboxAsync(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Account>().AsNoTracking().Where(z => !z.IsDelete && z.Status).Join(
            _unitOfWork.Repository<Account_Organization>().AsNoTracking().Where(z => z.Status && z.OrganizationId == organizationId),
            p => p.Id,
            c => c.AccountId,
            (p, c) => p);
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.UserName }).ToList();
            return result;
        }

        public async Task<Account> GetByIdAsync(int id)
        {
            Account client = null;
            client = _unitOfWork.Repository<Account>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return client;
        }
        public async Task<Account> GetAccountAsync(Account account)
        {
            var targetUser = AccountAsync(account);
            return targetUser;
        }
        public async Task<Account> GetAccountUserNameAsync(string userName)
        {
            var result = new Account();
            var query = _unitOfWork.Repository<Account>().AsNoTracking();
            result = query.Where(z => !z.IsDelete && z.UserName == userName).FirstOrDefault();
            return result;
        }
        private Account AccountAsync(Account account)
        {
            Account user = null;
            user = _unitOfWork.Repository<Account>()
            .Where(x => !x.IsDelete && x.Id == account.Id).AsNoTracking().FirstOrDefault();
            return user;
        }
        public async Task<Account> AddAsync(AccountModel account)
        {
            //    var userMax = _unitOfWork.Repository<Account>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            //    Account.Id = userMax == null ? 1 : userMax.Id + 1;
            var resInfo = JsonConvert.SerializeObject(account, Formatting.Indented);
            _logger.WriteInfoLog($"AddAccount --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Account>().AddAsync(account);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Account>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            account.Id = userMax == null ? 1 : userMax.Id;

            // Inser Account_Role
            var listRole = _roleService.GetByOrganizationAsync(account.OrganizationId).AsEnumerable().Where(z => account.TreeRoleAccs.Where(x => x.IsCheck).Select(x => x.Id).Contains(z.Id));
            // inser phân quyền
            var listRole_Func = listRole.Select(z => new Account_Role() { AccountId = account.Id, RoleId = z.Id, CreateBy = account.CreateBy, CreateOn = account.CreateOn, ModifiedBy = account.ModifiedBy, ModifiedOn = account.ModifiedOn });
            var resInser = _unitOfWork.Repository<Account_Role>().AddRangeAsync(listRole_Func);

            // insert Account_Organization
            var accountType = await _unitOfWork.Repository<AccountType>().AsNoTracking().FirstOrDefaultAsync(z => z.Code == "Normal");
            var resInsertAccOrg = await _unitOfWork.Repository<Account_Organization>().AddAsync(new Account_Organization()
            { OrganizationId = account.OrganizationId, AccountId = account.Id, AccountTypeId = accountType.Id, Status = true, CreateBy = account.CreateBy, CreateOn = account.CreateOn });
            await _unitOfWork.SaveChangesAsync();
            return account;
        }

        public async Task<bool> UpdateAsync(AccountModel account)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Account>().Where(x => x.Id == account.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Status = account.Status;
                    tmp.Password = account.Password;
                    tmp.ModifiedBy = account.ModifiedBy;
                    tmp.ModifiedOn = account.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateAccount --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Account>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                  
                    // list role check
                    var listRole = _roleService.GetByOrganizationAsync(account.OrganizationId).AsEnumerable().Where(z => account.TreeRoleAccs.Where(x => x.IsCheck).Select(x => x.Id).Contains(z.Id));
                    var listAllRoles = _roleService.GetByOrganizationAsync(account.OrganizationId).AsEnumerable().Where(z => account.TreeRoleAccs.Select(x => x.Id).Contains(z.Id));

                    ////Delete và Insert lại Account_Role
                    var resDelete = await _account_RoleService.DeleteAsync(account.Id, listAllRoles.Select(z=>z.Id).ToList());

                    // inser phân quyền
                    var listRole_Func = listRole.Select(z => new Account_Role() { AccountId = account.Id, RoleId = z.Id, CreateBy = tmp.CreateBy, CreateOn = tmp.CreateOn, ModifiedBy = account.ModifiedBy, ModifiedOn = account.ModifiedOn });
                    var resInser = _unitOfWork.Repository<Account_Role>().AddRangeAsync(listRole_Func);

                    //var accountType = await _unitOfWork.Repository<AccountType>().AsNoTracking().FirstOrDefaultAsync(z => z.Code == "Normal");
                    //// insert Account_Organization
                    //var acc_Organization = await _unitOfWork.Repository<Account_Organization>().AsNoTracking().FirstOrDefaultAsync(z => z.AccountId == account.Id && z.AccountTypeId == accountType.Id && z.OrganizationId == account.OrganizationId);
                    //if (acc_Organization != null)
                    //    _unitOfWork.Repository<Account_Organization>().Remove(acc_Organization);
                    //var resInsertAccOrg = await _unitOfWork.Repository<Account_Organization>().AddAsync(new Account_Organization()
                    //{ OrganizationId = account.OrganizationId, AccountId = account.Id, AccountTypeId = accountType.Id, Status = true, CreateBy = tmp.CreateBy, CreateOn = tmp.CreateOn });

                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Account>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteAccount --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Account>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public List<RoleModelTree> GetChildren(List<RoleModelTree> comments, int parentId, List<Account_Role> account_Roles)
        {
            return comments
                    .Where(c => c.ParentId == parentId)
                    .Select(c => new RoleModelTree
                    {
                        Id = c.Id,
                        Text = c.Text,
                        ParentId = c.ParentId,
                        State = new State() { Selected = account_Roles.FirstOrDefault(z => z.RoleId == c.Id) != null },
                        Children = GetChildren(comments, c.Id, account_Roles)
                    })
                    .ToList();
        }

    }
}
