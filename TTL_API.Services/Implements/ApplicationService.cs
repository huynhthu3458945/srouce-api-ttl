﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class ApplicationService : IApplicationService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public ApplicationService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Application>().AsNoTracking();
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.Where(z=>!z.IsDelete).OrderBy(x => x.Position).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllComboxAsync()
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Application>().AsNoTracking();
            var totalRows = query.Count();
            result.ListData = query.Where(z => !z.IsDelete).Select(z=> new ResponseCombo() { Id= z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }

        public async Task<Application> GetByIdAsync(int id)
        {
            Application application = null;
            application = _unitOfWork.Repository<Application>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return application;
        }
        public async Task<Application> AddAsync(Application application)
        {
            var resInfo = JsonConvert.SerializeObject(application, Formatting.Indented);
            _logger.WriteInfoLog($"AddApplication --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Application>().AddAsync(application);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Application>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            application.Id = userMax == null ? 1 : userMax.Id;
            return application;
        }

        public async Task<bool> UpdateAsync(Application Application)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Application>().Where(x => x.Id == Application.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = Application.Code;
                    tmp.Title = Application.Title;
                    tmp.Description = Application.Description;
                    tmp.Position = Application.Position;
                    tmp.Icon = Application.Icon;
                    tmp.Status = Application.Status;
                    tmp.ModifiedBy = Application.ModifiedBy;
                    tmp.ModifiedOn = Application.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateApplication --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Application>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Application>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteApplication --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Application>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

       
    }
}
