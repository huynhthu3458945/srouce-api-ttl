﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using Dapper;

namespace TTL_API.Services.Implements
{
    public class Role_FunctionService : IRole_FunctionService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public Role_FunctionService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IConfiguration configuration)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            Configuration = configuration;
        }
        public async Task<IEnumerable<Role_Function>> GetAllByRoleIdAsync(int roleId)
        {
           return _unitOfWork.Repository<Role_Function>().AsNoTracking().Where(z=>z.RoleId == roleId).ToList();
        }
        public async Task<Role_Function> AddAsync(Role_Function role_Function)
        {
            //    var userMax = _unitOfWork.Repository<Role_Function>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            //    Role_Function.Id = userMax == null ? 1 : userMax.Id + 1;
            var resInfo = JsonConvert.SerializeObject(role_Function, Formatting.Indented);
            _logger.WriteInfoLog($"AddRole_Function --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Role_Function>().AddAsync(role_Function);
            await _unitOfWork.SaveChangesAsync();
            return null;
        }

        public async Task<bool> DeleteAsync(int roleId)
        {
            try
            {
                bool result = false;
                var tmp = _unitOfWork.Repository<Role_Function>().Where(x => x.RoleId == roleId).AsNoTracking().ToList();
                if (tmp != null)
                {
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteRole_Function --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Role_Function>().RemoveRange(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public Task<IEnumerable<Role_Function>> GetAllByRoleIdsAsync(List<int> roleIds)
        {
            throw new NotImplementedException();
        }
    }
}
