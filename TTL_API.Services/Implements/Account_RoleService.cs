﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using Dapper;

namespace TTL_API.Services.Implements
{
    public class Account_RoleService : IAccount_RoleService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public Account_RoleService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IConfiguration configuration)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            Configuration = configuration;
        }
        public async Task<IEnumerable<Account_Role>> GetAllByAccountIdAsync(int accountId)
        {
           var res = _unitOfWork.Repository<Account_Role>().AsNoTracking().Where(z=>z.AccountId == accountId).ToList();
            return res;
        }
       
        public async Task<bool> DeleteAsync(int accountId, List<int> rolesCheck)
        {
            try
            {
                bool result = false;
                var tmp = _unitOfWork.Repository<Account_Role>().Where(x => x.AccountId == accountId && rolesCheck.Contains(x.RoleId)).AsNoTracking().ToList();
                if (tmp != null)
                {
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteAccount_Role --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Account_Role>().RemoveRange(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
