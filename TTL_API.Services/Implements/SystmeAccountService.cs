﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using System.Linq;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;
using Microsoft.Extensions.Configuration;

namespace TTL_API.Services.Implements
{
    public class SystmeAccountService : ISystmeAccountService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public SystmeAccountService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IConfiguration configuration)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            Configuration = configuration;
        }

        public async Task<SystemAccount> GetSystmeAccountAsync(SystemAccount account)
        {
            var targetUser = SystmeAccountAsync(account);
            return targetUser;
        }

        public async Task<SystemAccount> GetSystmeAccountUserNameAsync(string userName)
        {
            SystemAccount user = null;
            user = _unitOfWork.Repository<SystemAccount>()
            .Where(x => !x.IsDelete && x.UserName == userName).AsNoTracking().FirstOrDefault();
            return user;
        }

        private SystemAccount SystmeAccountAsync(SystemAccount account)
        {
            SystemAccount user = null;
            user = _unitOfWork.Repository<SystemAccount>()
            .Where(x => !x.IsDelete && x.Id == account.Id).AsNoTracking().FirstOrDefault();
            return user;
        }
    }
}
