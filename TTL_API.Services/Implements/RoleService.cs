﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;

namespace TTL_API.Services.Implements
{
    public class RoleService : IRoleService
    {
        private readonly ILoggerService _logger;
        private readonly IFunction_OrganizationService _function_OrganizationService;
        private readonly IRole_FunctionService _role_FunctionService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public RoleService(ILoggerService logger,
            IFunction_OrganizationService function_OrganizationService, IMapper mapper,
            IRole_FunctionService role_FunctionService,
            IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _function_OrganizationService = function_OrganizationService;
            _mapper = mapper;
            _role_FunctionService = role_FunctionService;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Role>().AsNoTracking();
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.Where(z => !z.IsDelete && z.OrganizationId == organizationId).OrderBy(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public IEnumerable<Role> GetByOrganizationAsync(int organizationId)
        {
            return _unitOfWork.Repository<Role>().AsNoTracking().Where(z => z.OrganizationId == organizationId).ToList();
        }
        public async Task<ResponseList> GetAllComboxAsync()
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Role>().AsNoTracking();
            var totalRows = query.Count();
            result.ListData = query.Where(z => !z.IsDelete && z.Status).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<Role> GetByIdAsync(int id)
        {
            Role Role = null;
            Role = _unitOfWork.Repository<Role>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return Role;
        }
        public async Task<Role> GetByCodeAsync(string code, int organizationId)
        {
            Role Role = null;
            Role = _unitOfWork.Repository<Role>()
            .Where(x => !x.IsDelete && x.Code == code && x.OrganizationId == organizationId).AsNoTracking().FirstOrDefault();
            return Role;
        }
        public async Task<AccountType> GetAccTypeByUserNameAsync(string userName)
        {
            AccountType accountType = null;
            var account = await _unitOfWork.Repository<Account>().AsNoTracking().FirstOrDefaultAsync(z => !z.IsDelete && z.UserName == userName);
            var accOrg = await _unitOfWork.Repository<Account_Organization>().AsNoTracking().FirstOrDefaultAsync(z => z.AccountId == account.Id);
            accountType = await _unitOfWork.Repository<AccountType>().AsNoTracking().FirstOrDefaultAsync(z => z.Id == accOrg.AccountTypeId);
            return accountType;
        }
        public async Task<Account_Organization> GetAccOrgByUserNameAsync(string userName)
        {
            Account_Organization accOrg = null;
            var account = await _unitOfWork.Repository<Account>().AsNoTracking().FirstOrDefaultAsync(z => !z.IsDelete && z.UserName == userName);
            accOrg = await _unitOfWork.Repository<Account_Organization>().AsNoTracking().FirstOrDefaultAsync(z => z.AccountId == account.Id);
            return accOrg;
        }
        public async Task<Role> AddAsync(RoleModel role)
        {
            //    var userMax = _unitOfWork.Repository<Role>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            //    Role.Id = userMax == null ? 1 : userMax.Id + 1;
            var resInfo = JsonConvert.SerializeObject(role, Formatting.Indented);
            _logger.WriteInfoLog($"AddRole --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Role>().AddAsync(role);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Role>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            role.Id = userMax == null ? 1 : userMax.Id;

            // Inser FunctionOrganization
            var listFunctionOrganization = _function_OrganizationService.GetById(role.OrganizationId, 0).AsEnumerable().Where(z => role.TreeFuncs.Where(x => x.IsCheck).Select(x => x.Id).Contains(z.FunctionSystemId));
            // inser phân quyền
            var listRole_Func = listFunctionOrganization.Select(z => new Role_Function() { RoleId = role.Id, FunctionId = z.Id, CreateBy = role.CreateBy, CreateOn = role.CreateOn, ModifiedBy = role.ModifiedBy, ModifiedOn = role.ModifiedOn });
            var resInser = _unitOfWork.Repository<Role_Function>().AddRangeAsync(listRole_Func);
            await _unitOfWork.SaveChangesAsync();
            return role;
        }

        public async Task<bool> UpdateAsync(RoleModel role)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Role>().Where(x => x.Id == role.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = role.Code;
                    tmp.Title = role.Title;
                    tmp.Description = role.Description;
                    tmp.Status = role.Status;
                    tmp.ModifiedBy = role.ModifiedBy;
                    tmp.ModifiedOn = role.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateRole --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Role>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    //Delete và Insert lại
                    var resDelete = await _role_FunctionService.DeleteAsync(role.Id);

                    var listFunctionOrganization = _function_OrganizationService.GetById(role.OrganizationId, 0).AsEnumerable().Where(z => role.TreeFuncs.Where(x => x.IsCheck).Select(x => x.Id).Contains(z.FunctionSystemId));
                    // inser phân quyền
                    var listRole_Func = listFunctionOrganization.Select(z => new Role_Function() { RoleId = role.Id, FunctionId = z.Id, CreateBy = tmp.CreateBy, CreateOn = tmp.CreateOn, ModifiedBy = role.ModifiedBy, ModifiedOn = role.ModifiedOn });
                    var resInser = _unitOfWork.Repository<Role_Function>().AddRangeAsync(listRole_Func);

                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Role>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    //tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteRole --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Role>().Remove(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public List<FunctionOrganizationModelTree> GetChildren(List<FunctionOrganizationModelTree> comments, int parentId, List<Role_Function> role_Functions)
        {
            return comments
                    .Where(c => c.ParentId == parentId)
                    .Select(c => new FunctionOrganizationModelTree
                    {
                        Id = c.Id,
                        Text = c.Text,
                        ParentId = c.ParentId,
                        State = new State() { Selected = role_Functions.FirstOrDefault(z => z.FunctionId == c.FuncId) != null && !CheckIsChildren(c.Id, comments) },
                        Children = GetChildren(comments, c.Id, role_Functions)
                    })
                    .ToList();
        }
        public bool CheckIsChildren(int id, List<FunctionOrganizationModelTree> list)
        {
            var res = list
                    .Where(c => c.ParentId == id)
                    .ToList();
            if (res != null && res.Count > 0)
                return true;
            else
                return false;
        }

    }
}
