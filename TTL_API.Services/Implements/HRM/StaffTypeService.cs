﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;
using TTL_API.Services.Extensions;

namespace TTL_API.Services.Implements
{
    public class StaffTypeService : IStaffTypeService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public StaffTypeService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<StaffType>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllStaffTypeCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<StaffType>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Position).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<StaffType> GetByCodeAsync(string code)
        {
            StaffType StaffType = null;
            StaffType = _unitOfWork.Repository<StaffType>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return StaffType;
        }
        public async Task<StaffType> GetByIdAsync(int id)
        {
            StaffType StaffType = null;
            StaffType = _unitOfWork.Repository<StaffType>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return StaffType;
        }
        public async Task<StaffType> AddAsync(StaffType StaffType)
        {
            var resInfo = JsonConvert.SerializeObject(StaffType, Formatting.Indented);
            _logger.WriteInfoLog($"AddStaffType --> resInfo: {resInfo}");
            await _unitOfWork.Repository<StaffType>().AddAsync(StaffType);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<StaffType>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            StaffType.Id = userMax == null ? 1 : userMax.Id;
            return StaffType;
        }

        public async Task<bool> UpdateAsync(StaffType StaffType)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<StaffType>().Where(x => x.Id == StaffType.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = StaffType.Code;
                    tmp.Title = StaffType.Title;
                    tmp.Description = StaffType.Description;
                    tmp.Status = StaffType.Status;
                    tmp.ModifiedBy = StaffType.ModifiedBy;
                    tmp.ModifiedOn = StaffType.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateStaffType --> resInfo: {resInfo}");
                    _unitOfWork.Repository<StaffType>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<StaffType>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteStaffType --> resInfo: {resInfo}");
                    _unitOfWork.Repository<StaffType>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
