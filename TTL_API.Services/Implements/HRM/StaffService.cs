﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;

namespace TTL_API.Services.Implements
{
    public class StaffService : IStaffService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IAccountService _accountService;
        private readonly IMapper _mapper;
        public IConfiguration Configuration { get; }
        public StaffService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IAccountService accountService, IMapper mapper, IConfiguration configuration)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _accountService = accountService;
            _mapper = mapper;
            Configuration = configuration;
        }
        public async Task<ResponseList> GetAllAsync(StaffFitter fitter, int limit, int page)
        {
            var result = new ResponseList();

            var query = _unitOfWork.Repository<Staff_Department>().AsNoTracking().Where(z => !z.IsDelete
            && (fitter.DepartmentId == 0 || (fitter.DepartmentId > 0 && fitter.DepartmentId == z.DepartmentId))
            ).Join(
                _unitOfWork.Repository<Staff>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == fitter.OrganizationId
                     && (string.IsNullOrEmpty(fitter.Code) || (!string.IsNullOrEmpty(fitter.Code) && z.Code.ToLower().Contains(fitter.Code.ToLower())))
                     && (string.IsNullOrEmpty(fitter.FullName) || (!string.IsNullOrEmpty(fitter.FullName) && z.FullName.ToLower().Contains(fitter.FullName.ToLower())))
                     && (string.IsNullOrEmpty(fitter.Email) || (!string.IsNullOrEmpty(fitter.Email) && z.Email.ToLower().Contains(fitter.Email.ToLower())))
                     && (string.IsNullOrEmpty(fitter.Phone) || (!string.IsNullOrEmpty(fitter.Phone) && z.Phone.ToLower().Contains(fitter.Phone.ToLower())))
                     && (fitter.StaffTypeId == 0 || (fitter.StaffTypeId > 0 && z.StaffTypeId == fitter.StaffTypeId))
                     && (fitter.RegencyId == 0 || (fitter.RegencyId > 0 && z.RegencyId == fitter.RegencyId))
                 ),
                p => p.StaffId,
                c => c.Id,
                (p, c) => c).Distinct();
            //var query = _unitOfWork.Repository<Staff>().AsNoTracking()
            //    .Where(z => !z.IsDelete
            //    && z.OrganizationId == fitter.OrganizationId
            //    && (string.IsNullOrEmpty(fitter.Code) || (!string.IsNullOrEmpty(fitter.Code) && z.Code.ToLower().Contains(fitter.Code.ToLower())))
            //    && (string.IsNullOrEmpty(fitter.FullName) || (!string.IsNullOrEmpty(fitter.FullName) && z.FullName.ToLower().Contains(fitter.FullName.ToLower())))
            //    && (string.IsNullOrEmpty(fitter.Email) || (!string.IsNullOrEmpty(fitter.Email) && z.Email.ToLower().Contains(fitter.Email.ToLower())))
            //    && (string.IsNullOrEmpty(fitter.Phone) || (!string.IsNullOrEmpty(fitter.Phone) && z.Phone.ToLower().Contains(fitter.Phone.ToLower())))
            //    && (fitter.StaffTypeId == 0 || (fitter.StaffTypeId > 0 && z.StaffTypeId == fitter.StaffTypeId))
            //    && (fitter.RegencyId == 0 || (fitter.RegencyId > 0 && z.RegencyId == fitter.RegencyId))
            //    ).ToList();
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            var res = _mapper.Map<IEnumerable<StaffModel>>(query).OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            res.ToList().ForEach(z =>
            {
                z.GenderName = _unitOfWork.Repository<Gender>().AsNoTracking().FirstOrDefault(x => !x.IsDelete && x.Id == z.GenderId).Title;
                z.StaffTypeName = _unitOfWork.Repository<StaffType>().AsNoTracking().FirstOrDefault(x => !x.IsDelete && x.Id == z.StaffTypeId).Title;
            });

            result.ListData = res;
            return result;
        }
        public async Task<ResponseList> GetAllStaffCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Staff>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Code + " - " + z.FullName }).ToList();
            return result;
        }

        public async Task<Staff> GetByCodeAsync(string code)
        {
            Staff Staff = null;
            Staff = _unitOfWork.Repository<Staff>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Staff;
        }

        public async Task<StaffModel> GetByIdAsync(int id)
        {
            StaffModel StaffModel = null;
            Staff Staff = null;
            Staff = _unitOfWork.Repository<Staff>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            StaffModel = _mapper.Map<StaffModel>(Staff);
            StaffModel.Departments = _unitOfWork.Repository<Staff_Department>()
            .Where(x => x.StaffId == StaffModel.Id).AsNoTracking().Select(z => z.DepartmentId).ToList();
            return StaffModel;
        }
        public async Task<Account> CheckUserName(string userName)
        {
            Account account = null;
            account = _unitOfWork.Repository<Account>()
            .Where(x => x.UserName == userName).AsNoTracking().FirstOrDefault();
            return account;
        }
        public async Task<Staff> GetStafByAccountId(int id)
        {
            Staff staff = null;
            staff = _unitOfWork.Repository<Staff>()
            .Where(x => x.AccountId == id).AsNoTracking().FirstOrDefault();
            return staff;
        }
        public async Task<StaffModel> AddAsync(StaffModel staff)
        {
            // Create Account
            await CreateAccountStaffAsync(staff);
            var resInfo = JsonConvert.SerializeObject(staff, Formatting.Indented);
            _logger.WriteInfoLog($"AddStaff --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Staff>().AddAsync(staff);
            await _unitOfWork.SaveChangesAsync();
            // Insert Staff_Department
            List<Staff_Department> staff_Departments = new List<Staff_Department>();
            foreach (var item in staff.Departments)
            {
                var staff_Department = new Staff_Department();
                staff_Department.DepartmentId = item;
                staff_Department.StaffId = staff.Id;
                staff_Department.CreateBy = staff.CreateBy;
                staff_Department.CreateOn = staff.CreateOn;
                staff_Departments.Add(staff_Department);
            }
            await _unitOfWork.Repository<Staff_Department>().AddRangeAsync(staff_Departments);
            await _unitOfWork.SaveChangesAsync();
            return staff;
        }

        public async Task<bool> UpdateAsync(StaffModel staff)
        {
            try
            {
                bool result = false;
                var tmp = _unitOfWork.Repository<Staff>().Where(x => x.Id == staff.Id).AsNoTracking().FirstOrDefault();
                if (tmp != null)
                {
                    tmp.FullName = staff.FullName;
                    tmp.Birthday = staff.Birthday;
                    tmp.GenderId = staff.GenderId;
                    tmp.Phone = staff.Phone;
                    tmp.Avatar = staff.Avatar;
                    tmp.Thumb = staff.Thumb;
                    tmp.Address = staff.Address;
                    tmp.Description = staff.Description;
                    tmp.Skype = staff.Skype;
                    tmp.Twitter = staff.Twitter;
                    tmp.Facebook = staff.Facebook;
                    tmp.Viber = staff.Viber;
                    tmp.Zalo = staff.Zalo;
                    tmp.Status = staff.Status;
                    tmp.StaffTypeId = staff.StaffTypeId;
                    tmp.RegencyId = staff.RegencyId;
                    tmp.ManagerId = staff.ManagerId;
                    tmp.ModifiedBy = staff.ModifiedBy;
                    tmp.ModifiedOn = staff.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateStaff --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Staff>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    ////Delete và Insert lại
                    var resDelete = await DeleteDepartmentAsync(staff.Id);
                    // Insert Contact
                    if (resDelete)
                    {
                        // Insert Staff_Department
                        List<Staff_Department> staff_Departments = new List<Staff_Department>();
                        foreach (var item in staff.Departments)
                        {
                            var staff_Department = new Staff_Department();
                            staff_Department.DepartmentId = item;
                            staff_Department.StaffId = staff.Id;
                            staff_Department.CreateBy = staff.CreateBy;
                            staff_Department.CreateOn = staff.CreateOn;
                            staff_Departments.Add(staff_Department);
                        }
                        await _unitOfWork.Repository<Staff_Department>().AddRangeAsync(staff_Departments);
                        await _unitOfWork.SaveChangesAsync();
                    }

                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Staff>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteStaff --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Staff>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        private async Task<bool> CreateAccountStaffAsync(StaffModel staff)
        {
            try
            {
                bool result = false;

                // Create Account 
                AccountModel account = new AccountModel();
                account.CreateBy = staff.CreateBy;
                account.CreateOn = staff.CreateOn;
                account.UserName = staff.UserName;
                account.Status = true;
                account.OrganizationId = staff.OrganizationId;
                account.Password = "P9PFP73h7WsfxQcXxjKSLA=="; // 1234567890
                _unitOfWork.Repository<Account>().Add(account);
                await _unitOfWork.SaveChangesAsync();
                staff.AccountId = account.Id;

                // Create Account_Organization
                Account_Organization account_Organization = new Account_Organization();
                account_Organization.AccountId = account.Id;
                account_Organization.OrganizationId = staff.OrganizationId;
                account_Organization.AccountTypeId = 2;
                account_Organization.Status = true;
                account_Organization.CreateBy = staff.CreateBy;
                account_Organization.CreateOn = staff.CreateOn;
                _unitOfWork.Repository<Account_Organization>().Add(account_Organization);
                await _unitOfWork.SaveChangesAsync();
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        private async Task<bool> DeleteDepartmentAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = _unitOfWork.Repository<Staff_Department>().Where(x => x.StaffId == id).AsNoTracking().ToList();
                if (tmp != null)
                {
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteStaff_Department --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Staff_Department>().RemoveRange(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
