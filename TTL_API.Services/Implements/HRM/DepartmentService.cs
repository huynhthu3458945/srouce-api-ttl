﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;
using TTL_API.Services.Extensions;

namespace TTL_API.Services.Implements
{
    public class DepartmentService : IDepartmentService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public DepartmentService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Department>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllDepartmentCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Department>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Position).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<Department> GetByCodeAsync(string code)
        {
            Department Department = null;
            Department = _unitOfWork.Repository<Department>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Department;
        }
        public async Task<Department> GetByIdAsync(int id)
        {
            Department Department = null;
            Department = _unitOfWork.Repository<Department>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return Department;
        }
        public async Task<Department> AddAsync(Department Department)
        {
            var resInfo = JsonConvert.SerializeObject(Department, Formatting.Indented);
            _logger.WriteInfoLog($"AddDepartment --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Department>().AddAsync(Department);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Department>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Department.Id = userMax == null ? 1 : userMax.Id;
            return Department;
        }

        public async Task<bool> UpdateAsync(Department Department)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Department>().Where(x => x.Id == Department.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = Department.Code;
                    tmp.Title = Department.Title;
                    tmp.Description = Department.Description;
                    tmp.Status = Department.Status;
                    tmp.ModifiedBy = Department.ModifiedBy;
                    tmp.ModifiedOn = Department.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateDepartment --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Department>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Department>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteDepartment --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Department>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
