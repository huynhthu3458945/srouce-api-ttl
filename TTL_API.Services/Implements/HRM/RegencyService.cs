﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;
using TTL_API.Services.Extensions;

namespace TTL_API.Services.Implements
{
    public class RegencyService : IRegencyService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public RegencyService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Regency>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllRegencyCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Regency>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Position).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<Regency> GetByCodeAsync(string code)
        {
            Regency Regency = null;
            Regency = _unitOfWork.Repository<Regency>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Regency;
        }
        public async Task<Regency> GetByIdAsync(int id)
        {
            Regency Regency = null;
            Regency = _unitOfWork.Repository<Regency>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return Regency;
        }
        public async Task<Regency> AddAsync(Regency Regency)
        {
            var resInfo = JsonConvert.SerializeObject(Regency, Formatting.Indented);
            _logger.WriteInfoLog($"AddRegency --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Regency>().AddAsync(Regency);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Regency>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Regency.Id = userMax == null ? 1 : userMax.Id;
            return Regency;
        }

        public async Task<bool> UpdateAsync(Regency Regency)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Regency>().Where(x => x.Id == Regency.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = Regency.Code;
                    tmp.Title = Regency.Title;
                    tmp.Description = Regency.Description;
                    tmp.Status = Regency.Status;
                    tmp.ModifiedBy = Regency.ModifiedBy;
                    tmp.ModifiedOn = Regency.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateRegency --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Regency>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Regency>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteRegency --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Regency>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
