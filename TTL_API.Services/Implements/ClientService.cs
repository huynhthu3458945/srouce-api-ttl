﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class ClientService : IClientService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public ClientService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Client>().AsNoTracking().Where(z => !z.IsDelete);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderBy(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllComboxAsync()
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Client>().AsNoTracking();
            var totalRows = query.Count();
            result.ListData = query.Where(z => !z.IsDelete && z.Status).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<Client> GetByIdAsync(int id)
        {
            Client client = null;
            client = _unitOfWork.Repository<Client>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return client;
        }
        public async Task<Client> AddAsync(Client client)
        {
            //    var userMax = _unitOfWork.Repository<Client>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            //    client.Id = userMax == null ? 1 : userMax.Id + 1;
            var resInfo = JsonConvert.SerializeObject(client, Formatting.Indented);
            _logger.WriteInfoLog($"AddClient --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Client>().AddAsync(client);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Client>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            client.Id = userMax == null ? 1 : userMax.Id;
            return client;
        }

        public async Task<bool> UpdateAsync(Client client)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Client>().Where(x => x.Id == client.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = client.Code;
                    tmp.Title = client.Title;
                    tmp.Description = client.Description;
                    tmp.Address = client.Address;
                    tmp.Phone = client.Phone;
                    tmp.Email = client.Email;
                    tmp.Status = client.Status;
                    tmp.ModifiedBy = client.ModifiedBy;
                    tmp.ModifiedOn = client.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateClient --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Client>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Client>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteClient --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Client>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
