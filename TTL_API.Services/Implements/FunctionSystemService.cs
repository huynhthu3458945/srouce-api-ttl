﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Dapper;

namespace TTL_API.Services.Implements
{
    public class FunctionSystemService : IFunctionSystemService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private IEnumerable<ResponseCombo> functionSystemTreeList;
        private string resTes = string.Empty;
        public FunctionSystemService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IConfiguration configuration)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            Configuration = configuration;
        }
        public async Task<ResponseList> GetAllAsync(FunctionSystemFitter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<FunctionSystem>().AsNoTracking().Where(z => !z.IsDelete
             && (string.IsNullOrEmpty(fitter.Title) || (!string.IsNullOrEmpty(fitter.Title) && z.Title.ToLower().Contains(fitter.Title.ToLower())))
              && (fitter.ParentId == 0 || (fitter.ParentId > 0 && z.ParentId == fitter.ParentId)));
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderBy(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetMenuAsync(string applicationCode, int organizationId, string userName)
        {
            var result = new ResponseList();

            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GET");
                paramaters.Add("@OrganizationId", organizationId);
                paramaters.Add("@ApplicationCode", applicationCode);
                paramaters.Add("@UserName", userName);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<FunctionSystem>("SP_Menu", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
                var retCode = paramaters.Get<Int32>("@RetCode");
                var retText = paramaters.Get<string>("@RetText");
                result.ListData = res.ToList();
            }
            
            return result;
        }
        public async Task<ResponseList> GetRuleActionAsync(string applicationCode, int organizationId, string userName, string area,string controller, string action)
        {
            var result = new ResponseList();

            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GET.RULE");
                paramaters.Add("@OrganizationId", organizationId);
                paramaters.Add("@ApplicationCode", applicationCode);
                paramaters.Add("@UserName", userName);
                paramaters.Add("@Area", area);
                paramaters.Add("@Controller", controller);
                paramaters.Add("@ActionRule", action);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<FunctionSystem>("SP_Menu", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
                var retCode = paramaters.Get<Int32>("@RetCode");
                var retText = paramaters.Get<string>("@RetText");
                result.ListData = res.ToList();
            }

            return result;
        }
        public async Task<ResponseList> GetRuleActionWareHouseAsync(string applicationCode, int organizationId, string userName, string area, string controller)
        {
            var result = new ResponseList();

            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GET.RULE.WAREHOUSE");
                paramaters.Add("@OrganizationId", organizationId);
                paramaters.Add("@ApplicationCode", applicationCode);
                paramaters.Add("@UserName", userName);
                paramaters.Add("@Area", area);
                paramaters.Add("@Controller", controller);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query    <FunctionSystem>("SP_Menu", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
                var retCode = paramaters.Get<Int32>("@RetCode");
                var retText = paramaters.Get<string>("@RetText");
                result.ListData = res.ToList();
            }

            return result;
        }

        public async Task<ResponseList> GetAllTreeAsync(int id)
        {
            var result = new ResponseList();
            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Id", id);
                functionSystemTreeList = conn.Query<ResponseCombo>("SP_FunctionSystem_Tree", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
            }
            result.ListData = functionSystemTreeList.ToList();
            return result;
        }
         
        public async Task<FunctionSystem> GetByIdAsync(int id)
        {
            FunctionSystem functionSystem = null;
            functionSystem = _unitOfWork.Repository<FunctionSystem>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return functionSystem;
        }
        public async Task<FunctionSystem> AddAsync(FunctionSystem functionSystem)
        {
            functionSystem.LinkName = GetLink(functionSystem.Title ,functionSystem.ParentId);
            var resInfo = JsonConvert.SerializeObject(functionSystem, Formatting.Indented);
            _logger.WriteInfoLog($"AddFunctionSystem --> resInfo: {resInfo}");
            await _unitOfWork.Repository<FunctionSystem>().AddAsync(functionSystem);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<FunctionSystem>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            functionSystem.Id = userMax == null ? 1 : userMax.Id;
            return functionSystem;
        }

        public async Task<bool> UpdateAsync(FunctionSystem functionSystem)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<FunctionSystem>().Where(x => x.Id == functionSystem.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.LinkName = GetLink(functionSystem.Title, functionSystem.ParentId);
                    tmp.Title = functionSystem.Title;
                    tmp.Description = functionSystem.Description;
                    tmp.Area = functionSystem.Area;
                    tmp.Controller = functionSystem.Controller;
                    tmp.Action = functionSystem.Action;
                    tmp.Note = functionSystem.Note;
                    tmp.Position = functionSystem.Position;
                    tmp.IsShow = functionSystem.IsShow;
                    tmp.Status = functionSystem.Status;
                    tmp.ApplicationId = functionSystem.ApplicationId;
                    tmp.ParentId = functionSystem.ParentId;
                    tmp.Icon = functionSystem.Icon;
                    tmp.ModifiedBy = functionSystem.ModifiedBy;
                    tmp.ModifiedOn = functionSystem.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateFunctionSystem --> resInfo: {resInfo}");
                    _unitOfWork.Repository<FunctionSystem>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<FunctionSystem>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteFunctionSystem --> resInfo: {resInfo}");
                    _unitOfWork.Repository<FunctionSystem>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        private string GetLink(string name, int parentid)
        {
            var links = new List<string>();
            FunctionSystem parentInfo = null;
            var linkStr = new StringBuilder();
            links.Add(name);
            if (parentid != 0)
            {
                do
                {
                    parentInfo = _unitOfWork.Repository<FunctionSystem>()
                        .Where(x => x.Id == parentid).AsNoTracking().FirstOrDefault();
                    links.Add(parentInfo.Title);
                    parentid = parentInfo.ParentId;
                } while (parentid != 0);
            }
            for (int i = links.Count(); i > 0; i--)
            {
                if ((i - 1) == 0)
                    linkStr.Append(links[i - 1]);
                else
                    linkStr.Append(links[i - 1] + " > ");
            }
            return linkStr.ToString();
        }
        public IEnumerable<FunctionSystem> GetAll(int applicationId)
        {
            var query = _unitOfWork.Repository<FunctionSystem>().Where(z => !z.IsDelete && z.Status
            && (applicationId == 0 || (applicationId > 0 && applicationId == z.ApplicationId))).ToList();
            return query;
        }

        public List<FunctionSystemModelTree> GetChildren(List<FunctionSystemModelTree> comments, int parentId, List<Function_Organization> function_OrganizationsList)
        {
            return comments
                    .Where(c => c.ParentId == parentId)
                    .Select(c => new FunctionSystemModelTree
                    {
                        Id = c.Id,
                        Text = c.Text,
                        ParentId = c.ParentId,
                        State = new State() { Selected = function_OrganizationsList.FirstOrDefault(z => z.FunctionSystemId == c.Id) != null && !CheckIsChildren(c.Id, comments) },
                        Children = GetChildren(comments, c.Id, function_OrganizationsList)
                    })
                    .ToList();
        }

        public bool CheckIsChildren(int id, List<FunctionSystemModelTree> list)
        {
            var res = list
                    .Where(c => c.ParentId == id)
                    .ToList();
            if (res != null && res.Count > 0)
                return true;
            else
                return false;
        }
    }
}
