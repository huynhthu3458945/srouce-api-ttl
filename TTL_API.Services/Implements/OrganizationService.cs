﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Dapper;
using AutoMapper;

namespace TTL_API.Services.Implements
{
    public class OrganizationService : IOrganizationService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IFunctionSystemService _functionSystemService;
        private readonly IFunction_OrganizationService _function_OrganizationService;
        private readonly IRole_FunctionService _role_FunctionService;
        private readonly IAccountService _accountService;
        private readonly IAccount_RoleService _account_RoleService;
        private readonly IRoleService _roleService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public OrganizationService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IConfiguration configuration
            , IMapper mapper
            , IFunctionSystemService functionSystemService
            , IRole_FunctionService role_FunctionService
            , IAccountService accountService
            , IAccount_RoleService account_RoleService
            , IRoleService roleService
            , IFunction_OrganizationService function_OrganizationService)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            Configuration = configuration;
            _mapper = mapper;
            _functionSystemService = functionSystemService;
            _role_FunctionService = role_FunctionService;
            _accountService = accountService;
            _account_RoleService = account_RoleService;
            _roleService = roleService;
            _function_OrganizationService = function_OrganizationService;
        }
        public async Task<ResponseList> GetAllAsync(int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Organization>().AsNoTracking();
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.Where(z => !z.IsDelete).OrderBy(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<Organization> GetByCodeAsync(string code)
        {
            Organization result = new Organization();
            var query = _unitOfWork.Repository<Organization>().AsNoTracking();
            result = query.Where(z => !z.IsDelete).OrderBy(x => x.Id).FirstOrDefault(z => z.Code == code);
            return result;
        }
        public async Task<Organization> GetByUserNameAsync(string userName)
        {
            Organization result = new Organization();
            var account = await _unitOfWork.Repository<Account>().AsNoTracking().FirstOrDefaultAsync(z => !z.IsDelete && z.UserName == userName);
            var accountRole = await _unitOfWork.Repository<Account_Role>().AsNoTracking().FirstOrDefaultAsync(z => z.AccountId == account.Id);
            var role = await _unitOfWork.Repository<Role>().AsNoTracking().FirstOrDefaultAsync(z => z.Id == accountRole.RoleId);
            result = await _unitOfWork.Repository<Organization>().AsNoTracking().FirstOrDefaultAsync(z => !z.IsDelete && z.Id == role.OrganizationId);
            return result;
        }

        public async Task<ResponseList> GetAllComboboxAsync()
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Organization>().AsNoTracking();
            var listCombox =
             query.Where(z => !z.IsDelete).Select(z => new ResponseCombo()
             {
                 Id = z.Id,
                 Value = z.Id.ToString(),
                 Text = z.Title
             }).OrderBy(x => x.Id).ToList();
            result.ListData = listCombox;
            return result;
        }

        public async Task<Organization> GetByIdAsync(int id)
        {
            Organization Organization = null;
            Organization = _unitOfWork.Repository<Organization>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return Organization;
        }
        public async Task<Organization> AddAsync(OrganizationModel organization)
        {
            var resInfo = JsonConvert.SerializeObject(organization, Formatting.Indented);
            _logger.WriteInfoLog($"AddOrganization --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Organization>().AddAsync(organization);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Organization>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            organization.Id = userMax == null ? 1 : userMax.Id;

            // Inser FunctionSystem
            var listFunctionSystem = _functionSystemService.GetAll(0).AsEnumerable().Where(z => organization.TreeFuncSystems.Where(x => x.IsCheck).Select(x => x.Id).Contains(z.Id));
            // inser phân quyền
            var listFunc_Organtion = _mapper.Map<IEnumerable<Function_Organization>>(listFunctionSystem);

            listFunc_Organtion.ToList().ForEach(z => z.OrganizationId = organization.Id);
            var resInsertFuncSystem = await _function_OrganizationService.AddAsync(listFunc_Organtion);

            if (resInsertFuncSystem)
            {
                var mainConnectString = Configuration["MainConnectionString"];
                using (var conn = new SqlConnection(mainConnectString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        conn.Open();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "Add");
                    paramaters.Add("@Id", organization.Id);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var res = conn.Query<string>("SP_AddOrganization_Account_Function", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
                    var retCode = paramaters.Get<Int32>("@RetCode");
                    var retText = paramaters.Get<string>("@RetText");

                }
            }

            return organization;
        }

        public async Task<bool> UpdateAsync(OrganizationModel organization)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Organization>().Where(x => x.Id == organization.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = organization.Code;
                    tmp.Title = organization.Title;
                    tmp.Phone = organization.Phone;
                    tmp.Address = organization.Address;
                    tmp.Phone = organization.Phone;
                    tmp.Email = organization.Email;
                    tmp.Fax = organization.Fax;
                    tmp.Icon = organization.Icon;
                    tmp.Logo = organization.Logo;
                    tmp.ClientId = organization.ClientId;
                    tmp.ModifiedBy = organization.ModifiedBy;
                    tmp.ModifiedOn = organization.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateOrganization --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Organization>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    //Delete và Insert lại
                    var account = await _accountService.GetAccountUserNameAsync(organization.OwnerName);
                    var account_Role = await _account_RoleService.GetAllByAccountIdAsync(account.Id);
                    //var roles = _roleService.GetByOrganizationAsync(organization.Id);

                    var listFunc_OrgantionNotCheck = _unitOfWork.Repository<Function_Organization>().AsEnumerable().Where(z => organization.TreeFuncSystems.Where(x => !x.IsCheck).Select(x => x.Id).Contains(z.FunctionSystemId)).ToList();

                    //delete Role_Function
                    //var role_Role_FucNotCheck = _unitOfWork.Repository<Role_Function>().AsEnumerable().Where(z => listFunc_OrgantionNotCheck.Select(x => x.Id).Contains(z.FunctionId) && roles.Select(e=>e.Id).Contains(z.RoleId)).ToList();
                    //_unitOfWork.Repository<Role_Function>().RemoveRange(role_Role_FucNotCheck);
                    //await _unitOfWork.SaveChangesAsync();
                    //delete Function_Organization
                    var deleteFunc_Org = await _function_OrganizationService.UpdateIsDeleteAsync(listFunc_OrgantionNotCheck, true);
                    // insert lại
                    var listFunctionSystem = _functionSystemService.GetAll(0).AsEnumerable().Where(z => organization.TreeFuncSystems.Where(x => x.IsCheck).Select(x => x.Id).Contains(z.Id)).ToList();
                    // inser listFunc_Organtion
                    var listFunc_OrgantionCheck = _mapper.Map<IEnumerable<Function_Organization>>(listFunctionSystem);
                    var listFunc_OrgantionAll = _unitOfWork.Repository<Function_Organization>().AsEnumerable().Where(z=> z.OrganizationId == organization.Id).ToList();
                    var listFunc_OrgantionNotDelete = _unitOfWork.Repository<Function_Organization>().AsEnumerable().Where(z => z.OrganizationId == organization.Id && !z.IsDelete).ToList();
                    // trường hợp chưa có
                    var notExit = listFunc_OrgantionCheck.Where(z => !listFunc_OrgantionAll.Select(x => x.FunctionSystemId).Contains(z.FunctionSystemId)).Count();
                    if (notExit > 0)
                    {
                        listFunc_OrgantionCheck = listFunc_OrgantionCheck.AsEnumerable().Where(z => !listFunc_OrgantionNotDelete.Select(x => x.FunctionSystemId).Contains(z.FunctionSystemId)).ToList();
                        listFunc_OrgantionCheck.ToList().ForEach(z => z.OrganizationId = organization.Id);
                        var resInsertFuncOrg = await _function_OrganizationService.AddAsync(listFunc_OrgantionCheck);
                        // insert Role_Function
                        var role_Role_FucCheck = listFunc_OrgantionCheck.Select(z => new Role_Function() { RoleId = account_Role.FirstOrDefault().RoleId, FunctionId = z.Id, CreateBy = z.CreateBy, CreateOn = z.CreateOn });
                        await _unitOfWork.Repository<Role_Function>().AddRangeAsync(role_Role_FucCheck);
                        await _unitOfWork.SaveChangesAsync();
                    }
                    else 
                    {

                        listFunc_OrgantionAll = listFunc_OrgantionAll.AsEnumerable().Where(z => listFunc_OrgantionCheck.Select(x => x.FunctionSystemId).Contains(z.FunctionSystemId)).ToList();
                        var resInsertFuncOrg = await _function_OrganizationService.UpdateIsDeleteAsync(listFunc_OrgantionAll.ToList(), false);
                    }

                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Organization>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteOrganization --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Organization>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
