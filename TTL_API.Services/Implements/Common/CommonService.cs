﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;
using System.Data.SqlClient;
using Dapper;
using TTL_API.DbContext.Models.StoredProcedure;
using Microsoft.Extensions.Configuration;

namespace TTL_API.Services.Implements
{
    public class CommonService : ICommonService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IRoleService _roleService;
        public CommonService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper, IRoleService roleService, IConfiguration configuration)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _roleService = roleService;
            Configuration = configuration;
        }
        public async Task<ResponseList> GetGenderAsync()
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Gender>().AsNoTracking().Where(z => !z.IsDelete && z.Status);
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<ResponseList> GetOrganizationByUserName(string userName)
        {
            var result = new ResponseList();
            var account = _unitOfWork.Repository<Account>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.UserName == userName).FirstOrDefault();
            var query = _unitOfWork.Repository<Account_Organization>().AsNoTracking().Where(z => z.AccountId == account.Id).Join(
                _unitOfWork.Repository<Organization>().AsNoTracking(),
                p => p.OrganizationId,
                c => c.Id,
                (p, c) => c);
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllOrganizationByUserName(string userName, int limit, int page)
        {
            var result = new ResponseList();
            var account = _unitOfWork.Repository<Account>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.UserName == userName).FirstOrDefault();
            var query = _unitOfWork.Repository<Account_Organization>().AsNoTracking().Where(z => z.AccountId == account.Id).Join(
                _unitOfWork.Repository<Organization>().AsNoTracking(),
                p => p.OrganizationId,
                c => c.Id,
                (p, c) => c).ToList();
           
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            var res = _mapper.Map<IEnumerable<OrganizationModel>>(query);
            res.ToList().ForEach(z => {

                z.Roles = _unitOfWork.Repository<Role>().AsNoTracking().Where(x=> x.OrganizationId == z.Id).Join(
                            _unitOfWork.Repository<Account_Role>().AsNoTracking().Where(z=>z.AccountId == account.Id),
                            p => p.Id,
                            c => c.RoleId,
                            (p, c) => p).ToList();
            });
            result.ListData = res.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }

        public async Task<ResponseList> GetAllPalletByStoreId(int storeId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Pallet>().AsNoTracking().Where(z => !z.IsDelete && z.StoreId == storeId);
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllBillStatus(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<BillStatus>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllOrderStatus(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<OrderStatus>().AsNoTracking().Where(z => !z.IsDelete && z.Status);
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }

        public async Task<ResponseList> GetAllTransport(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Transport>().AsNoTracking().Where(z => !z.IsDelete && z.Status);
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }

        public async Task<ResponseList> GetAllPayType(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<PayType>().AsNoTracking().Where(z => !z.IsDelete && z.Status);
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllProductNumber(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<ProductNumber>().AsNoTracking().Where(z => !z.IsDelete);
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<BillStatus> GetAllBillStatusByCode(string code)
        {
            BillStatus BillStatus = null;
            BillStatus = _unitOfWork.Repository<BillStatus>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return BillStatus;
        }
        public async Task<OrderStatus> GetAllOrderStatusByCode(string code)
        {
            OrderStatus OrderStatus = null;
            OrderStatus = _unitOfWork.Repository<OrderStatus>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return OrderStatus;
        }
        public async Task<PayType> GetAllPayTypeByCode(string code)
        {
            PayType PayType = null;
            PayType = _unitOfWork.Repository<PayType>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return PayType;
        }
        public async Task<ResponseList> GetAllConfigPayment()
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<ConfigPayment>().AsNoTracking().Where(z => z.Status);
            result.ListData = query.ToList();
            return result;
        }  
        public async Task<ResponseList> GetAllDiscountKind()
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<DiscountKind>().AsNoTracking().Where(z => z.Status);
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList(); ;
            return result;
        }
        public async Task<ResponseList> GetAllProductQuantityEnd(Product_StoreProcedureFitter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GET.ALL.PRODUCT");
                paramaters.Add("@OrganizationId", fitter.OrganizationId);
                paramaters.Add("@ProductCategoryId", fitter.ProductCategoryId);
                paramaters.Add("@ProductTypeId", fitter.ProductTypeId);
                paramaters.Add("@StoreId", fitter.StoreId);
                paramaters.Add("@Code", fitter.Code);
                paramaters.Add("@Title", fitter.Title);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<Product_StoreProcedure>("SP_COMMON", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
                var retCode = paramaters.Get<Int32>("@RetCode");
                var retText = paramaters.Get<string>("@RetText");

                var totalRows = res.Count();
                result.Paging = new Paging(totalRows, page, limit);
                int start = result.Paging.start;
                result.ListData = res.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            }
            return result;
        }
    }
}
