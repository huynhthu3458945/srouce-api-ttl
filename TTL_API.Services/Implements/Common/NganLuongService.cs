﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Extensions;
using TTL_API.Services.Extensions.NganLuongPayment;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using static TTL_API.DbContext.Models.Utils.NganLuongModels;

namespace TTL_API.Services.Implements
{
    public class NganLuongService : INganLuongService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IConfiguration _config;
        public NganLuongService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IConfiguration config)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _config = config;
        }
        public async Task<MessageResNganLuong> CheckPaymentNganLuongV2(string orderCode, string culture)
        {
            var messageResNganLuong = new MessageResNganLuong();
            var url = _config["NganLuong:CheckPaymentURL"];
            var merchantId = _config["NganLuong:MerchantId"];
            var sercurePass = _config["NganLuong:MerchantPassword"];
            var nganLuong = new ApiNganLuongService();
            var result = await nganLuong.CheckCashoutListV2WithRestSharp(orderCode, sercurePass, merchantId, url);
            if (result.Error_code == "00")
            {
                if (result.Transaction_Status == 4)
                {
                    messageResNganLuong.RetCode = 0;
                    messageResNganLuong.RetText = culture == "en-US" ? "Was successful" : "Đã thành công";
                }
                else if (result.Transaction_Status == 2 || result.Transaction_Status == 1)
                {
                    messageResNganLuong.RetCode = 1;
                    messageResNganLuong.RetText = culture == "en-US" ? "Pending" : "Đang chờ xử lý";
                }

                else
                {
                    messageResNganLuong.RetCode = 2;
                    messageResNganLuong.RetText = culture == "en-US" ? "Unsuccessful" : "Không thành công";
                }
                messageResNganLuong.Data = result;
            }
            else
            {
                messageResNganLuong.RetCode = 4;
                messageResNganLuong.RetText = nganLuong.GetErrorMessage(result.Error_code, culture);
                messageResNganLuong.Data = null;
            }
            return new MessageResNganLuong
            {
                Id = 1,
                Message = "Success"
            };
        }
        public async Task<MessageResNganLuong> CheckBalance()
        {
            var messageResNganLuong = new MessageResNganLuong();
            var request = new GetBalanceRequest();
            var nganLuong = new ApiNganLuongService();
            request.merchant_id = _config["NganLuong:MerchantId"];
            request.merchant_password = _config["NganLuong:MerchantPassword"];
            request.merchant_email = _config["NganLuong:ReceiverEmail"];
            var url = _config["NganLuong:CheckBalance"];
            var result = await nganLuong.CheckBalanceRestSharp(request, url);
            if (result.error_code == "00")
            {
                messageResNganLuong.RetCode = 0;
                //messageResNganLuong.Balance = result.data;
                messageResNganLuong.RetText = "Vấn tin thành công";
            }
            else
            {
                messageResNganLuong.RetCode = 4;
                //messageResNganLuong.Balance = result.data;
                messageResNganLuong.RetText = result.error_message;
            }
            return messageResNganLuong;
            //throw new NotImplementedException();
        }

        //Kiểm trạng tình trạng rút tiền
        public async Task<MessageResNganLuong> CheckCashoutList(NganLuongModels.CheckWithdrawalStatus value, string culture)
        {
            var messageResNganLuong = new MessageResNganLuong();
            var nganLuong = new ApiNganLuongService();
            var request = new CheckWithdrawalStatusRequestNew();
            request.Merchant_id = _config["NganLuong:MerchantId"];
            request.Merchant_password = _config["NganLuong:MerchantPassword"];
            request.Receiver_email = _config["NganLuong:ReceiverEmail"];
            request.RefCode = value.RefCode;
            //request.TransactionId = value.TransactionId;
            //request.TransactionId = value.TransactionId;
            var url = _config["NganLuong:WithdrawURL"];
            var result = await nganLuong.CheckCashoutListWithRestSharp(request, url);
            if (result.Error_code == "00")
            {
                if (result.Transaction_Status == 0)
                {
                    messageResNganLuong.RetCode = 0;
                    messageResNganLuong.RetText = culture == "en-US" ? "Was successful" : "Đã thành công";
                }
                else if (result.Transaction_Status == 1)
                {
                    messageResNganLuong.RetCode = 1;
                    messageResNganLuong.RetText = culture == "en-US" ? "Pending" : "Đang chờ xử lý";
                }
                else if (result.Transaction_Status == 3)
                {
                    messageResNganLuong.RetCode = 3;
                    messageResNganLuong.RetText = culture == "en-US" ? "The transaction has been refunded" : "Giao dịch đã hoàn trả";
                }
                else
                {
                    messageResNganLuong.RetCode = 2;
                    messageResNganLuong.RetText = culture == "en-US" ? "Unsuccessful" : "Không thành công";
                }
                messageResNganLuong.Data = result;
            }
            else
            {
                messageResNganLuong.RetCode = 4;
                messageResNganLuong.RetText = nganLuong.GetErrorMessage(result.Error_code, culture);
                messageResNganLuong.Data = null;
            }
            return messageResNganLuong;
        }

        //Kiểm tra tình trạng thanh toán
        public async Task<MessageResNganLuong> CheckPaymentNganLuong(CheckPaymentRequest value, string culture)
        {
            var messageRes = new MessageResNganLuong();
            var nganLuong = new ApiNganLuongThanhToan();
            string TokenKey = _config["NganLuong:TokenKey"];
            string CheckSum = _config["NganLuong:ChecksumKey"];
            string url = _config["PaumentURL"] + "/get-transaction-info";
            var data = await nganLuong.CheckPayment(value, TokenKey, CheckSum, url);
            if (data == null)
            {
                messageRes.RetCode = 2;
                messageRes.RetText = culture == "en-US" ? "Payment check failed" : "Kiểm tra thanh toán không thành công";
                messageRes.Data = null;
            }
            else
            {
                if (data.code == "000")
                {
                    if (data.status == "000")
                    {
                        messageRes.RetCode = int.Parse(data.status);
                    }
                    else
                    {
                        messageRes.RetCode = int.Parse(data.status);
                    }
                    messageRes.RetText = data.reason;
                    //else if(data.status )
                    //Thành công

                    messageRes.DataCheckPaymentResponse = data;
                }
                else
                {
                    messageRes.RetCode = 1;
                    messageRes.RetText = data.message;
                    messageRes.DataCheckPaymentResponse = null;
                }
            }
            return messageRes;
        }
        //Thanh toán
        public async Task<PaymentResponse> PaymentNganLuong(NganLuongModels.PaymentRequest value, string culture)
        {
            var apiThanhToan = new ApiNganLuongThanhToan();
            var tokenKey = _config["NganLuong:TokenKey"];
            var checkSum = _config["NganLuong:ChecksumKey"];

            var result = await apiThanhToan.Payment(value, tokenKey, checkSum, _config);
            return result ?? null;
        }

        public async Task<MessageResNganLuong> SetCashoutRequest(NganLuongModels.WithdrawalMoney value, string culture)
        {
            var messageResNganLuong = new MessageResNganLuong();
            var nganLuong = new ApiNganLuongService();
            var request = new WithdrawMoneyRequest();
            request.Merchant_id = _config["NganLuong:MerchantId"]; ;
            request.Merchant_password = _config["NganLuong:MerchantPassword"];
            request.Receiver_email = _config["NganLuong:ReceiverEmail"];
            request.RefCode = value.RefCode;
            request.Total_amount = value.TotalAmount;
            request.bank_code = value.BankCode;
            request.AccountType = 3;
            //Chỉ có agribank là accountype loại 2
            if (value.BankCode == "AGB")
            {
                request.AccountType = 2;
            }
            request.Card_fullname = value.CardFullName;
            request.CardNumber = value.CardNumber;
            //request.CardMonth = value.CardMonth;
            //request.CardYear = value.CardYear;
            request.BranchName = value.BranchName;
            request.ZoneId = value.ZoneId;
            request.Reason = value.Reason;
            var url = _config["NganLuong:WithdrawURL"];
            //WithdrawMoneyResponse result = nganLuong.SetCashoutRequest(request, "NL");
            var result = await nganLuong.SetCashoutRequestWithRestSharp(request, url, "NL");
            if (result.Error_code == "00")
            {
                if (result.Transaction_Status == "00")
                {
                    messageResNganLuong.RetCode = 0;
                    messageResNganLuong.RetText = culture == "en-US" ? "Was successful" : "Đã thành công";
                    //messageResNganLuong.Data = result;
                }
                else if (result.Transaction_Status == "01")
                {
                    messageResNganLuong.RetCode = 1;
                    messageResNganLuong.RetText = culture == "en-US" ? "Pending" : "Đang chờ xử lý";
                    //messageResNganLuong.Data = result;
                }
                else if (result.Transaction_Status == "03")
                {
                    messageResNganLuong.RetCode = 3;
                    messageResNganLuong.RetText = culture == "en-US" ? "The transaction has been refunded" : "Giao dịch đã hoàn trả";
                    //messageResNganLuong.Data = result;
                }
                else
                {
                    messageResNganLuong.RetCode = 2;
                    messageResNganLuong.RetText = culture == "en-US" ? "Unsuccessful" : "Không thành công";
                }

                messageResNganLuong.Data = result;
            }
            else
            {
                messageResNganLuong.RetCode = 4;
                messageResNganLuong.RetText = nganLuong.GetErrorMessage(result.Error_code, culture);
                messageResNganLuong.Data = null;
            }
            return messageResNganLuong;
        }
    }
}
