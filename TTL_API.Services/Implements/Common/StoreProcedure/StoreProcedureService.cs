﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;
using TTL_API.Services.Extensions;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using Dapper;
using TTL_API.DbContext.Models.StoredProcedure;

namespace TTL_API.Services.Implements
{
    public class StoreProcedureService : IStoreProcedureService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public StoreProcedureService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper, IConfiguration configuration)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            Configuration = configuration;
        }
        public async Task<ResponseList> GetImportExportListAsync(ImportExportListFillter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GET");
                paramaters.Add("@OrganizationId", fitter.OrganizationId);
                paramaters.Add("@StoreId", fitter.StoreId);
                paramaters.Add("@ProductId", fitter.ProductId);
                paramaters.Add("@BillType", fitter.BillType);
                paramaters.Add("@FromDate", fitter.FromDate);
                paramaters.Add("@ToDate", fitter.ToDate);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<ImportExportList>("SP_IMPORT_EXPORT", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
                var retCode = paramaters.Get<Int32>("@RetCode");
                var retText = paramaters.Get<string>("@RetText");

                var totalRows = res.Count();
                result.Paging = new Paging(totalRows, page, limit);
                int start = result.Paging.start;
                if (fitter.IsDownLoad)
                {
                    result.ListData = res.ToList();
                }
                else
                {
                    result.ListData = res.OrderByDescending(x => x.Code).Skip(start).Take(limit).ToList();
                }
            }
            return result;
        }
        public async Task<ResponseList> GetSyntheticWarehouse(SyntheticWarehouseFillter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GET");
                paramaters.Add("@OrganizationId", fitter.OrganizationId);
                paramaters.Add("@StoreId", fitter.StoreId);
                paramaters.Add("@ProductId", fitter.ProductId);
                paramaters.Add("@FromDate", fitter.FromDate);
                paramaters.Add("@ToDate", fitter.ToDate);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<SyntheticWarehouse>("SP_SYNTHETIC_WAREHOUSE", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
                var retCode = paramaters.Get<Int32>("@RetCode");
                var retText = paramaters.Get<string>("@RetText");

                var totalRows = res.Count();
                result.Paging = new Paging(totalRows, page, limit);
                int start = result.Paging.start;
                if (fitter.IsDownLoad)
                {
                    result.ListData = res.ToList();
                }
                else
                {
                    result.ListData = res.OrderByDescending(x => x.ProductCode).Skip(start).Take(limit).ToList();
                }
            }
            return result;
        }
    }
}
