﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class ProvinceService : IProvinceService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public ProvinceService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<ResponseList> GetAllAsync()
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Province>().AsNoTracking();
            result.ListData = query.ToList();
            return result;
        }
        public async Task<ResponseList> GetAllComboboxAsync()
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Province>().AsNoTracking();
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Name }).ToList();
            return result;
        }
        public async Task<Province> GetByIdAsync(int id)
        {
            var query = _unitOfWork.Repository<Province>().AsNoTracking();
            query = query.Where(z => z.Id == id);
            return query.FirstOrDefault();
        }
    }
}
