﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class DistrictService : IDistrictService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public DistrictService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<ResponseList> GetAllByProvinceIdAsync(int provinceId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<District>().AsNoTracking();
            result.ListData = query.Where(z=>z.ProvinceId == provinceId).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllComboboxByProvinceIdAsync(int provinceId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<District>().AsNoTracking().Where(z => z.ProvinceId == provinceId);
            result.ListData = query.Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Name }).ToList();
            return result;
        }
        public async Task<District> GetByIdAsync(int id)
        {
            var query = _unitOfWork.Repository<District>().AsNoTracking();
            query = query.Where(z => z.Id == id);
            return query.FirstOrDefault();
        }
    }
}
