﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using TTL_API.Services.Extensions;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using Dapper;

namespace TTL_API.Services.Implements
{
    public class CustomerService : ICustomerService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IContactService _contactService;
        private readonly IMapper _mapper;
        public IConfiguration Configuration { get; }
        public CustomerService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IContactService contactService, IMapper mapper, IConfiguration configuration)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _contactService = contactService;
            _mapper = mapper;
            Configuration = configuration;
        }
        //public async Task<ResponseList> GetAll(CustomerFitter fitter, int limit, int page)
        //{
        //    var result = new ResponseList();
        //    var mainConnectString = Configuration["MainConnectionString"];
        //    using (var conn = new SqlConnection(mainConnectString))
        //    {
        //        if (conn.State == System.Data.ConnectionState.Closed)
        //            conn.Open();
        //        var paramaters = new DynamicParameters();
        //        paramaters.Add("@Action", "GET.ALL");
        //        paramaters.Add("@OrganizationId", fitter.OrganizationId);
        //        paramaters.Add("@Phone", fitter.Phone);
        //        paramaters.Add("@FullName", fitter.FullName);
        //        paramaters.Add("@Email", fitter.Email);
        //        paramaters.Add("@PageNumber", page);
        //        paramaters.Add("@RowsOfPage", limit);
        //        paramaters.Add("@RetCount", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
        //        paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
        //        paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
        //        var res = conn.Query<CustomerModel>("SP_CUSTOMER", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
        //        var retCount = paramaters.Get<Int32>("@RetCount");
        //        var retCode = paramaters.Get<Int32>("@RetCode");
        //        var retText = paramaters.Get<string>("@RetText");
        //        var totalRows = retCount;
        //        res.ToList().ForEach(z =>
        //        {
        //            z.GenderName = _unitOfWork.Repository<Gender>().AsNoTracking().FirstOrDefault(x => !x.IsDelete && x.Id == z.GenderId).Title;
        //        });
        //        result.Paging = new Paging(totalRows, page, limit);
        //        result.ListData = res;
        //    }
        //    return result;
        //}
        public async Task<ResponseList> GetAll(CustomerFitter fitter, int limit, int page)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Customer>().AsNoTracking()
                .Where(z => !z.IsDelete
                && z.OrganizationId == fitter.OrganizationId
                && (z.CreateBy == fitter.CreateBy || fitter.CreateBy == 81)
                && (string.IsNullOrEmpty(fitter.Code) || (!string.IsNullOrEmpty(fitter.Code) && z.Code.ToLower().Contains(fitter.Code.ToLower())))
                && (string.IsNullOrEmpty(fitter.FullName) || (!string.IsNullOrEmpty(fitter.FullName) && z.FullName.ToLower().Contains(fitter.FullName.ToLower())))
                && (string.IsNullOrEmpty(fitter.Email) || (!string.IsNullOrEmpty(fitter.Email) && z.Email.ToLower().Contains(fitter.Email.ToLower())))
                && (string.IsNullOrEmpty(fitter.Phone) || (!string.IsNullOrEmpty(fitter.Phone) && z.Phone.ToLower().Contains(fitter.Phone.ToLower())))
                && (fitter.CustomerStatusId == 0 || (fitter.CustomerStatusId > 0 && z.CustomerStatusId == fitter.CustomerStatusId))
                && (fitter.CustomerTypeId == 0 || (fitter.CustomerTypeId > 0 && z.CustomerTypeId == fitter.CustomerTypeId))
                ).ToList();
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            var res = _mapper.Map<IEnumerable<CustomerModel>>(query).OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            res.ToList().ForEach(z =>
            {
                z.GenderName = _unitOfWork.Repository<Gender>().AsNoTracking().FirstOrDefault(x => !x.IsDelete && x.Id == z.GenderId).Title;
                z.Ref_Phone = GetAllByPhoneStore(68, z.Phone, string.Empty).Result.FirstOrDefault()?.Ref_Phone;
            });
            //var resTemp = _unitOfWork.Repository<CustomerTemp>().AsNoTracking()
            //   .Where(z => !z.IsDelete && (string.IsNullOrEmpty(fitter.Phone) || (!string.IsNullOrEmpty(fitter.Phone) && z.Phone.ToLower().Contains(fitter.Phone.ToLower())))
            //   ).ToList();
            //var res1 = _mapper.Map<IEnumerable<CustomerModel>>(resTemp);
            //res.ToList().AddRange(res1);
            result.ListData = res;

            //result.ListData = res.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllCustomerCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Customer>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId);
            var res = _mapper.Map<IEnumerable<CustomerModel>>(query).ToList();
            if (organizationId == 68) // TTL
            {
                //var resTemp = _unitOfWork.Repository<CustomerTemp>().AsNoTracking().ToList();
                //if (resTemp.Count > 1)
                //{
                //    resTemp = resTemp.Where(z => z.Ref_Source == "AGENCY").ToList();
                //}
                //var res1 = _mapper.Map<IEnumerable<CustomerModel>>(resTemp).ToList();
                //res.AddRange(res1);
            }
            result.ListData = res.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Code + " - " + z.FullName }).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllByPhoneAsync(int organizationId, string phone, string code)
        {
            var result = new ResponseList();
            var query = await _unitOfWork.Repository<Customer>().AsNoTracking().Where(z => !z.IsDelete
                && (organizationId == 0 || (organizationId > 0 && organizationId == z.OrganizationId))
                && ((!string.IsNullOrEmpty(phone) && z.Phone.ToLower().Contains(phone.ToLower())))
                || ((!string.IsNullOrEmpty(code) && z.Code.ToLower().Contains(code.ToLower())))
                ).ToListAsync();
            var res = _mapper.Map<IEnumerable<CustomerModel>>(query).ToList();
            res.ForEach(z =>
            {
                z.ContactList = _unitOfWork.Repository<Contact>().AsNoTracking().Where(x => !x.IsDelete && x.CustomerId == z.Id).ToList();
                z.CustomerStatusName = _unitOfWork.Repository<CustomerStatus>().AsNoTracking().FirstOrDefault(x => !x.IsDelete && x.Id == z.CustomerStatusId).Title;
                z.CustomerTypeName = _unitOfWork.Repository<CustomerType>().AsNoTracking().FirstOrDefault(x => !x.IsDelete && x.Id == z.CustomerTypeId).Title;
                z.GenderName = _unitOfWork.Repository<Gender>().AsNoTracking().FirstOrDefault(x => !x.IsDelete && x.Id == z.GenderId).Title;
                z.DistrictName = _unitOfWork.Repository<District>().AsNoTracking().FirstOrDefault(x => x.Id == z.DistrictId)?.Name;
                z.ProvinceName = _unitOfWork.Repository<Province>().AsNoTracking().FirstOrDefault(x => x.Id == z.ProvinceId)?.Name;
                z.OrganizationName = _unitOfWork.Repository<Organization>().AsNoTracking().FirstOrDefault(x => !x.IsDelete && x.Id == z.OrganizationId).Title;
            });
            var resTemp = await _unitOfWork.Repository<CustomerTemp>().AsNoTracking()
               .Where(z => !z.IsDelete && ((!string.IsNullOrEmpty(phone) && z.Phone.ToLower().Contains(phone.ToLower()))
                || ((!string.IsNullOrEmpty(code) && z.Code.ToLower().Contains(code.ToLower())))
               )
               ).ToListAsync();
            if (resTemp.Count > 1)
            {
                resTemp = resTemp.Where(z => z.Ref_Source == "AGENCY").ToList();
            }
            var res1 = _mapper.Map<IEnumerable<CustomerModel>>(resTemp).ToList();
            res.AddRange(res1);
            result.ListData = res;
            return result;
        }
        public async Task<ResponseList> GetAllByPhoneStoreAsync(int organizationId, string phone, string code)
        {
            var result = new ResponseList();
            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GET");
                paramaters.Add("@OrganizationId", organizationId);
                paramaters.Add("@Phone", phone);
                paramaters.Add("@Code", code);
                paramaters.Add("@RetCount", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<ViewCustomer>("SP_CUSTOMER", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
                var retCount = paramaters.Get<Int32>("@RetCount");
                var retCode = paramaters.Get<Int32>("@RetCode");
                var retText = paramaters.Get<string>("@RetText");
                result.ListData = res;
            }
            return result;
        }
        public async Task<ResponseList> GetListCustomerByPhoneStoreAsync(ViewCustomerFitter viewCustomerFitter, int limit, int page)
        {
            var result = new ResponseList();
            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();

                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GET.BYPHONE");
                paramaters.Add("@OrganizationId", viewCustomerFitter.OrganizationId);
                paramaters.Add("@UserName", viewCustomerFitter.UserName);
                paramaters.Add("@FullName", viewCustomerFitter.FullName);
                paramaters.Add("@Phone", viewCustomerFitter.Phone);
                paramaters.Add("@Email", viewCustomerFitter.Email);
                paramaters.Add("@PageNumber", page);
                paramaters.Add("@RowsOfPage", limit);
                paramaters.Add("@RetCount", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var resAll = conn.Query<ViewCustomer>("SP_CUSTOMER", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
                var retCount = paramaters.Get<Int32>("@RetCount");
                var retCode = paramaters.Get<Int32>("@RetCode");
                var retText = paramaters.Get<string>("@RetText");

                var totalRows = retCount;
                result.Paging = new Paging(totalRows, page, limit);
                int start = result.Paging.start;
                var resList = resAll;
                resList.ToList().ForEach(z =>
                {
                    z.GenderName = _unitOfWork.Repository<Gender>().AsNoTracking().FirstOrDefault(x => !x.IsDelete && x.Id == z.GenderId).Title;
                });
                result.ListData = resList;
            }
            return result;
        }
        public async Task<CustomerModel> GetByIdAsync(int id)
        {
            Customer customer = null;
            CustomerModel customerModel = null;
            customer = _unitOfWork.Repository<Customer>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            var contactList = _unitOfWork.Repository<Contact>().AsNoTracking().Where(x => !x.IsDelete && x.CustomerId == id).ToList();
            customerModel = _mapper.Map<CustomerModel>(customer);
            if (customerModel != null)
                customerModel.ContactList = contactList;
            return customerModel;
        }
        public async Task<Customer> GetByCodeAsync(string code)
        {
            Customer customer = null;
            customer = _unitOfWork.Repository<Customer>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return customer;
        }
        public async Task<string> GenerateCode()
        {
            var codeMax = _unitOfWork.Repository<Customer>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault()?.Code;
            return StringUtil.GetProductID("KH", codeMax, 6);
        }
        public async Task<Customer> CheckPhoneExistAsync(int organizationId, string phone)
        {
            Customer customer = null;
            customer = _unitOfWork.Repository<Customer>()
            .Where(x => !x.IsDelete && x.OrganizationId == organizationId && x.Phone == phone).AsNoTracking().FirstOrDefault();
            return customer;
        }
        public async Task<Contact> CheckContactExistAsync(string phone, string email)
        {
            Contact contact = null;
            contact = _unitOfWork.Repository<Contact>()
            .Where(x => !x.IsDelete && x.IsKey && x.Phone == phone || x.Email == email).AsNoTracking().FirstOrDefault();
            return contact;
        }
        public async Task<CustomerModel> AddAsync(CustomerModel customerModel)
        {

            //    var userMax = _unitOfWork.Repository<Account>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            //    Account.Id = userMax == null ? 1 : userMax.Id + 1;
            customerModel.CreateOn = DateTime.Now.Date;
            var codeMax = _unitOfWork.Repository<Customer>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault()?.Code;
            customerModel.Code = StringUtil.GetProductID("KH", codeMax, 6);
            // Get Kênh Bán hàng
            var channel = _unitOfWork.Repository<Channel>().AsNoTracking().FirstOrDefault(x => x.AccountId == customerModel.CreateBy);
            if (channel != null)
            {
                //customerModel.Ref_Id = channel.Id;
                //customerModel.Ref_Code = channel.Code;
                //customerModel.Ref_Name = channel.Title;
                customerModel.Ref_Source = channel.Code;
            }

            var resInfo = JsonConvert.SerializeObject(customerModel, Formatting.Indented);
            _logger.WriteInfoLog($"AddCustomer --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Customer>().AddAsync(customerModel);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Customer>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            customerModel.Id = userMax == null ? 1 : userMax.Id;
            if (customerModel.ContactList == null || customerModel.ContactList.Count() == 0)
            {
                customerModel.ContactList = new List<Contact>()
                {
                    new Contact
                    {
                        IsKey = true,
                        CustomerId = customerModel.Id,
                        Phone = customerModel.Phone,
                        Email = customerModel.Email,
                        Status = true,
                        IsDelete = false,
                        CreateBy = 81,
                        CreateOn = DateTime.Now,
                    }
                };
            }
            else
            {
                customerModel.ContactList.ToList().ForEach(z => { z.CreateBy = customerModel.CreateBy; z.CreateOn = customerModel.CreateOn; z.CustomerId = customerModel.Id; z.IsDelete = false; });
            }
            // Insert Contact
            await _contactService.AddAsync(customerModel.ContactList);
            return customerModel;
        }

        public async Task<bool> UpdateAsync(CustomerModel customerModel)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Customer>().Where(x => x.Id == customerModel.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.FullName = customerModel.FullName;
                    tmp.Birthday = customerModel.Birthday;
                    tmp.GenderId = customerModel.GenderId;
                    tmp.Phone = customerModel.Phone;
                    tmp.Tel = customerModel.Phone;
                    tmp.DistrictId = customerModel.DistrictId;
                    tmp.ProvinceId = customerModel.ProvinceId;
                    tmp.Address = customerModel.Address;
                    tmp.IsCompany = customerModel.IsCompany;
                    tmp.CompanyName = customerModel.CompanyName;
                    tmp.ParentName = customerModel.ParentName;
                    tmp.ParentEmail = customerModel.ParentEmail;
                    tmp.ParentPhone = customerModel.ParentPhone;
                    tmp.CustomerStatusId = customerModel.CustomerStatusId;
                    tmp.CustomerTypeId = customerModel.CustomerTypeId;
                    tmp.Email = customerModel.Email;
                    //tmp.AccountId = customerModel.AccountId;
                    //tmp.OrganizationId = customerModel.OrganizationId;
                    //tmp.Ref_Id = customerModel.Ref_Id;
                    //tmp.Ref_Code = customerModel.Ref_Code;
                    //tmp.Ref_Name = customerModel.Ref_Name;
                    //tmp.Ref_Source = customerModel.Ref_Source;
                    tmp.Note = customerModel.Note;
                    tmp.ModifiedBy = customerModel.ModifiedBy;
                    tmp.ModifiedOn = customerModel.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateCustomer --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Customer>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    ////Delete và Insert lại
                    var resDelete = await _contactService.DeleteAsync(customerModel.Id);
                    // Insert Contact
                    if (resDelete)
                    {
                        if (customerModel.ContactList == null)
                        {
                            customerModel.ContactList = new List<Contact>()
                                                                            {
                                                                                new Contact
                                                                                {
                                                                                    IsKey = true,
                                                                                    CustomerId = customerModel.Id,
                                                                                    Phone = customerModel.Phone,
                                                                                    Email = customerModel.Email,
                                                                                    Status = true,
                                                                                    IsDelete = false,
                                                                                    CreateBy = customerModel.ModifiedBy,
                                                                                    CreateOn = customerModel.ModifiedOn,
                                                                                }
                                                                            };
                        }
                        else
                        {
                            customerModel.ContactList.ToList().ForEach(z => { z.CreateBy = customerModel.ModifiedBy; z.CreateOn = customerModel.ModifiedOn; z.CustomerId = customerModel.Id; z.IsDelete = false; });
                        }
                        await _contactService.UpdateAsync(customerModel.ContactList);
                    }

                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Customer>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteCustomer --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Customer>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        private async Task<IEnumerable<ViewCustomer>> GetAllByPhoneStore(int organizationId, string phone, string code)
        {
            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "GET");
                paramaters.Add("@OrganizationId", organizationId);
                paramaters.Add("@Phone", phone);
                paramaters.Add("@Code", code);
                paramaters.Add("@RetCount", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var res = conn.Query<ViewCustomer>("SP_CUSTOMER", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
                var retCode = paramaters.Get<Int32>("@RetCode");
                var retText = paramaters.Get<string>("@RetText");
                return res;
            }
        }
    }
}
