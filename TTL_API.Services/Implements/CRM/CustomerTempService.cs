﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class CustomerTempService : ICustomerTempService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public CustomerTempService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<ResponseList> GetAllByPhoneAsync(string phone)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<CustomerTemp>().AsNoTracking();
            query = query.Where(z => !z.IsDelete && z.Phone == phone);
            result.ListData = query.ToList();
            return result;
        }
    }
}
