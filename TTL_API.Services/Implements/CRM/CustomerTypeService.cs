﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;

namespace TTL_API.Services.Implements
{
    public class CustomerTypeService : ICustomerTypeService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public CustomerTypeService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, string search, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<CustomerType>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId
            && ((string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Code.ToLower().Contains(search.ToLower())))
                || (string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Title.ToLower().Contains(search.ToLower())))));
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllCustomerTypeCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<CustomerType>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<CustomerType> GetByCodeAsync(string code)
        {
            CustomerType CustomerType = null;
            CustomerType = _unitOfWork.Repository<CustomerType>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return CustomerType;
        }
        public async Task<CustomerType> GetByIdAsync(int id)
        {
            var res = _unitOfWork.Repository<CustomerType>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return res;
        }
        public async Task<CustomerType> AddAsync(CustomerType CustomerType)
        {
            var resInfo = JsonConvert.SerializeObject(CustomerType, Formatting.Indented);
            _logger.WriteInfoLog($"AddCustomerType --> resInfo: {resInfo}");
            await _unitOfWork.Repository<CustomerType>().AddAsync(CustomerType);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<CustomerType>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            CustomerType.Id = userMax == null ? 1 : userMax.Id;
            await _unitOfWork.SaveChangesAsync();
            return CustomerType;
        }

        public async Task<bool> UpdateAsync(CustomerType CustomerType)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<CustomerType>().Where(x => x.Id == CustomerType.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = CustomerType.Code;
                    tmp.Title = CustomerType.Title;
                    tmp.Description = CustomerType.Description;
                    tmp.Status = CustomerType.Status;
                    tmp.ModifiedBy = CustomerType.ModifiedBy;
                    tmp.ModifiedOn = CustomerType.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateCustomerType --> resInfo: {resInfo}");
                    _unitOfWork.Repository<CustomerType>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<CustomerType>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteCustomerType --> resInfo: {resInfo}");
                    _unitOfWork.Repository<CustomerType>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
