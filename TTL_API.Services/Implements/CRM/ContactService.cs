﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class ContactService : IContactService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public ContactService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllCustomerIdAsync(int customerId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Contact>().AsNoTracking();
            query = query.Where(z => !z.IsDelete && z.CustomerId == customerId);
            result.ListData = query.ToList();
            return result;
        }

        public async Task<Contact> GetByIdAsync(int id)
        {
            Contact contact = null;
            contact = _unitOfWork.Repository<Contact>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return contact;
        }
        public async Task<bool> AddAsync(IEnumerable<Contact> contacts)
        {
            try
            {
                var resInfo = JsonConvert.SerializeObject(contacts, Formatting.Indented);
                _logger.WriteInfoLog($"AddContact --> resInfo: {resInfo}");
                await _unitOfWork.Repository<Contact>().AddRangeAsync(contacts);
                await _unitOfWork.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> UpdateAsync(IEnumerable<Contact> contacts)
        {
            try
            {
                await DeleteAsync(contacts.FirstOrDefault().CustomerId);
                return await AddAsync(contacts);
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public async Task<bool> DeleteAsync(int customerId)
        {
            try
            {
                bool result = false;
                var tmp = _unitOfWork.Repository<Contact>().Where(x => x.CustomerId == customerId).AsNoTracking().ToList();
                if (tmp != null)
                {
                    tmp.ForEach(z => z.IsDelete = true);
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteContact --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Contact>().UpdateRange(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
