﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;

namespace TTL_API.Services.Implements
{
    public class CustomerStatusService : ICustomerStatusService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public CustomerStatusService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, string search, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<CustomerStatus>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId
            && ((string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Code.ToLower().Contains(search.ToLower())))
                || (string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Title.ToLower().Contains(search.ToLower())))));
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllCustomerStatusCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<CustomerStatus>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<CustomerStatus> GetByCodeAsync(string code)
        {
            CustomerStatus CustomerStatus = null;
            CustomerStatus = _unitOfWork.Repository<CustomerStatus>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return CustomerStatus;
        }
        public async Task<CustomerStatus> GetByIdAsync(int id)
        {
            var res = _unitOfWork.Repository<CustomerStatus>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return res;
        }
        public async Task<CustomerStatus> AddAsync(CustomerStatus CustomerStatus)
        {
            var resInfo = JsonConvert.SerializeObject(CustomerStatus, Formatting.Indented);
            _logger.WriteInfoLog($"AddCustomerStatus --> resInfo: {resInfo}");
            await _unitOfWork.Repository<CustomerStatus>().AddAsync(CustomerStatus);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<CustomerStatus>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            CustomerStatus.Id = userMax == null ? 1 : userMax.Id;
            await _unitOfWork.SaveChangesAsync();
            return CustomerStatus;
        }

        public async Task<bool> UpdateAsync(CustomerStatus CustomerStatus)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<CustomerStatus>().Where(x => x.Id == CustomerStatus.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = CustomerStatus.Code;
                    tmp.Title = CustomerStatus.Title;
                    tmp.Description = CustomerStatus.Description;
                    tmp.Status = CustomerStatus.Status;
                    tmp.ModifiedBy = CustomerStatus.ModifiedBy;
                    tmp.ModifiedOn = CustomerStatus.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateCustomerStatus --> resInfo: {resInfo}");
                    _unitOfWork.Repository<CustomerStatus>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<CustomerStatus>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteCustomerStatus --> resInfo: {resInfo}");
                    _unitOfWork.Repository<CustomerStatus>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
