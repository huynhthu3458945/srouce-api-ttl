﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;

namespace TTL_API.Services.Implements
{
    public class PalletService : IPalletService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public PalletService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int storeId, string search, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Pallet>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId 
            && ((string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Code.ToLower().Contains(search.ToLower())))
                || (string.IsNullOrEmpty(search) || (!string.IsNullOrEmpty(search) && z.Title.ToLower().Contains(search.ToLower()))))).Join(
                _unitOfWork.Repository<Store>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId
                 && (storeId == 0 || (storeId > 0 && z.Id == storeId))),
                p => p.StoreId,
                c => c.Id,
                (p, c) => new PalletModel()
                {
                    Id = p.Id,
                    Code = p.Code,
                    Title = p.Title,
                    Description = p.Description,
                    StoreName = c.Title,
                    StoreId = c.Id,
                    Status = p.Status,
                    IsDelete = p.IsDelete,
                });
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllPalletProduct(PalletProductFitter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();

            var query = _unitOfWork.Repository<Store_Pallet_Product>().AsNoTracking().Where(z => !z.IsDelete
            && (fitter.StoreId == 0 || (fitter.StoreId > 0 && z.StoreId == z.StoreId))
            && (fitter.PalletId == 0 || (fitter.PalletId > 0 && z.PalletId == z.PalletId))
            ).Join(
                _unitOfWork.Repository<Product>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == fitter.OrganizationId
                 && ((string.IsNullOrEmpty(fitter.Code) || (!string.IsNullOrEmpty(fitter.Code) && z.Code.ToLower().Contains(fitter.Code.ToLower()))))
                 && ((string.IsNullOrEmpty(fitter.Title) || (!string.IsNullOrEmpty(fitter.Title) && z.Code.ToLower().Contains(fitter.Title.ToLower()))))
                 ),
                p => p.ProductId,
                c => c.Id,
                (p, c) => new Store_Pallet_ProductModel()
                {
                    Id = p.Id,
                    StoreId = p.StoreId,
                    PalletId = p.PalletId,
                    ProductCode = c.Code,
                    ProductName = c.Title,
                    Avatar = c.Avatar,
                    Note = p.Note,
                });
              
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            var res = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            res.ForEach(z => { z.StoreName = _unitOfWork.Repository<Store>().AsNoTracking().Where(x => x.Id == z.StoreId).FirstOrDefault().Title;
                               z.PalletName = _unitOfWork.Repository<Pallet>().AsNoTracking().Where(x => x.Id == z.PalletId).FirstOrDefault().Title;
            });
            result.ListData = res;
            return result;
        }

        public async Task<ResponseList> GetAllPalletCombox(int organizationId, int storeId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Pallet>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId && z.StoreId == storeId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<Pallet> GetByCodeAsync(string code)
        {
            Pallet Pallet = null;
            Pallet = _unitOfWork.Repository<Pallet>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Pallet;
        }
        public async Task<Store_Pallet_ProductModel> GetPalletProductAsync(PalletProductFitter fitter)
        {
            Store_Pallet_ProductModel store_Pallet_ProductModel = null;
            store_Pallet_ProductModel = _mapper.Map<Store_Pallet_ProductModel> (_unitOfWork.Repository<Store_Pallet_Product>()
            .Where( x => !x.IsDelete && x.StoreId == fitter.StoreId && x.PalletId == fitter.PalletId && x.ProductId == fitter.ProductId).AsNoTracking().FirstOrDefault());
            if (store_Pallet_ProductModel != null)
                store_Pallet_ProductModel.ProductCode = _unitOfWork.Repository<Product>()
               .Where(x => !x.IsDelete && x.Id == store_Pallet_ProductModel.ProductId).AsNoTracking().FirstOrDefault().Code;
            return store_Pallet_ProductModel;
        }
        public async Task<Pallet> GetByIdAsync(int id)
        {
            var res = _unitOfWork.Repository<Pallet>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return res;
        }
        public async Task<Pallet> AddAsync(PalletModel Pallet)
        {
            var resInfo = JsonConvert.SerializeObject(Pallet, Formatting.Indented);
            _logger.WriteInfoLog($"AddPallet --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Pallet>().AddAsync(Pallet);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Pallet>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Pallet.Id = userMax == null ? 1 : userMax.Id;
            await _unitOfWork.SaveChangesAsync();
            return Pallet;
        }

        public async Task<Store_Pallet_Product> AddPalletProductAsync(Store_Pallet_Product store_Pallet_Product)
        {
            var resInfo = JsonConvert.SerializeObject(store_Pallet_Product, Formatting.Indented);
            _logger.WriteInfoLog($"AddStore_Pallet_Product --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Store_Pallet_Product>().AddAsync(store_Pallet_Product);
            await _unitOfWork.SaveChangesAsync();
            return store_Pallet_Product;
        }

        public async Task<bool> UpdateAsync(PalletModel Pallet)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Pallet>().Where(x => x.Id == Pallet.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = Pallet.Code;
                    tmp.Title = Pallet.Title;
                    tmp.Description = Pallet.Description;
                    tmp.Status = Pallet.Status;
                    tmp.StoreId = Pallet.StoreId;
                    tmp.ModifiedBy = Pallet.ModifiedBy;
                    tmp.ModifiedOn = Pallet.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdatePallet --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Pallet>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Pallet>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeletePallet --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Pallet>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public async Task<bool> DeletePalletProductAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Store_Pallet_Product>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteStore_Pallet_ProductModel --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Store_Pallet_Product>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
