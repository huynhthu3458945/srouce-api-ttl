﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;

namespace TTL_API.Services.Implements
{
    public class StoreService : IStoreService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public StoreService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Store>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId).Join(
                _unitOfWork.Repository<StoreType>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId),
                p => p.StoreTypeId,
                c => c.Id,
                (p, c) => new StoreModel()
                {
                    Id = p.Id,
                    Code = p.Code,
                    Title = p.Title,
                    Description = p.Description,
                    StoreTypeName = c.Title,
                    Status = c.Status,
                    IsPallet = p.IsPallet,
                    IsDelete = p.IsDelete,
                });
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllStoreCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Store>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Code + " - " + z.Title }).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllStoreNumberCombox()
        {
            var result = new ResponseList();
            var storeType = await _unitOfWork.Repository<StoreType>().AsNoTracking().FirstOrDefaultAsync(z => !z.IsDelete && z.Status && z.Code == "KHOSO");
            var query = _unitOfWork.Repository<Store>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.StoreTypeId == storeType.Id); // Kho sản phẩm số
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Code + " - " + z.Title }).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllStoreIsPalletCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Store>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId && z.IsPallet);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Code + " - " + z.Title }).ToList();
            return result;
        }
        public async Task<Store> GetByCodeAsync(string code)
        {
            Store Store = null;
            Store = _unitOfWork.Repository<Store>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Store;
        }
        public async Task<StoreModel> GetByIdAsync(int id)
        {
            var res = _mapper.Map<StoreModel>(_unitOfWork.Repository<Store>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
            res.KeyId = _unitOfWork.Repository<Store_Account>()
            .Where(x => x.StoreId == id && x.IsKey).AsNoTracking().FirstOrDefault().AccountId;
            res.EmployeedId = _unitOfWork.Repository<Store_Account>()
           .Where(x => x.StoreId == id && !x.IsKey).AsNoTracking().Select(x=>x.AccountId).ToList();
            return res;
        }
        public async Task<StoreModel> AddAsync(StoreModel Store)
        {
            var resInfo = JsonConvert.SerializeObject(Store, Formatting.Indented);
            _logger.WriteInfoLog($"AddStore --> resInfo: {resInfo}");
            await _unitOfWork.Repository<DbContext.Models.Store>().AddAsync(Store);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Store>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Store.Id = userMax == null ? 1 : userMax.Id;
            await _unitOfWork.Repository<Store_Account>().AddAsync(new Store_Account() { StoreId = Store.Id, AccountId = Store.KeyId, IsKey = true, CreateBy = Store.CreateBy, CreateOn = Store.CreateOn });
            var listStore_Acccount = Store.EmployeedId.Select(z => new Store_Account() { StoreId = Store.Id, AccountId = z, CreateBy = Store.CreateBy, CreateOn = Store.CreateOn });
            await _unitOfWork.Repository<Store_Account>().AddRangeAsync(listStore_Acccount);
            await _unitOfWork.SaveChangesAsync();
            return Store;
        }

        public async Task<bool> UpdateAsync(StoreModel Store)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Store>().Where(x => x.Id == Store.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = Store.Code;
                    tmp.Title = Store.Title;
                    tmp.Description = Store.Description;
                    tmp.Status = Store.Status;
                    tmp.IsPallet = Store.IsPallet;
                    tmp.ModifiedBy = Store.ModifiedBy;
                    tmp.ModifiedOn = Store.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateStore --> resInfo: {resInfo}");
                    _unitOfWork.Repository<DbContext.Models.Store>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    var list = await _unitOfWork.Repository<Store_Account>().Where(x => x.StoreId == Store.Id).AsNoTracking().ToListAsync();
                     _unitOfWork.Repository<Store_Account>().RemoveRange(list);
                    await _unitOfWork.Repository<Store_Account>().AddAsync(new Store_Account() { StoreId = Store.Id, AccountId = Store.KeyId, IsKey = true, CreateBy = Store.ModifiedBy, CreateOn = Store.ModifiedOn, ModifiedBy = Store.ModifiedBy, ModifiedOn = Store.ModifiedOn });
                    var listStore_Acccount = Store.EmployeedId.Select(z => new Store_Account() { StoreId = Store.Id, AccountId = z, CreateBy = Store.ModifiedBy, CreateOn = Store.ModifiedOn, ModifiedBy = Store.ModifiedBy, ModifiedOn = Store.ModifiedOn });
                    await _unitOfWork.Repository<Store_Account>().AddRangeAsync(listStore_Acccount);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Store>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteStore --> resInfo: {resInfo}");
                    _unitOfWork.Repository<DbContext.Models.Store>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
