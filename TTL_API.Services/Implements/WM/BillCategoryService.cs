﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class BillCategoryService : IBillCategoryService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public BillCategoryService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<BillCategory>().AsNoTracking().Where(z => !z.IsDelete);//&& z.OrganizationId == organizationId);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        } 
        public async Task<ResponseList> GetAllBillCategoryCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<BillCategory>().AsNoTracking().Where(z => !z.IsDelete && z.Status); //&& z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Position).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<BillCategory> GetByCodeAsync(string code)
        {
            BillCategory BillCategory = null;
            BillCategory = _unitOfWork.Repository<BillCategory>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return BillCategory;
        }
        public async Task<BillCategory> GetByIdAsync(int id)
        {
            BillCategory BillCategory = null;
            BillCategory = _unitOfWork.Repository<BillCategory>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return BillCategory;
        }
        public async Task<BillCategory> AddAsync(BillCategory BillCategory)
        {
            var resInfo = JsonConvert.SerializeObject(BillCategory, Formatting.Indented);
            _logger.WriteInfoLog($"AddBillCategory --> resInfo: {resInfo}");
            await _unitOfWork.Repository<BillCategory>().AddAsync(BillCategory);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<BillCategory>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            BillCategory.Id = userMax == null ? 1 : userMax.Id;
            return BillCategory;
        }

        public async Task<bool> UpdateAsync(BillCategory BillCategory)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<BillCategory>().Where(x => x.Id == BillCategory.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = BillCategory.Code;
                    tmp.Title = BillCategory.Title;
                    tmp.Description = BillCategory.Description;
                    tmp.Status = BillCategory.Status;
                    tmp.ModifiedBy = BillCategory.ModifiedBy;
                    tmp.ModifiedOn = BillCategory.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateBillCategory --> resInfo: {resInfo}");
                    _unitOfWork.Repository<BillCategory>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<BillCategory>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteBillCategory --> resInfo: {resInfo}");
                    _unitOfWork.Repository<BillCategory>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
