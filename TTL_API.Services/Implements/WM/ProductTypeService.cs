﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class ProductTypeService : IProductTypeService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public ProductTypeService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<ProductType>().AsNoTracking().Where(z => !z.IsDelete);// && z.OrganizationId == organizationId);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        } 
        public async Task<ResponseList> GetAllProductTypeCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<ProductType>().AsNoTracking().Where(z => !z.IsDelete && z.Status);// && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<ProductType> GetByCodeAsync(string code)
        {
            ProductType ProductType = null;
            ProductType = _unitOfWork.Repository<ProductType>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return ProductType;
        }
        public async Task<ProductType> GetByIdAsync(int id)
        {
            ProductType ProductType = null;
            ProductType = _unitOfWork.Repository<ProductType>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return ProductType;
        }
        public async Task<ProductType> AddAsync(ProductType ProductType)
        {
            var resInfo = JsonConvert.SerializeObject(ProductType, Formatting.Indented);
            _logger.WriteInfoLog($"AddProductType --> resInfo: {resInfo}");
            await _unitOfWork.Repository<ProductType>().AddAsync(ProductType);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<ProductType>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            ProductType.Id = userMax == null ? 1 : userMax.Id;
            return ProductType;
        }

        public async Task<bool> UpdateAsync(ProductType ProductType)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<ProductType>().Where(x => x.Id == ProductType.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = ProductType.Code;
                    tmp.Title = ProductType.Title;
                    tmp.Description = ProductType.Description;
                    tmp.Status = ProductType.Status;
                    tmp.ModifiedBy = ProductType.ModifiedBy;
                    tmp.ModifiedOn = ProductType.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateProductType --> resInfo: {resInfo}");
                    _unitOfWork.Repository<ProductType>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<ProductType>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteProductType --> resInfo: {resInfo}");
                    _unitOfWork.Repository<ProductType>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
