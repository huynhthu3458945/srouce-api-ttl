﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class UnitService : IUnitService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public UnitService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Unit>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        } 
        public async Task<ResponseList> GetAllUnitCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Unit>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<Unit> GetByCodeAsync(string code)
        {
            Unit Unit = null;
            Unit = _unitOfWork.Repository<Unit>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Unit;
        }
        public async Task<Unit> GetByIdAsync(int id)
        {
            Unit Unit = null;
            Unit = _unitOfWork.Repository<Unit>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return Unit;
        }
        public async Task<Unit> AddAsync(Unit Unit)
        {
            var resInfo = JsonConvert.SerializeObject(Unit, Formatting.Indented);
            _logger.WriteInfoLog($"AddUnit --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Unit>().AddAsync(Unit);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Unit>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Unit.Id = userMax == null ? 1 : userMax.Id;
            return Unit;
        }

        public async Task<bool> UpdateAsync(Unit Unit)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Unit>().Where(x => x.Id == Unit.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = Unit.Code;
                    tmp.Title = Unit.Title;
                    tmp.Description = Unit.Description;
                    tmp.Status = Unit.Status;
                    tmp.ModifiedBy = Unit.ModifiedBy;
                    tmp.ModifiedOn = Unit.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateUnit --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Unit>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Unit>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteUnit --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Unit>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
       
    }
}
