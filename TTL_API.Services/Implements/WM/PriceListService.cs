﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;

namespace TTL_API.Services.Implements
{
    public class PriceListService : IPriceListService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public PriceListService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(PriceListFitter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<PriceList>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == fitter.OrganizationId
             && (string.IsNullOrEmpty(fitter.Code) || (!string.IsNullOrEmpty(fitter.Code) && z.Code.ToLower().Contains(fitter.Code.ToLower())))
             && (string.IsNullOrEmpty(fitter.Title) || (!string.IsNullOrEmpty(fitter.Title) && z.Title.ToLower().Contains(fitter.Title.ToLower())))
             );
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllPriceListCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<PriceList>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId
             && (z.IsUnlimited || (z.Startdate.Value.Date <= DateTime.Now.Date && z.Enddate.Value.Date >= DateTime.Now.Date)));
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllPriceAgencyListCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<PriceList>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId
             && z.IsAgency
             && (z.IsUnlimited || (z.Startdate.Value.Date <= DateTime.Now.Date && z.Enddate.Value.Date >= DateTime.Now.Date)));
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<PriceList> GetByCodeAsync(string code)
        {
            PriceList PriceList = null;
            PriceList = _unitOfWork.Repository<PriceList>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return PriceList;
        }
        public async Task<PriceListModel> GetByIdAsync(int id)
        {
            var res = _mapper.Map<PriceListModel>(_unitOfWork.Repository<PriceList>().Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
            res.PriceListDetail = _unitOfWork.Repository<PriceListDetail>().Where(x => x.PriceListId == id).AsNoTracking().ToList();
            return res;
        }
        public async Task<PriceListModel> AddAsync(PriceListModel PriceList)
        {
            var resInfo = JsonConvert.SerializeObject(PriceList, Formatting.Indented);
            _logger.WriteInfoLog($"AddPriceList --> resInfo: {resInfo}");
            await _unitOfWork.Repository<PriceList>().AddAsync(PriceList);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<PriceList>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            PriceList.Id = userMax == null ? 1 : userMax.Id;
            if (PriceList.PriceListDetail != null && PriceList.PriceListDetail.Count() > 0)
            {
                PriceList.PriceListDetail.ToList().ForEach(z =>
                {
                    z.PriceListId = PriceList.Id;
                    z.ComboId = z.ComboId > 0 ? z.ComboId : null;
                    z.ProductId = z.ProductId > 0 ? z.ProductId : null;
                    z.OrganizationId = PriceList.OrganizationId;
                    z.CreateBy = PriceList.CreateBy;
                    z.CreateOn = PriceList.CreateOn;
                });
                await _unitOfWork.Repository<PriceListDetail>().AddRangeAsync(PriceList.PriceListDetail);
                await _unitOfWork.SaveChangesAsync();
            }
            return PriceList;
        }

        public async Task<bool> UpdateAsync(PriceListModel PriceList)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<PriceList>().Where(x => x.Id == PriceList.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = PriceList.Code;
                    tmp.Title = PriceList.Title;
                    tmp.Description = PriceList.Description;
                    tmp.Status = PriceList.Status;
                    tmp.Note = PriceList.Note;
                    tmp.Startdate = PriceList.Startdate;
                    tmp.Enddate = PriceList.Enddate;
                    tmp.IsUnlimited = PriceList.IsUnlimited;
                    tmp.IsAgency = PriceList.IsAgency;
                    tmp.ModifiedBy = PriceList.ModifiedBy;
                    tmp.ModifiedOn = PriceList.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdatePriceList --> resInfo: {resInfo}");
                    _unitOfWork.Repository<PriceList>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();

                    if (PriceList.PriceListDetail != null && PriceList.PriceListDetail.Count() > 0)
                    {
                        var bundledGiftlist = _unitOfWork.Repository<PriceListDetail>().AsNoTracking().Where(z => z.PriceListId == PriceList.Id);
                        _unitOfWork.Repository<PriceListDetail>().RemoveRange(bundledGiftlist);
                        await _unitOfWork.SaveChangesAsync();
                        PriceList.PriceListDetail.ToList().ForEach(z =>
                        {
                            z.PriceListId = PriceList.Id;
                            z.ComboId = z.ComboId > 0 ? z.ComboId : null;
                            z.ProductId = z.ProductId > 0 ? z.ProductId : null;
                            z.OrganizationId = PriceList.OrganizationId;
                            z.CreateBy = tmp.CreateBy; z.CreateOn = tmp.CreateOn;
                            z.ModifiedBy = PriceList.ModifiedBy; z.ModifiedOn = PriceList.ModifiedOn;
                        });
                        await _unitOfWork.Repository<PriceListDetail>().AddRangeAsync(PriceList.PriceListDetail);
                        await _unitOfWork.SaveChangesAsync();
                    }


                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<PriceList>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeletePriceList --> resInfo: {resInfo}");
                    _unitOfWork.Repository<PriceList>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
