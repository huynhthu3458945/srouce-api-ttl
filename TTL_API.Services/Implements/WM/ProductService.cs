﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using TTL_API.Services.Extensions;
using Microsoft.AspNetCore.Hosting;
using AutoMapper;
using TTL_API.DbContext.Models.Utils;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using System.Net.Http.Headers;
using static TTL_API.Services.Implements.OrderService;

namespace TTL_API.Services.Implements
{
    public class ProductService : IProductService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        private HttpClientHandler clientHandler = new HttpClientHandler();
        public IConfiguration Configuration { get; }
        public ProductService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper, IConfiguration configuration)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            Configuration = configuration;
        }
        public async Task<ResponseList> GetAllAsync(ProductFitter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Product>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == fitter.OrganizationId
             && (string.IsNullOrEmpty(fitter.Code) || (!string.IsNullOrEmpty(fitter.Code) && z.Code.ToLower().Contains(fitter.Code.ToLower())))
             && (string.IsNullOrEmpty(fitter.Title) || (!string.IsNullOrEmpty(fitter.Title) && z.Title.ToLower().Contains(fitter.Title.ToLower())))
                && (fitter.ProductCategoryId == 0 || (fitter.ProductCategoryId > 0 && z.ProductCategoryId == fitter.ProductCategoryId))
                && (fitter.ProductTypeId == 0 || (fitter.ProductTypeId > 0 && z.ProductTypeId == fitter.ProductTypeId))
            );
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            var res = _mapper.Map<IEnumerable<ProductModel>>(query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList());
            res.ToList().ForEach(z =>
            {
                z.UnitName = _unitOfWork.Repository<Unit>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.UnitId).FirstOrDefault().Title;
            });
            result.ListData = res;
            return result;
        }
        public async Task<ResponseList> GetAllProductCombox(int organizationId, int productTypeId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Product>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId
                && (productTypeId == 0 || (productTypeId > 0 && z.ProductTypeId == productTypeId))
            );
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllProductIsInventoryCombox(int organizationId, int productTypeId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Product>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId && z.IsInventory
                && (productTypeId == 0 || (productTypeId > 0 && z.ProductTypeId == productTypeId))
            );
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<Product> GetByCodeAsync(string code)
        {
            Product Product = null;
            Product = _unitOfWork.Repository<Product>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Product;
        }
        public async Task<ProductModel> GetByIdAsync(int id)
        {
            var res = _mapper.Map<ProductModel>(_unitOfWork.Repository<Product>().Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
            res.BundledGiftList = _unitOfWork.Repository<BundledGift>().Where(x => x.ProductId == res.Id).AsNoTracking().ToList();
            res.AttributeList = _unitOfWork.Repository<Product_Attribute>().Where(x => x.ProductId == res.Id).AsNoTracking().ToList();
            return res;
        }
        public async Task<ProductModel> AddAsync(ProductModel Product)
        {
            var resInfo = JsonConvert.SerializeObject(Product, Formatting.Indented);
            _logger.WriteInfoLog($"AddProduct --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Product>().AddAsync(Product);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Product>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Product.Id = userMax == null ? 1 : userMax.Id;
            if (Product.IsBundledGift)
            {
                Product.BundledGiftList.ToList().ForEach(z => { z.ProductId = Product.Id; });
                await _unitOfWork.Repository<BundledGift>().AddRangeAsync(Product.BundledGiftList);
                await _unitOfWork.SaveChangesAsync();
            }
            if (Product.AttributeList != null)
            {
                Product.AttributeList.ToList().ForEach(z => { z.ProductId = Product.Id; });
                await _unitOfWork.Repository<Product_Attribute>().AddRangeAsync(Product.AttributeList);
                await _unitOfWork.SaveChangesAsync();
            }

            ProductCreateReq requestModel = new ProductCreateReq();
            requestModel.CategoryProductCode = _unitOfWork.Repository<ProductCategory>().Where(x => x.Id == Product.ProductCategoryId).AsNoTracking().FirstOrDefault()?.Code;
            requestModel.Code = Product.Code;
            requestModel.Title = Product.Title;
            requestModel.Price = Product.Price;
            requestModel.TokenKey = Configuration["Tokens:Key"];

            var domain = Configuration["ExtensionUrls:APIV2_URL"];
            var urlAPI = "/api/AffiliateProduct/create-product-ttl";
            // Call Api
            using (HttpClient httpClient = new HttpClient(clientHandler, false))
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);

                var resInfoApi = JsonConvert.SerializeObject(requestModel, Formatting.Indented);
                _logger.WriteInfoLog($"Call Api  --> resInfo: {resInfoApi}");

                var response = await httpClient.PostAsJsonAsync($"{domain}{urlAPI}", requestModel);
                var resResponse = (await response.Content.ReadAsAsync<GetResponse<ProductCreateReq>>());
                //if (resResponse.RetCode == RetCodeEnum.Ok)
                //    return resResponse.Data;
            }
            return Product;
        }

        public async Task<bool> UpdateAsync(ProductModel Product)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Product>().Where(x => x.Id == Product.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = Product.Code;
                    tmp.Title = Product.Title;
                    tmp.Description = Product.Description;
                    tmp.Avatar = Product.Avatar;
                    tmp.Thumb = Product.Thumb;
                    tmp.ProductCategoryId = Product.ProductCategoryId;
                    tmp.ProductTypeId = Product.ProductTypeId;
                    tmp.IsBundledGift = Product.IsBundledGift;
                    tmp.Price = Product.Price;
                    tmp.CostPrice = Product.CostPrice;
                    tmp.Content = Product.Content;
                    tmp.Sale = Product.Sale;
                    tmp.SaleDeadLine = Product.SaleDeadLine;
                    tmp.IsInventory = Product.IsInventory;
                    tmp.SupplierId = Product.SupplierId;
                    tmp.UnitId = Product.UnitId;
                    tmp.Status = Product.Status;
                    tmp.Mass = Product.Mass;
                    tmp.WidthX = Product.WidthX;
                    tmp.WidthY = Product.WidthY;
                    tmp.Height = Product.Height;
                    tmp.Weight = Product.Weight;
                    tmp.StoreId = Product.StoreId;
                    tmp.ModifiedBy = Product.ModifiedBy;
                    tmp.ModifiedOn = Product.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateProduct --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Product>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();

                    if (Product.IsBundledGift)
                    {
                        var bundledGiftlist = _unitOfWork.Repository<BundledGift>().AsNoTracking().Where(z=>z.ProductId == Product.Id);
                         _unitOfWork.Repository<BundledGift>().RemoveRange(bundledGiftlist);
                        await _unitOfWork.SaveChangesAsync();
                        Product.BundledGiftList.ToList().ForEach(z => { z.ProductId = Product.Id; });
                        await _unitOfWork.Repository<BundledGift>().AddRangeAsync(Product.BundledGiftList);
                        await _unitOfWork.SaveChangesAsync();
                    }

                    if (Product.AttributeList != null)
                    {
                        var attributeList = _unitOfWork.Repository<Product_Attribute>().AsNoTracking().Where(z => z.ProductId == Product.Id);
                        _unitOfWork.Repository<Product_Attribute>().RemoveRange(attributeList);
                        await _unitOfWork.SaveChangesAsync();
                        Product.AttributeList.ToList().ForEach(z => { z.ProductId = Product.Id; z.CreateBy = tmp.CreateBy; z.CreateOn = tmp.CreateOn; 
                            z.ModifiedBy = Product.ModifiedBy; z.ModifiedOn = Product.ModifiedOn;
                        });
                        await _unitOfWork.Repository<Product_Attribute>().AddRangeAsync(Product.AttributeList);
                        await _unitOfWork.SaveChangesAsync();
                    }

                    ProductUpdateReq requestModel = new ProductUpdateReq();
                    requestModel.CategoryProductCode = _unitOfWork.Repository<ProductCategory>().Where(x => x.Id == Product.ProductCategoryId).AsNoTracking().FirstOrDefault()?.Code;
                    requestModel.Code = Product.Code;
                    requestModel.OldCode = Product.Code;
                    requestModel.Title = Product.Title;
                    requestModel.Price = Product.Price;
                    requestModel.TokenKey = Configuration["Tokens:Key"];

                    var domain = Configuration["ExtensionUrls:APIV2_URL"];
                    var urlAPI = "/api/AffiliateProduct/update-product-ttl";
                    // Call Api
                    using (HttpClient httpClient = new HttpClient(clientHandler, false))
                    {
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);

                        var resInfoApi = JsonConvert.SerializeObject(requestModel, Formatting.Indented);
                        _logger.WriteInfoLog($"Call Api  --> resInfo: {resInfoApi}");

                        var response = await httpClient.PostAsJsonAsync($"{domain}{urlAPI}", requestModel);
                        var resResponse = (await response.Content.ReadAsAsync<GetResponse<ProductUpdateReq>>());
                        //if (resResponse.RetCode == RetCodeEnum.Ok)
                        //    return resResponse.Data;
                    }

                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Product>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteProduct --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Product>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
