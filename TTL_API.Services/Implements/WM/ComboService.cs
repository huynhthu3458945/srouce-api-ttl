﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using TTL_API.Services.Extensions;
using Microsoft.AspNetCore.Hosting;
using AutoMapper;

namespace TTL_API.Services.Implements
{
    public class ComboService : IComboService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public ComboService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(ComboFitter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Combo>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == fitter.OrganizationId
             && (string.IsNullOrEmpty(fitter.Code) || (!string.IsNullOrEmpty(fitter.Code) && z.Code.ToLower().Contains(fitter.Code.ToLower())))
             && (string.IsNullOrEmpty(fitter.Title) || (!string.IsNullOrEmpty(fitter.Title) && z.Title.ToLower().Contains(fitter.Title.ToLower())))
            );
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        }
        public async Task<ResponseList> GetAllComboCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Combo>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId
            );
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<Combo> GetByCodeAsync(string code)
        {
            Combo Combo = null;
            Combo = _unitOfWork.Repository<Combo>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Combo;
        }
        public async Task<ComboModel> GetByIdAsync(int id)
        {
            var res = _mapper.Map<ComboModel>(_unitOfWork.Repository<Combo>().Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
            res.ComboProductList = _unitOfWork.Repository<Combo_Product>().Where(x => x.ComboId == res.Id).AsNoTracking().ToList();
            return res;
        }
        public async Task<ComboModel> AddAsync(ComboModel Combo)
        {
            var resInfo = JsonConvert.SerializeObject(Combo, Formatting.Indented);
            _logger.WriteInfoLog($"AddCombo --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Combo>().AddAsync(Combo);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Combo>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Combo.Id = userMax == null ? 1 : userMax.Id;
            if (Combo.ComboProductList != null && Combo.ComboProductList.Count() > 0)
            {
                Combo.ComboProductList.ToList().ForEach(z =>
                {
                    z.ComboId = Combo.Id;
                    z.CreateBy = Combo.CreateBy; z.CreateOn = Combo.CreateOn;
                });
                await _unitOfWork.Repository<Combo_Product>().AddRangeAsync(Combo.ComboProductList);
                await _unitOfWork.SaveChangesAsync();
            }

            return Combo;
        }

        public async Task<bool> UpdateAsync(ComboModel Combo)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Combo>().Where(x => x.Id == Combo.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = Combo.Code;
                    tmp.Title = Combo.Title;
                    tmp.Description = Combo.Description;
                    tmp.Avatar = Combo.Avatar;
                    tmp.Thumb = Combo.Thumb;
                    tmp.Price = Combo.Price;
                    tmp.CostPrice = Combo.CostPrice;
                    tmp.Content = Combo.Content;
                    tmp.Sale = Combo.Sale;
                    tmp.SaleDeadLine = Combo.SaleDeadLine;
                    tmp.Status = Combo.Status;
                    tmp.ModifiedBy = Combo.ModifiedBy;
                    tmp.ModifiedOn = Combo.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateCombo --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Combo>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();

                    if (Combo.ComboProductList != null && Combo.ComboProductList.Count() > 0)
                    {
                        var bundledGiftlist = _unitOfWork.Repository<Combo_Product>().AsNoTracking().Where(z => z.ComboId == Combo.Id);
                        _unitOfWork.Repository<Combo_Product>().RemoveRange(bundledGiftlist);
                        await _unitOfWork.SaveChangesAsync();
                        Combo.ComboProductList.ToList().ForEach(z =>
                        {
                            z.ComboId = Combo.Id;
                            z.CreateBy = Combo.CreateBy; z.CreateOn = Combo.CreateOn;
                        });
                        await _unitOfWork.Repository<Combo_Product>().AddRangeAsync(Combo.ComboProductList);
                        await _unitOfWork.SaveChangesAsync();
                    }

                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Combo>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteCombo --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Combo>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
