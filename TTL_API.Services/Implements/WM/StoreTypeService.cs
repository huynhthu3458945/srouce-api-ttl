﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class StoreTypeService : IStoreTypeService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public StoreTypeService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<StoreType>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        } 
        public async Task<ResponseList> GetAllStoreTypeCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<StoreType>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<StoreType> GetByCodeAsync(string code)
        {
            StoreType StoreType = null;
            StoreType = _unitOfWork.Repository<StoreType>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return StoreType;
        }
        public async Task<StoreType> GetByIdAsync(int id)
        {
            StoreType StoreType = null;
            StoreType = _unitOfWork.Repository<StoreType>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return StoreType;
        }
        public async Task<StoreType> AddAsync(StoreType storeType)
        {
            var resInfo = JsonConvert.SerializeObject(storeType, Formatting.Indented);
            _logger.WriteInfoLog($"AddStoreType --> resInfo: {resInfo}");
            await _unitOfWork.Repository<StoreType>().AddAsync(storeType);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<StoreType>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            storeType.Id = userMax == null ? 1 : userMax.Id;
            return storeType;
        }

        public async Task<bool> UpdateAsync(StoreType storeType)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<StoreType>().Where(x => x.Id == storeType.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = storeType.Code;
                    tmp.Title = storeType.Title;
                    tmp.Description = storeType.Description;
                    tmp.Status = storeType.Status;
                    tmp.ModifiedBy = storeType.ModifiedBy;
                    tmp.ModifiedOn = storeType.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateStoreType --> resInfo: {resInfo}");
                    _unitOfWork.Repository<StoreType>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<StoreType>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteStoreType --> resInfo: {resInfo}");
                    _unitOfWork.Repository<StoreType>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
