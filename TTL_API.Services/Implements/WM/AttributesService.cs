﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class AttributesService : IAttributesService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public AttributesService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Attributes>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        } 
        public async Task<ResponseList> GetAllAttributesCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Attributes>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<Attributes> GetByCodeAsync(string code)
        {
            Attributes Attribute = null;
            Attribute = _unitOfWork.Repository<Attributes>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Attribute;
        }
        public async Task<Attributes> GetByIdAsync(int id)
        {
            Attributes Attribute = null;
            Attribute = _unitOfWork.Repository<Attributes>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return Attribute;
        }
        public async Task<Attributes> AddAsync(Attributes Attribute)
        {
            var resInfo = JsonConvert.SerializeObject(Attribute, Formatting.Indented);
            _logger.WriteInfoLog($"AddAttribute --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Attributes>().AddAsync(Attribute);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Attributes>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Attribute.Id = userMax == null ? 1 : userMax.Id;
            return Attribute;
        }

        public async Task<bool> UpdateAsync(Attributes Attribute)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Attributes>().Where(x => x.Id == Attribute.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = Attribute.Code;
                    tmp.Title = Attribute.Title;
                    tmp.Description = Attribute.Description;
                    tmp.Status = Attribute.Status;
                    tmp.ModifiedBy = Attribute.ModifiedBy;
                    tmp.ModifiedOn = Attribute.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateAttribute --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Attributes>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Attributes>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteAttribute --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Attributes>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
