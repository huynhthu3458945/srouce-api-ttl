﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;
using TTL_API.Services.Extensions;

namespace TTL_API.Services.Implements
{
    public class InventoryService : IInventoryService
    {
        private readonly ILoggerService _logger;
        private readonly IBillService _billService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public InventoryService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper, IBillService billService)
        {
            _logger = logger;
            _billService = billService;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(InventoryFitter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Inventory>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == fitter.OrganizationId
                && (fitter.StoreId == 0 || (fitter.StoreId > 0 && fitter.StoreId == z.StoreId))
                && (!fitter.FromDate.HasValue && !fitter.ToDate.HasValue) || (fitter.FromDate.HasValue && z.CreateOn.Value.Date >= fitter.FromDate.Value && z.CreateOn.Value.Date <= fitter.ToDate.Value)
             );
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            var res = _mapper.Map<IEnumerable<InventoryModel>>(query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList());
            res.ToList().ForEach(z =>
            {
                z.StoreName = _unitOfWork.Repository<DbContext.Models.Store>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.StoreId).FirstOrDefault()?.Title;
                z.BillId = (!string.IsNullOrEmpty(z.NoteDifference) ?
                        _unitOfWork.Repository<Bill>().AsNoTracking().Where(x => !x.IsDelete && x.Code == z.NoteDifference
                        && z.OrganizationId == z.OrganizationId).FirstOrDefault().Id
                       : 0);
            });

            result.ListData = res;
            return result;
        }
        public async Task<Inventory> GetByCodeAsync(string code)
        {
            Inventory Inventory = null;
            Inventory = _unitOfWork.Repository<Inventory>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Inventory;
        }
        public async Task<InventoryModel> GetByIdAsync(int id)
        {
            var res = _mapper.Map<InventoryModel>(_unitOfWork.Repository<Inventory>().Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
            res.InventoryDetail = _mapper.Map<IEnumerable<InventoryDetailModel>>(_unitOfWork.Repository<InventoryDetail>().Where(x => x.InventoryId == id).AsNoTracking().ToList());
            res.InventoryDetail.ToList().ForEach(z => z.ProductName = _unitOfWork.Repository<Product>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.ProductId).FirstOrDefault()?.Title);
            return res;
        }
        public async Task<string> GenerateCode(string type)
        {
            var codeMax = _unitOfWork.Repository<Inventory>().AsNoTracking().Where(z => z.Code.ToLower().Contains(type.ToLower())).OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault()?.Code;
            return StringUtil.GetProductID(type, codeMax, 6);
        }
        public async Task<InventoryModel> AddAsync(InventoryModel Inventory)
        {
            var resInfo = JsonConvert.SerializeObject(Inventory, Formatting.Indented);
            _logger.WriteInfoLog($"AddInventory --> resInfo: {resInfo}");
            //Inventory.CreateOn = DateTime.Now;
            await _unitOfWork.Repository<Inventory>().AddAsync(Inventory);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Inventory>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Inventory.Id = userMax == null ? 1 : userMax.Id;
            if (Inventory.InventoryDetail != null && Inventory.InventoryDetail.Count() > 0)
            {
                Inventory.InventoryDetail.ToList().ForEach(z =>
                {
                    z.InventoryId = Inventory.Id;
                    z.CreateBy = Inventory.CreateBy;
                    z.CreateOn = Inventory.CreateOn;
                });
                await _unitOfWork.Repository<InventoryDetail>().AddRangeAsync(Inventory.InventoryDetail);
                await _unitOfWork.SaveChangesAsync();
            }
            return Inventory;
        }

        public async Task<bool> UpdateAsync(InventoryModel Inventory)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Inventory>().Where(x => x.Id == Inventory.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.StoreId = Inventory.StoreId;
                    tmp.Title = Inventory.Title;
                    tmp.Note = Inventory.Note;
                    tmp.TotalQuantity = Inventory.TotalQuantityActual;
                    tmp.TotalQuantityActual = Inventory.TotalQuantityActual;
                    tmp.TotalQuantityDiff = Inventory.TotalQuantityDiff;
                    tmp.InventoryDate = Inventory.InventoryDate;
                    tmp.ModifiedBy = Inventory.ModifiedBy;
                    tmp.ModifiedOn = Inventory.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateInventory --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Inventory>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();

                    //if (Inventory.InventoryDetail != null && Inventory.InventoryDetail.Count() > 0)
                    //{
                    //    var bundledGiftlist = _unitOfWork.Repository<InventoryDetail>().AsNoTracking().Where(z => z.InventoryId == Inventory.Id);
                    //    _unitOfWork.Repository<InventoryDetail>().RemoveRange(bundledGiftlist);
                    //    await _unitOfWork.SaveChangesAsync();
                    //    Inventory.InventoryDetail.ToList().ForEach(z =>
                    //    {
                    //        z.InventoryId = Inventory.Id;
                    //        z.CreateBy = tmp.CreateBy; z.CreateOn = tmp.CreateOn;
                    //        z.ModifiedBy = Inventory.ModifiedBy; z.ModifiedOn = Inventory.ModifiedOn;
                    //    });
                    //    await _unitOfWork.Repository<InventoryDetail>().AddRangeAsync(Inventory.InventoryDetail);
                    //    await _unitOfWork.SaveChangesAsync();
                    //}

                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public async Task<bool> Balance(int id)
        {
            try
            {
                bool result = false;
                var inventory = await _unitOfWork.Repository<Inventory>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                var inventoryDetail = await _unitOfWork.Repository<InventoryDetail>().Where(x => x.InventoryId == id).AsNoTracking().ToListAsync();

                // khởi tạo Bill
                BillModel bill = new BillModel();
                bill.BillType = 4;// kiểm kê
                bill.BillCategoryId = _unitOfWork.Repository<BillCategory>().Where(x => x.Code == "CB").AsNoTracking().FirstOrDefault().Id;
                bill.StoreId = inventory.StoreId;
                bill.OrganizationId = inventory.OrganizationId;
                bill.BillStatusId = _unitOfWork.Repository<BillStatus>().Where(x => x.Code == "500").AsNoTracking().FirstOrDefault().Id;
                bill.CreateOn = DateTime.Now;
                bill.CreateBy = inventory.CreateBy;
                var codeMax = _unitOfWork.Repository<Bill>().AsNoTracking().Where(z => z.Code.ToLower().Contains("CB".ToLower())).OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault()?.Code;
                bill.Code = StringUtil.GetProductID("CB", codeMax, 6);

                // detail
                List<BillDetail> billDetails = new List<BillDetail>();
                foreach (var item in inventoryDetail)
                {
                    var product = await _unitOfWork.Repository<Product>().Where(x => x.Id == item.ProductId).AsNoTracking().FirstOrDefaultAsync();
                    BillDetail billDetail = new BillDetail();
                    billDetail.ProductId = item.ProductId;
                    billDetail.UnitId = product.UnitId;
                    billDetail.QuantityReal = item.QuantityDiff;
                    billDetail.TotalPrice = item.DifferenceValue;
                    if (item.QuantityDiff < 0)
                        billDetail.Price = product.Price;
                    else
                        billDetail.Price = product.CostPrice;
                    billDetails.Add(billDetail);

                }
                bill.BillDetail = billDetails;
                var res = await _billService.AddAsync(bill);

                // history
                var productHistoryList = new List<BillDetailHistory>();
                var listbillDetail = await _unitOfWork.Repository<BillDetail>().Where(x => x.BillId == bill.Id).AsNoTracking().ToListAsync();
                foreach (var item in listbillDetail)
                {
                    var productHistory = _mapper.Map<BillDetailHistory>(item);
                    productHistory.OrganizationId = bill.OrganizationId;
                    productHistory.StoreId = bill.StoreId;
                    if (item.QuantityReal < 0)
                        productHistory.BillType = 2; // Xuất
                    else
                        productHistory.BillType = 1; // Nhập
                    productHistoryList.Add(productHistory);
                }
                if (productHistoryList.Count > 0)
                {
                    await _unitOfWork.Repository<BillDetailHistory>().AddRangeAsync(productHistoryList);
                    await _unitOfWork.SaveChangesAsync();
                }
                // Update Inventory
                if (inventory != null)
                {
                    inventory.HandleDifference = true;
                    inventory.NoteDifference = bill.Code;

                     _unitOfWork.Repository<Inventory>().Update(inventory);
                    await _unitOfWork.SaveChangesAsync();
                }
                result = true;

                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }


        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Inventory>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteInventory --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Inventory>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
