﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using AutoMapper;
using TTL_API.Services.Extensions;

namespace TTL_API.Services.Implements
{
    public class BillService : IBillService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private readonly IMapper _mapper;
        public BillService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(BillFitter fitter, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Bill>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == fitter.OrganizationId
                && (fitter.BillCategoryId == 0 || (fitter.BillCategoryId > 0 && fitter.BillCategoryId == z.BillCategoryId))
                && (fitter.StoreId == 0 || (fitter.StoreId > 0 && fitter.StoreId == z.StoreId))
                && (fitter.StoreExportId == 0 || (fitter.StoreExportId > 0 && fitter.StoreExportId == z.StoreExportId))
                && (fitter.SupplierId == 0 || (fitter.SupplierId > 0 && fitter.SupplierId == z.SupplierId))
                && (fitter.BillStatusId == 0 || (fitter.BillStatusId > 0 && fitter.BillStatusId == z.BillStatusId))
                && (!fitter.FromDate.HasValue && !fitter.ToDate.HasValue || (fitter.FromDate.HasValue && z.CreateOn.Value.Date >= fitter.FromDate.Value && z.CreateOn.Value.Date <= fitter.ToDate.Value))
             );
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            var res = _mapper.Map<IEnumerable<BillModel>>(query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList());
            res.ToList().ForEach(z =>
            {
                z.SupplierName = _unitOfWork.Repository<Supplier>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.SupplierId).FirstOrDefault()?.Title;
                z.StoreName = _unitOfWork.Repository<DbContext.Models.Store>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.StoreId).FirstOrDefault()?.Title;
                z.StoreExportName = _unitOfWork.Repository<DbContext.Models.Store>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.StoreExportId).FirstOrDefault()?.Title;
                z.BillCategoryName = _unitOfWork.Repository<BillCategory>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.BillCategoryId).FirstOrDefault()?.Title;
                z.BillStatusCode = _unitOfWork.Repository<BillStatus>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.BillStatusId).FirstOrDefault()?.Code;
                z.BillStatusName = _unitOfWork.Repository<BillStatus>().AsNoTracking().Where(x => !x.IsDelete && x.Id == z.BillStatusId).FirstOrDefault()?.Title;
                z.OrderCode = _unitOfWork.Repository<Order>().Where(x => x.Id == z.OrderId).AsNoTracking().FirstOrDefault()?.Code;
            });

            result.ListData = res;
            return result;
        }
        public async Task<Bill> GetByCodeAsync(string code)
        {
            Bill Bill = null;
            Bill = _unitOfWork.Repository<Bill>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Bill;
        }
        public async Task<BillModel> GetByIdAsync(int id)
        {
            var res = _mapper.Map<BillModel>(_unitOfWork.Repository<Bill>().Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
            res.BillDetail = _unitOfWork.Repository<BillDetail>().Where(x => x.BillId == id).AsNoTracking().ToList();
            res.StoreDetail = _unitOfWork.Repository<StoreDetail>().Where(x => x.BillId == id || x.BillExportId == id).AsNoTracking().ToList();
            res.OrderCode = _unitOfWork.Repository<Order>().Where(x => x.Id == res.OrderId).AsNoTracking().FirstOrDefault()?.Code;
            return res;
        }
        public async Task<string> GenerateCode(string type)
        {
            var codeMax = _unitOfWork.Repository<Bill>().AsNoTracking().Where(z => z.Code.ToLower().Contains(type.ToLower())).OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault()?.Code;
            return StringUtil.CheckLetter(type, codeMax, 6);
        }
        public async Task<BillModel> AddAsync(BillModel Bill)
        {
            var resInfo = JsonConvert.SerializeObject(Bill, Formatting.Indented);
            _logger.WriteInfoLog($"AddBill --> resInfo: {resInfo}");
            //Bill.CreateOn = DateTime.Now;
            await _unitOfWork.Repository<Bill>().AddAsync(Bill);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Bill>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Bill.Id = userMax == null ? 1 : userMax.Id;
            if (Bill.BillDetail != null && Bill.BillDetail.Count() > 0)
            {
                Bill.BillDetail.ToList().ForEach(z =>
                {
                    z.BillId = Bill.Id;
                    z.CreateBy = Bill.CreateBy;
                    z.CreateOn = Bill.CreateOn;
                });
                await _unitOfWork.Repository<BillDetail>().AddRangeAsync(Bill.BillDetail);
                await _unitOfWork.SaveChangesAsync();
            }
            if (Bill.BillType == 1) // Nhập kho
            {
                if (Bill.StoreDetail != null && Bill.StoreDetail.Count() > 0)
                {
                    Bill.StoreDetail.ToList().ForEach(z =>
                    {
                        z.StoreId = Bill.StoreId;
                        z.ProductId = Bill.ProductId;
                        z.BillId = Bill.Id;
                        z.CreateBy = Bill.CreateBy;
                        z.CreateOn = Bill.CreateOn;
                    });
                    await _unitOfWork.Repository<StoreDetail>().AddRangeAsync(Bill.StoreDetail);
                    await _unitOfWork.SaveChangesAsync();
                }
            }
            else if (Bill.BillType == 2) // Xuất
            {
                if(Bill.Quantity > 0)
                {
                    var storeDetail = _unitOfWork.Repository<StoreDetail>().AsNoTracking().Where(z => z.StoreId == Bill.StoreId && z.ProductId == Bill.ProductId && !z.Status).ToList();
                    var storeDetailTake = storeDetail.Skip(0).Take(Bill.Quantity).ToList();
                    foreach (var item in storeDetailTake)
                    {
                        item.Status = true;
                        item.BillExportId = Bill.Id;
                    }
                    _unitOfWork.Repository<StoreDetail>().UpdateRange(storeDetailTake);
                    await _unitOfWork.SaveChangesAsync();
                    Bill.StoreDetail = storeDetailTake;
                }
            }
            return Bill;
        }

        public async Task<bool> UpdateAsync(BillModel Bill)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Bill>().Where(x => x.Id == Bill.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.SupplierId = Bill.SupplierId;
                    tmp.StoreId = Bill.StoreId;
                    tmp.StoreExportId = Bill.StoreExportId;
                    tmp.StoreExportId = Bill.StoreExportId;
                    tmp.BillCategoryId = Bill.BillCategoryId;
                    tmp.TotalAmount = Bill.TotalAmount;
                    var billStatus = await _unitOfWork.Repository<BillStatus>().Where(x => x.Id == Bill.BillStatusId).AsNoTracking().FirstOrDefaultAsync();
                    if (billStatus.Code != "600")
                        tmp.BillStatusId = Bill.BillStatusId;
                    tmp.DiscountValue = Bill.DiscountValue;
                    tmp.Note = Bill.Note;
                    tmp.IsDelivery = Bill.IsDelivery;
                    tmp.DeliveryFee = Bill.DeliveryFee;
                    tmp.ModifiedBy = Bill.ModifiedBy;
                    tmp.ModifiedOn = Bill.ModifiedOn;
                    tmp.MT_DiscountKindId = Bill.MT_DiscountKindId;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateBill --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Bill>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();

                    if (Bill.BillDetail != null && Bill.BillDetail.Count() > 0)
                    {
                        var bundledGiftlist = _unitOfWork.Repository<BillDetail>().AsNoTracking().Where(z => z.BillId == Bill.Id);
                        _unitOfWork.Repository<BillDetail>().RemoveRange(bundledGiftlist);
                        await _unitOfWork.SaveChangesAsync();
                        Bill.BillDetail.ToList().ForEach(z =>
                        {
                            z.BillId = Bill.Id;
                            z.CreateBy = tmp.CreateBy; z.CreateOn = tmp.CreateOn;
                            z.ModifiedBy = Bill.ModifiedBy; z.ModifiedOn = Bill.ModifiedOn;
                        });
                        await _unitOfWork.Repository<BillDetail>().AddRangeAsync(Bill.BillDetail);
                        await _unitOfWork.SaveChangesAsync();
                    }
                    // UpdateStatus
                    //await UpdateStatus(Bill.Id, Bill.BillStatusId);

                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public async Task<bool> UpdateStatusAsync(int id, string billStatusCode)
        {
            try
            {
                bool result = false;
                var bill = await _unitOfWork.Repository<Bill>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                var billStatus = await _unitOfWork.Repository<BillStatus>().Where(x => x.Code == billStatusCode).AsNoTracking().FirstOrDefaultAsync();
                var billStatus700 = await _unitOfWork.Repository<BillStatus>().Where(x => x.Code == "700").AsNoTracking().FirstOrDefaultAsync();
                if (billStatus.Code == "600") // Đã xuất kho
                {
                    var productHistoryList = new List<BillDetailHistory>();

                    var billDetail = _unitOfWork.Repository<BillDetail>().Where(x => x.BillId == id).AsNoTracking().ToList();
                    foreach (var item in billDetail)
                    {
                        var productHistory = _mapper.Map<BillDetailHistory>(item);
                        var productHistoryClone = _mapper.Map<BillDetailHistory>(item);
                        productHistory.OrganizationId = bill.OrganizationId;
                        productHistoryClone.OrganizationId = bill.OrganizationId;
                        var product = _unitOfWork.Repository<Product>().Where(x => x.Id == item.ProductId).AsNoTracking().FirstOrDefault();
                        if (item.UnitId != product.UnitId) // Check đơn vị tính
                        {
                            var unitConvert = _unitOfWork.Repository<UnitConverted>().Where(x => x.BasicUnitId == product.UnitId).AsNoTracking().FirstOrDefault();
                            productHistory.Quantity = unitConvert.BasicUnitNumber * productHistory.Quantity;
                            productHistory.QuantityReal = unitConvert.BasicUnitNumber * productHistory.QuantityReal;
                            productHistory.QuantityFail = unitConvert.BasicUnitNumber * productHistory.QuantityFail;
                            productHistory.UnitId = product.UnitId;
                        }

                        if (bill.BillType == 1) // Nhập kho
                        {
                            productHistory.BillType = 1;
                            productHistory.StoreId = bill.StoreId;
                        }
                        else if (bill.BillType == 2) // Xuất kho
                        {
                            productHistory.BillType = 2;
                            productHistory.StoreId = bill.StoreId;

                            productHistory.Quantity = -productHistory.Quantity;
                            productHistory.QuantityReal = -productHistory.QuantityReal;
                            productHistory.QuantityFail = -productHistory.QuantityFail;
                        }
                        else if (bill.BillType == 3) // Chuyển kho
                        {
                            // Nhập kho 
                            productHistory.StoreId = bill.StoreId;
                            productHistory.BillType = 1;
                            // xuất kho
                            productHistoryClone.BillType = 2;
                            productHistoryClone.StoreId = bill.StoreExportId;
                            productHistoryClone.Quantity = -productHistory.Quantity;
                            productHistoryClone.QuantityReal = -productHistory.QuantityReal;
                            productHistoryClone.QuantityFail = -productHistory.QuantityFail;
                            productHistoryList.Add(productHistoryClone);
                        }
                        productHistoryList.Add(productHistory);
                    }
                    if (productHistoryList.Count > 0)
                    {
                        await _unitOfWork.Repository<BillDetailHistory>().AddRangeAsync(productHistoryList);
                        await _unitOfWork.SaveChangesAsync();
                    }
                    result = true;
                }

                //Luồng duyệt
                //Update Status
                if (bill != null)
                {
                    if(billStatusCode == "600")
                    {
                        // đã nhập kho
                        if (bill.BillType == 1)
                            bill.BillStatusId = billStatus700.Id;
                        // đã nhập xuất
                        else if (bill.BillType == 2 || bill.BillType == 3)
                            bill.BillStatusId = billStatus.Id;
                    }
                    else
                    {
                        bill.BillStatusId = billStatus.Id;
                    }
                    var resInfo = JsonConvert.SerializeObject(bill, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateBill --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Bill>().Update(bill);
                    await _unitOfWork.SaveChangesAsync();

                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }


        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Bill>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteBill --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Bill>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        public async Task<ResponseList> GetAllProductNumber(BillModel billModel)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<StoreDetail>().AsNoTracking().Where(z => !z.IsDelete && !z.Status && z.StoreId == billModel.StoreId && z.ProductId == billModel.ProductId);
            result.ListData = query;
            return result;
        }
    }
}
