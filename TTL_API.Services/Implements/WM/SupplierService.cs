﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class SupplierService : ISupplierService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public SupplierService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Supplier>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        } 
        public async Task<ResponseList> GetAllSupplierCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<Supplier>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<Supplier> GetByCodeAsync(string code)
        {
            Supplier Supplier = null;
            Supplier = _unitOfWork.Repository<Supplier>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return Supplier;
        }
        public async Task<Supplier> GetByIdAsync(int id)
        {
            Supplier Supplier = null;
            Supplier = _unitOfWork.Repository<Supplier>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return Supplier;
        }
        public async Task<Supplier> AddAsync(Supplier Supplier)
        {
            var resInfo = JsonConvert.SerializeObject(Supplier, Formatting.Indented);
            _logger.WriteInfoLog($"AddSupplier --> resInfo: {resInfo}");
            await _unitOfWork.Repository<Supplier>().AddAsync(Supplier);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<Supplier>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            Supplier.Id = userMax == null ? 1 : userMax.Id;
            return Supplier;
        }

        public async Task<bool> UpdateAsync(Supplier Supplier)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Supplier>().Where(x => x.Id == Supplier.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = Supplier.Code;
                    tmp.Title = Supplier.Title;
                    tmp.Description = Supplier.Description;
                    tmp.Email = Supplier.Email;
                    tmp.Phone = Supplier.Phone;
                    tmp.Tel = Supplier.Tel;
                    tmp.Hotline = Supplier.Hotline;
                    tmp.Fax = Supplier.Fax;
                    tmp.Address = Supplier.Address;
                    tmp.Status = Supplier.Status;
                    tmp.ModifiedBy = Supplier.ModifiedBy;
                    tmp.ModifiedOn = Supplier.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateSupplier --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Supplier>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<Supplier>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteSupplier --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Supplier>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
