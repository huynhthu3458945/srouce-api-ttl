﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using Dapper;
using System.Net.Http;
using System.Net.Http.Headers;
using TTL_API.DbContext.Models.Utils;
using static TTL_API.Services.Implements.OrderService;

namespace TTL_API.Services.Implements
{
    public class ProductCategoryService : IProductCategoryService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        private IEnumerable<ResponseCombo> _productCategoryTreeList;
        private HttpClientHandler clientHandler = new HttpClientHandler();
        public IConfiguration Configuration { get; }
        public ProductCategoryService(ILoggerService logger, IGenericDbContext<TTL_APIContext> unitOfWork, IConfiguration configuration)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            Configuration = configuration;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<ProductCategory>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        } 
        public async Task<ResponseList> GetAllProductCategoryCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _unitOfWork.Repository<ProductCategory>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            var mainConnectString = Configuration["MainConnectionString"];
            using (var conn = new SqlConnection(mainConnectString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    conn.Open();
                var paramaters = new DynamicParameters();
                paramaters.Add("@OrganizationId", organizationId);
                _productCategoryTreeList = conn.Query<ResponseCombo>("SP_ProductCategory_Tree", paramaters, null, true, 120, System.Data.CommandType.StoredProcedure);
            }
            result.ListData = _productCategoryTreeList.ToList();
            return result;
        }
        public async Task<ProductCategory> GetByCodeAsync(string code)
        {
            ProductCategory ProductCategory = null;
            ProductCategory = _unitOfWork.Repository<ProductCategory>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return ProductCategory;
        }
        public async Task<ProductCategory> GetByIdAsync(int id)
        {
            ProductCategory ProductCategory = null;
            ProductCategory = _unitOfWork.Repository<ProductCategory>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return ProductCategory;
        }
        public async Task<ProductCategory> AddAsync(ProductCategory ProductCategory)
        {
            var resInfo = JsonConvert.SerializeObject(ProductCategory, Formatting.Indented);
            _logger.WriteInfoLog($"AddProductCategory --> resInfo: {resInfo}");
            await _unitOfWork.Repository<ProductCategory>().AddAsync(ProductCategory);
            await _unitOfWork.SaveChangesAsync();
            var userMax = _unitOfWork.Repository<ProductCategory>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            ProductCategory.Id = userMax == null ? 1 : userMax.Id;

            ProductCategoryCreateReq requestModel = new ProductCategoryCreateReq();
            requestModel.Code = ProductCategory.Code;
            requestModel.Title = ProductCategory.Title;
            requestModel.TokenKey = Configuration["Tokens:Key"];

            var domain = Configuration["ExtensionUrls:APIV2_URL"];
            var urlAPI = "/api/AffiliateProduct/create-product-category-ttl";
            // Call Api
            using (HttpClient httpClient = new HttpClient(clientHandler, false))
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);

                var resInfoApi = JsonConvert.SerializeObject(requestModel, Formatting.Indented);
                _logger.WriteInfoLog($"Call Api  --> resInfo: {resInfoApi}");

                var response = await httpClient.PostAsJsonAsync($"{domain}{urlAPI}", requestModel);
                var resResponse = (await response.Content.ReadAsAsync<GetResponse<ProductCategoryCreateReq>>());
                //if (resResponse.RetCode == RetCodeEnum.Ok)
                //    return resResponse.Data;
            }
            return ProductCategory;
        }

        public async Task<bool> UpdateAsync(ProductCategory ProductCategory)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<ProductCategory>().Where(x => x.Id == ProductCategory.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = ProductCategory.Code;
                    tmp.Title = ProductCategory.Title;
                    tmp.Description = ProductCategory.Description;
                    tmp.Status = ProductCategory.Status;
                    tmp.ParentId = ProductCategory.ParentId;
                    tmp.Thumb = ProductCategory.Thumb;
                    tmp.Avatar = ProductCategory.Avatar;
                    tmp.ModifiedBy = ProductCategory.ModifiedBy;
                    tmp.ModifiedOn = ProductCategory.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateProductCategory --> resInfo: {resInfo}");
                    _unitOfWork.Repository<ProductCategory>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();

                    ProductCategoryUpdateReq requestModel = new ProductCategoryUpdateReq();
                    requestModel.Code = ProductCategory.Code;
                    requestModel.OldCode = ProductCategory.Code;
                    requestModel.Title = ProductCategory.Title;
                    requestModel.TokenKey = Configuration["Tokens:Key"];

                    var domain = Configuration["ExtensionUrls:APIV2_URL"];
                    var urlAPI = "/api/AffiliateProduct/update-product-category-ttl";
                    // Call Api
                    using (HttpClient httpClient = new HttpClient(clientHandler, false))
                    {
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        //httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", userLogin.Token);

                        var resInfoApi = JsonConvert.SerializeObject(requestModel, Formatting.Indented);
                        _logger.WriteInfoLog($"Call Api  --> resInfo: {resInfoApi}");

                        var response = await httpClient.PostAsJsonAsync($"{domain}{urlAPI}", requestModel);
                        var resResponse = (await response.Content.ReadAsAsync<GetResponse<ProductCategoryUpdateReq>>());
                        //if (resResponse.RetCode == RetCodeEnum.Ok)
                        //    return resResponse.Data;
                    }
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<ProductCategory>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteProductCategory --> resInfo: {resInfo}");
                    _unitOfWork.Repository<ProductCategory>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
