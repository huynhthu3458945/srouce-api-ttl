﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TTL_API.Services.Implements
{
    public class UnitConvertedService : IUnitConvertedService
    {
        private readonly ILoggerService _logger;
        private readonly IGenericDbContext<TTL_APIContext> _UnitConvertedOfWork;
        public UnitConvertedService(ILoggerService logger, IGenericDbContext<TTL_APIContext> UnitConvertedOfWork)
        {
            _logger = logger;
            _UnitConvertedOfWork = UnitConvertedOfWork;
        }
        public async Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1)
        {
            var result = new ResponseList();
            var query = _UnitConvertedOfWork.Repository<UnitConverted>().AsNoTracking().Where(z => !z.IsDelete && z.OrganizationId == organizationId);
            var totalRows = query.Count();
            result.Paging = new Paging(totalRows, page, limit);
            int start = result.Paging.start;
            result.ListData = query.OrderByDescending(x => x.Id).Skip(start).Take(limit).ToList();
            return result;
        } 
        public async Task<ResponseList> GetAllUnitConvertedCombox(int organizationId)
        {
            var result = new ResponseList();
            var query = _UnitConvertedOfWork.Repository<UnitConverted>().AsNoTracking().Where(z => !z.IsDelete && z.Status && z.OrganizationId == organizationId);
            result.ListData = query.OrderBy(x => x.Id).Select(z => new ResponseCombo() { Id = z.Id, Value = z.Id.ToString(), Text = z.Title }).ToList();
            return result;
        }
        public async Task<UnitConverted> GetByCodeAsync(string code)
        {
            UnitConverted UnitConverted = null;
            UnitConverted = _UnitConvertedOfWork.Repository<UnitConverted>()
            .Where(x => x.Code == code).AsNoTracking().FirstOrDefault();
            return UnitConverted;
        }
        public async Task<UnitConverted> GetByIdAsync(int id)
        {
            UnitConverted UnitConverted = null;
            UnitConverted = _UnitConvertedOfWork.Repository<UnitConverted>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            return UnitConverted;
        }   
        public async Task<UnitConverted> GetByBasicUnitId(int basicUnitId)
        {
            UnitConverted UnitConverted = null;
            UnitConverted = _UnitConvertedOfWork.Repository<UnitConverted>()
            .Where(x => x.BasicUnitId == basicUnitId).AsNoTracking().FirstOrDefault();
            return UnitConverted;
        }
        public async Task<UnitConverted> AddAsync(UnitConverted UnitConverted)
        {
            var resInfo = JsonConvert.SerializeObject(UnitConverted, Formatting.Indented);
            _logger.WriteInfoLog($"AddUnitConverted --> resInfo: {resInfo}");
            await _UnitConvertedOfWork.Repository<UnitConverted>().AddAsync(UnitConverted);
            await _UnitConvertedOfWork.SaveChangesAsync();
            var userMax = _UnitConvertedOfWork.Repository<UnitConverted>().OrderByDescending(x => x.Id).AsNoTracking().FirstOrDefault();
            UnitConverted.Id = userMax == null ? 1 : userMax.Id;
            return UnitConverted;
        }

        public async Task<bool> UpdateAsync(UnitConverted UnitConverted)
        {
            try
            {
                bool result = false;
                var tmp = await _UnitConvertedOfWork.Repository<UnitConverted>().Where(x => x.Id == UnitConverted.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.Code = UnitConverted.Code;
                    tmp.Title = UnitConverted.Title;
                    tmp.Description = UnitConverted.Description;
                    tmp.Status = UnitConverted.Status;
                    tmp.BasicUnitId = UnitConverted.BasicUnitId;
                    tmp.BasicUnitNumber = UnitConverted.BasicUnitNumber;
                    tmp.ModifiedBy = UnitConverted.ModifiedBy;
                    tmp.ModifiedOn = UnitConverted.ModifiedOn;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateUnitConverted --> resInfo: {resInfo}");
                    _UnitConvertedOfWork.Repository<UnitConverted>().Update(tmp);
                    await _UnitConvertedOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _UnitConvertedOfWork.Repository<UnitConverted>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteUnitConverted --> resInfo: {resInfo}");
                    _UnitConvertedOfWork.Repository<UnitConverted>().Update(tmp);
                    await _UnitConvertedOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
        
    }
}
