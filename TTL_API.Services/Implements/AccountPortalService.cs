﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Dapper;
using AutoMapper;
using TTL_API.DbContext.Models.Utils;

namespace TTL_API.Services.Implements
{
    public class AccountPortalService : IAccountPortalService
    {
        public IConfiguration Configuration { get; }
        private readonly ILoggerService _logger;
        private readonly IRoleService _roleService;
        private readonly IAccount_RoleService _account_RoleService;
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public AccountPortalService(ILoggerService logger
            , IGenericDbContext<TTL_APIContext> unitOfWork
            , IConfiguration configuration
            , IAccount_RoleService account_RoleService
            , ICustomerService customerService
            , IRoleService roleService
            , IMapper mapper)
        {
            _logger = logger;
            _logger = logger;
            _unitOfWork = unitOfWork;
            _roleService = roleService;
            _account_RoleService = account_RoleService;
            _customerService = customerService;
            Configuration = configuration;
            _mapper = mapper;
        }

        public async Task<AccountPortalRep> GetByIdAsync(int id)
        {
            AccountPortalRep model = null;
            AccountPortal client = null;
            client = _unitOfWork.Repository<AccountPortal>()
            .Where(x => x.Id == id).AsNoTracking().FirstOrDefault();
            model = _mapper.Map<AccountPortalRep>(client);
            var viewCustomer = await _customerService.GetAllByPhoneStoreAsync(68, client.UserName, string.Empty);
            var resViewList = ((IEnumerable<object>)viewCustomer.ListData).Cast<ViewCustomer>().OrderByDescending(z => z.Ref_Source).FirstOrDefault();
            model.Birthday = resViewList.Birthday;
            model.Address = resViewList.Address;
            model.GenderId = resViewList.GenderId;
            model.DistrictId = resViewList.DistrictId;
            model.ProvinceId = resViewList.ProvinceId;
            model.FullName = resViewList.FullName;
            model.Email = resViewList.Email;
            model.Phone = resViewList.Phone;
            model.Identitycard = resViewList.Identitycard;
            model.Bank = resViewList.Bank;
            model.BankBranch = resViewList.BankBranch;
            model.BankAccountName = resViewList.BankAccountName;
            model.BankAccount = resViewList.BankAccount;
            return model;
        }
        public async Task<AccountPortal> GetAccountAsync(AccountPortal account)
        {
            var targetUser = AccountAsync(account);
            return targetUser;
        }
        public async Task<AccountPortal> GetAccountUserNameAsync(string userName)
        {
            var result = new AccountPortal();
            var query = _unitOfWork.Repository<AccountPortal>().AsNoTracking();
            result = query.Where(z => !z.IsDelete && z.UserName == userName).FirstOrDefault();
            return result;
        }
        private AccountPortal AccountAsync(AccountPortal account)
        {
            AccountPortal user = null;
            user = _unitOfWork.Repository<AccountPortal>()
            .Where(x => !x.IsDelete && x.Id == account.Id).AsNoTracking().FirstOrDefault();
            return user;
        }
        public async Task<bool> AddAsync(AccountPortalReq account, ViewCustomer viewCustomer,  bool isCreateCustomer)
        {
            try
            {
                bool result = false;
                var modelCustomer = new Customer();
                if (isCreateCustomer)
                {
                    modelCustomer.Code = await _customerService.GenerateCode();
                    modelCustomer.FullName = account.FullName;
                    modelCustomer.Phone = account.Phone;
                    modelCustomer.Email = account.Email;
                    modelCustomer.GenderId = 3;
                    modelCustomer.OrganizationId = 68;
                    modelCustomer.CustomerTypeId = 1;
                    modelCustomer.CustomerStatusId = 1;
                    modelCustomer.CreateBy = 81;
                    modelCustomer.CreateOn = DateTime.Now;
                    modelCustomer.Identitycard = account.Identitycard;
                    modelCustomer.Bank = account.Bank;
                    modelCustomer.BankBranch = account.BankBranch;
                    modelCustomer.BankAccount = account.BankAccount;
                    modelCustomer.BankAccountName = account.BankAccountName;
                    modelCustomer.CityBank = account.CityBank;
                    modelCustomer.Ref_Id = 2; 
                    modelCustomer.Ref_Code = "KH000001";
                    await _unitOfWork.Repository<Customer>().AddAsync(modelCustomer);
                    await _unitOfWork.SaveChangesAsync();
                }
                else
                {
                    modelCustomer.Id = viewCustomer.Id;
                    modelCustomer.Code = viewCustomer.Code;
                    if (viewCustomer.Ref_Source == "AGENCY")
                    {
                        account.Roles = "AGENCY";
                    }
                }
                var model = _mapper.Map<AccountPortal>(account);
                model.OrganizationId = 68;
                model.CustomerId = modelCustomer.Id;
                model.CustomerCode = modelCustomer.Code;
                model.CreateOn = DateTime.Now;
                var resInfo = JsonConvert.SerializeObject(model, Formatting.Indented);
                _logger.WriteInfoLog($"AddAccount --> resInfo: {resInfo}");
                model.CountryCode = account.CountryCode;
                model.email = account.Email;
                await _unitOfWork.Repository<AccountPortal>().AddAsync(model);
                await _unitOfWork.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> UpdateAsync(AccountPortalReq account)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<AccountPortal>().Where(x => x.Id == account.Id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    //tmp.Status = account.Status;
                    //tmp.Password = account.Password;
                    tmp.ModifiedBy = account.CreateBy;
                    tmp.ModifiedOn = DateTime.Now;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateAccount --> resInfo: {resInfo}");
                    _unitOfWork.Repository<AccountPortal>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }

                var customer = await _unitOfWork.Repository<Customer>().Where(x => x.Code == tmp.CustomerCode).AsNoTracking().FirstOrDefaultAsync();
                if (customer != null)
                {
                    customer.FullName = account.FullName;
                    customer.Email = account.Email;
                    customer.Identitycard = account.Email;
                    customer.Bank = account.Bank;
                    customer.BankAccount = account.BankAccount;
                    customer.BankAccountName = account.BankAccountName;
                    customer.BankBranch = account.BankBranch;
                    customer.Birthday = account.Birthday;
                    customer.Address = account.Address;
                    customer.GenderId = account.GenderId;
                    customer.DistrictId = account.DistrictId;
                    customer.ProvinceId = account.ProvinceId;
                    customer.ModifiedBy = account.CreateBy;
                    customer.ModifiedOn = DateTime.Now;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateCustomer --> resInfo: {resInfo}");
                    _unitOfWork.Repository<Customer>().Update(customer);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                var customerTemp = await _unitOfWork.Repository<CustomerTemp>().Where(x => x.Code == tmp.CustomerCode).AsNoTracking().FirstOrDefaultAsync();
                if (customerTemp != null)
                {
                    customer.FullName = account.FullName;
                    customer.Email = account.Email;
                    customer.Identitycard = account.Email;
                    customer.Bank = account.Bank;
                    customer.BankAccount = account.BankAccount;
                    customer.BankAccountName = account.BankAccountName;
                    customer.BankBranch = account.BankBranch;
                    customer.Birthday = account.Birthday;
                    customer.Address = account.Address;
                    customer.GenderId = account.GenderId;
                    customer.DistrictId = account.DistrictId;
                    customer.ProvinceId = account.ProvinceId;
                    customer.ModifiedBy = account.CreateBy;
                    customer.ModifiedOn = DateTime.Now;
                    var resInfo = JsonConvert.SerializeObject(tmp, Formatting.Indented);
                    _logger.WriteInfoLog($"UpdateCustomerTemp --> resInfo: {resInfo}");
                    _unitOfWork.Repository<CustomerTemp>().Update(customerTemp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                bool result = false;
                var tmp = await _unitOfWork.Repository<AccountPortal>().Where(x => x.Id == id).AsNoTracking().FirstOrDefaultAsync();
                if (tmp != null)
                {
                    tmp.IsDelete = true;
                    var resInfo = JsonConvert.SerializeObject(result, Formatting.Indented);
                    _logger.WriteInfoLog($"DeleteAccount --> resInfo: {resInfo}");
                    _unitOfWork.Repository<AccountPortal>().Update(tmp);
                    await _unitOfWork.SaveChangesAsync();
                    result = true;
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex);
                return false;
            }
        }
    }
}
