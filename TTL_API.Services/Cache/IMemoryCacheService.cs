﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.Services.Cache
{
    public interface IMemoryCacheService
    {
        bool TryGetValue<T>(string key, out T outputValue);
        void SetValueToCache(string key, string value, int expMintues);
        void RemoveValueFromCache(string key);
    }
}
