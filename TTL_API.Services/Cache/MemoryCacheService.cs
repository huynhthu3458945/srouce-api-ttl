﻿using Microsoft.Extensions.Caching.Memory;
using System;
using TTL_API.Services.Logger;

namespace TTL_API.Services.Cache
{
    public class MemoryCacheService : IMemoryCacheService
    {
        private readonly ILoggerService _logger;
        private readonly IMemoryCache _memoryCache;

        public MemoryCacheService(ILoggerService logger, IMemoryCache memoryCache)
        {
            _logger = logger;
            _memoryCache = memoryCache;
        }

        public bool TryGetValue<T>(string key, out T outputValue)
        {
            return _memoryCache.TryGetValue<T>(key, out outputValue);
        }

        public void SetValueToCache(string key, string value, int expMintues)
        {
            var options = new MemoryCacheEntryOptions();
            options.AbsoluteExpiration = DateTime.Now.AddMinutes(expMintues);
            if (_memoryCache.TryGetValue<string>(key, out var valueOutput))
            {
                _memoryCache.Remove(key);
            }
            _memoryCache.Set(key, value, options);
        }

        public void RemoveValueFromCache(string key)
        {
            if (_memoryCache.TryGetValue<string>(key, out var valueOutput))
            {
                _memoryCache.Remove(key);
            }
        }
    }
}
