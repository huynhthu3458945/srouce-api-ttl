﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface ICustomerStatusService
    {
        Task<ResponseList> GetAllAsync(int organizationId, string search, int limit, int page);
        Task<ResponseList> GetAllCustomerStatusCombox(int organizationId);
        Task<CustomerStatus> GetByIdAsync(int id);
        Task<CustomerStatus> GetByCodeAsync(string code);
        Task<CustomerStatus> AddAsync(CustomerStatus CustomerStatus);
        Task<bool> UpdateAsync(CustomerStatus CustomerStatus);
        Task<bool> DeleteAsync(int id);
    }
}
