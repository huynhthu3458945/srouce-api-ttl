﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface ICustomerService
    {
        Task<ResponseList> GetAll(CustomerFitter fitter, int limit, int page);
        Task<ResponseList> GetAllCustomerCombox(int organizationId);
        Task<ResponseList> GetAllByPhoneAsync(int organizationId, string phone, string code);
        Task<ResponseList> GetAllByPhoneStoreAsync(int organizationId, string phone, string code);
        Task<ResponseList> GetListCustomerByPhoneStoreAsync(ViewCustomerFitter viewCustomerFitter, int limit, int page);
        Task<CustomerModel> GetByIdAsync(int id);
        Task<Customer> GetByCodeAsync(string code);
        Task<string> GenerateCode();
        Task<Customer> CheckPhoneExistAsync(int organizationId, string phone);
        Task<Contact> CheckContactExistAsync(string phone, string email);
        Task<CustomerModel> AddAsync(CustomerModel customerModel);
        Task<bool> UpdateAsync(CustomerModel customerModel);
        Task<bool> DeleteAsync(int id);
    }
}
