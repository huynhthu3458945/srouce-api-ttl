﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IContactService
    {
        Task<ResponseList> GetAllCustomerIdAsync(int customerId);
        Task<Contact> GetByIdAsync(int id);
        Task<bool> AddAsync(IEnumerable<Contact> contacts);
        Task<bool> UpdateAsync(IEnumerable<Contact> contacts);
        Task<bool> DeleteAsync(int customerId);
    }
}
