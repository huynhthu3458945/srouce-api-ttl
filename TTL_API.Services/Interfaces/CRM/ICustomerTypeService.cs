﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface ICustomerTypeService
    {
        Task<ResponseList> GetAllAsync(int organizationId, string search, int limit, int page);
        Task<ResponseList> GetAllCustomerTypeCombox(int organizationId);
        Task<CustomerType> GetByIdAsync(int id);
        Task<CustomerType> GetByCodeAsync(string code);
        Task<CustomerType> AddAsync(CustomerType CustomerType);
        Task<bool> UpdateAsync(CustomerType CustomerType);
        Task<bool> DeleteAsync(int id);
    }
}
