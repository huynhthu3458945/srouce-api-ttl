﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.Utils;

namespace TTL_API.Services.Interfaces
{
    public interface IAccountPortalService
    {
        Task<AccountPortalRep> GetByIdAsync(int id);
        Task<AccountPortal> GetAccountAsync(AccountPortal account);
        Task<AccountPortal> GetAccountUserNameAsync(string userName);
        Task<bool> AddAsync(AccountPortalReq accountPortal, ViewCustomer viewCustomer, bool isCreateCustomer);
        Task<bool> UpdateAsync(AccountPortalReq accountPortal);
        Task<bool> DeleteAsync(int id);
    }
}
