﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface ISystmeAccountService
    {
        Task<SystemAccount> GetSystmeAccountAsync(SystemAccount account);
        Task<SystemAccount> GetSystmeAccountUserNameAsync(string userName);
    }
}
