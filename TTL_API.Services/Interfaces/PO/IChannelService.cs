﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IChannelService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllChannelCombox(int organizationId);
        Task<ChannelModel> GetByIdAsync(int id);
        Task<Channel> GetByCodeAsync(string code);
        Task<ChannelModel> AddAsync(ChannelModel SStore);
        Task<bool> UpdateAsync(ChannelModel SStore);
        Task<bool> DeleteAsync(int id);
    }
}
