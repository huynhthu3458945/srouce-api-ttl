﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.Utils;

namespace TTL_API.Services.Interfaces
{
    public interface IOrderService
    {
        Task<ResponseList> GetListOrderByPhoneParent(OrderFitter fitter, int limit, int page);
        Task<ResponseList> GetAllAsync(OrderFitter fitter, int limit, int page);
        Task<OrderModel> GetByIdAsync(int id);
        Task<ResponseList> GetViewByIdAsync(int id);
        Task<OrderModel> GetByCodeAsync(string code);
        Task<string> GenerateCode(string type);
        Task<OrderModel> AddAsync(OrderModel Attribute);
        Task<OrderModel> AddByShareAsync(OrderReq Attribute, int createBy);
        Task<bool> UpdateAsync(OrderModel Attribute);
        Task<string> CheckEndQuantityProductNumber(int id, string OrderStatusCode);
        //Task<string> CreatePayMent(int id, string CreatePayMent);
        Task<bool> UpdateStatusAsync(int id, string OrderStatusCode);
        Task<bool> DeleteAsync(int id);
    }
}
