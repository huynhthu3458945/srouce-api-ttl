﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IRole_FunctionService
    {
        Task<Role_Function> AddAsync(Role_Function role_Function);
        Task<bool> DeleteAsync(int id);
        Task<IEnumerable<Role_Function>> GetAllByRoleIdAsync(int roleId);
        Task<IEnumerable<Role_Function>> GetAllByRoleIdsAsync(List<int> roleIds);
    }
}
