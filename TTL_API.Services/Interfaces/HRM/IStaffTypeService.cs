﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IStaffTypeService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllStaffTypeCombox(int organizationId);
        Task<StaffType> GetByIdAsync(int id);
        Task<StaffType> GetByCodeAsync(string code);
        Task<StaffType> AddAsync(StaffType StaffType);
        Task<bool> UpdateAsync(StaffType StaffType);
        Task<bool> DeleteAsync(int id);
    }
}
