﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IDepartmentService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllDepartmentCombox(int organizationId);
        Task<Department> GetByIdAsync(int id);
        Task<Department> GetByCodeAsync(string code);
        Task<Department> AddAsync(Department Department);
        Task<bool> UpdateAsync(Department Department);
        Task<bool> DeleteAsync(int id);
    }
}
