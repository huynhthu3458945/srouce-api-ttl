﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IRegencyService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllRegencyCombox(int organizationId);
        Task<Regency> GetByIdAsync(int id);
        Task<Regency> GetByCodeAsync(string code);
        Task<Regency> AddAsync(Regency Regency);
        Task<bool> UpdateAsync(Regency Regency);
        Task<bool> DeleteAsync(int id);
    }
}
