﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;

namespace TTL_API.Services.Interfaces
{
    public interface IStaffService
    {
        Task<ResponseList> GetAllAsync(StaffFitter fitter, int limit, int page);
        Task<ResponseList> GetAllStaffCombox(int organizationId);
        Task<StaffModel> GetByIdAsync(int id);
        Task<Account> CheckUserName(string userName);
        Task<Staff> GetStafByAccountId(int id);
        Task<Staff> GetByCodeAsync(string code);
        Task<StaffModel> AddAsync(StaffModel staff);
        Task<bool> UpdateAsync(StaffModel staff);
        Task<bool> DeleteAsync(int id);
    }
}
