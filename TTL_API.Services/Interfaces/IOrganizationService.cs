﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IOrganizationService
    {
        Task<ResponseList> GetAllAsync(int limit, int page);
        Task<Organization> GetByUserNameAsync(string userName);
        Task<Organization> GetByCodeAsync(string code);
        Task<ResponseList> GetAllComboboxAsync();
        Task<Organization> GetByIdAsync(int id);
        Task<Organization> AddAsync(OrganizationModel organization);
        Task<bool> UpdateAsync(OrganizationModel organization);
        Task<bool> DeleteAsync(int id);
    }
}
