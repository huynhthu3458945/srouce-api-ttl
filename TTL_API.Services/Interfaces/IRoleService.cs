﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IRoleService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<Role> GetByIdAsync(int id);
        Task<Role> GetByCodeAsync(string code, int organizationId);
        IEnumerable<Role> GetByOrganizationAsync(int organizationId);
        Task<AccountType> GetAccTypeByUserNameAsync(string userName);
        Task<Account_Organization> GetAccOrgByUserNameAsync(string userName);
        Task<ResponseList> GetAllComboxAsync();
        Task<Role> AddAsync(RoleModel Role);
        Task<bool> UpdateAsync(RoleModel Role);
        Task<bool> DeleteAsync(int id);
        List<FunctionOrganizationModelTree> GetChildren(List<FunctionOrganizationModelTree> comments, int parentId, List<Role_Function> role_Functions);
    }
}
