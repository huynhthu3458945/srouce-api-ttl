﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IProvinceService
    {
        Task<ResponseList> GetAllAsync();
        Task<ResponseList> GetAllComboboxAsync();
        Task<Province> GetByIdAsync(int id);
    }
}
