﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.StoredProcedure;

namespace TTL_API.Services.Interfaces
{
    public interface IStoreProcedureService
    {
        Task<ResponseList> GetImportExportListAsync(ImportExportListFillter fitter, int limit, int page);
        Task<ResponseList> GetSyntheticWarehouse(SyntheticWarehouseFillter fitter, int limit, int page);
    }
}
