﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.StoredProcedure;

namespace TTL_API.Services.Interfaces
{
    public interface ICommonService
    {
        Task<ResponseList> GetGenderAsync();
        Task<ResponseList> GetOrganizationByUserName(string userName);
        Task<ResponseList> GetAllOrganizationByUserName(string userName, int limit, int page);
        Task<ResponseList> GetAllPalletByStoreId(int storeId);
        Task<ResponseList> GetAllBillStatus(int organizationId);
        Task<ResponseList> GetAllOrderStatus(int organizationId);
        Task<ResponseList> GetAllTransport(int organizationId);
        Task<ResponseList> GetAllPayType(int organizationId);
        Task<ResponseList> GetAllProductNumber(int organizationId);
        Task<BillStatus> GetAllBillStatusByCode(string code);
        Task<OrderStatus> GetAllOrderStatusByCode(string code);
        Task<PayType> GetAllPayTypeByCode(string code);
        Task<ResponseList> GetAllConfigPayment();
        Task<ResponseList> GetAllDiscountKind();
        Task<ResponseList> GetAllProductQuantityEnd(Product_StoreProcedureFitter fitter, int limit = 25, int page = 1);
    }
}
