﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IDistrictService
    {
        Task<ResponseList> GetAllByProvinceIdAsync(int provinceId);
        Task<ResponseList> GetAllComboboxByProvinceIdAsync(int provinceId);
        Task<District> GetByIdAsync(int id);
    }
}
