﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.Utils;
using static TTL_API.DbContext.Models.Utils.NganLuongModels;

namespace TTL_API.Services.Interfaces
{
    public interface INganLuongService
    {
        Task<MessageResNganLuong> SetCashoutRequest(WithdrawalMoney value, string culture);
        Task<MessageResNganLuong> CheckCashoutList(CheckWithdrawalStatus value, string culture);
        Task<PaymentResponse> PaymentNganLuong(PaymentRequest value, string culture);
        Task<MessageResNganLuong> CheckPaymentNganLuong(CheckPaymentRequest value, string culture);
        Task<MessageResNganLuong> CheckBalance();
        Task<MessageResNganLuong> CheckPaymentNganLuongV2(string orderCode, string culture);
    }
}
