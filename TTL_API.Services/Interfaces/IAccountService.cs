﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IAccountService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit = 25, int page = 1); 
        Task<ResponseList> GetAllComboxAsync(int organizationId);
        Task<Account> GetByIdAsync(int id);
        Task<Account> GetAccountAsync(Account account);
        Task<Account> GetAccountUserNameAsync(string userName);
        Task<Account> AddAsync(AccountModel accountModel);
        Task<bool> UpdateAsync(AccountModel accountModel);
        Task<bool> DeleteAsync(int id);
        List<RoleModelTree> GetChildren(List<RoleModelTree> comments, int parentId, List<Account_Role> account_Roles);
    }
}
