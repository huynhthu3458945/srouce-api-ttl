﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IAccount_RoleService
    {
        Task<bool> DeleteAsync(int accountId, List<int> rolesCheck);
        Task<IEnumerable<Account_Role>> GetAllByAccountIdAsync(int accountId);
    }
}
