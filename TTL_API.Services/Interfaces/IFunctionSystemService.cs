﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IFunctionSystemService
    {
        Task<ResponseList> GetAllAsync(FunctionSystemFitter fitter, int limit, int page);
        Task<ResponseList> GetMenuAsync(string applicationCode, int organizationId, string userName);
        Task<ResponseList> GetRuleActionAsync(string applicationCode, int organizationId, string userName, string area, string controller, string action);
        Task<ResponseList> GetRuleActionWareHouseAsync(string applicationCode, int organizationId, string userName, string area, string controller);
        Task<ResponseList> GetAllTreeAsync(int id);
        Task<FunctionSystem> GetByIdAsync(int id);
        Task<FunctionSystem> AddAsync(FunctionSystem functionSystem);
        Task<bool> UpdateAsync(FunctionSystem functionSystem);
        Task<bool> DeleteAsync(int id);
        IEnumerable<FunctionSystem> GetAll(int applicationId);
        List<FunctionSystemModelTree> GetChildren(List<FunctionSystemModelTree> comments, int parentId, List<Function_Organization> function_OrganizationsList);
    }
}
