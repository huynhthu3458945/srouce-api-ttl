﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IStoreService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllStoreCombox(int organizationId);
        Task<ResponseList> GetAllStoreNumberCombox();
        Task<ResponseList> GetAllStoreIsPalletCombox(int organizationId);
        Task<StoreModel> GetByIdAsync(int id);
        Task<DbContext.Models.Store> GetByCodeAsync(string code);
        Task<StoreModel> AddAsync(StoreModel SStore);
        Task<bool> UpdateAsync(StoreModel SStore);
        Task<bool> DeleteAsync(int id);
    }
}
