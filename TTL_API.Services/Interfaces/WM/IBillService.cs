﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IBillService
    {
        Task<ResponseList> GetAllAsync(BillFitter fitter, int limit, int page);
        Task<BillModel> GetByIdAsync(int id);
        Task<Bill> GetByCodeAsync(string code);
        Task<string> GenerateCode(string type);
        Task<BillModel> AddAsync(BillModel Attribute);
        Task<bool> UpdateAsync(BillModel Attribute);
        Task<bool> UpdateStatusAsync(int id, string billStatusCode);
        Task<bool> DeleteAsync(int id);
        Task<ResponseList> GetAllProductNumber(BillModel billModel);
    }
}
