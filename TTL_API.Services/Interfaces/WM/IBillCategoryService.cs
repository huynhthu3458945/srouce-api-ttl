﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IBillCategoryService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllBillCategoryCombox(int organizationId);
        Task<BillCategory> GetByIdAsync(int id);
        Task<BillCategory> GetByCodeAsync(string code);
        Task<BillCategory> AddAsync(BillCategory BillCategory);
        Task<bool> UpdateAsync(BillCategory BillCategory);
        Task<bool> DeleteAsync(int id);
    }
}
