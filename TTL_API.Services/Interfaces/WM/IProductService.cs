﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IProductService
    {
        Task<ResponseList> GetAllAsync(ProductFitter fitter, int limit, int page);
        Task<ResponseList> GetAllProductCombox(int organizationId, int productTypeId);
        Task<ResponseList> GetAllProductIsInventoryCombox(int organizationId, int productTypeId);
        Task<ProductModel> GetByIdAsync(int id);
        Task<Product> GetByCodeAsync(string code);
        Task<ProductModel> AddAsync(ProductModel Product);
        Task<bool> UpdateAsync(ProductModel Product);
        Task<bool> DeleteAsync(int id);
    }
}
