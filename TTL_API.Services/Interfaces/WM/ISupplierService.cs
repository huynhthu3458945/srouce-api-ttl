﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface ISupplierService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllSupplierCombox(int organizationId);
        Task<Supplier> GetByIdAsync(int id);
        Task<Supplier> GetByCodeAsync(string code);
        Task<Supplier> AddAsync(Supplier Supplier);
        Task<bool> UpdateAsync(Supplier Supplier);
        Task<bool> DeleteAsync(int id);
    }
}
