﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IUnitService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllUnitCombox(int organizationId);
        Task<Unit> GetByIdAsync(int id);
        Task<Unit> GetByCodeAsync(string code);
        Task<Unit> AddAsync(Unit Unit);
        Task<bool> UpdateAsync(Unit Unit);
        Task<bool> DeleteAsync(int id);
    }
}
