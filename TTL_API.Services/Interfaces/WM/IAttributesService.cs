﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IAttributesService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllAttributesCombox(int organizationId);
        Task<Attributes> GetByIdAsync(int id);
        Task<Attributes> GetByCodeAsync(string code);
        Task<Attributes> AddAsync(Attributes Attribute);
        Task<bool> UpdateAsync(Attributes Attribute);
        Task<bool> DeleteAsync(int id);
    }
}
