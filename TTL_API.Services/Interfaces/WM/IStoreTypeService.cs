﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IStoreTypeService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllStoreTypeCombox(int organizationId);
        Task<StoreType> GetByIdAsync(int id);
        Task<StoreType> GetByCodeAsync(string code);
        Task<StoreType> AddAsync(StoreType StoreType);
        Task<bool> UpdateAsync(StoreType StoreType);
        Task<bool> DeleteAsync(int id);
    }
}
