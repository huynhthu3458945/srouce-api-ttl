﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IComboService
    {
        Task<ResponseList> GetAllAsync(ComboFitter fitter, int limit, int page);
        Task<ResponseList> GetAllComboCombox(int organizationId);
        Task<ComboModel> GetByIdAsync(int id);
        Task<Combo> GetByCodeAsync(string code);
        Task<ComboModel> AddAsync(ComboModel Combo);
        Task<bool> UpdateAsync(ComboModel Combo);
        Task<bool> DeleteAsync(int id);
    }
}
