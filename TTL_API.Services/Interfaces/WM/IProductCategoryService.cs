﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IProductCategoryService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllProductCategoryCombox(int organizationId);
        Task<ProductCategory> GetByIdAsync(int id);
        Task<ProductCategory> GetByCodeAsync(string code);
        Task<ProductCategory> AddAsync(ProductCategory ProductCategory);
        Task<bool> UpdateAsync(ProductCategory ProductCategory);
        Task<bool> DeleteAsync(int id);
    }
}
