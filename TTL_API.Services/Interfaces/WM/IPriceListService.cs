﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IPriceListService
    {
        Task<ResponseList> GetAllAsync(PriceListFitter fitter, int limit, int page);
        Task<ResponseList> GetAllPriceListCombox(int organizationId);
        Task<ResponseList> GetAllPriceAgencyListCombox(int organizationId);
        Task<PriceListModel> GetByIdAsync(int id);
        Task<PriceList> GetByCodeAsync(string code);
        Task<PriceListModel> AddAsync(PriceListModel Attribute);
        Task<bool> UpdateAsync(PriceListModel Attribute);
        Task<bool> DeleteAsync(int id);
    }
}
