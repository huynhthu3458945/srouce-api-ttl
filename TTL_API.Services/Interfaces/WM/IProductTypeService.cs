﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IProductTypeService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllProductTypeCombox(int organizationId);
        Task<ProductType> GetByIdAsync(int id);
        Task<ProductType> GetByCodeAsync(string code);
        Task<ProductType> AddAsync(ProductType ProductType);
        Task<bool> UpdateAsync(ProductType ProductType);
        Task<bool> DeleteAsync(int id);
    }
}
