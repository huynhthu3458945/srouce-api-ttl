﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IUnitConvertedService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int limit, int page);
        Task<ResponseList> GetAllUnitConvertedCombox(int organizationId);
        Task<UnitConverted> GetByIdAsync(int id);
        Task<UnitConverted> GetByBasicUnitId(int basicUnitId);
        Task<UnitConverted> GetByCodeAsync(string code);
        Task<UnitConverted> AddAsync(UnitConverted UnitConverted);
        Task<bool> UpdateAsync(UnitConverted UnitConverted);
        Task<bool> DeleteAsync(int id);
    }
}
