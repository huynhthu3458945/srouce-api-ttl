﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IInventoryService
    {
        Task<ResponseList> GetAllAsync(InventoryFitter fitter, int limit, int page);
        Task<InventoryModel> GetByIdAsync(int id);
        Task<Inventory> GetByCodeAsync(string code);
        Task<string> GenerateCode(string type);
        Task<InventoryModel> AddAsync(InventoryModel inventoryModel);
        Task<bool> UpdateAsync(InventoryModel inventoryModel);
        Task<bool> DeleteAsync(int id);
        Task<bool> Balance(int inventoryId);
    }
}
