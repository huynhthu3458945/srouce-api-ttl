﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IPalletService
    {
        Task<ResponseList> GetAllAsync(int organizationId, int storeId, string search, int limit, int page);
        Task<ResponseList> GetAllPalletProduct(PalletProductFitter fitter, int limit, int page);
        Task<ResponseList> GetAllPalletCombox(int organizationId, int storeId);
        Task<Pallet> GetByIdAsync(int id);
        Task<Pallet> GetByCodeAsync(string code);
        Task<Store_Pallet_ProductModel> GetPalletProductAsync(PalletProductFitter fitter);
        Task<Pallet> AddAsync(PalletModel SPallet);
        Task<Store_Pallet_Product> AddPalletProductAsync(Store_Pallet_Product store_Pallet_Product);
        Task<bool> UpdateAsync(PalletModel SPallet);
        Task<bool> DeleteAsync(int id);
        Task<bool> DeletePalletProductAsync(int id);
    }
}
