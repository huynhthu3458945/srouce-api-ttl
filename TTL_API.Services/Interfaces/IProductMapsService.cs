﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IProductMapsService
    {
        Task<ResponseList> GetAllAsync(ProductMapsFitter fitter, int limit = 25, int page = 1); 
        Task<ProductMaps> GetByIdAsync(int id);
        Task<ProductMaps> GetByProductMapsbyPorductId(int productId);
        Task<ProductMaps> AddAsync(ProductMaps ProductMaps);
        Task<bool> UpdateAsync(ProductMaps ProductMaps);
        Task<bool> DeleteAsync(int id);
    }
}
