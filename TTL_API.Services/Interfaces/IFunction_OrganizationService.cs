﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IFunction_OrganizationService
    {
        IEnumerable<Function_Organization> GetById(int organizationId, int applicationId);
        Task<bool> AddAsync(IEnumerable<Function_Organization> function_OrganizationList);
        Task<bool> UpdateAsync(IEnumerable<Function_Organization> function_OrganizationList);
        Task<bool> DeleteAsync(int organizationId);
        Task<bool> UpdateIsDeleteAsync(List<Function_Organization> listNotCheck, bool isDelete);
    }
}
