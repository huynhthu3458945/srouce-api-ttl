﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IClientService
    {
        Task<ResponseList> GetAllAsync(int limit, int page);
        Task<Client> GetByIdAsync(int id);
        Task<ResponseList> GetAllComboxAsync();
        Task<Client> AddAsync(Client client);
        Task<bool> UpdateAsync(Client client);
        Task<bool> DeleteAsync(int id);
    }
}
