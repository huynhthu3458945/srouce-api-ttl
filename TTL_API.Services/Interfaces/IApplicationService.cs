﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models;

namespace TTL_API.Services.Interfaces
{
    public interface IApplicationService
    {
        Task<ResponseList> GetAllAsync(int limit, int page);
        Task<ResponseList> GetAllComboxAsync();
        Task<Application> GetByIdAsync(int id);
        Task<Application> AddAsync(Application application);
        Task<bool> UpdateAsync(Application application);
        Task<bool> DeleteAsync(int id);
    }
}
