﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TTL_API.DbContext.Base;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;

namespace TTL_API.DbContext
{
	public static class DbContextDI
	{
		public static IServiceCollection ConfigureDbContext(this IServiceCollection services, string connectString)
		{
			services.AddScoped<IContext, TTL_APIContext>();
			services.AddScoped(typeof(IGenericDbContext<>), typeof(GenericDbContext<>));
			services.AddDbContext<TTL_APIContext>(options => options.UseSqlServer(connectString, o => o.CommandTimeout(180)));
			return services;
		}
	}
}
