ALTER PROC [dbo].[SP_API_AccountHelper]
(
	@Action  VARCHAR(40) = NULL
	, @Phone VARCHAR(50) = NULL
	, @Username VARCHAR(40) = NULL
	, @PassOld VARCHAR(100) = NULL
	, @PassNew VARCHAR(100) = NULL
	, @OTP VARCHAR(10) = NULL
	, @Email VARCHAR(40) = NULL
	, @IsVN bit = NULL
	, @RetCode int OUTPUT
	, @RetText NVARCHAR(80) OUTPUT
) AS BEGIN
	DECLARE @IDNo NVARCHAR(20)
				, @ActCode NVARCHAR(20)
				, @Content NVARCHAR(MAX)
				, @RespontSMS NVARCHAR(250)
				, @BodyJson AS VARCHAR(8000)	
				, @SetupOTP NVARCHAR(50)
				, @ServiceSMS NVARCHAR(50)
	 set @SetupOTP='otp';
 	 --set @ServiceSMS='ESMS';
		set @ServiceSMS='VIETGUYS';
	
	IF @Action = 'FORGET.ACCOUNT' BEGIN
		IF NOT EXISTS (SELECT u.userName FROM AccountPortal u WHERE u.userName = @Phone) BEGIN
			SELECT @RetCode = 1, @RetText = N'Số điện thoại không tồn tại trong hệ thống'
			RETURN
		END

		IF (SELECT COUNT(*) FROM M_SMS s WHERE s.PhoneNo = @Phone AND CAST(s.DateInput as Date) = CAST(GETDATE() as Date)) > 5 BEGIN
			SELECT @RetCode = 2, @RetText = N'Số điện thoại này đã gửi 5 lần, bạn vui lòng quay lại ngày hôm sau'
			RETURN
		END
			 if @SetupOTP='otp' begin
				SET @OTP = CONVERT(VARCHAR(20), FLOOR(RAND(CHECKSUM(NEWID()))*(9999-1000+1)+1000))
				IF @ServiceSMS = 'ESMS'
				BEGIN
				SET @Content = 'TAM-TRI-LUC Ma OTP xac thuc so dien thoai la ' + @OTP + ', thoi han 5 phut.'
				SET @BodyJson = dbo.fn_sms_content(@Phone, @Content)
				EXEC SP_SEND_SMS @URL = 'https://agency.sieutrinhohocduong.com/api/SMS', @Body = @BodyJson, @RetText = @RespontSMS OUTPUT
				END
				ELSE
				BEGIN
				SET @Content = 'TAM-TRI-LUC Ma OTP xac thuc HitaFun la ' + @OTP + ', hieu luc trong 5 phut'
				SET @BodyJson = dbo.fn_sms_content(@Phone, @Content)
				EXEC SP_SEND_SMS @URL = 'https://agency.sieutrinhohocduong.com/api/SMSVietGuys', @Body = @BodyJson, @RetText = @RespontSMS OUTPUT
				END

				EXEC [SP_SMS] @Action = 'SENDOPT', @Type = 1, @PhoneNo = @Phone, @Content = @Content, @OTP = @OTP, @Username = '', @FullName = '', @ResultContent = @RespontSMS
				SELECT @RetCode = 0, @RetText = N'OTP đã được gửi đến số điện thoại: ' + @Phone
				Select @Phone as 'Phone'		
			end
			
		else begin
			SELECT @RetCode = 0, @RetText = N'OTP đã được gửi đến số điện thoại: ' + @Phone
			Select @Phone as 'Phone'
		end
		
	END
	ELSE IF @Action = 'CONFIRM.ACCOUNT' BEGIN	
		
		 if @SetupOTP='otp' begin
			IF ISNULL(@OTP, '') = '' BEGIN
				SELECT @RetCode = 1, @RetText = N'Kích hoạt thất bại, vui lòng nhập mã OTP'
				RETURN
			END
			IF NOT EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP) BEGIN
				SELECT @RetCode = 2, @RetText = N'Mã OTP: ' + @OTP + N' không tồn tại'
				RETURN
			END
			IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP AND GETDATE() > DateInput) BEGIN
				SELECT @RetCode = 3, @RetText = N'OTP ' + @OTP + N' đã hết hiệu lực'
				RETURN
			END
			IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP AND s.IsConfirm = 1) BEGIN
				SELECT @RetCode = 4, @RetText = N'OTP ' + @OTP + N' đã hết hiệu lực'
				RETURN
			END		
			update M_SMS set IsConfirm=1 where PhoneNo = @Phone AND OTP = @OTP;
		end
		SELECT t.Id
				, t.userName
				, '' as fullName
				, t.[Password]
		FROM AccountPortal t
		where t.userName = @Phone
		SELECT @RetCode = 0, @RetText = N'Danh sách tài khoản'
	END
	ELSE IF @Action = 'FORGET.PASSWORD' BEGIN
		if @IsVN=0 and @Email is not null begin
			IF NOT EXISTS (SELECT u.userName FROM AccountPortal u WHERE u.email = @Email AND (@Phone IS NULL OR u.userName = @Phone)) and
				NOT EXISTS (SELECT u.Id FROM Customer u WHERE u.email = @Email AND (@Phone IS NULL OR u.phone = @Phone)) BEGIN
				SELECT @RetCode = 1, @RetText = N'Email không tồn tại trong hệ thống'
				RETURN
			END
			IF (SELECT COUNT(*) FROM M_SMS s WHERE s.PhoneNo = @Email AND CAST(s.DateInput as Date) = CAST(GETDATE() as Date)) > 5 BEGIN
				SELECT @RetCode = 2, @RetText = N'Email này đã gửi 5 lần, bạn vui lòng quay lại ngày hôm sau'
				RETURN
			END
			SET @OTP = CONVERT(VARCHAR(20), FLOOR(RAND(CHECKSUM(NEWID()))*(9999-1000+1)+1000))
			SET @Content = 'TAM-TRI-LUC Ma OTP xac thuc HitaFun la ' + @OTP + ', hieu luc trong 5 phut'
			EXEC [SP_SMS] @Action = 'SENDOPT', @Type = 1, @PhoneNo = @Email, @Content = @Content, @OTP = @OTP, @Username = '', @FullName = '', @ResultContent = @RespontSMS
			SELECT @RetCode = 0, @RetText =@Content
		end else begin		
		IF NOT EXISTS (SELECT u.userName FROM AccountPortal u WHERE u.userName = @Phone AND (@Email is null or email IS NULL OR u.email = @Email) ) BEGIN
			SELECT @RetCode = 1, @RetText = N'Số điện thoại không tồn tại trong hệ thống'
			RETURN
		END		
			IF (SELECT COUNT(*) FROM M_SMS s WHERE s.PhoneNo = @Phone AND CAST(s.DateInput as Date) = CAST(GETDATE() as Date)) > 5 BEGIN
				SELECT @RetCode = 2, @RetText = N'Số điện thoại này đã gửi 5 lần, bạn vui lòng quay lại ngày hôm sau'
				RETURN
			END
			if @SetupOTP='otp' begin
				SET @OTP = CONVERT(VARCHAR(20), FLOOR(RAND(CHECKSUM(NEWID()))*(9999-1000+1)+1000))
				IF @ServiceSMS = 'ESMS'
				BEGIN
				SET @Content = 'TAM-TRI-LUC Ma OTP xac thuc so dien thoai la ' + @OTP + ', thoi han 5 phut.'
				SET @BodyJson = dbo.fn_sms_content(@Phone, @Content)
				EXEC SP_SEND_SMS @URL = 'https://agency.sieutrinhohocduong.com/api/SMS', @Body = @BodyJson, @RetText = @RespontSMS OUTPUT
				END
				ELSE
				BEGIN
				SET @Content = 'TAM-TRI-LUC Ma OTP xac thuc HitaFun la ' + @OTP + ', hieu luc trong 5 phut'
				SET @BodyJson = dbo.fn_sms_content(@Phone, @Content)
				EXEC SP_SEND_SMS @URL = 'https://agency.sieutrinhohocduong.com/api/SMSVietGuys', @Body = @BodyJson, @RetText = @RespontSMS OUTPUT
				END

				EXEC [SP_SMS] @Action = 'SENDOPT', @Type = 1, @PhoneNo = @Phone, @Content = @Content, @OTP = @OTP, @Username = '', @FullName = '', @ResultContent = @RespontSMS
				SELECT @RetCode = 0, @RetText = N'OTP đã được gửi đến số điện thoại: ' + @Phone
				SELECT @Phone as 'Phone', @Username as 'UserName'		
			end
			else begin
			SELECT @RetCode = 0, @RetText = N'OTP đã được gửi đến số điện thoại: ' + @Phone
			Select @Phone as 'Phone'
		end
		end
	END
	ELSE IF @Action = 'CONFIRM.ACCOUNT.PASSWORD' BEGIN	
		if @SetupOTP='otp' begin
			IF ISNULL(@OTP, '') = '' BEGIN
				SELECT @RetCode = 1, @RetText = N'Kích hoạt thất bại, vui lòng nhập mã OTP'
				RETURN
			END
			IF NOT EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP) BEGIN
				SELECT @RetCode = 2, @RetText = N'Mã OTP: ' + @OTP + N' không tồn tại'
				RETURN
			END
			IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP AND GETDATE() > DateInput) BEGIN
				SELECT @RetCode = 3, @RetText = N'OTP ' + @OTP + N' đã hết hiệu lực'
				RETURN
			END
			IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP AND s.IsConfirm = 1) BEGIN
				SELECT @RetCode = 4, @RetText = N'OTP ' + @OTP + N' đã hết hiệu lực'
				RETURN
			END
			update M_SMS set IsConfirm=1 where PhoneNo = @Phone AND OTP = @OTP;
		end
		SELECT t.Id
				, t.userName
				, '' as fullName
				, t.[Password]
		FROM AccountPortal t
		where t.userName = @Phone or ( t.email = @Phone and @IsVN = 0)
		SELECT @RetCode = 0, @RetText = N'Danh sách tài khoản'
	END
	ELSE IF @Action = 'COMFIRM.OTP' BEGIN
		IF ISNULL(@OTP, '') = '' BEGIN
			SELECT @RetCode = 1, @RetText = N'Mã OTP chưa nhập'
			RETURN
		END
		IF NOT EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP) BEGIN
			SELECT @RetCode = 2, @RetText = N'Mã OTP: ' + @OTP + N' không tồn tại'
			RETURN
		END
		IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP AND GETDATE() > DateInput) BEGIN
			SELECT @RetCode = 3, @RetText = N'OTP ' + @OTP + N' đã hết hiệu lực'
			RETURN
		END
		IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP AND s.IsConfirm = 1) BEGIN
			SELECT @RetCode = 4, @RetText = N'OTP ' + @OTP + N' đã được sử dụng'
			RETURN
		END
		SELECT @RetCode = 0, @RetText = N'OTP hợp lệ'
	END
	IF @Action = 'SEND.OTP' BEGIN
	
	if @Email is not null BEGIN
			if not exists ( select u.userName from AccountPortal u where u.email = @Email and u.IsDelete != 'true' and u.CountryCode like '%84%' )
			BEGIN SET @IsVN = 0; END
			else BEGIN SET @IsVN = 1; END
	END
	
	if @IsVN=0 and @Email is not null begin
			IF NOT EXISTS (SELECT u.userName FROM AccountPortal u WHERE u.email = @Email) and
				NOT EXISTS (SELECT u.Id FROM Customer u WHERE u.email = @Email) BEGIN
				SELECT @RetCode = 1, @RetText = N'Email không tồn tại trong hệ thống'
				RETURN
			END
			IF (SELECT COUNT(*) FROM M_SMS s WHERE s.PhoneNo = @Email AND CAST(s.DateInput as Date) = CAST(GETDATE() as Date)) > 5 BEGIN
				SELECT @RetCode = 2, @RetText = N'Email này đã gửi 5 lần, bạn vui lòng quay lại ngày hôm sau'
				RETURN
			END
			SET @OTP = CONVERT(VARCHAR(20), FLOOR(RAND(CHECKSUM(NEWID()))*(999999-100000+1)+100000))
			SET @Content = 'TAM-TRI-LUC Ma OTP xac thuc HitaFun la ' + @OTP + ', hieu luc trong 5 phut'
			EXEC [SP_SMS] @Action = 'SENDOPT', @Type = 1, @PhoneNo = @Email, @Content = @Content, @OTP = @OTP, @Username = '', @FullName = '', @ResultContent = @RespontSMS
			select @IsVn as IsVN , @Phone as Phone , @Email as Email
			SELECT @RetCode = 0, @RetText = @content
			RETURN
		end
		IF NOT EXISTS (SELECT u.userName FROM AccountPortal u WHERE u.userName = @Phone) BEGIN
			SELECT @RetCode = 1, @RetText = N'Số điện thoại không tồn tại trong hệ thống'
			RETURN
		END

		IF (SELECT COUNT(*) FROM M_SMS s WHERE s.PhoneNo = @Phone AND CAST(s.DateInput as Date) = CAST(GETDATE() as Date)) > 5 BEGIN
			SELECT @RetCode = 2, @RetText = N'Số điện thoại này đã gửi 5 lần, bạn vui lòng quay lại ngày hôm sau'
			RETURN
		END
			 if @SetupOTP='otp' begin
				SET @OTP = CONVERT(VARCHAR(20), FLOOR(RAND(CHECKSUM(NEWID()))*(999999-100000+1)+100000))
				IF @ServiceSMS = 'ESMS'
				BEGIN
				SET @Content = 'TAM-TRI-LUC Ma OTP xac thuc so dien thoai la ' + @OTP + ', thoi han 5 phut.'
				SET @BodyJson = dbo.fn_sms_content(@Phone, @Content)
				EXEC SP_SEND_SMS @URL = 'https://agency.sieutrinhohocduong.com/api/SMS', @Body = @BodyJson, @RetText = @RespontSMS OUTPUT
				END
				ELSE
				BEGIN
				SET @Content = 'TAM-TRI-LUC Ma OTP xac thuc HitaFun la ' + @OTP + ', hieu luc trong 5 phut'
				SET @BodyJson = dbo.fn_sms_content(@Phone, @Content)
				EXEC SP_SEND_SMS @URL = 'https://agency.sieutrinhohocduong.com/api/SMSVietGuys', @Body = @BodyJson, @RetText = @RespontSMS OUTPUT
				END

				EXEC [SP_SMS] @Action = 'SENDOPT', @Type = 1, @PhoneNo = @Phone, @Content = @Content, @OTP = @OTP, @Username = '', @FullName = '', @ResultContent = @RespontSMS
				SELECT @RetCode = 0, @RetText = N'OTP đã được gửi đến số điện thoại: ' + @Phone
				Select @Phone as 'Phone'		
			end
			
		else begin
			SELECT @RetCode = 0, @RetText = N'OTP đã được gửi đến số điện thoại: ' + @Phone
			Select @Phone as 'Phone'
		end
		
	END
	ELSE IF @Action = 'CHANGE.PASSWORD' BEGIN
		IF ISNULL(@OTP, '') = '' BEGIN
			SELECT @RetCode = 1, @RetText = N'Mã OTP chưa nhập'
			RETURN
		END
		
		if @Email is not null BEGIN
			if not exists ( select u.userName from AccountPortal u where u.email = @Email and u.IsDelete != 'true' and u.CountryCode like '%84%' )
			BEGIN SET @IsVN = 0; END
			else BEGIN SET @IsVN = 1; END
		END
		
		if @IsVN=1 
		BEGIN
	IF NOT EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP) BEGIN
			SELECT @RetCode = 2, @RetText = N'Mã OTP: ' + @OTP + N' không tồn tại'
			RETURN
		END
		IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP AND GETDATE() > DateInput) BEGIN
			SELECT @RetCode = 3, @RetText = N'OTP ' + @OTP + N' đã hết hiệu lực'
			RETURN
		END
		IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP AND s.IsConfirm = 1) BEGIN
			SELECT @RetCode = 4, @RetText = N'OTP ' + @OTP + N' đã được sử dụng'
			RETURN
		END
		END
		ELSE IF @IsVN = 0
		BEGIN
			IF NOT EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Email AND s.OTP = @OTP) BEGIN
			SELECT @RetCode = 2, @RetText = N'Mã OTP: ' + @OTP + N' không tồn tại'
			RETURN
		END
		IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Email AND s.OTP = @OTP AND GETDATE() > DateInput) BEGIN
			SELECT @RetCode = 3, @RetText = N'OTP ' + @OTP + N' đã hết hiệu lực'
			RETURN
		END
		IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Email AND s.OTP = @OTP AND s.IsConfirm = 1) BEGIN
			SELECT @RetCode = 4, @RetText = N'OTP ' + @OTP + N' đã được sử dụng'
			RETURN
		END
		END
		
		IF NOT EXISTS (SELECT * FROM AccountPortal s WHERE s.UserName = @Phone AND s.Password = @PassOld ) BEGIN
			SELECT @RetCode = 5, @RetText = N'Mật khẩu không đúng'
			RETURN
		END 
		update M_SMS set IsConfirm=1 where PhoneNo = @Phone AND OTP = @OTP;
		update AccountPortal set Password = @PassNew
		where UserName = @Phone
		SELECT @RetCode = 0, @RetText = N'Cập nhật thành công'
	END
	ELSE IF @Action = 'SEND.OTP.CREATE' BEGIN
	IF EXISTS (SELECT u.userName FROM AccountPortal u WHERE u.userName = @Phone)
			BEGIN
				SELECT @RetCode = 1, @RetText = N'Số điện thoại đã tồn tại trong hệ thống'
				RETURN
			END
	IF EXISTS (SELECT u.userName FROM AccountPortal u WHERE u.Email = @Email)
			BEGIN
				SELECT @RetCode = 1, @RetText = N'Email đã tồn tại trong hệ thống'
				RETURN
			END
		if @IsVN=0 and @Email is not null begin
			IF (SELECT COUNT(*) FROM M_SMS s WHERE s.PhoneNo = @Email AND CAST(s.DateInput as Date) = CAST(GETDATE() as Date)) > 5 BEGIN
				SELECT @RetCode = 2, @RetText = N'Email này đã gửi 5 lần, bạn vui lòng quay lại ngày hôm sau'
				RETURN
			END
			SET @OTP = CONVERT(VARCHAR(20), FLOOR(RAND(CHECKSUM(NEWID()))*(999999-100000+1)+100000))
			SET @Content = 'TAM-TRI-LUC Ma OTP xac thuc HitaFun la ' + @OTP + ', hieu luc trong 5 phut'
			EXEC [SP_SMS] @Action = 'SENDOPT', @Type = 1, @PhoneNo = @Email, @Content = @Content, @OTP = @OTP, @Username = '', @FullName = '', @ResultContent = @RespontSMS
			SELECT @RetCode = 0, @RetText =@Content
		end else begin
		IF (SELECT COUNT(*) FROM M_SMS s WHERE s.PhoneNo = @Phone AND CAST(s.DateInput as Date) = CAST(GETDATE() as Date)) > 5 BEGIN
			SELECT @RetCode = 2, @RetText = N'Số điện thoại này đã gửi 5 lần, bạn vui lòng quay lại ngày hôm sau'
			RETURN
		END
		if @SetupOTP='otp' begin	
			SET @OTP = CONVERT(VARCHAR(20), FLOOR(RAND(CHECKSUM(NEWID()))*(999999-100000+1)+100000))
			IF @ServiceSMS = 'ESMS'
			BEGIN
			SET @Content = 'TAM-TRI-LUC Ma OTP xac thuc so dien thoai la ' + @OTP + ', thoi han 5 phut.'
			SET @BodyJson = dbo.fn_sms_content(@Phone, @Content)
			EXEC SP_SEND_SMS @URL = 'https://agency.sieutrinhohocduong.com/api/SMS', @Body = @BodyJson, @RetText = @RespontSMS OUTPUT
			END
			ELSE
			BEGIN
			SET @Content = 'TAM-TRI-LUC Ma OTP xac thuc HitaFun la ' + @OTP + ', hieu luc trong 5 phut'
			SET @BodyJson = dbo.fn_sms_content(@Phone, @Content)
			EXEC SP_SEND_SMS @URL = 'https://agency.sieutrinhohocduong.com/api/SMSVietGuys', @Body = @BodyJson, @RetText = @RespontSMS OUTPUT
			END
		
			EXEC [SP_SMS] @Action = 'SENDOPT', @Type = 1, @PhoneNo = @Phone, @Content = @Content, @OTP = @OTP, @Username = '', @FullName = '', @ResultContent = @RespontSMS
			SELECT @RetCode = 0, @RetText = N'OTP đã được gửi đến số điện thoại: ' + @Phone
			Select @Phone as 'Phone'		
		end
			
	else begin
		SELECT @RetCode = 0, @RetText = N'OTP đã được gửi đến số điện thoại: ' + @Phone
		Select @Phone as 'Phone'
	end
		end
	END
	ELSE IF @Action = 'COMFIRM.OTP.CREATE' BEGIN
		IF ISNULL(@OTP, '') = '' BEGIN
			SELECT @RetCode = 1, @RetText = N'Mã OTP chưa nhập'
			RETURN
		END
		IF NOT EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP) BEGIN
			SELECT @RetCode = 2, @RetText = N'Mã OTP: ' + @OTP + N' không tồn tại'
			RETURN
		END
		IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP AND GETDATE() > DateInput) BEGIN
			SELECT @RetCode = 3, @RetText = N'OTP ' + @OTP + N' đã hết hiệu lực'
			RETURN
		END
		IF EXISTS (SELECT * FROM M_SMS s WHERE s.PhoneNo = @Phone AND s.OTP = @OTP AND s.IsConfirm = 1) BEGIN
			SELECT @RetCode = 4, @RetText = N'OTP ' + @OTP + N' đã được sử dụng'
			RETURN
		END
		update M_SMS set IsConfirm=1 where PhoneNo = @Phone AND OTP = @OTP;
		SELECT @RetCode = 0, @RetText = N'OTP hợp lệ'
	END
END