﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class District
    {
        public int Id { get; set; }
        public string Prefix { get; set; }
        public string Name { get; set; }
        public int ProvinceId { get; set; }
    }
}
