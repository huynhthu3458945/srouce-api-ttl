﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Contact : BaseEntitySystem
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public bool IsKey { get; set; }
    }
}
