﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Customer : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Birthday { get; set; }
        public int GenderId { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public int DistrictId { get; set; }
        public int ProvinceId { get; set; }
        public bool IsCompany { get; set; }
        public string CompanyName { get; set; }
        public string ParentName { get; set; }
        public string ParentPhone { get; set; }
        public string ParentEmail { get; set; }
        public int CustomerTypeId { get; set; }
        public int CustomerStatusId { get; set; }
        public int OrganizationId { get; set; }
        public int AccountId { get; set; }
        public int Ref_Id { get; set; }
        public string Ref_Code { get; set; }
        public string Ref_Name { get; set; }
        public string Ref_Source { get; set; }
        public string Note { get; set; }
        public string Identitycard { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccount { get; set; }
        public string CityBank { get; set; }
    }

    public class CustomerModel : Customer
    {
        public string Ref_Phone { get; set; }
        public string DistrictName { get; set; }
        public string ProvinceName { get; set; }
        public string GenderName { get; set; }
        public string CustomerStatusName { get; set; }
        public string CustomerTypeName { get; set; }
        public string OrganizationName { get; set; }
        public IEnumerable<Contact> ContactList { get; set; }
    }

    public class CustomerFitter
    {
        public int CreateBy { get; set; }
        public int CustomerTypeId { get; set; }
        public int CustomerStatusId { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int OrganizationId { get; set; }
    }

    public class ViewCustomer 
    {
        public int Id { get; set; }
        public string Birthday { get; set; }
        public int GenderId { get; set; }
        public string GenderName { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int Ref_Id { get; set; }
        public string Ref_Code { get; set; }
        public string Ref_Source { get; set; }
        public string Ref_Code_ERP { get; set; }
        public string Ref_Phone { get; set; }
        public string Ref_Email { get; set; }
        public string Ref_Source_Parent { get; set; }
        public string Identitycard { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccount { get; set; }
        public string CityBank { get; set; }
        public string Address { get; set; }
        public int DistrictId { get; set; }
        public int ProvinceId { get; set; }
    }
    public class ViewCustomerFitter
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int OrganizationId { get; set; }
    }
}
