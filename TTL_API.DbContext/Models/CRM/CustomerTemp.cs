﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class CustomerTemp : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Birthday { get; set; }
        public int GenderId { get; set; }
        public string Phone { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
        public bool IsCompany { get; set; }
        public string CompanyName { get; set; }
        public string ParentName { get; set; }
        public string ParentPhone { get; set; }
        public string ParentEmail { get; set; }
        public int CustomerTypeId { get; set; }
        public int OrganizationId { get; set; }
        public int Ref_Id { get; set; }
        public string Ref_Code { get; set; }
        public string Ref_Name { get; set; }
        public string Ref_Source { get; set; }
        public string Note { get; set; }
        public int User_id { get; set; }
        public string Identitycard { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccount { get; set; }
        public string CityBank { get; set; }
    }

    public class CustomerTempModel : CustomerTemp
    {
        public string GenderName { get; set; }
        public string CustomerTypeName { get; set; }
        public string OrganizationName { get; set; }
    }
}
