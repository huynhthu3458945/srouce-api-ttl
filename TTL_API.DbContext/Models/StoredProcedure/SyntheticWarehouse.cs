﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.StoredProcedure
{
    public class SyntheticWarehouse
    {
        public string Code { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string UnitName { get; set; }

        public decimal QuantityBegin { get; set; }
        public decimal PriceBegin { get; set; }
        public decimal TotalBegin { get; set; }

        public decimal QuantityImport { get; set; }
        public decimal CostPrice { get; set; }
        public decimal TotalImport { get; set; }

        public decimal QuantityExport { get; set; }
        public decimal Price { get; set; }
        public decimal TotalExport { get; set; }

        public decimal QuantityEnd { get; set; }
        public decimal PriceEnd { get; set; }
        public decimal TotalEnd { get; set; }
    }
    public class SyntheticWarehouseFillter
    {
        public int StoreId { get; set; }
        public int ProductId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool IsDownLoad { get; set; }
        public int OrganizationId { get; set; }
    }
}
