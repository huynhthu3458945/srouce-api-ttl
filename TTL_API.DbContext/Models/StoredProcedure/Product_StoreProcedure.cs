﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.StoredProcedure
{
    public class Product_StoreProcedure
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductTypeId { get; set; }
        public string Content { get; set; }
        public decimal Price { get; set; }
        public decimal CostPrice { get; set; }
        public decimal PriceDiscount { get; set; }
        public int Sale { get; set; }
        public DateTime? SaleDeadLine { get; set; }
        public bool IsInventory { get; set; }
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public bool IsBundledGift { get; set; }
        public int OrganizationId { get; set; }
        public int SupplierId { get; set; }
        public bool Status { get; set; }
        public int Mass { get; set; }
        public int WidthX { get; set; }
        public int WidthY { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public int Quantity { get; set; }
        public int QuantityReal { get; set; }
        public int QuantityFail { get; set; }
    }
    public class Product_StoreProcedureFitter
    {
        public int StoreId { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductTypeId { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public int OrganizationId { get; set; }
    }
}
