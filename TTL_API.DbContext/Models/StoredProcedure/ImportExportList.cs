﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.StoredProcedure
{
    public class ImportExportList
    {
        public string Code { get; set; }
        public DateTime CreateOn { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string UnitName { get; set; }
        public string StoreName { get; set; }
        public string Note { get; set; }
        public decimal QuantityImport { get; set; }
        public decimal CostPrice { get; set; }
        public decimal TotalImport { get; set; }
        public decimal QuantityExport { get; set; }
        public decimal Price { get; set; }
        public decimal TotalExport { get; set; }
    }
    public class ImportExportListFillter
    {
        public int StoreId { get; set; }
        public int ProductId { get; set; }
        public int BillType { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public bool IsDownLoad { get; set; }
        public int OrganizationId { get; set; }
    }
}
