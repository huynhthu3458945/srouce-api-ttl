﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.Utils
{
   public class AccountPortalReq
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Roles { get; set; }
        public bool Status { get; set; }
        public int OrganizationId { get; set; }
        public int? CreateBy { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Identitycard { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccount { get; set; }
        public string CityBank { get; set; }
        public string Birthday { get; set; }
        public string Address { get; set; }
        public int GenderId { get; set; }
        public int DistrictId { get; set; }
        public int ProvinceId { get; set; }
        public string OTP { get; set; }
        public string CountryCode { get; set; }
        public bool IsVN { get; set; }
    }

    public class AccountPortalRep
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Roles { get; set; }
        public bool Status { get; set; }
        public int OrganizationId { get; set; }
        public int? CreateBy { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Birthday { get; set; }
        public string Identitycard { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccount { get; set; }
        public string CityBank { get; set; }
        public string Address { get; set; }
        public int GenderId { get; set; }
        public int DistrictId { get; set; }
        public int ProvinceId { get; set; }
    }
}
