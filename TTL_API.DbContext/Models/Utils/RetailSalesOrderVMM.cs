﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.Utils
{
    public class RetailSalesOrderVMM
    {
        public string Code { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public string Address { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public double TotalAmount { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
        public string ActionCode { get; set; }
        public string DurationName { get; set; }
        public string URLRequestOnePay { get; set; }
        public string CodeOnePay { get; set; }
        public string Deeplink { get; set; }
        public string QrCodeUrl { get; set; }
        public string TypeService { get; set; }
        public string MessageService { get; set; }
        public DateTime CreateTime { get; set; }
        public bool IsRelative { get; set; }
        public string RelativeFullName { get; set; }
        public string RelativePhone { get; set; }
        public string RelativeEmail { get; set; }
        public string Culture { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        public int PrductId { get; set; }
    }
    public class InvoiceCreateRequestVMM
    {
        public string ApiKey { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string DurationCode { get; set; }
        public string DurationName { get; set; }
        public double TotalAmount { get; set; }
        public string Code { get; set; }
        public string CardList { get; set; }
        public string URLResponseOnepay { get; set; }
        public bool IsRelative { get; set; }
        public string RelativeFullName { get; set; }
        public string RelativePhone { get; set; }
        public string RelativeEmail { get; set; }
        public string Culture { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        //public string PayerId { get; set; }
        //public PayPalInfoCard? PayPalInfoCard { get; set; }
    }
    public class InvoiceUpdateRequestVMM
    {
        public string ApiKey { get; set; }
        public string Code { get; set; }
        public string StatusCode { get; set; }
        public string reasonName { get; set; }
    }
}
