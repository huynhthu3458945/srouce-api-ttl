﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.Utils
{
    public class UserFilter
    {

    }
    public class Login
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class UserReq
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
        public int OrganizationId { get; set; }
        public string Token { get; set; }
    }
}
