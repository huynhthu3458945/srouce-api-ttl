﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models
{
    public class ResponseList
    {
        public Paging Paging { get; set; }
        public Object ListData { get; set; }
    }

    public class ResponseCombo
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
    }

    public class ResponseAffiliateList
    {
        public Paging Paging { get; set; }
        public Object ExtendsData { get; set; }
        public Object ListData { get; set; }
    }
}
