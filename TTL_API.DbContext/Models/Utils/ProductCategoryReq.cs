﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.Utils
{
    public class ProductCategoryCreateReq
    {
        public int CreateBy { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string TokenKey { get; set; }
    }
    public class ProductCategoryUpdateReq
    {
        public int ModifiedOn { get; set; }
        public string Code { get; set; }
        public string OldCode { get; set; }
        public string Title { get; set; }
        public string TokenKey { get; set; }
    }
}
