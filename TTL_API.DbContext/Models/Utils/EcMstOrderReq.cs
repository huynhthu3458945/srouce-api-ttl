﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.Utils
{
    public class RequestGeneralERP
    {
        public string TonkenKey { get; set; }
        public string CustomerFullName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerAddress { get; set; }
        public decimal TotalAmount { get; set; }
        public double? PriceDiscount { get; set; }
        public int? DiscountValue { get; set; }
        public List<ItemProduct> ItemProducts { get; set; }
    }
    public class ItemProduct
    {
        public int Quantity { get; set; }
        public Guid ProductId { get; set; }
    }

    public class EcMstOrderResponeMKT
    {
        public Guid Id { get; set; }
        public string OrderCode { get; set; }
        public string InvoiStatus { get; set; }
        public List<ProductActiveMKT> ListProductActive { get; set; }
    }
    public class ProductActiveMKT
    {
        public Guid ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal? ProductPrice { get; set; }
        //public decimal? ProductDiscountPrice { get; set; }
        //public decimal? ProductFlashSalePrice { get; set; }
        public string ProductName { get; set; }
        public string OrderCode { get; set; }
        public string ProductDetail { get; set; }
        public string ProductAvatar { get; set; }
        public string Description { get; set; }
        public string ActiveCode { get; set; }
        public string SerialNumber { get; set; }
        //public List<ProductActiveDetail> ListProductDetail { get; set; }
        public int? Type { get; set; }
    }
}
