﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.Utils
{
    public class OrderReq
    {
        //public string customerCodeParent { get; set; }
        //public string customerPhoneParent { get; set; }
        //public string saleCode { get; set; }
        public string salePhone { get; set; }
        //public string acceptedCode { get; set; }
        public string acceptedPhone { get; set; }
        //public string customerCode { get; set; }
        //public string orderStatusCode { get; set; }
        //public int orderType { get; set; }
        //public int priceId { get; set; }
        //public int organizationId { get; set; }
        //public int discountValue { get; set; }
        //public string noteDiscount { get; set; }  
        public string note { get; set; }
        //public bool isDelivery { get; set; }
        //public int deliveryFee { get; set; }
        //public string noteDeliveryFee { get; set; }
        //public int totalAmount { get; set; }
        //public int totalDiscountAmount { get; set; }
        public string fullName { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        //public string phone2 { get; set; }
        public string address { get; set; }
        public string payTypeCode { get; set; }
        public string orderCodeHitaBot { get; set; }
        public string uRLResponseOnepay { get; set; }
        //public bool isDiscountPercent { get; set; }
        //public string companyName { get; set; }
        //public string companyCode { get; set; }
        //public string companyAddress { get; set; }
        //public int transportId { get; set; }
        //public int? createBy { get; set; }
        public List<OrderDetailReq> orderDetail { get; set; }
    }
    public class OrderDetailReq
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }

    public class OrderDetailSS
    {
        public string productCode { get; set; }
        public int totalAmount { get; set; }
        public int productPrice { get; set; }
        public int quantity { get; set; }
    }

    public class OrderReqSS
    {
        //public string fullNameSeller { get; set; }
        //public string emailSeller { get; set; }
        public string tokenKey { get; set; }
        public string orderCode { get; set; }
        public string phoneSeller { get; set; }
        public string fullNameBuyer { get; set; }
        public string emailBuyer { get; set; }
        public string phoneBuyer { get; set; }
        public int totalAmount { get; set; }
        public List<OrderDetailSS> orderDetails { get; set; }
        public bool isThird { get; set; }
        public bool autoProcess { get; set; }
        public string fromSource { get; set; }
    }

}

