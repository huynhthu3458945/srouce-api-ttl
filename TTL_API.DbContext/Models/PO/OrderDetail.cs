﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class OrderDetail : BaseEntitySystem
    {
        public int Id { get; set; }
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int UnitId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal TotalPrice { get; set; }
        public string Note { get; set; }
        public decimal OriginalPrice { get; set; }
        public decimal PriceDiscount { get; set; }
        public int ComboId { get; set; }
        public int DT_DiscountKindId { get; set; }
    }

    public class OrderDetailView : OrderDetail
    {
        public string  CreateDateStr { get; set; }
        public string  Code { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string CreateName { get; set; }
        public decimal TotalAmount { get; set; }
        public string  ProductName { get; set; }
        public string URLRequestOnePay { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
    }
}
