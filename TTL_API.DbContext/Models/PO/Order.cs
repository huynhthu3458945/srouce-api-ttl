﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Order : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int CustomerId { get; set; }
        public string CustomerCodeParent { get; set; }
        public string CustomerPhoneParent { get; set; }
        public string SaleCode { get; set; }
        public string SalePhone { get; set; }
        public string AcceptedCode { get; set; }
        public string AcceptedPhone { get; set; }
        public string CustomerCode { get; set; }
        public int OrderStatusId { get; set; }
        public int OrderType { get; set; }
        public int PriceId { get; set; }
        public int OrganizationId { get; set; }
        public int DiscountValue { get; set; }
        public string Note { get; set; }
        public bool IsDelivery { get; set; }
        public decimal DeliveryFee { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalDiscountAmount { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Phone2 { get; set; }
        public string Address { get; set; }
        public int PayTypeId { get; set; }
        public string NoteDeliveryFee { get; set; }
        public bool IsDiscountPercent { get; set; }
        public string NoteDiscount { get; set; }
        public int ParentId { get; set; }
        public bool IsVat { get; set; }
        public decimal InvoiceValue { get; set; }
        public string InvoiceCode { get; set; }
        public DateTime? InvoiceDay { get; set; }
        public int ChannelId { get; set; }
        public decimal TotalCustomerToPay { get; set; }
        public decimal ExtraMoney { get; set; }
        public int Ref_Id { get; set; }
        public string Ref_Code { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string CompanyAddress { get; set; }
        public int TransportId { get; set; }
        public string  LinkPayment { get; set; }
        public string URLRequestOnePay { get; set; }
        public string OrderCodeSS { get; set; }
        public string OrderCodeHitaBot { get; set; }
        public int MT_DiscountKindId { get; set; }
    }
    public class OrderModel : Order
    {
        public string CustomerName { get; set; }
        public string OrderStatusCode { get; set; }
        public string OrderStatusName { get; set; }
        public string URLResponseOnepay { get; set; }
        public IEnumerable<OrderDetail> OrderDetail { get; set; }
    }
    public class OrderFitter
    {
        public string CustomerPhoneParent { get; set; }
        public int CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string Phone { get; set; }
        public int OrderStatusId { get; set; }
        public int OrganizationId { get; set; }
        public int CreateBy { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
