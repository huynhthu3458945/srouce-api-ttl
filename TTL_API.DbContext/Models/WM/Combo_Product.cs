﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Combo_Product : BaseEntitySystem
    {
        public int ProductId { get; set; }
        public int ComboId { get; set; }
        public int Quantity { get; set; }
        public string Note { get; set; }
    }
}
