﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Combo : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public decimal Price { get; set; }
        public decimal CostPrice { get; set; }
        public int Sale { get; set; }
        public DateTime? SaleDeadLine { get; set; }
        public int OrganizationId { get; set; }
        public bool Status { get; set; }

    }
    public class ComboModel : Combo
    {
        public IEnumerable<Combo_Product> ComboProductList { get; set; }
    }

    public class ComboFitter
    {
        public int ProductCategoryId { get; set; }
        public int ProductTypeId { get; set; }
        public string ProductTypeCode { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public int OrganizationId { get; set; }
    }
}
