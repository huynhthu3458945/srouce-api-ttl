﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class BillDetail : BaseEntitySystem
    {
        public int Id { get; set; }
        public int BillId { get; set; }
        public int ProductId { get; set; }
        public int UnitId { get; set; }
        public int Quantity { get; set; }
        public int QuantityReal { get; set; }
        public int QuantityFail { get; set; }
        public decimal Price { get; set; }
        public decimal PriceDiscount { get; set; }
        public decimal TotalPrice { get; set; }
        public string Note { get; set; }
        public int DT_DiscountKindId { get; set; }
    }
}
