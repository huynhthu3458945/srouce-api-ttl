﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Bill : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int SupplierId { get; set; }
        public int StoreId { get; set; }
        public int StoreExportId { get; set; }
        public int BillCategoryId { get; set; }
        public int BillType { get; set; }
        public int OrganizationId { get; set; }
        public decimal TotalAmount { get; set; }
        public int BillStatusId { get; set; }
        public int DiscountValue { get; set; }
        public decimal TotalDiscountAmount { get; set; }
        public int OrderId { get; set; }
        public string Note { get; set; }
        public bool IsDelivery { get; set; }
        public decimal DeliveryFee { get; set; }
        public int MT_DiscountKindId { get; set; }
        
    }

    public class BillModel : Bill
    {
        public string SupplierName { get; set; }
        public string StoreName { get; set; }
        public string StoreExportName { get; set; }
        public string BillCategoryName { get; set; }
        public string BillStatusCode { get; set; }
        public string BillStatusName { get; set; }
        public string OrderCode { get; set; }
        public IEnumerable<BillDetail> BillDetail { get; set; }
        public IEnumerable<StoreDetail> StoreDetail { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
    public class BillFitter
    {
        public int SupplierId { get; set; }
        public int StoreId { get; set; }
        public int StoreExportId { get; set; }
        public int BillCategoryId { get; set; }
        public int BillStatusId { get; set; }
        public int OrganizationId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
