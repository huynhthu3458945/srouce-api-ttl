﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class PriceList : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Note { get; set; }
        public bool Status { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public bool IsUnlimited { get; set; }
        public bool IsAgency { get; set; }
        public int OrganizationId { get; set; }
    }

    public class PriceListModel : PriceList
    {
        public IEnumerable<PriceListDetail> PriceListDetail { get; set; }
    }
    public class PriceListFitter
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public int OrganizationId { get; set; }
    }
}
