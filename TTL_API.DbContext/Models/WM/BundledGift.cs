﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models
{
    public class BundledGift
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int ProductBundled { get; set; }
        public int Quantity { get; set; }
        public int UnitId { get; set; }
        public string Note { get; set; }
    }
}
