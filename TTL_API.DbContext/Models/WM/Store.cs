﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Store : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public string Address { get; set; }
        public bool IsPallet { get; set; }
        public int StoreTypeId { get; set; }
        public int OrganizationId { get; set; }
    }
    public class StoreModel : Store
    {
        public string StoreTypeName { get; set; }
        public int KeyId { get; set; }
        public List<int> EmployeedId { get; set; }
    }

      
}
