﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class PriceListDetail : BaseEntitySystem
    {
        public int Id { get; set; }
        public int PriceListId { get; set; }
        public int? ProductId { get; set; }
        public int? ComboId { get; set; }
        public decimal Price { get; set; }
        public decimal CostPrice { get; set; }
        public decimal PriceNew { get; set; }
        public int OrganizationId { get; set; }
    }
}
