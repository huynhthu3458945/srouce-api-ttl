﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models
{
   public class DiscountKind
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public bool Status { get; set; }
    }
}
