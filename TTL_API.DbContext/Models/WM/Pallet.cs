﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Pallet : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public int StoreId { get; set; }
        public int OrganizationId { get; set; }
    }
    public class PalletModel : Pallet
    {
        public string StoreName { get; set; }
    }
}
