﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models
{
    public class Store_Account
    {
        public int StoreId { get; set; }
        public int AccountId { get; set; }
        public bool IsKey { get; set; }
        public string Note { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

    }
}
