﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class InventoryDetail : BaseEntitySystem
    {
        public int InventoryId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public int QuantityActual { get; set; }
        public int QuantityDiff { get; set; }
        public decimal DifferenceValue { get; set; }
    }

    public class InventoryDetailModel : InventoryDetail
    {
        public string ProductName { get; set; }
    }
}
