﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Inventory : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Note { get; set; }
        public int StoreId { get; set; }
        public int TotalQuantity { get; set; }
        public int TotalQuantityActual { get; set; }
        public int TotalQuantityDiff { get; set; }
        public DateTime? InventoryDate { get; set; }
        public int OrganizationId { get; set; }
        public bool Status { get; set; }
        public bool IsDifference { get; set; }
        public bool HandleDifference { get; set; }
        public string NoteDifference { get; set; }
    }

    public class InventoryModel : Inventory
    {
        public int BillId { get; set; }
        public string StoreName { get; set; }
        public IEnumerable<InventoryDetailModel> InventoryDetail { get; set; }
    }
    public class InventoryFitter
    {
        public int StoreId { get; set; }
        public int OrganizationId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
