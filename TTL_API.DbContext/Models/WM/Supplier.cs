﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Supplier : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Tel { get; set; }
        public string Hotline { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public bool Status { get; set; }
        public int OrganizationId { get; set; }
    }
}
