﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
   public class StoreDetail : BaseEntitySystem
    {
        public int Id { get; set; }
        public string CodeValue { get; set; }
        public string Title { get; set; }
        public string SeriNumber  { get; set; }
        public int StoreId { get; set; }
        public int BillId { get; set; }
        public int BillExportId { get; set; }
        public int ProductId { get; set; }
        public bool Status { get; set; }
    }
}
