﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
   public class Store_Pallet_Product : BaseEntitySystem
    {
        public int Id { get; set; }
        public int StoreId { get; set; }
        public int PalletId { get; set; }
        public int ProductId { get; set; }
        public bool IsKey { get; set; }
        public string Note { get; set; }
    }

    public class Store_Pallet_ProductModel : Store_Pallet_Product
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string StoreName { get; set; }
        public string PalletName { get; set; }
        public string Avatar { get; set; }
    }
    public class PalletProductFitter
    {
        public int StoreId { get; set; }
        public int PalletId { get; set; }
        public int ProductId { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public int OrganizationId { get; set; }
    }
}
