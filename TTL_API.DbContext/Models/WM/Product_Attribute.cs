﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models
{
    public class Product_Attribute
    {
        public int AttributeId { get; set; }
        public int ProductId { get; set; }
        public string Value { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
