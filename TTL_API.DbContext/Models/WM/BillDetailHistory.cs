﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class BillDetailHistory 
    {
        public int Id { get; set; }
        public int BillId { get; set; }
        public int BillType { get; set; }
        public int BillDetailId  { get; set; }
        public int StoreId { get; set; }
        public int ProductId { get; set; }
        public int UnitId { get; set; }
        public int Quantity { get; set; }
        public int QuantityReal { get; set; }
        public int QuantityFail { get; set; }
        public decimal Price { get; set; }
        public decimal PriceDiscount { get; set; }
        public decimal TotalPrice { get; set; }
        public int OrganizationId { get; set; }
    }
}
