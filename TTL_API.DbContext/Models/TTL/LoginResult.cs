﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TTL_API.DbContext.Models.TTL
{
    public class LoginResult
    {
        public string Avartar { get; set; }
        public int UserId { get; set; }
        public string userName { get; set; }
        public string fullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public STNHDType STNHDType { get; set; }
        public string STNHDTypeName { get; set; }
        public string IsDaiSu { get; set; }
        public string ChildFullName1 { get; set; }
        public string GenderCode { get; set; }
        public string GenderName { get; set; }
        public DateTime? Dayofbirth { get; set; }
        public DateTime? DateCreate { get; set; }
        public int? CityId { get; set; }
        public string CityName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string Address { get; set; }
        public int ClassId { get; set; }
        public string RoleCode { get; set; }
        public string Rolename { get; set; }
        public List<UserOfGroup> UserOfGroups { get; set; }
        public List<LicenseOfUser> LicenseOfUsers { get; set; }
        public string Account { get; set; }
        public DateTime DateActive { get; set; }
        public string DurationName { get; set; }
        public DateTime? DateExpired { get; set; }
        public string ActiveNameEn { get; set; }
        public string AccountEn { get; set; }
        public string CodeInvite { get; set; }
        public string LinkInvite { get; set; }
        public int InviteUserId { get; set; }
        public double? CommisionTotal { get; set; }
        public int? InviteUserCount { get; set; }
        public string ShortCodeLink { get; set; }
        public int? UserType { get; set; }
        public Guid? IdInvite { get; set; }
        public int? MaxDeviceLogin { get; set; }
        public LoginResult()
        {
            UserOfGroups = new List<UserOfGroup>();
            LicenseOfUsers = new List<LicenseOfUser>();
        }
    }
    public enum STNHDType : byte
    {
        [Description("Chưa đăng ký trải nghiệm")]
        NotExperienced,
        [Description("Tài khoản trải nghiệm")]
        ExperienceAccount,
        [Description("Tài khoản đã kích hoạt")]
        Active
    }

    public class UserOfGroup
    {
        public int userId { get; set; }
        public string userName { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
    }

    public class LicenseOfUser
    {
        public int userId { get; set; }
        public string userName { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public DateTime DateInput { get; set; }
        public DateTime DateActive { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string StatusCode { get; set; }
        public string StatusName { get; set; }
    }
}
