﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.System
{
    public class Account : BaseEntitySystem
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool Status { get; set; }
    }
   
    public class AccountModel : Account
    {
        public int OrganizationId { get; set; }
        public List<TreeRoleAcc> TreeRoleAccs { get; set; }
    }
    public class TreeRoleAcc
    {
        public int Id { get; set; }
        public bool IsCheck { get; set; }
    }
}
