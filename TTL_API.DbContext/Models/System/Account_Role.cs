﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.System
{
   public class Account_Role
    {
        public int AccountId { get; set; }
        public int RoleId { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
    public class RoleModelTree
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public State State { get; set; }
        public string Text { get; set; }
        public List<RoleModelTree> Children { get; set; }
        public RoleModelTree()
        {
            Children = new List<RoleModelTree>();
        }
    }
}
