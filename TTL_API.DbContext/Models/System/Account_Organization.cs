﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.System
{
   public class Account_Organization 
    {
        public int AccountId { get; set; }
        public int AccountTypeId { get; set; }
        public int OrganizationId { get; set; }
        public bool Status { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
    }
}
