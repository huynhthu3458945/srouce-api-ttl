﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.System
{
    public class ProductMaps : BaseEntitySystem
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int KeyProductId { get; set; }
        public int OrganizationId { get; set; }
    }
    public class ProductMapsModel : ProductMaps
    {
        public string ProductName { get; set; }
        public string KeyProductName { get; set; }
    }
    public class ProductMapsFitter
    {
        public int ProductId { get; set; }
        public int KeyProductId { get; set; }
        public int OrganizationId { get; set; }
    }
}
