﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.System
{
    public class FunctionSystem : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Note { get; set; }
        public string LinkName { get; set; }
        public int Position { get; set; }
        public string Icon { get; set; }
        public bool Status { get; set; }
        public bool IsShow { get; set; }
        public int ParentId { get; set; }
        public int ApplicationId { get; set; }
    }

    public class FunctionSystemModelTree
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public State State { get; set; }
        public string Text { get; set; }
        public List<FunctionSystemModelTree> Children { get; set; }
        public FunctionSystemModelTree()
        {
            Children = new List<FunctionSystemModelTree>();
        }
    }
    public class State
    {
        public bool Selected { get; set; }
    }
    public class FunctionSystemFitter
    {
        public string Title { get; set; }
        public int ParentId { get; set; }
    }
}
