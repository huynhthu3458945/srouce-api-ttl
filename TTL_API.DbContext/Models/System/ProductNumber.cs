﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.System
{
    public class ProductNumber : BaseEntitySystem
    {
        public int Id { get; set; }
        public string KeyProduct { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string UrlApiCreate { get; set; }
        public string UrlApiUpdate { get; set; }
        public string Token { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
        public string DurationCode { get; set; }
        public string DurationName { get; set; }
        public int OrganizationId { get; set; }
    }
}
