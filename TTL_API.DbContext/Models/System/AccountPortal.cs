﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.System
{
    public class AccountPortal : BaseEntitySystem
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string? email { get; set; }
        public string Roles { get; set; }
        public bool Status { get; set; }
        public int OrganizationId { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerCode { get; set; }
        public string? CountryCode { get; set; }  
    }

    public class AccountPortalModel : AccountPortal
    {
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
