﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.System
{
    public class Role : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public int Position { get; set; }
        public int OrganizationId { get; set; }
    }
    public class RoleModel : Role
    {
        public List<TreeFunc> TreeFuncs { get; set; }
    }
    public class TreeFunc
    {
        public int Id { get; set; }
        public bool IsCheck { get; set; }
    }
}
