﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.System
{
    public class Function_Organization : BaseEntitySystem
    {
        public int Id { get; set; }
        public int FunctionSystemId { get; set; }
        public int OrganizationId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string LinkName { get; set; }
        public string Note { get; set; }
        public int Position { get; set; }
        public bool Status { get; set; }
        public bool IsShow { get; set; }
        public int ParentId { get; set; }
        public int ApplicationId { get; set; }
    }
    public class FunctionOrganizationModelTree
    {
        public int FuncId { get; set; }
        public int Id { get; set; }
        public int ParentId { get; set; }
        public State State { get; set; }
        public string Text { get; set; }
        public List<FunctionOrganizationModelTree> Children { get; set; }
        public FunctionOrganizationModelTree()
        {
            Children = new List<FunctionOrganizationModelTree>();
        }
    }
}
