﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TTL_API.DbContext.Models.System
{
    public class Organization : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string OwnerName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public int ClientId { get; set; }
        public string Logo { get; set; }
        public string Icon { get; set; }
    }

    public class OrganizationModel : Organization
    {
        public List<TreeFuncSystem> TreeFuncSystems { get; set; }
        public List<Role> Roles { get; set; }
    }
    public class TreeFuncSystem
    {
        public int Id { get; set; }
        public bool IsCheck { get; set; }
    }
}
