﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Staff_Department : BaseEntitySystem
    {
        public int DepartmentId { get; set; }
        public int StaffId { get; set; }
        public string Note { get; set; }
    }
}
