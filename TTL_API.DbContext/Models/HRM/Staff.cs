﻿using System;
using System.Collections.Generic;
using System.Text;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public class Staff : BaseEntitySystem
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string Thumb { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public string Skype { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Viber { get; set; }
        public string Zalo { get; set; }
        public bool Status { get; set; }
        public int GenderId { get; set; }
        public string Birthday { get; set; }
        public int StaffTypeId { get; set; }
        public int OrganizationId { get; set; }
        public int RegencyId { get; set; }
        public int ManagerId { get; set; }
        public int AccountId { get; set; }
        public string UserName { get; set; }
    }
    public class StaffModel : Staff
    {
        public string GenderName { get; set; }
        public string StaffTypeName { get; set; }
        public List<int> Departments { get; set; }
    }
    public class StaffFitter
    {
        public int DepartmentId { get; set; }
        public int StaffTypeId { get; set; }
        public int RegencyId { get; set; }
        public string Code { get; set; }
        public string FullName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int OrganizationId { get; set; }
    }
}
