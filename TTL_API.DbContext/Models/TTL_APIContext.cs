﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using TTL_API.DbContext.Base;
using TTL_API.DbContext.Models.System;

namespace TTL_API.DbContext.Models
{
    public partial class TTL_APIContext : Microsoft.EntityFrameworkCore.DbContext, IContext, IDisposable
    {
        private bool _disposed = false;

        public TTL_APIContext()
        {
        }

        public TTL_APIContext(DbContextOptions<TTL_APIContext> options)
            : base(options)
        {
            this.ChangeTracker.LazyLoadingEnabled = false;
        }
        public virtual DbSet<SystemAccount> SystemAccount { get; set; }
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<Application> Application { get; set; }
        public virtual DbSet<FunctionSystem> FunctionSystem { get; set; }
        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Organization> Organization { get; set; }
        public virtual DbSet<Function_Organization> Function_Organization { get; set; }
        public virtual DbSet<Role_Function> Role_Function { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<Account_Role> Account_Role { get; set; }
        public virtual DbSet<Account_Organization> Account_Organization { get; set; }
        public virtual DbSet<AccountType> AccountType { get; set; }
        public virtual DbSet<StoreType> StoreType { get; set; }
        public virtual DbSet<Store> Store { get; set; }
        public virtual DbSet<Store_Account> Store_Account { get; set; }
        public virtual DbSet<Pallet> Pallet { get; set; }
        public virtual DbSet<ProductCategory> ProductCategory { get; set; }
        public virtual DbSet<ProductType> ProductType { get; set; }
        public virtual DbSet<BillCategory> BillCategory { get; set; }
        public virtual DbSet<Unit> Unit { get; set; }
        public virtual DbSet<UnitConverted> UnitConverted { get; set; }
        public virtual DbSet<Supplier> Supplier { get; set; }
        public virtual DbSet<CustomerTemp> CustomerTemp { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<CustomerType> CustomerType { get; set; }
        public virtual DbSet<CustomerStatus> CustomerStatus { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Contact> Contact { get; set; }
        public virtual DbSet<Province> Province { get; set; }
        public virtual DbSet<District> District { get; set; }
        public virtual DbSet<Gender> Gender { get; set; }
        public virtual DbSet<BundledGift> BundledGift { get; set; }
        public virtual DbSet<Combo> Combo { get; set; }
        public virtual DbSet<Combo_Product> Combo_Product { get; set; }
        public virtual DbSet<Attributes> Attributes { get; set; }
        public virtual DbSet<Product_Attribute> Product_Attribute { get; set; }
        public virtual DbSet<PriceList> PriceList { get; set; }
        public virtual DbSet<PriceListDetail> PriceListDetail { get; set; }
        public virtual DbSet<Store_Pallet_Product> Store_Pallet_Product { get; set; }
        public virtual DbSet<Bill> Bill { get; set; }
        public virtual DbSet<BillDetail> BillDetail { get; set; }
        public virtual DbSet<BillStatus> BillStatus { get; set; }
        public virtual DbSet<BillDetailHistory> BillDetailHistory { get; set; }
        public virtual DbSet<StoreDetail> StoreDetail { get; set; }
        public virtual DbSet<Inventory> Inventory { get; set; }
        public virtual DbSet<InventoryDetail> InventoryDetail { get; set; }
        public virtual DbSet<Channel> Channel { get; set; }
        public virtual DbSet<OrderStatus> OrderStatus { get; set; }
        public virtual DbSet<PayType> PayType { get; set; }
        public virtual DbSet<Order> Order { get; set; }
        public virtual DbSet<OrderDetail> OrderDetail { get; set; }
        public virtual DbSet<Transport> Transport { get; set; }
        public virtual DbSet<ProductMaps> ProductMaps { get; set; }
        public virtual DbSet<ProductNumber> ProductNumber { get; set; }
        public virtual DbSet<AccountPortal> AccountPortal { get; set; }
        public virtual DbSet<ConfigPayment> ConfigPayment { get; set; }
        public virtual DbSet<Department> Department { get; set; }
        public virtual DbSet<Staff> Staff { get; set; }
        public virtual DbSet<StaffType> StaffType { get; set; }
        public virtual DbSet<Regency> Regency { get; set; }
        public virtual DbSet<Staff_Department> Staff_Department { get; set; }
        public virtual DbSet<DiscountKind> DiscountKind { get; set; }
        public DbSet<T> Repository<T>() where T : class
        {
            return Set<T>();
        }

        public int SaveChange()
        {
            return base.SaveChanges();
        }

        public async Task<int> SaveChangeAsync()
        {
            return await base.SaveChangesAsync();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account_Role>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.RoleId })
                    .HasName("PK_Account_Role");
            });

            modelBuilder.Entity<Role_Function>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.FunctionId })
                    .HasName("PK_Role_Function");
            });
            modelBuilder.Entity<Account_Organization>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.AccountTypeId, e.OrganizationId })
                    .HasName("PK_Account_Organization");
            });
            modelBuilder.Entity<Store_Account>(entity =>
            {
                entity.HasKey(e => new { e.AccountId, e.StoreId })
                    .HasName("PK_Store_Account");
            });
            modelBuilder.Entity<Combo_Product>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.ComboId })
                    .HasName("PK_Combo_Product");
            });
            modelBuilder.Entity<Product_Attribute>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.AttributeId });
            });   
           modelBuilder.Entity<InventoryDetail>(entity =>
            {
                entity.HasKey(e => new { e.ProductId, e.InventoryId });
            });  
           modelBuilder.Entity<Staff_Department>(entity =>
            {
                entity.HasKey(e => new { e.DepartmentId, e.StaffId });
            });   
       
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (_disposed) return;
            if (isDisposing)
            {
            }
            _disposed = true;
        }
    }
}
