﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;

namespace TTL_API.WebApi.AutoMapper
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<OrganizationModel, Organization>().IncludeAllDerived().ReverseMap();
            CreateMap<AccountPortalReq, AccountPortal>().IncludeAllDerived().ReverseMap();
            CreateMap<AccountPortal, AccountPortalRep>().IncludeAllDerived().ReverseMap();

            CreateMap<FunctionSystem, Function_Organization>()
                .ForMember(dest => dest.Id, act => act.Ignore())
                .ForMember(des => des.FunctionSystemId, act => act.MapFrom(src => src.Id));
            CreateMap<StoreModel, Store>().IncludeAllDerived().ReverseMap();
            CreateMap<CustomerModel, Customer>().IncludeAllDerived().ReverseMap();
            CreateMap<CustomerModel, CustomerTemp>().IncludeAllDerived().ReverseMap();
            CreateMap<ProductModel, Product>().IncludeAllDerived().ReverseMap();
            CreateMap<ComboModel, Combo>().IncludeAllDerived().ReverseMap();
            CreateMap<PriceListModel, PriceList>().IncludeAllDerived().ReverseMap();
            CreateMap<Store_Pallet_ProductModel, Store_Pallet_Product>().IncludeAllDerived().ReverseMap();
            CreateMap<BillModel, Bill>().IncludeAllDerived().ReverseMap();
            CreateMap<BillDetail, BillDetailHistory>()
                .ForMember(dest => dest.Id, act => act.Ignore())
                .ForMember(des => des.BillDetailId, act => act.MapFrom(src => src.Id));


            CreateMap<ProductMapsModel, ProductMaps>().IncludeAllDerived().ReverseMap();

            CreateMap<InventoryModel, Inventory>().IncludeAllDerived().ReverseMap();
            CreateMap<InventoryDetailModel, InventoryDetail>().IncludeAllDerived().ReverseMap();

            // PO
            CreateMap<ChannelModel, Channel>().IncludeAllDerived().ReverseMap();
            CreateMap<OrderModel, Order>().IncludeAllDerived().ReverseMap();
            CreateMap<OrderReq, OrderModel>().IncludeAllDerived().ReverseMap();
            CreateMap<OrderDetailReq, OrderDetail>().IncludeAllDerived().ReverseMap();

            //HRM
            CreateMap<StaffModel, Staff>().IncludeAllDerived().ReverseMap();

            //CreateMap<MstNotificationsReq, MstNotifications>().IncludeAllDerived().ReverseMap();
            //CreateMap<MstNotifications, MstNotificationsRes>().IncludeAllDerived().ReverseMap();
            //CreateMap<InfoGiftCardUserReq, InfoGiftCardUser>().IncludeAllDerived().ReverseMap();
            //CreateMap<InfoMindmapAnswerReq, InfoMindmapAnswer>().ReverseMap();
            //CreateMap<MstMindmapMinigameReq, MstMindmapMinigame>().IncludeAllDerived()
            //    .ForMember(dest => dest.InfoMindmapAnswers, opt => opt.MapFrom(src => src.InfoMindmapAnswers))
            //    .ReverseMap();
            //CreateMap<MstMindmapImageReq, MstMindmapImage>().IncludeAllDerived().ReverseMap();
            //CreateMap<InfoQuestionAnswerReq, InfoQuestionAnswer>().ReverseMap();
            //CreateMap<MstQuestionReq, MstQuestion>().IncludeAllDerived()
            //    .ForMember(dest => dest.InfoQuestionAnswers, opt => opt.MapFrom(src => src.InfoQuestionAnswers))
            //    .ReverseMap();
            //CreateMap<HistoryStudyUserReq, InfoHistoryStudyUser>().IncludeAllDerived().ReverseMap();
            //CreateMap<MindmapResultReq, InfoMindmapResult>().IncludeAllDerived().ReverseMap();
            //CreateMap<InfoQuestionResultReq, InfoQuestionResult>().IncludeAllDerived().ReverseMap();
            //CreateMap<InfoBookCoursesReq, InfoBookCourse>().ReverseMap();
            //CreateMap<MstBookReq, MstBook>().IncludeAllDerived()
            //    .ForMember(dest => dest.InfoBookCourses, opt => opt.MapFrom(src => src.InfoBookCourses))
            //    .ReverseMap();
            //CreateMap<InfoBookCourseReq, InfoBookCourse>().IncludeAllDerived().ReverseMap();
            //CreateMap<DeployAppRes, MstVersionApp>().IncludeAllDerived().ReverseMap();
            //CreateMap<VersionAppRes, MstVersionApp>().IncludeAllDerived().ReverseMap();

        }
    }
}
