﻿using AutoMapper;
using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Localization.Routing;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using TTL_API.DbContext;
using TTL_API.Services.Cache;
using TTL_API.Services.Implements;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.AutoMapper;
using TTL_API.WebApi.Resources;

namespace TTL.WebApi
{
    public class Startup
	{
		public IConfiguration Configuration { get; }

		public Startup(IConfiguration configuration)
		{
			Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(configuration).CreateLogger();
			Configuration = configuration;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			var mainConnectString = Configuration["MainConnectionString"];

			var supportedCultures = new[]
			   {
					new CultureInfo("en-US"),
					new CultureInfo("vi-VN"),
				};
			var credential = GoogleCredential.FromFile("config\\service_account.json");
			FirebaseApp.Create(new AppOptions()
			{

				Credential = credential
			});
			var options = new RequestLocalizationOptions()
			{
				DefaultRequestCulture = new RequestCulture(culture: "vi-VN", uiCulture: "vi-VN"),
				SupportedCultures = supportedCultures,
				SupportedUICultures = supportedCultures
			};
			options.RequestCultureProviders = new[]
			{
				 new RouteDataRequestCultureProvider() { Options = options }
			};

			services.AddCors(options =>
			{
				options.AddDefaultPolicy(
					builder =>
					{
						builder.AllowAnyOrigin()
								.AllowAnyMethod()
								.AllowAnyHeader()
								.WithExposedHeaders("Content-Disposition");
					});
			});
			services.AddSingleton(options);
			services.AddSingleton<LocService>();

			services.AddLocalization(otp => otp.ResourcesPath = "Resources");

			services.AddMvc()
				.AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
				 .AddDataAnnotationsLocalization(otp =>
				 {
					 otp.DataAnnotationLocalizerProvider = (type, factory) =>
					 {
						 var assemblyName = new AssemblyName(typeof(SharedResource).GetTypeInfo().Assembly.FullName);
						 return factory.Create("SharedResource", assemblyName.Name);
					 };
				 });

			services.AddControllers();

			services.AddSwaggerGen(swa => {
				swa.SwaggerDoc("v1", new OpenApiInfo
				{
					Title = "API TÂM TRÍ LỰC Application",
					Version = "v1"
				});
				var filePath = Path.Combine(AppContext.BaseDirectory, "TTL_API.WebApi.xml");
				swa.IncludeXmlComments(filePath);

				var securitySchema = new OpenApiSecurityScheme
				{
					Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
					Name = "Authorization",
					In = ParameterLocation.Header,
					Type = SecuritySchemeType.Http,
					Scheme = JwtBearerDefaults.AuthenticationScheme,
					Reference = new OpenApiReference
					{
						Type = ReferenceType.SecurityScheme,
						Id = JwtBearerDefaults.AuthenticationScheme
					}
				};
				swa.AddSecurityDefinition("Bearer", securitySchema);

				var securityRequirement = new OpenApiSecurityRequirement
				{
					{ securitySchema, new[] { "Bearer" } }
				};
				swa.AddSecurityRequirement(securityRequirement);
			});

			// Config Services
			services.ConfigureDbContext(mainConnectString);

			// For Cache
			services.AddMemoryCache();
			services.AddScoped<IMemoryCacheService, MemoryCacheService>();
			services.AddScoped<ILoggerService, LoggerService>();
			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

			// Common
			services.AddScoped<IProvinceService, ProvinceService>();
			services.AddScoped<IDistrictService, DistrictService>();
			services.AddScoped<ICommonService, CommonService>();

			services.AddScoped<IAccountService, AccountService>();
			services.AddScoped<IAccountPortalService, AccountPortalService>();
			services.AddScoped<ISystmeAccountService, SystmeAccountService>();
			services.AddScoped<IClientService, ClientService>();
			services.AddScoped<IApplicationService, ApplicationService>();
			services.AddScoped<IFunctionSystemService, FunctionSystemService>();
			services.AddScoped<IFunction_OrganizationService, Function_OrganizationService>();
			services.AddScoped<IOrganizationService, OrganizationService>();
			services.AddScoped<IRoleService, RoleService>();
			services.AddScoped<IRole_FunctionService, Role_FunctionService>();
			services.AddScoped<IAccount_RoleService, Account_RoleService>();
			services.AddScoped<IProductMapsService, ProductMapsService>();

			// WM
			services.AddScoped<IStoreTypeService, StoreTypeService>();
			services.AddScoped<IStoreService, StoreService>();
			services.AddScoped<ICustomerTypeService, CustomerTypeService>();
			services.AddScoped<IProductCategoryService, ProductCategoryService>();
			services.AddScoped<IProductTypeService, ProductTypeService>();
			services.AddScoped<IBillCategoryService, BillCategoryService>();
			services.AddScoped<IPalletService, PalletService>();
			services.AddScoped<IUnitService, UnitService>();
			services.AddScoped<IUnitConvertedService, UnitConvertedService>();
			services.AddScoped<ISupplierService, SupplierService>();
			services.AddScoped<IProductService, ProductService>();
			services.AddScoped<IComboService, ComboService>();
			services.AddScoped<IAttributesService, AttributesService>();
			services.AddScoped<IPriceListService, PriceListService>();
			services.AddScoped<IBillService, BillService>();
			services.AddScoped<IInventoryService, InventoryService>();
			services.AddScoped<IStoreProcedureService, StoreProcedureService>();

			//CRM 
			services.AddScoped<ICustomerTempService, CustomerTempService>();
			services.AddScoped<ICustomerService, CustomerService>();
			services.AddScoped<IContactService, ContactService>();
			services.AddScoped<ICustomerTypeService, CustomerTypeService>();
			services.AddScoped<ICustomerStatusService, CustomerStatusService>();

			//PO
			services.AddScoped<IChannelService, ChannelService>();
			services.AddScoped<IOrderService, OrderService>();
			services.AddScoped<INganLuongService, NganLuongService>();

			//HRM
			services.AddScoped<IDepartmentService, DepartmentService>();
			services.AddScoped<IRegencyService, RegencyService>();
			services.AddScoped<IStaffTypeService, StaffTypeService>();
			services.AddScoped<IStaffService, StaffService>();

			// Auto Mapper Configurations  
			var mappingConfig = new MapperConfiguration(mc =>
			{
				mc.AddProfile(new MappingProfile());
			});
			IMapper mapper = mappingConfig.CreateMapper();
			services.AddSingleton(mapper);
			services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
			// Config Token
			var issuer = Configuration.GetValue<string>("Tokens:Issuer");
			var signingKey = Configuration.GetValue<string>("Tokens:Key");
			var signingKeyBytes = System.Text.Encoding.UTF8.GetBytes(signingKey);
			services.AddAuthentication(opt =>
			{
				opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
			}).AddJwtBearer(options =>
			{
				options.RequireHttpsMetadata = false;
				options.SaveToken = true;
				options.TokenValidationParameters = new TokenValidationParameters()
				{
					ValidateIssuer = true,
					ValidIssuer = issuer,
					ValidateAudience = true,
					ValidAudience = issuer,
					ValidateLifetime = true,
					ValidateIssuerSigningKey = true,
					ClockSkew = System.TimeSpan.Zero,
					IssuerSigningKey = new SymmetricSecurityKey(signingKeyBytes)
				};
			});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			app.UseSwagger();
			app.UseSwaggerUI(c => {
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "API TÂM TRÍ LỰC Application");
			});
			app.UseHttpsRedirection();
			app.UseCors();
			app.UseRouting();

			app.UseAuthentication();

			app.UseAuthorization();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
			});

			loggerFactory.AddSerilog();
		}
	}
}
