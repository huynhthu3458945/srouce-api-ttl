﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TTL_API.WebApi.Extensions
{
    public class SMSVietguys
    {
        public string SendSMSOTP(string url, string u, string pwd, string from, string phone, string content, string bid)
        {
            try
            {
                //bid thay đổi không trùng nhau
                var client = new RestClient(string.Format(url + "?u={0}&pwd={1}&from={2}&phone={3}&sms={4}&bid={5}&json=1&type=1", u, pwd, from, phone, content, bid));
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                IRestResponse response = client.Execute(request);
                return response.Content;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
        public string VoiceOTP(string url, string u, string pwd, string from, string phone, string content, string bid)
        {
            try
            {
                //bid thay đổi không trùng nhau
                var client = new RestClient(string.Format(url + "?u={0}&pwd={1}&from={2}&phone={3}&sms={4}&bid={5}&json=1&type=1", u, pwd, from, phone, content, bid));
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                IRestResponse response = client.Execute(request);
                return response.Content;
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}

