﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using TTL_API.WebApi.Extensions.NganLuong;
using TTL_API.DbContext.Models;
using TTL_API.Services.Extensions;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;

namespace TTL_API.WebApi.Extensions
{
    public class FunctionsPayment
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;

        public FunctionsPayment(
           ILoggerService logger,
           IConfiguration config
           )
        {
            _logger = logger;
            _config = config;

        }
        public async Task<OrderModel> InvoiceByOnePay(OrderModel orderModel)
        {
            //Onepay
            // Khoi tao lop thu vien va gan gia tri cac tham so gui sang cong thanh toan
            VPCRequest onePay = new VPCRequest(_config["OnePay:VPCRequest"]); //Dev
            //VPCRequest onePay = new VPCRequest("https://onepay.vn/paygate/vpcpay.op"); //GOLIVE
            onePay.SetSecureSecret(_config["OnePay:SecureSecret"]);
            // Add the Digital Order Fields for the functionality you wish to use
            // Core Transaction Fields
            onePay.AddDigitalOrderField("AgainLink", "http://onepay.vn");
            onePay.AddDigitalOrderField("Title", "Mua Khóa Học 5 Phút Thuộc Bài");
            onePay.AddDigitalOrderField("vpc_Locale", "vn");//Chon ngon ngu hien thi tren cong thanh toan (vn/en)
            onePay.AddDigitalOrderField("vpc_Version", "2");
            onePay.AddDigitalOrderField("vpc_Command", "pay");
            //onePay.AddDigitalOrderField("vpc_CardList", invoiceCreateRequest.CardList);
            onePay.AddDigitalOrderField("vpc_CardList", "DOMESTIC");
            onePay.AddDigitalOrderField("vpc_Merchant", _config["OnePay:vpc_Merchant"]); //Dev
                                                                                         //onePay.AddDigitalOrderField("vpc_Merchant", "TAMTRILUC"); //GOLIVE
            onePay.AddDigitalOrderField("vpc_AccessCode", _config["OnePay:vpc_AccessCode"]); //Dev
                                                                                             // onePay.AddDigitalOrderField("vpc_AccessCode", "E2807B66"); //GOLIVE
            var vpc_MerchTxnRef = "TTL_" + DateTime.Now.ToString("yyyyMMddHHmmss");
            var vpc_OrderInfo = orderModel.Code;
            onePay.AddDigitalOrderField("vpc_MerchTxnRef", vpc_MerchTxnRef);
            onePay.AddDigitalOrderField("vpc_OrderInfo", vpc_OrderInfo);
            onePay.AddDigitalOrderField("vpc_Amount", ((double)orderModel.TotalAmount * 100).ToString());
            if (orderModel.URLResponseOnepay != null && orderModel.URLResponseOnepay != "")
            {
                onePay.AddDigitalOrderField("vpc_ReturnURL", orderModel.URLResponseOnepay);
            }
            else
            {
                onePay.AddDigitalOrderField("vpc_ReturnURL", _config["OnePay:vpc_ReturnURL"]);  //Dev
            }
            onePay.AddDigitalOrderField("vpc_Customer_Phone", orderModel.Phone);
            onePay.AddDigitalOrderField("vpc_Customer_Email", orderModel.Email);
            onePay.AddDigitalOrderField("vpc_Customer_Id", orderModel.FullName);
            // Dia chi IP cua khach hang
            string myIP = "";
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    myIP = ip.ToString();
                }
            }
            onePay.AddDigitalOrderField("vpc_TicketNo", myIP);//ip
                                                              // Chuyen huong trinh duyet sang cong thanh toan
            string url = onePay.Create3PartyQueryString();
            if (!string.IsNullOrEmpty(url))
            {
                orderModel.URLRequestOnePay = url;
            }
            return orderModel;
        }

        public async Task<OrderModel> InvoiceByNganLuongPayAsync(INganLuongService _nganLuongService, OrderModel orderModel, int listCount, string cityName)
        {
            _logger.WriteInfoLog("NganLuongController,Case with NganLuong");
            //Thanh toán ngân lượng
            //Hiện tại do test thanh toán visa từ 100k trở lên nhưng có 1 số gói dưới 100k
            var paymentRequest = new PaymentRequest();
            paymentRequest.orderCode = orderModel.Code;
            paymentRequest.amount = (double)orderModel.TotalAmount;
            //paymentRequest.amount = invoice.TotalAmount < 100000 ? 100000 : invoice.TotalAmount;
            //paymentRequest.amount = 100000001;
            paymentRequest.currency = "VND";
            paymentRequest.orderDescription = "NganLuong";
            paymentRequest.totalItem = listCount;
            //DeepLink chỗ nếu cần
            if (!string.IsNullOrEmpty(orderModel.URLResponseOnepay))
            {
                paymentRequest.returnUrl = orderModel.URLResponseOnepay + "?orderId=" + orderModel.Code;
                paymentRequest.cancelUrl = orderModel.URLResponseOnepay + "?orderId=" + orderModel.Code + "&transactionCode=&errorCode=&cancel=true";
            }
            else
            {
                //Truyền thằng orderCode vào callback
                paymentRequest.returnUrl = _config["NganLuong:return_url"] + "?orderId=" + orderModel.Code;
                paymentRequest.cancelUrl = _config["NganLuong:return_url"];
            }
            _logger.WriteInfoLog(paymentRequest.returnUrl);

            paymentRequest.buyerName = orderModel.FullName;
            paymentRequest.buyerPhone = orderModel.Phone;
            paymentRequest.buyerEmail = orderModel.Email;
            paymentRequest.buyerAddress = string.IsNullOrEmpty(orderModel.Address) ? "Ttl Address" : orderModel.Address;
            paymentRequest.buyerCity = cityName;
            paymentRequest.buyerCountry = "Viet Nam";
            var api = new NganLuong.APINganLuong();
            var url = _config["NganLuong:PaymentURL"];
            var merchant = _config["NganLuong:MerchantId"];
            var sercurePass = _config["NganLuong:MerchantPassword"];
            var emailMerchant = _config["NganLuong:ReceiverEmail"];
            var cancelUrl = _config["NganLuong:cancel_url"] + "?order_code=" + orderModel.Code;
            var notifyUrl = _config["NganLuong:notify_url"];
            var str = api.buildCheckoutUrlNew(url, merchant, sercurePass, emailMerchant, "vnd", notifyUrl, cancelUrl, paymentRequest);
            orderModel.URLRequestOnePay = str;
            #region Nếu dùng cổng alepay(ngân lượng) -> mở comment
            //var resultNganluong = await _nganLuongService.PaymentNganLuong(paymentRequest, "vi-vn");
            //if (resultNganluong.code == "000")
            //{
            //    //orderModel.TransactionCode = resultNganluong.transactionCode;
            //    //Thanh toán thành công thì trả về trang checkoutUrl của ngân lượng để người dùng dùng cổng thanh toán
            //    orderModel.URLRequestOnePay = resultNganluong.checkoutUrl;
            //    _logger.WriteInfoLog("NganLuongController,Case with Onepay Call NganLuong" + JsonConvert.SerializeObject(resultNganluong, Formatting.Indented));
            //}
            #endregion
            return orderModel;
        }
    }
}
