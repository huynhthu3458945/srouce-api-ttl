﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Net;
using System.Xml;
using Newtonsoft.Json;
using System.Collections;
using TTL_API.WebApi.Extensions.NganLuong;

namespace TTL_API.WebApi.Extensions.NganLuong
{
    public class APINganLuong
    {
        //Dùng để yêu cầu rút tiền từ tài khoản ví ngân lượng về tài khoản ngân hàng
        public WithdrawMoneyResponse SetCashoutRequest(WithdrawMoneyRequest requestContent, string payment_method = "NL")
        {
            String requestinfo = GetParamWithdrawMoneyPost(requestContent);
            String result = HttpPost(requestinfo);
            result = result.Replace("&", "&amp;");

            var json = JsonConvert.DeserializeObject<WithdrawMoneyResponse>(result);
            return json;
        }
        //public ResponseCheckOrder GetTransactionDetail(RequestCheckOrder info)
        //{


        //    String request = "";
        //    request += "function=" + info.Funtion;
        //    request += "&version=" + info.Version;
        //    request += "&merchant_id=" + info.Merchant_id;
        //    request += "&merchant_password=" + CreateMD5Hash(info.Merchant_password);
        //    request += "&token=" + info.Token;
        //    String result = HttpPost(request);
        //    result = result.Replace("&", "&amp;");
        //    XmlDocument dom = new XmlDocument();
        //    dom.LoadXml(result);
        //    XmlNodeList root = dom.DocumentElement.ChildNodes;

        //    ResponseCheckOrder objResult = new ResponseCheckOrder();


        //    objResult.errorCode = root.Item(0).InnerText;
        //    objResult.token = root.Item(1).InnerText;
        //    objResult.description = root.Item(2).InnerText;
        //    objResult.transactionStatus = root.Item(3).InnerText;
        //    //objResult.receiver_email = root.Item(4).InnerText; //receiver_email
        //    objResult.order_code = root.Item(5).InnerText;
        //    objResult.paymentAmount = root.Item(6).InnerText; //total_amount
        //    //objResult.payment_method = root.Item(7).InnerText; //payment_method
        //    //objResult.bank_code = root.Item(8).InnerText; //bank_code
        //    //objResult.payment_type = root.Item(9).InnerText; //payment_type
        //    //objResult.description = root.Item(10).InnerText; //order_description
        //    //objResult.tax_amount = root.Item(11).InnerText; //tax_amount
        //    //objResult.discount_amount = root.Item(12).InnerText; //discount_amount
        //    //objResult.fee_shipping = root.Item(13).InnerText; //fee_shipping
        //    //objResult.return_url = root.Item(14).InnerText; //return_url
        //    // objResult.cancel_url = root.Item(15).InnerText; //cancel_url
        //    objResult.payerName = root.Item(16).InnerText; //buyer_fullname
        //    objResult.payerEmail = root.Item(17).InnerText; //buyer_email
        //    objResult.payerMobile = root.Item(18).InnerText; //buyer_mobile
        //    //objResult.buyer_address = root.Item(19).InnerText; //buyer_address
        //    //objResult.affiliate_code = root.Item(20).InnerText; //affiliate_code
        //    objResult.transactionId = root.Item(21).InnerText;


        //    objResult.errorCode = root.Item(0).InnerText;
        //    objResult.token = root.Item(1).InnerText;
        //    objResult.description = root.Item(2).InnerText;
        //    objResult.transactionStatus = root.Item(3).InnerText;
        //    objResult.order_code = root.Item(5).InnerText;
        //    objResult.paymentAmount = root.Item(6).InnerText;
        //    objResult.transactionId = root.Item(21).InnerText;

        //    /*
        //    String error_code =root.Item(0).InnerText;
        //    String token =root.Item(1).InnerText;
        //    String description =root.Item(2).InnerText;
        //    String transaction_status =root.Item(3).InnerText;
        //    String receiver_email =root.Item(4).InnerText;
        //    String order_code =root.Item(5).InnerText;
        //    String total_amount =root.Item(6).InnerText;
        //    String payment_method =root.Item(7).InnerText;
        //    String bank_code =root.Item(8).InnerText;
        //    String payment_type =root.Item(9).InnerText;
        //    String order_description =root.Item(10).InnerText;
        //    String tax_amount =root.Item(11).InnerText;
        //    String discount_amount =root.Item(12).InnerText;
        //    String fee_shipping =root.Item(13).InnerText;
        //    String return_url =root.Item(14).InnerText;
        //    String cancel_url =root.Item(15).InnerText;
        //    String buyer_fullname =root.Item(16).InnerText;            
        //    String buyer_email =root.Item(17).InnerText;
        //    String buyer_mobile =root.Item(18).InnerText;
        //    String buyer_address =root.Item(19).InnerText;
        //    String affiliate_code =root.Item(20).InnerText;
        //    String transaction_id =root.Item(21).InnerText;
        // */
        //    return objResult;
        //}
        private static String GetParamWithdrawMoneyPost(WithdrawMoneyRequest info)
        {

            String request = "";

            request += "func=" + info.Funtion;
            request += "&merchant_id=" + info.Merchant_id;
            request += "&merchant_password=" + CreateMD5Hash(info.Merchant_password);
            request += "&receiver_email=" + info.Receiver_email;
            request += "&ref_code=" + info.RefCode;
            request += "&total_amount=" + info.Total_amount;
            request += "&account_type=" + info.AccountType;
            request += "&bank_code=" + info.bank_code;
            request += "&card_fullname=" + info.Card_fullname;
            request += "&card_number=" + info.CardNumber;
            request += "&card_month=" + info.CardMonth;
            request += "&card_year=" + info.CardYear;
            request += "&branch_name=" + info.BranchName;
            request += "&zone_id=" + info.ZoneId;
            request += "&reason=" + info.Reason;

            return request;
        }
        //Dùng để kiểm tra trạng thái giao dịch rút tiền(hàm cũ)
        public CheckWithdrawalStatusResponse CheckCashout(CheckWithdrawalStatusRequestOld requestContent, string payment_method = "NL")
        {

            //requestContent.Payment_method = payment_method;
            String requestinfo = GetParamCheckWithdrawalStatus(requestContent);
            String result = HttpPost(requestinfo);
            result = result.Replace("&", "&amp;");
            var json = JsonConvert.DeserializeObject<CheckWithdrawalStatusResponse>(result);

            return json;
        }
        private static String GetParamCheckWithdrawalStatus(CheckWithdrawalStatusRequestOld info)
        {

            String request = "";

            request += "func=" + info.Funtion;
            request += "&merchant_id=" + info.Merchant_id;
            request += "&merchant_password=" + CreateMD5Hash(info.Merchant_password);
            request += "&receiver_email=" + info.Receiver_email;
            request += "&transaction_id=" + info.TransactionId;
            request += "&ref_code=" + info.RefCode;

            return request;
        }
        //Dùng để kiểm tra trạng thái giao dịch rút tiền(hàm mới)
        public IList<CheckWithdrawalStatusResponse> CheckCashoutList(CheckWithdrawalStatusRequestNew requestContent, string payment_method = "NL")
        {
            //requestContent.Payment_method = payment_method;
            String requestinfo = GetParamCheckWithdrawalStatusNew(requestContent);
            var result1 = HttpPost(requestinfo);
            //result = result.Replace("&", "&amp;");
            var json = JsonConvert.DeserializeObject<IList<CheckWithdrawalStatusResponse>>(result1);

            return json;
        }
        private static String GetParamCheckWithdrawalStatusNew(CheckWithdrawalStatusRequestNew info)
        {
            String request = "";

            request += "func=" + info.Funtion;
            request += "&merchant_id=" + info.Merchant_id;
            request += "&merchant_password=" + CreateMD5Hash(info.Merchant_password);
            request += "&receiver_email=" + info.Receiver_email;
            request += "&transaction_id=" + info.TransactionId;
            request += "&ref_code=" + info.RefCode;

            return request;
        }

        private static String HttpPost(string postData)
        {

            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] data = encoding.GetBytes(postData);

            // Prepare web request...
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create("https://sandbox.nganluong.vn/nl35/withdraw.api.post.php");
            myRequest.Method = "POST";
            myRequest.ContentType = "application/x-www-form-urlencoded";
            myRequest.ContentLength = data.Length;
            Stream newStream = myRequest.GetRequestStream();
            // Send the data.

            newStream.Write(data, 0, data.Length);
            newStream.Close();

            HttpWebResponse response = (HttpWebResponse)myRequest.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string output = reader.ReadToEnd();
            response.Close();
            return output;
        }

        private static String CreateMD5Hash(string input)
        {
            // Use input string to calculate MD5 hash
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("x2"));
            }
            return sb.ToString();
        }
       
        public string buildCheckoutUrlNew(string urlNl,string merchantId,string merchantPass,string receiver, string currency,string notifyUrl,string cancelUrl, PaymentRequest request)
        {
            //order_description = "Bình nước & Lock Lock 1.2 lít";
            //buyer_info = "Họ tên người mua *|* Địa chỉ Email *|* Điện thoại *|* Địa chỉ nhận hàng";// "Họ tên người mua *|* Địa chỉ Email *|* Điện thoại *|* Địa chỉ nhận hàng";
            string str_return_url = request.returnUrl.ToLower();
            string security_code = "";
            security_code += "" + merchantId;
            security_code += " " + str_return_url;
            security_code += " " + receiver;//tài khoản ngân lượng
            security_code += " " + request.orderCode;
            security_code += " " + request.orderCode;
            security_code += " " + request.amount;
            security_code += " " + currency;//hỗ trợ 2 loại tiền tệ currency usd,vnd
            security_code += " " + request.totalItem;//số lượng mặc định 1
            security_code += " " + 0;
            security_code += " " + 0;
            security_code += " " + 0;
            security_code += " " + 0;
            security_code += " " + "5PTB Paymen NL";
            //String payinfo = "";// Convert.ToString("Họ tên người mua *|* Địa chỉ Email *|* Điện thoại *|* Địa chỉ nhận hàng");
            security_code += " " + request.buyerInfo;
            security_code += " " + request.affiliatecode;
            //security_code += " " + cancelUrl;
            //security_code += " " + notifyUrl;
            security_code += " " + merchantPass;
            //return security_code;
            string md5 = CreateMD5Hash(security_code);

            //security_code += " " + md5;
            Hashtable ht = new Hashtable();

            ht.Add("merchant_site_code", merchantId);
            ht.Add("return_url", HttpUtility.UrlEncode(str_return_url).ToLower());
            ht.Add("receiver", HttpUtility.UrlEncode(receiver));
            ht.Add("transaction_info", request.orderCode);
            ht.Add("order_code", request.orderCode);
            ht.Add("price", request.amount);
            ht.Add("currency", currency);
            ht.Add("quantity", request.totalItem);
            ht.Add("tax", 0);
            ht.Add("discount", 0);
            ht.Add("fee_cal", 0);
            ht.Add("fee_shipping", 0);
            ht.Add("order_description", HttpUtility.UrlEncode("5PTB Paymen NL"));
            ht.Add("buyer_info", HttpUtility.UrlEncode(request.buyerInfo));// "Họ tên người mua *|* Địa chỉ Email *|* Điện thoại *|* Địa chỉ nhận hàng");// "Họ tên người mua *|* Địa chỉ Email *|* Điện thoại *|* Địa chỉ nhận hàng");
            ht.Add("affiliate_code", request.affiliatecode ??"");
            ht.Add("cancel_url", HttpUtility.UrlEncode(cancelUrl).ToLower() ?? "");
            ht.Add("notify_url", HttpUtility.UrlEncode(notifyUrl).ToLower() ?? "");
            ht.Add("secure_code", md5);
            // T?o url redirect

            String redirect_url = urlNl;

            if (redirect_url.IndexOf("?") == -1)
            {
                redirect_url += "?";
            }
            else if (redirect_url.Substring(redirect_url.Length - 1, 1) != "?" && redirect_url.IndexOf("&") == -1)
            {
                redirect_url += "&";
            }

            String url = "";

            // Duy?t các ph?n t? trong m?ng bam ht1 d? t?o redirect url
            IDictionaryEnumerator en = ht.GetEnumerator();

            while (en.MoveNext())
            {
                if (url == "")
                    url += en.Key.ToString() + "=" + en.Value.ToString();
                else
                    url += "&" + en.Key.ToString() + "=" + en.Value.ToString();
            }

            String rdu = redirect_url + url;

            return rdu;
        }
        public string GetErrorMessage(string _ErrorCode, string culture)
        {
            string _Message = "";
            switch (_ErrorCode)
            {
                case "00":
                    _Message = culture == "en-US" ? "Successful" : "Thành công";
                    break;
                case "01":
                    _Message = culture == "en-US" ? "Merchant is not allowed to use this method" : "Merchant không được phép sử dụng phương thức này";
                    break;
                case "02":
                    _Message = culture == "en-US" ? "Incorrect card information" : "Thông tin thẻ sai định dạng";
                    break;
                case "03":
                    _Message = culture == "en-US" ? "Merchant information is incorrect" : "Thông tin merchant không chính xác";
                    break;
                case "04":
                    _Message = culture == "en-US" ? "There was an error during the connection" : "Có lỗi trong quá trình kết nối";
                    break;
                case "05":
                    _Message = culture == "en-US" ? "Invalid amount" : "Số tiền không hợp lệ";
                    break;
                case "06":
                    _Message = culture == "en-US" ? "Invalid cardholder name" : "Tên chủ thẻ không hợp lệ";
                    break;
                case "07":
                    _Message = culture == "en-US" ? "Invalid account number" : "Số tài khoản không hợp lệ";
                    break;
                case "08":
                    _Message = culture == "en-US" ? "Error connecting to the bank. The error occurs when the bank is maintaining and upgrading without coming from the merchant" : "Lỗi kết nối tới ngân hàng. Lỗi xảy ra khi ngân hàng đang bảo trì, nâng cấp mà không xuất phát từ merchant";
                    break;
                case "09":
                    _Message = culture == "en-US" ? "Bank_code is not valid" : "Bank_code không hợp lệ";
                    break;
                case "10":
                    _Message = culture == "en-US" ? "Account balance is not enough to make a transaction" : "Số dư tài khoản không đủ để thực hiện giao dịch";
                    break;
                case "11":
                    _Message = culture == "en-US" ? "Invalid reference code ( ref_code )" : "Mã tham chiếu ( ref_code ) không hợp lệ";
                    break;
                case "12":
                    _Message = culture == "en-US" ? "The reference code ( ref_code ) already exists" : "Mã tham chiếu ( ref_code ) đã tồn tại";
                    break;
                case "14":
                    _Message = culture == "en-US" ? "Function is not correct" : "Function không đúng";
                    break;
                case "16":
                    _Message = culture == "en-US" ? "Receiver_email is locked or blocked and cannot be transacted" : "Receiver_email đang bị khóa hoặc phong tỏa không thể giao dịch";
                    break;
                case "17":
                    _Message = culture == "en-US" ? "Invalid account_type" : "Account_type không hợp lệ";
                    break;
                case "18":
                    _Message = culture == "en-US" ? "The bank is under maintenance" : "Ngân hàng đang bảo trì";
                    break;
                case "99":
                    _Message = culture == "en-US" ? "An unknown error" : "Lỗi không xác định";
                    break;
            }
            return _Message;
        }

        public bool verifyPaymentUrl(string merchantId, string merchantPass,string transaction_info, string order_code, 
            string price, string payment_id, string payment_type, string error_text, string secure_code)
        {
            string str = "";

            str += " " + HttpUtility.HtmlDecode(transaction_info);

            str += " " + order_code;

            str += " " + price;

            str += " " + payment_id;

            str += " " + payment_type;

            str += " " + HttpUtility.HtmlDecode(error_text);

            str += " " + merchantId;

            str += " " + merchantPass;

            // Mã hóa các tham s?
            string verify_secure_code = "";

            verify_secure_code = CreateMD5Hash(str);

            // Xác th?c mã c?a ch? web v?i mã tr? v? t? nganluong.vn
            if (verify_secure_code == secure_code) return true;

            return false;
        }

    }

    #region entity RequestCheckOrder
    public class RequestCheckOrder
    {
        private String _Funtion = "GetTransactionDetail";

        public String Funtion
        {
            get { return _Funtion; }
        }

        private String _Version = "3.1";

        public String Version
        {
            get { return _Version; }
        }
        private String _Merchant_id = String.Empty;
        public String Merchant_id
        {
            get { return _Merchant_id; }
            set { _Merchant_id = value; }
        }
        private String _Merchant_password = String.Empty;

        public String Merchant_password
        {
            get { return _Merchant_password; }
            set { _Merchant_password = value; }
        }
        private String _token = String.Empty;

        public String Token
        {
            get { return _token; }
            set { _token = value; }
        }
    }

    public class ResponseCheckOrder
    {
        private string error_code = string.Empty;

        public string errorCode
        {
            get { return error_code; }
            set { error_code = value; }
        }
        private string error_description = string.Empty;

        public string description
        {
            get { return error_description; }
            set { error_description = value; }
        }
        private string time_limit = string.Empty;

        public string timeLimit
        {
            get { return time_limit; }
            set { time_limit = value; }
        }
        private string _token = string.Empty;

        public string token
        {
            get { return _token; }
            set { _token = value; }
        }
        private string transaction_id = string.Empty;

        public string transactionId
        {
            get { return transaction_id; }
            set { transaction_id = value; }
        }
        private string amount = string.Empty;

        public string paymentAmount
        {
            get { return amount; }
            set { amount = value; }
        }
        private string _order_code = string.Empty;

        public string order_code
        {
            get { return _order_code; }
            set { _order_code = value; }
        }
        private string transaction_type = string.Empty;

        public string transactionType
        {
            get { return transaction_type; }
            set { transaction_type = value; }
        }
        private string transaction_status = string.Empty;

        public string transactionStatus
        {
            get { return transaction_status; }
            set { transaction_status = value; }
        }
        private string payer_name = string.Empty;

        public string payerName
        {
            get { return payer_name; }
            set { payer_name = value; }
        }
        private string payer_email = string.Empty;

        public string payerEmail
        {
            get { return payer_email; }
            set { payer_email = value; }
        }
        private string payer_mobile = string.Empty;

        public string payerMobile
        {
            get { return payer_mobile; }
            set { payer_mobile = value; }
        }
        private string receiver_name = string.Empty;

        public string merchantName
        {
            get { return receiver_name; }
            set { receiver_name = value; }
        }
        private string receiver_address = string.Empty;

        public string merchantAddress
        {
            get { return receiver_address; }
            set { receiver_address = value; }
        }
        private string receiver_mobile = string.Empty;

        public string merchantMobile
        {
            get { return receiver_mobile; }
            set { receiver_mobile = value; }
        }
        private string payment_method = string.Empty;

        public string paymentMethod
        {
            get { return payment_method; }
            set { payment_method = value; }
        }
    }

    #endregion

    #region Entity RequestInfo
    //Thông tin đầu vào của hàm yêu cầu rút tiền SetCashoutRequest
    public class WithdrawMoneyRequest
    {
        private String _Funtion = "SetCashoutRequest";

        public String Funtion
        {
            get { return _Funtion; }
        }
        private String _Merchant_id = String.Empty;
        public String Merchant_id
        {
            get { return _Merchant_id; }
            set { _Merchant_id = value; }
        }
        private String _Receiver_email = String.Empty;

        public String Receiver_email
        {
            get { return _Receiver_email; }
            set { _Receiver_email = value; }
        }
        private String _Merchant_password = String.Empty;

        public String Merchant_password
        {
            get { return _Merchant_password; }
            set { _Merchant_password = value; }
        }
        private String _Ref_code = String.Empty;

        public String RefCode
        {
            get { return _Ref_code; }
            set { _Ref_code = value; }
        }
        private int _Total_amount = 0;
        public int Total_amount
        {
            get { return _Total_amount; }
            set { _Total_amount = value; }
        }
        private int _Account_type = 0;
        public int AccountType
        {
            get { return _Account_type; }
            set { _Account_type = value; }
        }
        private String _bank_code = String.Empty;

        public String bank_code
        {
            get { return _bank_code; }
            set { _bank_code = value; }
        }
        private String _Card_fullname = String.Empty;
        public String Card_fullname
        {
            get { return _Card_fullname; }
            set { _Card_fullname = value; }
        }
        private String _Card_number = String.Empty;
        public String CardNumber
        {
            get { return _Card_number; }
            set { _Card_number = value; }
        }
        private String _Card_month = String.Empty;
        public String CardMonth
        {
            get { return _Card_month; }
            set { _Card_month = value; }
        }
        private String _Card_year = String.Empty;
        public String CardYear
        {
            get { return _Card_year; }
            set { _Card_year = value; }
        }
        private String _branch_name = String.Empty;
        public String BranchName
        {
            get { return _branch_name; }
            set { _branch_name = value; }
        }
        private int? _zone_id = null;
        public int? ZoneId
        {
            get { return _zone_id; }
            set { _zone_id = value; }
        }
        private String _reason = String.Empty;
        public String Reason
        {
            get { return _reason; }
            set { _reason = value; }
        }
    }
    //Thông tin trả về cảu hàm yêu cầu rút tiền SetCashoutRequest
    public class WithdrawMoneyResponse
    {
        private String _error_code = string.Empty;

        public String Error_code
        {
            get { return _error_code; }
            set { _error_code = value; }
        }
        private String _Ref_code = String.Empty;

        public String Ref_Code
        {
            get { return _Ref_code; }
            set { _Ref_code = value; }
        }
        private String _transaction_id = String.Empty;

        public String Transaction_Id
        {
            get { return _transaction_id; }
            set { _transaction_id = value; }
        }

        private String _transaction_status = String.Empty;

        public String Transaction_Status
        {
            get { return _transaction_status; }
            set { _transaction_status = value; }
        }

        private int? _total_amount = null;

        public int? Total_Amount
        {
            get { return _total_amount; }
            set { _total_amount = value; }
        }
    }
    //Thông tin đầu vào của hàm kiểm tra trạng thái giao dịch rút tiền(hàm cũ)
    public class CheckWithdrawalStatusRequestOld
    {
        private String _Funtion = "CheckCashout";

        public String Funtion
        {
            get { return _Funtion; }
        }
        private String _Merchant_id = String.Empty;
        public String Merchant_id
        {
            get { return _Merchant_id; }
            set { _Merchant_id = value; }
        }
        private String _Receiver_email = String.Empty;

        public String Receiver_email
        {
            get { return _Receiver_email; }
            set { _Receiver_email = value; }
        }
        private String _Merchant_password = String.Empty;

        public String Merchant_password
        {
            get { return _Merchant_password; }
            set { _Merchant_password = value; }
        }
        private String _Ref_code = String.Empty;

        public String RefCode
        {
            get { return _Ref_code; }
            set { _Ref_code = value; }
        }
        private String _transaction_id = String.Empty;

        public String TransactionId
        {
            get { return _transaction_id; }
            set { _transaction_id = value; }
        }
    }
    //Thông tin đầu vào của hàm kiểm tra trạng thái giao dịch rút tiền(hàm mới)
    public class CheckWithdrawalStatusRequestNew
    {
        private String _Funtion = "CheckCashoutList";

        public String Funtion
        {
            get { return _Funtion; }
        }
        private String _Merchant_id = String.Empty;
        public String Merchant_id
        {
            get { return _Merchant_id; }
            set { _Merchant_id = value; }
        }
        private String _Receiver_email = String.Empty;

        public String Receiver_email
        {
            get { return _Receiver_email; }
            set { _Receiver_email = value; }
        }
        private String _Merchant_password = String.Empty;

        public String Merchant_password
        {
            get { return _Merchant_password; }
            set { _Merchant_password = value; }
        }
        private String _Ref_code = String.Empty;

        public String RefCode
        {
            get { return _Ref_code; }
            set { _Ref_code = value; }
        }
        private String _transaction_id = String.Empty;

        public String TransactionId
        {
            get { return _transaction_id; }
            set { _transaction_id = value; }
        }
    }
    //Thông tin quả trả về của hàm kiểm tra trạng thái giao dịch rút tiền
    public class CheckWithdrawalStatusResponse
    {
        private String _error_code = string.Empty;
        public String Error_code
        {
            get { return _error_code; }
            set { _error_code = value; }
        }
        private String _Ref_code = String.Empty;
        public String Ref_Code
        {
            get { return _Ref_code; }
            set { _Ref_code = value; }
        }
        private int? _total_amount = null;
        public int? Total_Amount
        {
            get { return _total_amount; }
            set { _total_amount = value; }
        }
        private int? _transaction_status = null;
        public int? Transaction_Status
        {
            get { return _transaction_status; }
            set { _transaction_status = value; }
        }
        private int? _transaction_id = null;
        public int? Transaction_Id
        {
            get { return _transaction_id; }
            set { _transaction_id = value; }
        }
        private String _transaction_refund_id = String.Empty;
        public String Transaction_Refund_Id
        {
            get { return _transaction_refund_id; }
            set { _transaction_refund_id = value; }
        }
    }
    public class WithdrawalMoney
    {
        public string RefCode { get; set; }
        public int TotalAmount { get; set; }
        public string BankCode { get; set; }
        public string CardFullName { get; set; }
        public string CardNumber { get; set; }
        public string CardMonth { get; set; }
        public string CardYear { get; set; }
        public string BranchName { get; set; }
        public int? ZoneId { get; set; }
        public string Reason { get; set; }
    }
    public class CheckWithdrawalStatus
    {
        public string TransactionId { get; set; }
        public string RefCode { get; set; }
    }    
    #endregion
    
}