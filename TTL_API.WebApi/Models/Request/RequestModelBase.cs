﻿
using System;
using System.Collections.Generic;

namespace TTL_API.WebApi.Models.Request
{
    public class RequestModelBase
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public int OrganizationId { get; set; }
    }
    public class DeleteReq
    {
        public List<int> ListID { get; set; }
        public int UserID { get; set; }
        public bool IsAdmin { get; set; }
    }
    public class DeleteCodeReq
    {
        public List<string> ListID { get; set; }
        public int UserID { get; set; }
    }

    public class DeleteGuidReq
    {
        public List<Guid> ListID { get; set; }
        public int UserID { get; set; }
    }

    public class SendOTP
    {
        public string PhoneNo { get; set; }       
        public int UserID { get; set; }
        public string TypeCode { get; set; }
        public string Type { get; set; }
    }
    public class ConfirmOTP
    {
        public string PhoneNo { get; set; }
        public string OTP { get; set; }
        public int UserID { get; set; }
        public string Token { get; set; }
        public string Uid { get; set; }
        public string Type { get; set; }
    }
    public class ForgotPass
    {
        public string UserName { get; set; }
        public string PhoneNo { get; set; }
    }


    public class ForgotUser
    {
        public string PhoneNo { get; set; }
        public string OTP { get; set; }
        public string Token { get; set; }
        public string Uid { get; set; }
        public string Email { get; set; }
        public string Type { get; set; }
    }
    public class Forgot
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string RedirectURL { get; set; }
    }

    public class ChangePass
    {
        public int UserId { get; set; }
        public string Newpass { get; set; }
        public string CurrentPass { get; set; }
        public string ConfirmPass { get; set; }
        public string KeyChange { get; set; }
    }

    public class ChangePassOTP
    {
        public int UserId { get; set; }
        public string Newpass { get; set; }
        public string CurrentPass { get; set; }
        public string ConfirmPass { get; set; }
        public string OTP { get; set; }
        public string Token { get; set; }
        public string Uid { get; set; }
    }

    public class ChangePassByPhone
    {
        public int UserId { get; set; }
        public string Phone { get; set; }
        public string Newpass { get; set; }
        public string ConfirmPass { get; set; }
    }
    public class CheckPhoneAndEmailReq
    {
        public string Phone { get; set; }
        public string Email { get; set; }
    }
    public class ChangeInfoParentOTP
    {
        public int UserId { get; set; }
        public string ParentFullName { get; set; }
        public string ParentEmail { get; set; }
        public string PhoneNumber { get; set; }
        public string OTP { get; set; }
        public string Token { get; set; }
        public string Uid { get; set; }
    }
    public class ChangeMstUserTvOTP
    {
        public string Phone { get; set; }
        public string Newpass { get; set; }
        public string ConfirmPass { get; set; }
    }

    public class CheckOTP
    {
        public string Phone { get; set; }
        public string OTP { get; set; }
    }

    public class ActiveCourse
    {
        public string ActionCode { get; set; }
        public int course_id { get; set; }
        public int user_id { get; set; }
    }
    public class LessonReq
    {
        public int LessionID { get; set; }
        public int UserID { get; set; }
        public int ChapterLessonId { get; set; }
        public int CourseId { get; set; }
    }
    public class ResultLession
    {
        public int LessionID { get; set; }
        public int UserID { get; set; }
        public int ChapterLessonUserId { get; set; }
        public string TypeCode { get; set; }
    }

    public class RejectReq
    {
        public int ChapterLessonUserId { get; set; }
        public int UserID { get; set; }
        public string Type { get; set; } //"homework","test"
    }
    public class MarkHomeWorkReq
    {
        public int ChapterLessonUserId { get; set; }
        public int UserID { get; set; }
        public double PointMark { get; set; }
    }

    public class GetUserTitleDetailRequest
    {
        public int userId { get; set; }
        public int titleId { get; set; }
    }
    public class GetUserDetailRequest
    {
        public int userId { get; set; }
        public string code { get; set; }
    }

    public class MstNotificationsFitter
    {
        public int userId { get; set; }
        public bool? seen { get; set; }
        public string keySearch { get; set; }
        public string sortType { get; set; }

    }

    public class getListTitleRequestFilter
    {
        public int userId { get; set; }
        public bool paging { get; set; }
        public int? page { get; set; }
        public int? limit { get; set; }
        public bool? completed { get; set; }
    }
    public class LeaveCourse
    {
        public int course_id { get; set; }
        public int user_id { get; set; }
    }

    public class GoogleUserRequest
    {
        //public const string PROVIDER = "google";

        //public string IdToken { get; set; }
        public string ProviderName { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ProfilePicURL { get; set; }
        public string AppName { get; set; }
    }

    public class CheckEmailReq
    {
        public int UserId { get; set; }
        public string Email { get; set; }
    }
    public class UnLockReq
    {
        public int UserId { get; set; }
        public bool IsBanned { get; set; }
    }
}
