﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Helpers;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.SystemAdmin
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class AccountController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly IAccountService _AccountService;
        private readonly IAccount_RoleService _account_FunctionService;
        private readonly IRole_FunctionService _role_FunctionService;
        private readonly IRoleService _roleService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public AccountController(
              ILoggerService logger,
              IConfiguration config,
              IAccountService accountService,
              IAccount_RoleService account_FunctionService,
              IRole_FunctionService role_FunctionService,
              IRoleService roleService,
              IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _AccountService = accountService;
            _account_FunctionService = account_FunctionService;
            _role_FunctionService = role_FunctionService;
            _roleService = roleService;
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<ResponseResult<ResponseList>> GetAll(int organizationId, int limit, int page)
        {
            try
            {
                var a = await _AccountService.GetAllAsync(organizationId, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllAccount")]
        public async Task<ResponseResult<ResponseList>> GetAllCombox(int organizationId)
        {
            try
            {
                var a = await _AccountService.GetAllComboxAsync(organizationId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetById")]
        public async Task<ResponseResult<Account>> GetByIdAsync(int id)
        {
            try
            {
                var a = await _AccountService.GetByIdAsync(id);
                a.Password = CryptorEngine.Decrypt(a.Password, true, "Hieu.Le-GC.Soft");
                var result = new ResponseResult<Account>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Account>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ResponseResult<Account>> Add(AccountModel Account)
        {
            try
            {
                Account.Password = CryptorEngine.Encrypt(Account.Password, true, "Hieu.Le-GC.Soft");
                var a = await _AccountService.AddAsync(Account);
                var result = new ResponseResult<Account>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Account>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Update")]
        public async Task<ResponseResult<string>> Update(AccountModel Account)
        {
            try
            {
                Account.Password = CryptorEngine.Encrypt(Account.Password, true, "Hieu.Le-GC.Soft");
                var a = await _AccountService.UpdateAsync(Account);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<ResponseResult<string>> Delete(int id)
        {
            try
            {
                var a = await _AccountService.DeleteAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetTreeRole")]
        public async Task<ResponseResult<List<RoleModelTree>>> GetTreeRole(int organizationId, int accountId)
        {
            try
            {
                var resList = _roleService.GetByOrganizationAsync(organizationId).Where(z=>z.Code!="SupperAdmin").Select(z => new RoleModelTree() { Id = z.Id, Text = z.Title, ParentId = 0 });
                var account_FunctionList = await _account_FunctionService.GetAllByAccountIdAsync(accountId);
                var tets = _AccountService.GetChildren(resList.ToList(), 0, account_FunctionList.ToList());
                var resInfo = JsonConvert.SerializeObject(tets, Formatting.Indented);
                return CreateResponseResult<List<RoleModelTree>>(tets);
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<List<RoleModelTree>>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
    }
}
