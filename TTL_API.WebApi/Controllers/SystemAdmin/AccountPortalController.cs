﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Extensions;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Helpers;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.SystemAdmin
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class AccountPortalController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly IAccountPortalService _AccountService;
        private readonly ICustomerService _customerService;
        public AccountPortalController(
              ILoggerService logger,
              IConfiguration config,
              IAccountPortalService accountService,
              ICustomerService customerService
              )
        {
            _logger = logger;
            _config = config;
            _AccountService = accountService;
            _customerService = customerService;
        }
        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public async Task<ResponseResult<UserReq>> Login([FromBody] Login request)
        {
            try
            {

                // Suppper Admin và Nhân Viên
                var account = await _AccountService.GetAccountUserNameAsync(request.UserName);

                if (account != null && account.UserName == request.UserName)
                {
                    if (!account.Status)
                        return new ResponseResult<UserReq>(RetCodeEnum.ApiError, "Tài khoản đã tạm dừng hoạt động", null);
                    var pass = CryptorEngine.Encrypt(request.Password, true, "Hieu.Le-GC.Soft");
                    if (account.Password == pass)
                    {

                        var resView = await _customerService.GetAllByPhoneStoreAsync(68, account.UserName, account.CustomerCode);
                        var resViewList = ((IEnumerable<object>)resView.ListData).Cast<ViewCustomer>().OrderByDescending(z => z.Ref_Source).FirstOrDefault();
                        var userRes = new UserReq()
                        {
                            Id = account.Id,
                            UserName = account.UserName,
                            FullName = resViewList?.FullName,
                            Password = account.Password,
                            Role = account.Roles
                            //,
                            //OrganizationId = roleCode.OrganizationId

                        };
                        //userRes.Token = CreateToken(userRes);
                        var result = CreateResponseResult(userRes);
                        return result;
                    }
                    else
                    {
                        var userRes = new UserReq()
                        {
                            UserName = account.UserName,
                            Password = account.Password,

                        };
                        var res2 = new ResponseResult<UserReq>(RetCodeEnum.ApiError, "Mật khẩu không đúng", userRes);
                        return res2;
                    }
                }
                return new ResponseResult<UserReq>(RetCodeEnum.ApiError, "Tài khoản không tồn tài", null);
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return CreateResponseApiError<UserReq>(ex.Message);
            }
        }

        [HttpGet]
        [Route("CheckUserName")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> CheckUserName(string userName)
        {
            try
            {
                var res = string.Empty;
                var a = await _AccountService.GetAccountUserNameAsync(userName);
                if (a != null && a.Id > 0)
                {
                    res = "Tài khoản đã tồn tại";
                    return new ResponseResult<string>(RetCodeEnum.ApiError, res, res);
                }
                return new ResponseResult<string>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), res);

            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetById")]
        [AllowAnonymous]
        public async Task<ResponseResult<AccountPortalRep>> GetByIdAsync(int id)
        {
            try
            {
                var a = await _AccountService.GetByIdAsync(id);
                a.Password = CryptorEngine.Decrypt(a.Password, true, "Hieu.Le-GC.Soft");
                var result = new ResponseResult<AccountPortalRep>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<AccountPortalRep>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpPost]
        [Route("GetListCustomerByPhone")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetListCustomerByPhoneAsync(ViewCustomerFitter viewCustomerFitter, int limit, int page)
        {
            try
            {
                var a = await _customerService.GetListCustomerByPhoneStoreAsync(viewCustomerFitter, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Add")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> Add(AccountPortalReq Account)
        {
            try
            {
                // check otp 
                var _connectionString = _config["MainConnectionString"];
                using (var conn = new SqlConnection(_connectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        await conn.OpenAsync();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "COMFIRM.OTP.CREATE");
                    if(Account.IsVN==false && !string.IsNullOrEmpty(Account.Email))
                    {
                        paramaters.Add("@Phone", Account.Email);
                    }
                    else
                    {
                        paramaters.Add("@Phone", Account.UserName);
                    }
                    paramaters.Add("@OTP", Account.OTP);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = await conn.QueryAsync<string>("SP_API_AccountHelper", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    var _apiResult = new ApiResult<string>();
                    _apiResult.RetCode = paramaters.Get<Int32>("@RetCode");
                    _apiResult.RetText = paramaters.Get<string>("@RetText");
                    _apiResult.Data = result.FirstOrDefault();
                    if (_apiResult.RetCode != 0)
                        return new ResponseResult<string>((RetCodeEnum)paramaters.Get<Int32>("@RetCode"), paramaters.Get<string>("@RetText"), null);
                }
                // check tồn tại
                var checkUserName = await CheckUserName(Account.UserName);
                if (checkUserName.RetCode == RetCodeEnum.ApiError)
                    return new ResponseResult<string>(RetCodeEnum.ApiError, checkUserName.Data, null);
                // check trong cái view_Customer đã có chưa
                var resView = await _customerService.GetAllByPhoneStoreAsync(68, Account.UserName, string.Empty);
                bool isCreateCustomer = true;
                var resViewList = ((IEnumerable<object>)resView.ListData).Cast<ViewCustomer>().OrderByDescending(z => z.Ref_Source).FirstOrDefault();
                if (resViewList == null)
                {
                    if (string.IsNullOrEmpty(Account.FullName))
                        return new ResponseResult<string>(RetCodeEnum.ApiError, "Họ tên không đc bỏ trống.", "Họ tên không đc bỏ trống.");
                    if (string.IsNullOrEmpty(Account.Phone))
                        return new ResponseResult<string>(RetCodeEnum.ApiError, "Số điện thoại không đc bỏ trống.", "Số điện thoại không đc bỏ trống.");
                    //if (string.IsNullOrEmpty(Account.Email))
                    //    return new ResponseResult<string>(RetCodeEnum.ApiError, "Email không đc bỏ trống.", "Email không đc bỏ trống.");
                }
                else
                {
                    isCreateCustomer = false;
                }
                Account.Password = CryptorEngine.Encrypt(Account.Password, true, "Hieu.Le-GC.Soft");
                //Account.CreateBy = GetCurrentUserId();
                var a = await _AccountService.AddAsync(Account, resViewList, isCreateCustomer);
                if (a)
                {
                    return new ResponseResult<string>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a.ToString());
                }
                else
                {
                    return new ResponseResult<string>(RetCodeEnum.ApiError, RetCodeEnum.Ok.ToString(), "Đã xảy ra lỗi");
                }
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Update")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> Update(AccountPortalReq Account)
        {
            try
            {
                //Account.Password = CryptorEngine.Encrypt(Account.Password, true, "Hieu.Le-GC.Soft");
                var a = await _AccountService.UpdateAsync(Account);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Delete")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> Delete(int id)
        {
            try
            {
                var a = await _AccountService.DeleteAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        //[Route("ForgetUsername")]
        //[HttpPost]
        //[AllowAnonymous]
        //public async Task<IActionResult> ForgetUsername([FromBody] ForgetUserRequest forgetInfo)
        //{

        //    var mainConnectString = _config["MainConnectionString"];
        //    using (var conn = new SqlConnection(mainConnectString))
        //    {
        //        if (conn.State == System.Data.ConnectionState.Closed)
        //            await conn.OpenAsync();
        //        var paramaters = new DynamicParameters();
        //        paramaters.Add("@Action", "FORGET.ACCOUNT");
        //        paramaters.Add("@Phone", forgetInfo.Phone);
        //        paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
        //        paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
        //        var result = await conn.QueryAsync<ForgetUserRequest>("SP_API_AccountHelper", paramaters, null, null, System.Data.CommandType.StoredProcedure);
        //        var _apiResult = new ApiResult<ForgetUserRequest>();
        //        _apiResult.RetCode = paramaters.Get<Int32>("@RetCode");
        //        _apiResult.RetText = paramaters.Get<string>("@RetText");
        //        _apiResult.Data = result.FirstOrDefault();
        //        return Ok(_apiResult);
        //    }
        //}
        //[Route("OTPConfirmAccount")]
        //[HttpPost]
        //[AllowAnonymous]
        //public async Task<IActionResult> OTPConfirmAccount([FromBody] OTPConfirmAccount forgetInfo)
        //{

        //    var _connectionString = _config["MainConnectionString"];
        //    using (var conn = new SqlConnection(_connectionString))
        //    {
        //        if (conn.State == System.Data.ConnectionState.Closed)
        //            await conn.OpenAsync();
        //        var paramaters = new DynamicParameters();
        //        paramaters.Add("@Action", "CONFIRM.ACCOUNT");
        //        paramaters.Add("@Phone", forgetInfo.Phone);
        //        paramaters.Add("@OTP", forgetInfo.OTP);
        //        paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
        //        paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
        //        var result = await conn.QueryAsync<AccountInfo>("SP_API_AccountHelper", paramaters, null, null, System.Data.CommandType.StoredProcedure);
        //        var _apiResult = new ApiResults<AccountInfo>();
        //        _apiResult.RetCode = paramaters.Get<Int32>("@RetCode");
        //        _apiResult.RetText = paramaters.Get<string>("@RetText");
        //        result.ToList().ForEach(
        //            z => z.Password = CryptorEngine.Decrypt(z.Password, true, "Hieu.Le-GC.Soft"));
        //        _apiResult.Data = result.ToList();
        //        return Ok(_apiResult);
        //    }
        //}
        [Route("ForgetPassword")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgetPassword([FromBody] ForgetPasswordRequest forgetInfo)
        {           
            var _connectionString = _config["MainConnectionString"];
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    await conn.OpenAsync();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "FORGET.PASSWORD");
                paramaters.Add("@Phone", forgetInfo.Phone);
                paramaters.Add("@Email", forgetInfo.Email);
                paramaters.Add("@IsVN", forgetInfo.IsVN);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = await conn.QueryAsync<ForgetPasswordRequest>("SP_API_AccountHelper", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                var _apiResult = new ApiResult<ForgetPasswordRequest>();
                _apiResult.RetCode = paramaters.Get<Int32>("@RetCode");
                _apiResult.RetText = paramaters.Get<string>("@RetText");
                _apiResult.Data = result.FirstOrDefault();
                if (_apiResult.RetCode == 0 && forgetInfo.IsVN == false && !string.IsNullOrEmpty(forgetInfo.Email))
                {
                     // SendMail
                    CallSendEmail(forgetInfo.Email, _apiResult.RetText, "TAM-TRI-LUC");
                    _apiResult.RetText = $"OTP đã được gửi đến Email:  {forgetInfo.Email}";        
                }
                return Ok(_apiResult);
            }
        }
        [Route("OTPConfirmPassword")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> OTPConfirmPassword([FromBody] OTPConfirmPassword forgetInfo)
        {

            var _connectionString = _config["MainConnectionString"];
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    await conn.OpenAsync();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "CONFIRM.ACCOUNT.PASSWORD");
                if (forgetInfo.IsVN == false && !string.IsNullOrEmpty(forgetInfo.Email))
                {
                    paramaters.Add("@Phone", forgetInfo.Email);
                }
                else
                    paramaters.Add("@Phone", forgetInfo.Phone);
                paramaters.Add("@OTP", forgetInfo.OTP);
                paramaters.Add("@IsVN", forgetInfo.IsVN);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = await conn.QueryAsync<AccountInfo>("SP_API_AccountHelper", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                var _apiResult = new ApiResults<AccountInfo>();
                _apiResult.RetCode = paramaters.Get<Int32>("@RetCode");
                _apiResult.RetText = paramaters.Get<string>("@RetText");
                result.ToList().ForEach(
                   z => z.Password = CryptorEngine.Decrypt(z.Password, true, "Hieu.Le-GC.Soft"));
                _apiResult.Data = result.ToList();
                return Ok(_apiResult);
            }
        }

        [Route("SendOTP")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SendOTP([FromBody] ForgetPasswordRequest forgetInfo)
        {

            var _connectionString = _config["MainConnectionString"];
            using (var conn = new SqlConnection(_connectionString))
            {
                forgetInfo.IsVN = true;
                if (conn.State == System.Data.ConnectionState.Closed)
                    await conn.OpenAsync();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "SEND.OTP");
                paramaters.Add("@Phone", forgetInfo.Phone);
                paramaters.Add("@Email", forgetInfo.Email);
                paramaters.Add("@IsVN", forgetInfo.IsVN);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = await conn.QueryAsync<ForgetPasswordRequest>("SP_API_AccountHelper", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                var _apiResult = new ApiResult<ForgetPasswordRequest>();
                _apiResult.RetCode = paramaters.Get<Int32>("@RetCode");
                _apiResult.RetText = paramaters.Get<string>("@RetText");
                _apiResult.Data = result.FirstOrDefault();
                if (_apiResult.RetCode == 0 && _apiResult.Data != null && _apiResult.Data.IsVN == false && !string.IsNullOrEmpty(forgetInfo.Email))
                {
                    // SendMail
                    CallSendEmail(forgetInfo.Email, _apiResult.RetText, "TAM-TRI-LUC");
                    _apiResult.RetText = $"OTP đã được gửi đến Email:  {forgetInfo.Email}";
                    _apiResult.Data = null;
                }
                return Ok(_apiResult);
            }
        }
        [Route("SendOTPCreate")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SendOTPCreate([FromBody] ForgetPasswordRequest forgetInfo)
        {

            var _connectionString = _config["MainConnectionString"];
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    await conn.OpenAsync();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "SEND.OTP.CREATE");
                paramaters.Add("@Phone", forgetInfo.Phone);
                paramaters.Add("@Email", forgetInfo.Email);
                paramaters.Add("@IsVN", forgetInfo.IsVN);
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = await conn.QueryAsync<ForgetPasswordRequest>("SP_API_AccountHelper", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                var _apiResult = new ApiResult<ForgetPasswordRequest>();
                _apiResult.RetCode = paramaters.Get<Int32>("@RetCode");
                _apiResult.RetText = paramaters.Get<string>("@RetText");
                _apiResult.Data = result.FirstOrDefault();
                if (_apiResult.RetCode == 0 && forgetInfo.IsVN == false && !string.IsNullOrEmpty(forgetInfo.Email))
                {
                    // SendMail
                    CallSendEmail(forgetInfo.Email, _apiResult.RetText, "TAM-TRI-LUC");
                    _apiResult.RetText = $"OTP đã được gửi đến Email:  {forgetInfo.Email}";
                }
                return Ok(_apiResult);
            }
        }

        [Route("ChangePassword")]
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordRequest forgetInfo)
        {
            bool IsVN = true;
            if (forgetInfo.PassNew != forgetInfo.PassConfirm)
            {
                var _apiResult = new ApiResult<string>();
                _apiResult.RetCode = 1;
                _apiResult.RetText = "Mật khẩu chưa khớp nhau";
                _apiResult.Data = _apiResult.RetText;
                return Ok(_apiResult);
            }    
            var _connectionString = _config["MainConnectionString"];
            using (var conn = new SqlConnection(_connectionString))
            {
                if (conn.State == System.Data.ConnectionState.Closed)
                    await conn.OpenAsync();
                var paramaters = new DynamicParameters();
                paramaters.Add("@Action", "CHANGE.PASSWORD");
                paramaters.Add("@OTP", forgetInfo.OTP);
                paramaters.Add("@IsVN", IsVN);
                paramaters.Add("@Phone", forgetInfo.Phone);
                paramaters.Add("@Email", forgetInfo.Email);
                paramaters.Add("@PassOld", CryptorEngine.Encrypt(forgetInfo.PassOld, true, "Hieu.Le-GC.Soft"));
                paramaters.Add("@PassNew", CryptorEngine.Encrypt(forgetInfo.PassNew, true, "Hieu.Le-GC.Soft"));
                paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                var result = await conn.QueryAsync<ForgetPasswordRequest>("SP_API_AccountHelper", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                var _apiResult = new ApiResult<string>();
                _apiResult.RetCode = paramaters.Get<Int32>("@RetCode");
                _apiResult.RetText = paramaters.Get<string>("@RetText");
                _apiResult.Data = _apiResult.RetText;
                return Ok(_apiResult);
            }
        }

        private void CallSendEmail(string email, string content, string appId)
        {
            if (Services.Extensions.StringUtil.Validate(email))
            {
                var mailTitle = "Mã OTP Xác Thực";
                var displayName = string.Empty;
                switch (appId)
                {
                    case "TAM-TRI-LUC":
                        displayName = "TÂM TRÍ LỰC";
                        break;                   
                }

                var mailContent = content;
                var emailTos = new List<string>();
                emailTos.Add(email);
                var _mailHelper = new EmailHelper()
                {
                    EmailTos = emailTos,
                    DisplayName = displayName,
                    Title = mailTitle,
                    Body = mailContent
                };
                var hasValue = _mailHelper.SendEmailsThread();
            }
        }
    }
    public class ForgetUserRequest
    {
        public string Phone { get; set; }
    }
    public class ForgetPasswordRequest
    {
        public string Phone { get; set; }
        public string? Email { get; set; }
        public bool? IsVN { get; set; }

    }
    public class ChangePasswordRequest
    {
        public string OTP { get; set; }
        public string? Phone { get; set; }
        public string? Email { get; set; }
        public string PassOld { get; set; }
        public string PassNew { get; set; }
        public string PassConfirm { get; set; }
    }

    public class OTPConfirmAccount
    {
        public string Phone { get; set; }
        public string OTP { get; set; }
    }
    public class OTPConfirmPassword
    {
        public string Phone { get; set; }
        public string OTP { get; set; }
         public string? Email { get; set; }
        public bool? IsVN { get; set; }
    }
    public class AccountInfo
    {
        public int Id { get; set; }
        public string fullName { get; set; }
        public string userName { get; set; }
        public string Password { get; set; }
    }
}
