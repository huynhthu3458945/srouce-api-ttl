﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.SystemAdmin
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class OrganizationController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly IOrganizationService _OrganizationService;
        private readonly IFunctionSystemService _functionSystemService;
        private readonly IFunction_OrganizationService _function_OrganizationService;
        private readonly IAccountService _accountService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public OrganizationController(
              ILoggerService logger,
              IConfiguration config,
              IOrganizationService organizationService,
              IFunctionSystemService functionSystemService,
              IFunction_OrganizationService function_OrganizationService,
              IAccountService accountService,IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _OrganizationService = organizationService;
            _functionSystemService = functionSystemService;
            _function_OrganizationService = function_OrganizationService;
            _accountService = accountService;
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<ResponseResult<ResponseList>> GetAll(int limit, int page)
        {
            try
            {
                var a = await _OrganizationService.GetAllAsync(limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetByCode")]
        public async Task<ResponseResult<Organization>> GetByCode(string code)
        {
            try
            {
                var a = await _OrganizationService.GetByCodeAsync(code);
                var result = new ResponseResult<Organization>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Organization>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetAllCombobox")]
        public async Task<ResponseResult<ResponseList>> GetCombobox()
        {
            try
            {
                var a = await _OrganizationService.GetAllComboboxAsync();
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetById")]
        public async Task<ResponseResult<Organization>> GetByIdAsync(int id)
        {
            try
            {
                var a = await _OrganizationService.GetByIdAsync(id);
                var result = new ResponseResult<Organization>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Organization>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ResponseResult<Organization>> Add(OrganizationModel Organization)
        {
            try
            {
                var a = await _OrganizationService.AddAsync(Organization);
                var result = new ResponseResult<Organization>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Organization>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Update")]
        public async Task<ResponseResult<string>> Update(OrganizationModel Organization)
        {
            try
            {
                var a = await _OrganizationService.UpdateAsync(Organization);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<ResponseResult<string>> Delete(int id)
        {
            try
            {
                var a = await _OrganizationService.DeleteAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetTreeFunctionSysTem")]
        public async Task<ResponseResult<List<FunctionSystemModelTree>>> GetTreeFunctionSysTem(int organizationId, int applicationId)
        {
            try
            {
                var resList = _functionSystemService.GetAll(applicationId).Select(z => new FunctionSystemModelTree() { Id = z.Id, Text = z.Title, ParentId = z.ParentId });
                var organizationList = _function_OrganizationService.GetById(organizationId, applicationId);
                var tets = _functionSystemService.GetChildren(resList.ToList(), 0, organizationList.ToList());
                var resInfo = JsonConvert.SerializeObject(tets, Formatting.Indented);
                return CreateResponseResult<List<FunctionSystemModelTree>>(tets);
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<List<FunctionSystemModelTree>>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("CheckExistUserName")]
        public async Task<ResponseResult<Account>> CheckExistUserName(string userName)
        {
            try
            {
                var a = await _accountService.GetAccountUserNameAsync(userName);
                var result = new ResponseResult<Account>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Account>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
    }
}
