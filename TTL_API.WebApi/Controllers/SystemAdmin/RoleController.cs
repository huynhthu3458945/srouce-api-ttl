﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.SystemAdmin
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class RoleController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly IRoleService _roleService;
        private readonly IRole_FunctionService _role_FunctionService;
        private readonly IFunction_OrganizationService _function_OrganizationService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public RoleController(
              ILoggerService logger,
              IConfiguration config,
              IRoleService roleService,
              IRole_FunctionService role_FunctionService,
              IFunction_OrganizationService function_OrganizationService,
              IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _roleService = roleService;
            _role_FunctionService = role_FunctionService;
            _function_OrganizationService = function_OrganizationService;
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<ResponseResult<ResponseList>> GetAll(int organizationId, int limit, int page)
        {
            try
            {
                var a = await _roleService.GetAllAsync(organizationId, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllCombobox")]
        public async Task<ResponseResult<ResponseList>> GetAllCombox(int limit, int page)
        {
            try
            {
                var a = await _roleService.GetAllComboxAsync();
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetById")]
        public async Task<ResponseResult<Role>> GetByIdAsync(int id)
        {
            try
            {
                var a = await _roleService.GetByIdAsync(id);
                var result = new ResponseResult<Role>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Role>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetByCode")]
        public async Task<ResponseResult<Role>> GetByCodeAsync(string code, int organizationId)
        {
            try
            {
                var a = await _roleService.GetByCodeAsync(code, organizationId);
                var result = new ResponseResult<Role>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Role>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ResponseResult<Role>> Add(RoleModel Role)
        {
            try
            {
                var a = await _roleService.AddAsync(Role);
                var result = new ResponseResult<Role>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Role>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Update")]
        public async Task<ResponseResult<string>> Update(RoleModel Role)
        {
            try
            {
                var a = await _roleService.UpdateAsync(Role);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<ResponseResult<string>> Delete(int id)
        {
            try
            {
                var a = await _roleService.DeleteAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetTreeFunctionOrganization")]
        public async Task<ResponseResult<List<FunctionOrganizationModelTree>>> GetTreeFunctionOrganization(int organizationId, int applicationId, int roleId)
        {
            try
            {
                var resList = _function_OrganizationService.GetById(organizationId, applicationId).OrderBy(z=>z.ParentId).ThenBy(z=>z.Position).Select(z => new FunctionOrganizationModelTree() { FuncId = z.Id, Id = z.FunctionSystemId, Text = z.Title, ParentId = z.ParentId });
                var role_FunctionList = await _role_FunctionService.GetAllByRoleIdAsync(roleId);
                var tets = _roleService.GetChildren(resList.ToList(), 0, role_FunctionList.ToList());
                var resInfo = JsonConvert.SerializeObject(tets, Formatting.Indented);
                return CreateResponseResult<List<FunctionOrganizationModelTree>>(tets);
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<List<FunctionOrganizationModelTree>>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
    }
}
