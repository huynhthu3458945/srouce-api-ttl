﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.SystemAdmin
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class FunctionSystemController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly IFunctionSystemService _FunctionSystemService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public FunctionSystemController(
              ILoggerService logger,
              IConfiguration config,
              IFunctionSystemService FunctionSystemService,
              ISystmeAccountService systmeAccountService, IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _FunctionSystemService = FunctionSystemService;
            _unitOfWork = unitOfWork;
        }
        [HttpPost]
        [Route("GetAll")]
        public async Task<ResponseResult<ResponseList>> GetAll(FunctionSystemFitter fitter, int limit, int page)
        {
            try
            {
                var a = await _FunctionSystemService.GetAllAsync(fitter, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetMenu")]
        public async Task<ResponseResult<ResponseList>> GetMenu(string applicationCode, int organizationId, string userName)
        {
            try
            {
                var a = await _FunctionSystemService.GetMenuAsync(applicationCode, organizationId, userName);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetRuleAction")]
        public async Task<ResponseResult<ResponseList>> GetRuleAction(string applicationCode, int organizationId, string userName, string area, string controller, string action)
        {
            try
            {
                var a = await _FunctionSystemService.GetRuleActionAsync(applicationCode, organizationId, userName, area, controller, action);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetRuleWareHouseAction")]
        public async Task<ResponseResult<ResponseList>> GetRuleWareHouseAction(string applicationCode, int organizationId, string userName, string area, string controller)
        {
            try
            {
                var a = await _FunctionSystemService.GetRuleActionWareHouseAsync(applicationCode, organizationId, userName, area, controller);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetAllTreeCombobox")]
        public async Task<ResponseResult<ResponseList>> GetAllTree(int id)
        {
            try
            {
                var a = await _FunctionSystemService.GetAllTreeAsync(id);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetById")]
        public async Task<ResponseResult<FunctionSystem>> GetByIdAsync(int id)
        {
            try
            {
                var a = await _FunctionSystemService.GetByIdAsync(id);
                var result = new ResponseResult<FunctionSystem>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<FunctionSystem>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ResponseResult<FunctionSystem>> Add(FunctionSystem functionSystem)
        {
            try
            {
                var a = await _FunctionSystemService.AddAsync(functionSystem);
                var result = new ResponseResult<FunctionSystem>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<FunctionSystem>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Update")]
        public async Task<ResponseResult<string>> Update(FunctionSystem functionSystem)
        {
            try
            {
                var a = await _FunctionSystemService.UpdateAsync(functionSystem);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<ResponseResult<string>> Delete(int id)
        {
            try
            {
                var a = await _FunctionSystemService.DeleteAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

    }
}
