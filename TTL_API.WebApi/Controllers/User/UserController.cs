﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Helpers;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;

namespace TTL_API.WebApi.Controllers.User
{

    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class UserController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly ISystmeAccountService _systmeAccountService;
        private readonly IAccountService _accountService;
        private readonly IStaffService _staffService;
        private readonly IRoleService _roleService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public UserController(
              ILoggerService logger,
              IConfiguration config,
              ISystmeAccountService systmeAccountService,
              IAccountService accountService,
              IStaffService staffService,
              IRoleService roleService,
              IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _systmeAccountService = systmeAccountService;
            _accountService = accountService;
            _staffService = staffService;
            _roleService = roleService;
            _unitOfWork = unitOfWork;
        }
        public class UserLoginVMs
        {
            public string DivisionID { get; set; }
            public int ID { get; set; }
            public string Username { get; set; }
            public string FullName { get; set; }
            public string Password { get; set; }
            public string Email { get; set; }
            public string Role { get; set; }
            public string Token { get; set; }
        }
        [HttpPost]
        [Route("Login")]
        [AllowAnonymous]
        public async Task<ResponseResult<UserReq>> Login([FromBody] Login request)
        {
            try
            {
                // User hệ thống
                SystemAccount model = new SystemAccount()
                {
                    UserName = request.UserName,
                    Password = request.Password
                };
                var systemAccount = await _systmeAccountService.GetSystmeAccountUserNameAsync(model.UserName);
                if (systemAccount != null && systemAccount.UserName == request.UserName)
                {
                    if (systemAccount.Password == request.Password)
                    {
                        var userRes = new UserReq()
                        {
                            Id = systemAccount.Id,
                            UserName = systemAccount.UserName,
                            FullName = systemAccount.Name,
                            Password = systemAccount.Password,
                            Role = "Administrator_full"

                        };
                        userRes.Token = CreateToken(userRes);
                        var result = CreateResponseResult(userRes);
                        return result;
                    }
                    else
                    {
                        var userRes = new UserReq()
                        {
                            UserName = systemAccount.UserName,
                            Password = systemAccount.Password,

                        };
                        var res2 = new ResponseResult<UserReq>(RetCodeEnum.ApiError, "Mật khẩu không đúng", userRes);
                        return res2;
                    }
                }
                // Suppper Admin và Nhân Viên
                var account = await _accountService.GetAccountUserNameAsync(request.UserName);

                if (account != null && account.UserName == request.UserName)
                {
                    if (!account.Status)
                        return new ResponseResult<UserReq>(RetCodeEnum.ApiError, "Tài khoản đã tạm dừng hoạt động", null);
                    var pass = CryptorEngine.Encrypt(request.Password, true, "Hieu.Le-GC.Soft");
                    if (account.Password == pass)
                    {
                        var roleCode = await _roleService.GetAccOrgByUserNameAsync(account.UserName);
                        var accType = await _roleService.GetAccTypeByUserNameAsync(account.UserName);
                        var staff = await _staffService.GetStafByAccountId(account.Id);
                        var userRes = new UserReq()
                        {
                            Id = account.Id,
                            UserName = account.UserName,
                            FullName = staff != null ? staff.FullName : "Admin",
                            Password = account.Password,
                            Role = accType.Code
                            //,
                            //OrganizationId = roleCode.OrganizationId

                        };
                        if (accType.Code == "SupperAdmin")
                            userRes.OrganizationId = roleCode.OrganizationId;
                        userRes.Token = CreateToken(userRes);
                        var result = CreateResponseResult(userRes);
                        return result;
                    }
                    else
                    {
                        var userRes = new UserReq()
                        {
                            UserName = account.UserName,
                            Password = account.Password,

                        };
                        var res2 = new ResponseResult<UserReq>(RetCodeEnum.ApiError, "Mật khẩu không đúng", userRes);
                        return res2;
                    }
                }
                return new ResponseResult<UserReq>(RetCodeEnum.ApiError, "Tài khoản không tồn tài", null);
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return CreateResponseApiError<UserReq>(ex.Message);
            }
        }
        [HttpPost]
        [Route("GetToken")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> GetUserTokenAsync([FromBody] RequestModelBase request)
        {
            try
            {
                if (request.UserId <= 0) return new ResponseResult<string>(RetCodeEnum.ResultNotExists, RetCodeEnum.ResultNotExists.ToString(), null);

                // Account System
                var userModel = new SystemAccount()
                {
                    Id = request.UserId,
                    UserName = request.UserName
                };

                var user = await _systmeAccountService.GetSystmeAccountAsync(userModel);
                var token = string.Empty;
                if (user != null && user.Id > 0)
                {
                    var userRes = new UserReq()
                    {
                        Id = user.Id,
                        UserName = user.UserName,
                        FullName = user.Name,
                        Password = user.Password

                    };
                    token = CreateToken(userRes);
                }
                // Account 
                var userAccount = new Account()
                {
                    Id = request.UserId,
                    UserName = request.UserName
                };

                var account = await _accountService.GetAccountAsync(userAccount);
                token = string.Empty;
                if (account != null && account.Id > 0)
                {
                    var roleCode = await _roleService.GetAccOrgByUserNameAsync(account.UserName);
                    var accType = await _roleService.GetAccTypeByUserNameAsync(account.UserName);
                    var userRes = new UserReq()
                    {
                        Id = account.Id,
                        UserName = account.UserName,
                        FullName = accType.Title,
                        Password = account.Password
                        //,
                        //OrganizationId = roleCode.OrganizationId

                    };
                    if (accType.Code == "SupperAdmin")
                        userRes.OrganizationId = roleCode.OrganizationId;
                    token = CreateToken(userRes);
                }
                var result = CreateResponseResult(token);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return CreateResponseApiError<string>(ex.Message);
            }
        }
        #region "Private Methods"
        private string CreateToken(UserReq user)
        {
            if (user == null) return string.Empty;

            var tokenString = CreateTokenString(
                string.IsNullOrEmpty(user.Email) ? user.Id.ToString() : user.Email,
                user.FullName ?? user.Email,
                user.Id.ToString(),
                user.UserName,
                (user.Phone == null ? user.Id.ToString() : user.Phone),
               // user.OrganizationId.ToString(),
                _config["Tokens:Key"],
                _config["Tokens:Issuer"]);
            return tokenString;
        }
        #endregion "Private Methods"
    }
}
