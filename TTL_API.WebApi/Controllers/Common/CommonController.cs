﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.StoredProcedure;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Helpers;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.SystemAdmin
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class CommonController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly IDistrictService _districtService;
        private readonly IProvinceService _provinceService;
        private readonly ICommonService _commonService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public CommonController(
              ILoggerService logger,
              IConfiguration config,
              IDistrictService districtService,
              IProvinceService provinceService,
              ICommonService commonService,
              ISystmeAccountService systmeAccountService, IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _districtService = districtService;
            _provinceService = provinceService;
            _commonService = commonService;
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        [Route("GetAllProvince")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllProvince()
        {
            try
            {
                var a = await _provinceService.GetAllAsync();
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllComboboxProvince")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllComboboxProvince()
        {
            try
            {
                var a = await _provinceService.GetAllComboboxAsync();
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetProvinceById")]
        [AllowAnonymous]
        public async Task<ResponseResult<Province>> GetProvinceById(int id)
        {
            try
            {
                var a = await _provinceService.GetByIdAsync(id);
                var result = new ResponseResult<Province>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Province>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllDistrictByProvinceId")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllDistrictByProvinceId(int provinceId)
        {
            try
            {
                var a = await _districtService.GetAllByProvinceIdAsync(provinceId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllComboboxDistrict")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllComboboxDistrict(int provinceId)
        {
            try
            {
                var a = await _districtService.GetAllComboboxByProvinceIdAsync(provinceId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetDistrictById")]
        [AllowAnonymous]
        public async Task<ResponseResult<District>> GetDistrictById(int id)
        {
            try
            {
                var a = await _districtService.GetByIdAsync(id);
                var result = new ResponseResult<District>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<District>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("ComboboxGender")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> ComboboxGender()
        {
            try
            {
                var a = await _commonService.GetGenderAsync();
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllComBoxOrganizationByUserName")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllComBoxOrganizationByUserName(string userName)
        {
            try
            {
                var a = await _commonService.GetOrganizationByUserName(userName);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllOrganizationByUserName")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllOrganizationByUserName(string userName, int limit, int page)
        {
            try
            {
                var a = await _commonService.GetAllOrganizationByUserName(userName, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllPalletByStoreId")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllPalletByStoreId(int storeId)
        {
            try
            {
                var a = await _commonService.GetAllPalletByStoreId(storeId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetAllComboboxBillStatus")]
        public async Task<ResponseResult<ResponseList>> GetAllComboboxBillStatus(int organizationId)
        {
            try
            {
                var a = await _commonService.GetAllBillStatus(organizationId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllComboboxOrderStatus")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllComboboxOrderStatus(int organizationId)
        {
            try
            {
                var a = await _commonService.GetAllOrderStatus(organizationId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetAllComboboxTransport")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllComboboxTransport(int organizationId)
        {
            try
            {
                var a = await _commonService.GetAllTransport(organizationId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }


        [HttpGet]
        [Route("GetAllComboboxPayType")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllComboboxPayType(int organizationId)
        {
            try
            {
                var a = await _commonService.GetAllPayType(organizationId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetAllComboboxProductNumber")]
        public async Task<ResponseResult<ResponseList>> GetAllComboboxProductNumber(int organizationId)
        {
            try
            {
                var a = await _commonService.GetAllProductNumber(organizationId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetBillStatusByCode")]
        public async Task<ResponseResult<BillStatus>> GetBillStatusByCode(string code)
        {
            try
            {
                var a = await _commonService.GetAllBillStatusByCode(code);
                var result = new ResponseResult<BillStatus>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<BillStatus>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetOrderStatusByCode")]
        [AllowAnonymous]
        public async Task<ResponseResult<OrderStatus>> GetOrderStatusByCode(string code)
        {
            try
            {
                var a = await _commonService.GetAllOrderStatusByCode(code);
                var result = new ResponseResult<OrderStatus>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<OrderStatus>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetPayTypeByCode")]
        [AllowAnonymous]
        public async Task<ResponseResult<PayType>> GetPayTypeByCode(string code)
        {
            try
            {
                var a = await _commonService.GetAllPayTypeByCode(code);
                var result = new ResponseResult<PayType>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<PayType>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpPost]
        [Route("GetAllProductQuantityEnd")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllProductQuantityEnd(Product_StoreProcedureFitter fitter, int limit = 25, int page = 1)
        {
            try
            {
                var a = await _commonService.GetAllProductQuantityEnd(fitter, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetInfoCard")]
        [AllowAnonymous]
        public async Task<IActionResult> GetInfoCard(string cardNo)
        {
            try
            {
                var _connectionString = _config["MainConnectionString"];
                using (var conn = new SqlConnection(_connectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        await conn.OpenAsync();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@Action", "GET.INFOCARD");
                    paramaters.Add("@CardNo", CryptorEngine.Encrypt(cardNo, true, "TTLSTNHD@CARD"));
                    paramaters.Add("@Serial", cardNo);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    var result = await conn.QueryAsync<InfoCard>("SP_COMMON", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    var _apiResult = new ApiResult<InfoCard>();
                    _apiResult.RetCode = paramaters.Get<Int32>("@RetCode");
                    _apiResult.RetText = paramaters.Get<string>("@RetText");
                    _apiResult.Data = result.FirstOrDefault();
                    return Ok(_apiResult);
                }
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                var _apiResult = new ApiResult<InfoCard>();
                _apiResult.RetCode = 1; 
                _apiResult.RetText = ex.Message.ToString();
                _apiResult.Data = null;
                return Ok(_apiResult);
            }
        }
        [HttpGet]
        [Route("GetAllConfigPayment")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllConfigPayment()
        {
            try
            {
                var a = await _commonService.GetAllConfigPayment();
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllComboboxDiscountKind")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllComboboxDiscountKind()
        {
            try
            {
                var a = await _commonService.GetAllDiscountKind();
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
    }

    public class InfoCard
    {
        public string SerialNumber { get; set; }
        public string CardNo { get; set; }
        public string LevelCode { get; set; }
        public string LevelName { get; set; }
    }
}
