﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;
namespace TTL_API.WebApi.Controllers.HRM
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class StaffController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly IStaffService _staffService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public StaffController(
              ILoggerService logger,
              IConfiguration config,
              IStaffService staffService,
              ISystmeAccountService systmeAccountService, IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _staffService = staffService;
            _unitOfWork = unitOfWork;
        }
        [HttpPost]
        [Route("GetAll")]
        public async Task<ResponseResult<ResponseList>> GetAll(StaffFitter fitter, int limit, int page)
        {
            try
            {
                var a = await _staffService.GetAllAsync(fitter, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllStaff")]
        public async Task<ResponseResult<ResponseList>> GetAllStaffCombox(int organizationId)
        {
            try
            {
                var a = await _staffService.GetAllStaffCombox(organizationId);

                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetByCode")]
        public async Task<ResponseResult<Staff>> GetByCodeAsync(string code)
        {
            try
            {
                var a = await _staffService.GetByCodeAsync(code);
                var result = new ResponseResult<Staff>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Staff>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetById")]
        public async Task<ResponseResult<StaffModel>> GetByIdAsync(int id)
        {
            try
            {
                var a = await _staffService.GetByIdAsync(id);
                var result = new ResponseResult<StaffModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<StaffModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("CheckUserName")]
        public async Task<ResponseResult<Account>> CheckUserName(string userName)
        {
            try
            {
                var a = await _staffService.CheckUserName(userName);
                var result = new ResponseResult<Account>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Account>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }  
        [HttpGet]
        [Route("GetStafByAccountId")]
        public async Task<ResponseResult<Staff>> GetStafByAccountId(int id)
        {
            try
            {
                var a = await _staffService.GetStafByAccountId(id);
                var result = new ResponseResult<Staff>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Staff>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpPost]
        [Route("Add")]
        public async Task<ResponseResult<StaffModel>> Add(StaffModel staffModel)
        {
            try
            {
                var a = await _staffService.AddAsync(staffModel);
                var result = new ResponseResult<StaffModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<StaffModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Update")]
        public async Task<ResponseResult<string>> Update(StaffModel staffModel)
        {
            try
            {
                var a = await _staffService.UpdateAsync(staffModel);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<ResponseResult<string>> Delete(int id)
        {
            try
            {
                var a = await _staffService.DeleteAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
    }
}
