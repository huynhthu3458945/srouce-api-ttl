﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Extensions;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Extensions.NganLuong;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.WM
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class OrderController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IMapper _mapper;
        private readonly IConfiguration _config;
        private readonly IOrderService _OrderService;
        private readonly INganLuongService _nganLuongService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public OrderController(
              ILoggerService logger,
              IMapper mapper,
              IConfiguration config,
              IOrderService OrderService,
              INganLuongService nganLuongService,
              ISystmeAccountService systmeAccountService, IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _mapper = mapper;
            _config = config;
            _OrderService = OrderService;
            _unitOfWork = unitOfWork;
            _nganLuongService = nganLuongService;
        }
        [HttpPost]
        [Route("GetListOrderByPhoneParent")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetListOrderByPhoneParent(OrderFitter fitter, int limit, int page)
        {
            try
            {
                var a = await _OrderService.GetListOrderByPhoneParent(fitter, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("GetAll")]
        public async Task<ResponseResult<ResponseList>> GetAll(OrderFitter fitter, int limit, int page)
        {
            try
            {
                var a = await _OrderService.GetAllAsync(fitter, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetByCode")]
        [AllowAnonymous]
        public async Task<ResponseResult<OrderModel>> GetByCodeAsync(string code)
        {
            try
            {
                var a = await _OrderService.GetByCodeAsync(code);
                var result = new ResponseResult<OrderModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<OrderModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetById")]
        [AllowAnonymous]
        public async Task<ResponseResult<OrderModel>> GetByIdAsync(int id)
        {
            try
            {
                var a = await _OrderService.GetByIdAsync(id);
                var result = new ResponseResult<OrderModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<OrderModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetViewById")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetViewByIdAsync(int id)
        {
            try
            {
                var a = await _OrderService.GetViewByIdAsync(id);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GenerateCode")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> GenerateCode(string type)
        {
            try
            {
                //var organizationId = GetOrganizationId();
                var a = await _OrderService.GenerateCode(type);
                var result = new ResponseResult<string>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpPost]
        [Route("Add")]
        [AllowAnonymous]
        public async Task<ResponseResult<OrderModel>> Add(OrderModel Order)
        {
            try
            {   // check không được trống
                if (string.IsNullOrEmpty(Order.FullName))
                    return new ResponseResult<OrderModel>(RetCodeEnum.ApiError, "Họ tên không được bỏ trống", null);
                if (string.IsNullOrEmpty(Order.Phone))
                    return new ResponseResult<OrderModel>(RetCodeEnum.ApiError, "Số điện thoại không được bỏ trống", null);
                if (string.IsNullOrEmpty(Order.Email))
                    return new ResponseResult<OrderModel>(RetCodeEnum.ApiError, "Email không được bỏ trống", null);
                if (Order.OrderStatusId == 0)
                    Order.OrderStatusId = 1;
                if (Order.OrderType == 0)
                    Order.OrderType = 1;
                if (Order.OrganizationId == 0)
                    Order.OrganizationId = 68;
                var a = await _OrderService.AddAsync(Order);
                var result = new ResponseResult<OrderModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<OrderModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpPost]
        [Route("AddShare")]
        public async Task<ResponseResult<OrderModel>> AddShare(OrderReq Order)
        {
            try
            {
                var a = await _OrderService.AddByShareAsync(Order, GetCurrentUserId());
                var result = new ResponseResult<OrderModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<OrderModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Update")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> Update(OrderModel Order)
        {
            try
            {
                var a = await _OrderService.UpdateAsync(Order);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("UpdateStatus")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> UpdateStatus(int id, string OrderStatusCode)
        {
            try
            {
                var a = await _OrderService.UpdateStatusAsync(id, OrderStatusCode);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpPost]
        [Route("CreatePayMent")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> CreatePayMent(int id, string cardList)
        {
            try
            {
                string res = string.Empty;
                var Order = _mapper.Map<OrderModel>(_unitOfWork.Repository<Order>().Where(x => x.Id == id).AsNoTracking().FirstOrDefault());
                var orderList = _unitOfWork.Repository<OrderDetail>().Where(x => x.OrderId == Order.Id).AsNoTracking().ToList();
                var functionPaymant = new FunctionsPayment(_logger, _config);
                switch (cardList)
                {
                    case "OnPay":
                        Order = await functionPaymant.InvoiceByOnePay(Order);
                        break;
                    case "NganLuong":
                        string cityName = "Ho Chi Minh";
                        Order = await functionPaymant.InvoiceByNganLuongPayAsync(_nganLuongService, Order, orderList.Count, cityName);
                        break;
                }
                // Cập nhật link thanh toán và req Onpay
                _logger.WriteInfoLog($"Update--URLRequestOnePay -- {cardList} --> URLRequestOnePay: {Order.URLRequestOnePay}");
                _unitOfWork.Repository<Order>().Update(Order);
                await _unitOfWork.SaveChangesAsync();
                var result = CreateResponseResult<string>(Order.URLRequestOnePay);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpPost]
        [Route("CheckEndQuantityProductNumber")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> CheckEndQuantityProductNumber(int id, string OrderStatusCode)
        {
            try
            {
                var a = await _OrderService.CheckEndQuantityProductNumber(id, OrderStatusCode);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [Route("ResponeOnepay")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ResponeOnepay(string vpc_Command, string vpc_Locale, string vpc_CurrencyCode, string vpc_MerchTxnRef,
           string vpc_Merchant, string vpc_OrderInfo, string vpc_Amount, string vpc_TxnResponseCode, string vpc_TransactionNo,
           string vpc_Message, string vpc_Card, string vpc_CardNum, string vpc_PayChannel, string vpc_CardUid, string vpc_CardHolderName, string vpc_ItaBank,
           string vpc_ItaFeeAmount, string vpc_ItaTime, string vpc_OrderAmount, string vpc_ItaMobile, string vpc_ItaEmail, string vpc_SecureHash, string vpc_Version)
        {
            var result = "vpc_Command=" + vpc_Command + "; vpc_Locale=" + vpc_Locale + "; vpc_CurrencyCode=" + vpc_CurrencyCode
                + "; vpc_MerchTxnRef=" + vpc_MerchTxnRef + "; vpc_Merchant=" + vpc_Merchant
                + "; vpc_OrderInfo=" + vpc_OrderInfo + "; vpc_Amount=" + vpc_Amount
                + "; vpc_TxnResponseCode=" + vpc_TxnResponseCode + "; vpc_TransactionNo=" + vpc_TransactionNo
                + "; vpc_Message=" + vpc_Message + "; vpc_Card=" + vpc_Card + "; vpc_CardNum=" + vpc_CardNum
                + "; vpc_PayChannel=" + vpc_PayChannel + "; vpc_CardUid=" + vpc_CardUid
                + "; vpc_CardHolderName=" + vpc_CardHolderName + "; vpc_ItaBank=" + vpc_ItaBank
                + "; vpc_ItaFeeAmount=" + vpc_ItaFeeAmount + "; vpc_ItaTime=" + vpc_ItaTime
                + "; vpc_OrderAmount=" + vpc_OrderAmount + "; vpc_ItaMobile=" + vpc_ItaMobile
                + "; vpc_ItaEmail=" + vpc_ItaEmail + "; vpc_SecureHash=" + vpc_SecureHash + "; vpc_Version=" + vpc_Version;
            _logger.WriteInfoLog($"ResponeOnepay->{result}");
            //// Khoi tao lop thu vien
            VPCRequest onePay = new VPCRequest("http://onepay.vn");
            onePay.SetSecureSecret(_config["OnePay:SecureSecret"]);
            // Xu ly tham so tra ve va kiem tra chuoi du lieu ma hoa
            string hashvalidateResult = onePay.Process3PartyResponse(Request.Query);
            string sMessage = "";
            var orderStatusCode = string.Empty;
            if (hashvalidateResult == "CORRECTED" && vpc_TxnResponseCode == "0")
            {
                sMessage = "SUCCESS";
                orderStatusCode = "600";

            }
            else if (hashvalidateResult == "INVALIDATED" && vpc_TxnResponseCode == "0")
            {
                sMessage = "PENDING";
                orderStatusCode = "800";
            }
            else
            {
                sMessage = "UNSUCCESSFUL";
                orderStatusCode = "900";
            }
            var _apiResult = new ApiResult<string>();
            var order = await _OrderService.GetByCodeAsync(vpc_OrderInfo);
            await _OrderService.UpdateStatusAsync(order.Id, orderStatusCode);
            _apiResult.RetCode = 0;
            _apiResult.RetText = "Oke";
            _apiResult.Data = sMessage;
            return Ok(_apiResult);

        }

        //[HttpGet]
        //[Route("ResponeNganLuong")]
        //[AllowAnonymous]
        //public async Task<IActionResult> ResponeNganLuong(string orderId, string transactionCode, string errorCode, bool cancel)
        //{
        //    _logger.WriteInfoLog($"NganLuong Order Id->{orderId}");
        //    _logger.WriteInfoLog($"NganLuong transactionCode20022023->{transactionCode}");
        //    string orderStatusCode = "900";
        //    var _apiResult = new ApiResult<string>();
        //    if (string.IsNullOrEmpty(transactionCode) && cancel == true && string.IsNullOrEmpty(errorCode))
        //    {
        //        _apiResult.RetCode = 0;
        //        _apiResult.RetText = "Thanh toán thất bại.";
        //        _apiResult.Data = "UNSUCCESSFUL";
        //        return Ok(_apiResult);
        //    }
        //    var checkpayment = new CheckPaymentRequest();
        //    checkpayment.transactionCode = transactionCode;
        //    var result = await _nganLuongService.CheckPaymentNganLuong(checkpayment, "vi-vn");
        //    //ar _apiResult = new ApiResult<string>();
        //    var jsonData = JsonConvert.SerializeObject(result);
        //    _logger.WriteInfoLog($"NganLuong checkPaymentNganLuongData->{jsonData}");
        //    if (result != null)
        //    {
        //        var sMessage = "UNSUCCESSFUL";
        //        orderStatusCode = "900";
        //        switch (result.RetCode)
        //        {
        //            case 0:
        //                sMessage = "SUCCESS";
        //                orderStatusCode = "600";
        //                if (string.IsNullOrEmpty(orderId))
        //                {
        //                    orderId = result.DataCheckPaymentResponse.orderCode;
        //                }
        //                break;
        //            case 107:
        //                sMessage = "PENDING";
        //                orderStatusCode = "800";
        //                if (string.IsNullOrEmpty(orderId))
        //                {
        //                    orderId = result.DataCheckPaymentResponse.orderCode;
        //                }
        //                break;
        //            default:
        //                sMessage = "FAILED";
        //                orderStatusCode = "900";
        //                break;
        //        }
        //        _apiResult.RetCode = result.RetCode == 0 ? result.RetCode : 2;
        //        _apiResult.RetText = result.RetText;
        //        _apiResult.Data = sMessage;
        //        var order = await _OrderService.GetByCodeAsync(orderId);
        //        await _OrderService.UpdateStatusAsync(order.Id, orderStatusCode);
        //    }

        //    return Ok(_apiResult);
        //}

        [Route("ResponeNganLuong")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ResponeNganLuongV2(string order_code, string transaction_info, string receiver, string price,
           string payment_id, string payment_type, string error_text, string secure_code)
        {
            string orderStatusCode = string.Empty;
            _logger.WriteInfoLog($"Phase1");
            _logger.WriteInfoLog($"NganLuong Order Id->{order_code}");
            _logger.WriteInfoLog($"NganLuong transactionCode20022023->{transaction_info}");
            var _apiResult = new ApiResult<string>();
            if (!string.IsNullOrEmpty(transaction_info) && !string.IsNullOrEmpty(error_text))
            {
                _logger.WriteInfoLog($"error_text: {error_text}");

                _logger.WriteInfoLog($"Ngan luong Response 20022023 update unsuccessful ->{order_code}");
                _apiResult.RetCode = 0;
                _apiResult.RetText = "Thanh toán thất bại.";
                _apiResult.Data = "UNSUCCESSFUL";
                return Ok(_apiResult);
            }
            // Call update
            var api = new APINganLuong();
            var merchant = _config["NganLuong:MerchantId"];
            var sercurePass = _config["NganLuong:MerchantPassword"];
            bool rs = api.verifyPaymentUrl(merchant, sercurePass, transaction_info, order_code, price, payment_id, payment_type, error_text, secure_code);
            var sMessage = "UNSUCCESSFUL";
            if (!string.IsNullOrEmpty(transaction_info))
            {
                orderStatusCode = "900";
                var retCode = 0;
                if (rs == true)
                {
                    sMessage = "SUCCESS";
                    orderStatusCode = "600";
                }
                else
                {
                    retCode = 2;
                    sMessage = "PENDING";
                    orderStatusCode = "800";
                }
                _apiResult.RetCode = retCode;
                _apiResult.RetText = payment_type == "1" ? "Thanh toán ngay (tiền đã chuyển vào tài khoản NgânLượng.vn của người bán)" :
                    "Thanh toán Tạm giữ (tiền người mua đã thanh toán nhưng NgânLượng.vn đang giữ hộ)";
                _apiResult.Data = sMessage;

            }
            else
            {
                sMessage = "UNSUCCESSFUL";
                orderStatusCode = "900";
            }
            var order = await _OrderService.GetByCodeAsync(order_code);
            await _OrderService.UpdateStatusAsync(order.Id, orderStatusCode);

            _apiResult.RetCode = 0;
            _apiResult.RetText = "Oke";
            _apiResult.Data = sMessage;

            return Ok(_apiResult);
        }

        [HttpPost]
        [Route("Delete")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> Delete(int id)
        {
            try
            {
                var a = await _OrderService.DeleteAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
    }
}
