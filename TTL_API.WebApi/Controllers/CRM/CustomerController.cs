﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.CRM
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class CustomerController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly ICustomerService _customerService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public CustomerController(
              ILoggerService logger,
              IConfiguration config,
              ICustomerService CustomerService,
              ISystmeAccountService systmeAccountService, IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _customerService = CustomerService;
            _unitOfWork = unitOfWork;
        }
        [HttpPost]
        [Route("GetAll")]
        public async Task<ResponseResult<ResponseList>> GetAll(CustomerFitter fitter, int limit, int page)
        {
            try
            {
                //var organizationId = GetOrganizationId();
                var a = await _customerService.GetAll(fitter, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllCustomer")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllCustomerCombox(int organizationId, int storeId)
        {
            try
            {
                var a = await _customerService.GetAllCustomerCombox(organizationId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllByPhone")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllByPhone(int organizationId, string phone, string code)
        {
            try
            {
                if(string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(code))
                {
                    return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, "Phải nhập 1 trong 2 điều kiện", null);
                }
                //var organizationId = GetOrganizationId();
                var a = await _customerService.GetAllByPhoneAsync(organizationId, phone, code);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllByPhoneStore")]
        [AllowAnonymous]
        public async Task<ResponseResult<ResponseList>> GetAllByPhoneStore(int organizationId, string phone, string code)
        {
            try
            {
                if(string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(code))
                {
                    return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, "Phải nhập 1 trong 2 điều kiện", null);
                }
                //var organizationId = GetOrganizationId();
                var a = await _customerService.GetAllByPhoneStoreAsync(organizationId, phone, code);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GetById")]
        [AllowAnonymous]
        public async Task<ResponseResult<CustomerModel>> GetById(int id)
        {
            try
            {
                //var organizationId = GetOrganizationId();
                var a = await _customerService.GetByIdAsync(id);
                var result = new ResponseResult<CustomerModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<CustomerModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetByCode")]
        [AllowAnonymous]
        public async Task<ResponseResult<Customer>> GetByCode(string code)
        {
            try
            {
                //var organizationId = GetOrganizationId();
                var a = await _customerService.GetByCodeAsync(code);
                var result = new ResponseResult<Customer>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Customer>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpGet]
        [Route("GenerateCode")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> GenerateCode()
        {
            try
            {
                //var organizationId = GetOrganizationId();
                var a = await _customerService.GenerateCode();
                var result = new ResponseResult<string>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpPost]
        [Route("Add")]
        public async Task<ResponseResult<CustomerModel>> Add(CustomerModel customerModel)
        {
            try
            {
                // check không được trống
                if(string.IsNullOrEmpty(customerModel.FullName))
                    return new ResponseResult<CustomerModel>(RetCodeEnum.ApiError, "Họ tên không được bỏ trống", null);
                if (string.IsNullOrEmpty(customerModel.Phone))
                    return new ResponseResult<CustomerModel>(RetCodeEnum.ApiError, "Số điện thoại không được bỏ trống", null);
                customerModel.CreateBy = customerModel.CreateBy != 0 ? GetCurrentUserId() : 81;

                if (customerModel.CustomerStatusId == 0)
                    customerModel.CustomerStatusId = 1;
                if (customerModel.CustomerTypeId == 0)
                    customerModel.CustomerTypeId = 1;
                if (customerModel.GenderId == 0)
                    customerModel.GenderId = 3;
                //customerModel.CreateOn = DateTime.Now.Date;
                //customerModel.OrganizationId = GetOrganizationId();
                var resCheck = new ResponseResult<string>();
                // check số điện thoại
                resCheck = await CheckExistAsync(customerModel.OrganizationId, customerModel.Phone);
                if (resCheck.RetCode != RetCodeEnum.Ok)
                {
                    return new ResponseResult<CustomerModel>(resCheck.RetCode, resCheck.RetText.ToString(), null);
                }
                // check số điện liên hệ
                if (customerModel.ContactList != null)
                {
                    foreach (var item in customerModel.ContactList)
                    {
                        resCheck = await CheckContactExistAsync(item.Phone, item.Email);
                        if (resCheck.RetCode != RetCodeEnum.Ok)
                        {
                            return new ResponseResult<CustomerModel>(resCheck.RetCode, resCheck.RetText.ToString(), null);
                        }
                    }
                }
                if (customerModel.OrganizationId == 0)
                    customerModel.OrganizationId = 68;
                var a = await _customerService.AddAsync(customerModel);
                var result = new ResponseResult<CustomerModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<CustomerModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpPost]
        [Route("Add-ss")]
        [AllowAnonymous]
        public async Task<IActionResult> AddSS(int userId)
        {
            try
            {
                var _connectionString = _config["5TPTBConnectionString"];
                using (var conn = new SqlConnection(_connectionString))
                {
                    if (conn.State == System.Data.ConnectionState.Closed)
                        await conn.OpenAsync();
                    var paramaters = new DynamicParameters();
                    paramaters.Add("@UserId", userId);
                    paramaters.Add("@RetCode", dbType: System.Data.DbType.Int32, direction: System.Data.ParameterDirection.Output);
                    paramaters.Add("@RetText", dbType: System.Data.DbType.String, direction: System.Data.ParameterDirection.Output, size: 250);
                    _logger.WriteInfoLog($"Call SP_CUSTOMERERP_SS  --> userId: {userId}");
                    await conn.QueryAsync<string>("SP_CUSTOMERERP_SS", paramaters, null, null, System.Data.CommandType.StoredProcedure);
                    var _apiResult = new ApiResult<string>();
                    _apiResult.RetCode = paramaters.Get<Int32>("@RetCode");
                    _apiResult.RetText = paramaters.Get<string>("@RetText");
                    _apiResult.Data = paramaters.Get<string>("@RetText");
                    return Ok(_apiResult);
                }
            }
            catch (Exception ex)
            {
                //await _logger.WriteErrorLogAsync(ex, Request);
                _logger.WriteInfoLog($"Call SP_CUSTOMERERP_SS  --> Error: {ex.ToString()}");
                var _apiResult = new ApiResult<string>();
                _apiResult.RetCode = 1;
                _apiResult.RetText = ex.Message.ToString();
                _apiResult.Data = null;
                return Ok(_apiResult);
            }
        }
        [HttpPost]
        [Route("Add-Agency")]
        [AllowAnonymous]
        public async Task<ResponseResult<CustomerModel>> AddAgency(CustomerModel customerModel)
        {
            try
            {
                // check không được trống
                if (string.IsNullOrEmpty(customerModel.FullName))
                    return new ResponseResult<CustomerModel>(RetCodeEnum.ApiError, "Họ tên không được bỏ trống", null);
                if (string.IsNullOrEmpty(customerModel.Phone))
                    return new ResponseResult<CustomerModel>(RetCodeEnum.ApiError, "Số điện thoại không được bỏ trống", null);
                customerModel.CreateBy = 81;
                customerModel.GenderId = 3;
                customerModel.CustomerStatusId = 1;
                customerModel.CustomerTypeId = 1;
                //customerModel.CreateOn = DateTime.Now.Date;
                //customerModel.OrganizationId = GetOrganizationId();
                var resCheck = new ResponseResult<string>();
                // check số điện thoại
                resCheck = await CheckExistAsync(customerModel.OrganizationId, customerModel.Phone);
                if (resCheck.RetCode != RetCodeEnum.Ok)
                {
                    return new ResponseResult<CustomerModel>(resCheck.RetCode, resCheck.RetText.ToString(), null);
                }
                // check số điện liên hệ
                if (customerModel.ContactList != null)
                {
                    foreach (var item in customerModel.ContactList)
                    {
                        resCheck = await CheckContactExistAsync(item.Phone, item.Email);
                        if (resCheck.RetCode != RetCodeEnum.Ok)
                        {
                            return new ResponseResult<CustomerModel>(resCheck.RetCode, resCheck.RetText.ToString(), null);
                        }
                    }
                }
                if (customerModel.OrganizationId == 0)
                    customerModel.OrganizationId = 68;
                var a = await _customerService.AddAsync(customerModel);
                var result = new ResponseResult<CustomerModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<CustomerModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Update")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> Update(CustomerModel customerModel)
        {
            try
            {
                customerModel.ModifiedBy = customerModel.CreateBy != 0 ? GetCurrentUserId() : 81;
                customerModel.ModifiedOn = DateTime.Now.Date;
                var a = await _customerService.UpdateAsync(customerModel);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Delete")]
        [AllowAnonymous]
        public async Task<ResponseResult<string>> Delete(int id)
        {
            try
            {
                var a = await _customerService.DeleteAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        private async Task<ResponseResult<string>> CheckExistAsync(int organizationId, string phone)
        {
            try
            {
                //var organizationId = GetOrganizationId();
                //var a = await _customerService.CheckPhoneExistAsync(organizationId, phone);
                //var result = new ResponseResult<string>();
                //if (a == null)
                //    result = new ResponseResult<string>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), null);
                //else
                //    result = new ResponseResult<string>(RetCodeEnum.ApiError, "Số điện thoại đã tồn tại", null);

                var result = new ResponseResult<string>();
                var viewCustomer = await _customerService.GetAllByPhoneStoreAsync(68, phone, string.Empty);
                var resViewList = ((IEnumerable<object>)viewCustomer.ListData).Cast<ViewCustomer>().OrderByDescending(z => z.Ref_Source).FirstOrDefault();
                if (resViewList == null)
                {
                    result = new ResponseResult<string>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), null);
                }
                else
                {
                    result = new ResponseResult<string>(RetCodeEnum.ApiError, "Số điện thoại đã tồn tại", null);
                }
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        private async Task<ResponseResult<string>> CheckContactExistAsync(string phone, string email)
        {
            try
            {
                var a = await _customerService.CheckContactExistAsync(phone, email);
                var result = new ResponseResult<string>();
                if (a != null && a.Id > 0)
                    if (a.Phone == phone)
                        result = new ResponseResult<string>(RetCodeEnum.ApiError, $"Số điện thoại: {phone} liên hệ đã tồn tại", null);
                    else if (a.Email == email)
                        result = new ResponseResult<string>(RetCodeEnum.ApiError, $"Email: {email} liên hệ đã tồn tại", null);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
    }
}
