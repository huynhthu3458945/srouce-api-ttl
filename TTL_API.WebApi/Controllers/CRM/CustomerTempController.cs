﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.CRM
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class CustomerTempController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly ICustomerTempService _customerTempService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public CustomerTempController(
              ILoggerService logger,
              IConfiguration config,
              ICustomerTempService customerTempService,
              ISystmeAccountService systmeAccountService, IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _customerTempService = customerTempService;
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        [Route("GetAllByPhone")]
        public async Task<ResponseResult<ResponseList>> GetAllByPhone(string phone)
        {
            try
            {
                var a = await _customerTempService.GetAllByPhoneAsync(phone);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
    }
}
