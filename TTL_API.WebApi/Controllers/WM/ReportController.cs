﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.StoredProcedure;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Models.Response;

namespace TTL_API.WebApi.Controllers.WM
{
    [Route("api/{culture}/WM/[controller]")]
    [ApiController]
    [Authorize]
    public class ReportController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly IStoreProcedureService _storeProcedureService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public ReportController(
              ILoggerService logger,
              IConfiguration config,
              IStoreProcedureService storeProcedureService,
              ISystmeAccountService systmeAccountService, IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _storeProcedureService = storeProcedureService;
            _unitOfWork = unitOfWork;
        }

        [HttpPost]
        [Route("GetImportExportList")]
        public async Task<ResponseResult<ResponseList>> GetImportExportList(ImportExportListFillter fitter, int limit, int page)
        {
            try
            {
                var a = await _storeProcedureService.GetImportExportListAsync(fitter, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpPost]
        [Route("GetSyntheticWarehouse")]
        public async Task<ResponseResult<ResponseList>> GetSyntheticWarehouse(SyntheticWarehouseFillter fitter, int limit, int page)
        {
            try
            {
                var a = await _storeProcedureService.GetSyntheticWarehouse(fitter, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
    }
}
