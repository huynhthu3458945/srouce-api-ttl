﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.WM
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class PriceListController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly IPriceListService _PriceListService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public PriceListController(
              ILoggerService logger,
              IConfiguration config,
              IPriceListService PriceListService,
              ISystmeAccountService systmeAccountService, IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _PriceListService = PriceListService;
            _unitOfWork = unitOfWork;
        }
        [HttpPost]
        [Route("GetAll")]
        public async Task<ResponseResult<ResponseList>> GetAll(PriceListFitter fitter, int limit, int page)
        {
            try
            {
                var a = await _PriceListService.GetAllAsync(fitter, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("GetAllPriceList")]
        public async Task<ResponseResult<ResponseList>> GetAllPriceListCombox(int organizationId)
        {
            try
            {
                var a = await _PriceListService.GetAllPriceListCombox(organizationId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("GetAllPriceAgencyList")]
        public async Task<ResponseResult<ResponseList>> GetAllPriceAgencyListCombox(int organizationId)
        {
            try
            {
                var a = await _PriceListService.GetAllPriceAgencyListCombox(organizationId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetByCode")]
        public async Task<ResponseResult<PriceList>> GetByCodeAsync(string code)
        {
            try
            {
                var a = await _PriceListService.GetByCodeAsync(code);
                var result = new ResponseResult<PriceList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<PriceList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetById")]
        [AllowAnonymous]
        public async Task<ResponseResult<PriceListModel>> GetByIdAsync(int id)
        {
            try
            {
                var a = await _PriceListService.GetByIdAsync(id);
                var result = new ResponseResult<PriceListModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<PriceListModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ResponseResult<PriceListModel>> Add(PriceListModel PriceList)
        {
            try
            {
                var a = await _PriceListService.AddAsync(PriceList);
                var result = new ResponseResult<PriceListModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<PriceListModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Update")]
        public async Task<ResponseResult<string>> Update(PriceListModel PriceList)
        {
            try
            {
                var a = await _PriceListService.UpdateAsync(PriceList);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<ResponseResult<string>> Delete(int id)
        {
            try
            {
                var a = await _PriceListService.DeleteAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

    }
}
