﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.WM
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class StoreTypeController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly IStoreTypeService _StoreTypeService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public StoreTypeController(
              ILoggerService logger,
              IConfiguration config,
              IStoreTypeService StoreTypeService,
              ISystmeAccountService systmeAccountService, IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _StoreTypeService = StoreTypeService;
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<ResponseResult<ResponseList>> GetAll(int organizationId, int limit, int page)
        {
            try
            {
                var a = await _StoreTypeService.GetAllAsync(organizationId, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllStoreType")]
        public async Task<ResponseResult<ResponseList>> GetAllStoreTypeCombox(int organizationId)
        {
            try
            {
                var a = await _StoreTypeService.GetAllStoreTypeCombox(organizationId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetByCode")]
        public async Task<ResponseResult<StoreType>> GetByCodeAsync(string code)
        {
            try
            {
                var a = await _StoreTypeService.GetByCodeAsync(code);
                var result = new ResponseResult<StoreType>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<StoreType>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetById")]
        public async Task<ResponseResult<StoreType>> GetByIdAsync(int id)
        {
            try
            {
                var a = await _StoreTypeService.GetByIdAsync(id);
                var result = new ResponseResult<StoreType>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<StoreType>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ResponseResult<StoreType>> Add(StoreType StoreType)
        {
            try
            {
                var a = await _StoreTypeService.AddAsync(StoreType);
                var result = new ResponseResult<StoreType>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<StoreType>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Update")]
        public async Task<ResponseResult<string>> Update(StoreType StoreType)
        {
            try
            {
                var a = await _StoreTypeService.UpdateAsync(StoreType);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<ResponseResult<string>> Delete(int id)
        {
            try
            {
                var a = await _StoreTypeService.DeleteAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

    }
}
