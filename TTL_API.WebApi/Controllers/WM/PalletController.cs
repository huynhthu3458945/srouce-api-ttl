﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TTL_API.DbContext.Common;
using TTL_API.DbContext.Models;
using TTL_API.DbContext.Models.System;
using TTL_API.DbContext.Models.Utils;
using TTL_API.Services.Interfaces;
using TTL_API.Services.Logger;
using TTL_API.WebApi.Controllers.Base;
using TTL_API.WebApi.Extensions;
using TTL_API.WebApi.Models.Request;
using TTL_API.WebApi.Models.Response;


namespace TTL_API.WebApi.Controllers.WM
{
    [Route("api/{culture}/[controller]")]
    [ApiController]
    [Authorize]
    [MiddlewareFilter(typeof(LocalizationPipeline))]
    public class PalletController : BaseApiController
    {
        private readonly ILoggerService _logger;
        private readonly IConfiguration _config;
        private readonly IPalletService _PalletService;
        private readonly IGenericDbContext<TTL_APIContext> _unitOfWork;
        public PalletController(
              ILoggerService logger,
              IConfiguration config,
              IPalletService PalletService,
              ISystmeAccountService systmeAccountService, IGenericDbContext<TTL_APIContext> unitOfWork

              )
        {
            _logger = logger;
            _config = config;
            _PalletService = PalletService;
            _unitOfWork = unitOfWork;
        }
        [HttpGet]
        [Route("GetAll")]
        public async Task<ResponseResult<ResponseList>> GetAll(int organizationId, int storeId, string search, int limit, int page)
        {
            try
            {
                var a = await _PalletService.GetAllAsync(organizationId, storeId, search, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpPost]
        [Route("GetAllPalletProduct")]
        public async Task<ResponseResult<ResponseList>> GetAllPalletProduct(PalletProductFitter fitter, int limit, int page)
        {
            try
            {
                var a = await _PalletService.GetAllPalletProduct(fitter, limit, page);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetAllPallet")]
        public async Task<ResponseResult<ResponseList>> GetAllPalletCombox(int organizationId, int storeId)
        {
            try
            {
                var a = await _PalletService.GetAllPalletCombox(organizationId, storeId);
                var result = new ResponseResult<ResponseList>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<ResponseList>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetByCode")]
        public async Task<ResponseResult<Pallet>> GetByCodeAsync(string code)
        {
            try
            {
                var a = await _PalletService.GetByCodeAsync(code);
                var result = new ResponseResult<Pallet>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Pallet>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("GetPalletProduct")]
        public async Task<ResponseResult<Store_Pallet_ProductModel>> GetPalletProduct(PalletProductFitter fitter)
        {
            try
            {
                var a = await _PalletService.GetPalletProductAsync(fitter);
                var result = new ResponseResult<Store_Pallet_ProductModel>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Store_Pallet_ProductModel>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }
        [HttpGet]
        [Route("GetById")]
        public async Task<ResponseResult<Pallet>> GetByIdAsync(int id)
        {
            try
            {
                var a = await _PalletService.GetByIdAsync(id);
                var result = new ResponseResult<Pallet>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Pallet>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Add")]
        public async Task<ResponseResult<Pallet>> Add(PalletModel Pallet)
        {
            try
            {
                var a = await _PalletService.AddAsync(Pallet);
                var result = new ResponseResult<Pallet>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Pallet>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("AddPalletProduct")]
        public async Task<ResponseResult<Store_Pallet_Product>> AddPalletProduct(Store_Pallet_Product store_Pallet_Product)
        {
            try
            {
                var a = await _PalletService.AddPalletProductAsync(store_Pallet_Product);
                var result = new ResponseResult<Store_Pallet_Product>(RetCodeEnum.Ok, RetCodeEnum.Ok.ToString(), a);
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<Store_Pallet_Product>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Update")]
        public async Task<ResponseResult<string>> Update(PalletModel Pallet)
        {
            try
            {
                var a = await _PalletService.UpdateAsync(Pallet);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

        [HttpPost]
        [Route("Delete")]
        public async Task<ResponseResult<string>> Delete(int id)
        {
            try
            {
                var a = await _PalletService.DeleteAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }   
        
        [HttpPost]
        [Route("DeletePalletProduct")]
        public async Task<ResponseResult<string>> DeletePalletProduct(int id)
        {
            try
            {
                var a = await _PalletService.DeletePalletProductAsync(id);
                var result = CreateResponseResult<string>(a.ToString());
                return result;
            }
            catch (Exception ex)
            {
                await _logger.WriteErrorLogAsync(ex, Request);
                return new ResponseResult<string>(RetCodeEnum.ApiError, ex.Message, null);
            }
        }

    }
}
